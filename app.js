require('dotenv').config();
const CONNECTION_URL = "mongodb://localhost:27017/tutorplus_db";
const mongoose = require('mongoose');
const mongoDB = process.env.MONGODB_URI || CONNECTION_URL;
mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify:false,useCreateIndex:true }).then((connect)=> {
  console.log(connect.models)
});
mongoose.Promise = global.Promise;
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

const express = require('express')
const app = express();

const morgan = require('morgan');
app.use(morgan('combined'));

const rateLimit = require("express-rate-limit");
const authLimiter = rateLimit({
  windowMs: 60 * 60 * 1000, // 1 hour window
  max: 100, // start blocking after 100 requests
  message:
    "Too many requests from this IP, please try again after an hour"
});

const PushNotificationFactory = require('./utils/pushNotificationFactory.utils');
const PushNotificationProvider = PushNotificationFactory.create(process.env.PUSHPROVIDER || 'Fcm');

// PushNotificationProvider.sendPush(["60a11a0c9b14933768f4dc4f"], ["fl_7IwfXAL2Vp_1swkx3v6:APA91bEmoVqLrnnaTT5QWe8vlgJ4vN-WAz0_PONZleejjQkUZ1_mbXEIOBP_uroB-2lcgYsWOOFCaQQyOOzl0BU7mDdxys0ZFYItpw_FyRBY2qaCQh8LuTmgIjMlKSfCe301CB4uL1OG"], "New Title", "Great new Message", {data: "Great", newData: "Great Data"}, (err, code) => {

// })

const bodyParser = require('body-parser');
app.use(express.json({limit: '1mb'}));
app.use(express.urlencoded({limit: '1mb', extended: true}));

const cors = require('cors');
const corsOpts = {
    origin:'*',  
    methods:'*',  
    allowedHeaders:'*',
    
  };
app.use(cors(corsOpts)); // include before other routes

const { verifyUser } = require('./utils/passport.utils');
const { verifyAdmin } = require('./utils/adminPassport.utils')

const otp = require('./routes/otp.route'); 
app.use('/otp', authLimiter, otp);

const auth = require('./routes/auth.route'); 
app.use('/auth', authLimiter, auth);

const tutor = require('./routes/tutor.route');
app.use('/profiles', verifyUser, tutor);

const organization = require('./routes/organization.route');
app.use('/org', verifyUser, organization);

const adminAuth = require('./routes/adminAuth.route'); 
app.use('/adminAuth', authLimiter, adminAuth);

const admin = require('./routes/admin.route');
app.use('/admin', verifyAdmin, admin);

const kyc = require('./routes/kycDetails.route');
app.use('/kyc', kyc);

const masterData = require('./routes/masterData.route');
app.use('/masterdata', verifyUser, masterData);

const batch = require('./routes/batch.route');
app.use('/batch', verifyUser, batch);

const batchOrg = require('./routes/batchOrg.route');
app.use('/org/batch', verifyUser, batchOrg);

const schedule = require('./routes/schedule.route');
app.use('/schedule', verifyUser, schedule);

const scheduleOrg = require('./routes/scheduleOrg.route');
app.use('/org/schedule', verifyUser, scheduleOrg);

const chapter = require('./routes/chapter.route');
app.use('/chapter', verifyUser, chapter);

const content = require('./routes/content.route');
app.use('/content', verifyUser, content);

const contentOrg = require('./routes/contentOrg.route');
app.use('/org/content', verifyUser, contentOrg);

const batchChapter = require('./routes/batchChapter.route');
app.use('/batchChapter', verifyUser, batchChapter);

const studentContent = require('./routes/studentContent.route');
app.use('/studentContent', verifyUser, studentContent);

const assessment = require('./routes/assessment.route');
app.use('/assessment', verifyUser, assessment);

const attemptAssessment = require('./routes/attemptassessment.route');
app.use('/attemptassessment', verifyUser, attemptAssessment);

const assessmentQuestion = require('./routes/assessmentquestion.route');
app.use('/assessmentquestion', verifyUser, assessmentQuestion);

const upload = require('./routes/upload.route');
app.use('/upload', verifyUser, upload);

const abilityStore = require('./routes/abilityStore.route');
app.use('/ability', verifyUser, abilityStore);

const bbbMeetings = require('./routes/bbbMeetings.route');
app.use('/bbb', verifyUser, bbbMeetings)

const bbbRecordings = require('./routes/bbbRecordings.route');
app.use('/bbb/recordings', verifyUser, bbbRecordings)

const bbbWebHooks = require('./routes/bbbWebHooks.route');
app.use('/bbbhooks', bbbWebHooks)

const bbbEvents = require('./routes/bbbEvents.route');
app.use('/bbbevents', bbbEvents)

const notifications = require('./routes/notifications.route');
app.use('/notifications', verifyUser, notifications)

const analytics = require('./routes/analytics.route');
app.use('/analytics', verifyUser, analytics)

const payments = require('./routes/payments.route');
app.use('/payments', verifyUser, payments)

const paymentWebHooks = require('./routes/paymentsWebhook.route.js')
app.use('/paymenthooks', paymentWebHooks)

const assignments = require('./routes/assignments.route');
app.use('/assignments', verifyUser, assignments);


// Cron Jobs
const { paymentCron_calculateStudentPaymentAcc, paymentCron_calculateStudentPayableCron, paymentCron_calculateRetainershipFeeCron, paymentCron_batchEndCheckerCron } = require('./utils/paymentCronJob.utils');
const { notificationCron } = require('./utils/notificationCronJob.utils');
paymentCron_calculateStudentPaymentAcc()
paymentCron_calculateStudentPayableCron()
paymentCron_calculateRetainershipFeeCron()
paymentCron_batchEndCheckerCron()
notificationCron()


const HOST = process.env.HOST || 'localhost';
const PORT = process.env.PORT || 8092;

// This code is used for service registry and discovery using Eureka Server
//const appName =  'BATCH-SCHEDULE-SERVICE';
//const eurekaHost = HOST;
//const eurekaPort = 8761;
//const {startEurekaClient} = require('./utils/eureka.utils')
//startEurekaClient(HOST, PORT, appName, eurekaHost, eurekaPort);

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);


