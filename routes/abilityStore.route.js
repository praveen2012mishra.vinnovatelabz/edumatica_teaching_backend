const express = require('express');
const router = express.Router();
const { validate } = require('../validators/validator.js')
const {abilityStore_createAbility,abilityStore_patchAbility,abilityStore_getAbilities} = require("../controllers/abilityStore.controller")
const {abilitystore_createability_ValidationRules,abilitystore_patchability_ValidationRules} = require('../validators/abilitystore.validator')
const {casl} = require('../utils/casl/casl.middleware')

router.post('/addability',casl,validate,abilitystore_createability_ValidationRules(), abilityStore_createAbility)

router.patch('/patchability',casl,validate,abilitystore_patchability_ValidationRules(),abilityStore_patchAbility)

router.get('/abilities',casl,validate,abilityStore_getAbilities)


module.exports = router