const express = require('express');
const router = express.Router();
const content_controller = require('../controllers/contentOrg.controller');
const { validate } = require('../validators/validator.js')
const {
    contentCreate_ValidationRules,
    contentDelete_ValidationRules,
    getSignedUrlForUpload_ValidationRules,
} = require('../validators/content.validator')
const {casl} = require('../utils/casl/casl.middleware')

router.post('/create',casl , contentCreate_ValidationRules(), validate,
 content_controller.content_create);
router.delete('/delete',casl , contentDelete_ValidationRules(), validate,
 content_controller.content_delete);
router.get('/getsignedurlforupload',casl , getSignedUrlForUpload_ValidationRules(), validate,
 content_controller.content_getSignedURLForUpload);

module.exports = router;