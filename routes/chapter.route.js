const express = require('express');
const router = express.Router();
const chapter_controller = require('../controllers/chapter.controller');
const { validate } = require('../validators/validator.js')
const {
    chapterCreate_ValidationRules,
    chapterDetails_ValidationRules, chapterGetChapters_ValidationRules,
    chapterUpdate_ValidationRules, chapterDelete_ValidationRules, addMasterChapters_ValidationRules,
} = require('../validators/chapter.validator')
const {casl} = require('../utils/casl/casl.middleware')


router.get('/chapters',casl , chapterGetChapters_ValidationRules(), validate, chapter_controller.chapter_chapters);
router.get('/detail',casl , chapterDetails_ValidationRules(), validate, chapter_controller.chapter_details);
router.post('/create',casl , chapterCreate_ValidationRules(), validate, chapter_controller.chapter_create);
router.post('/addmasterchapters', addMasterChapters_ValidationRules(), validate, chapter_controller.chapter_addMasterChapters);
router.put('/update',casl , chapterUpdate_ValidationRules(), validate, chapter_controller.chapter_update);
router.delete('/delete',casl , chapterDelete_ValidationRules(), validate, chapter_controller.chapter_delete);
module.exports = router;