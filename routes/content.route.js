const express = require('express');
const router = express.Router();
const content_controller = require('../controllers/content.controller');
const { validate } = require('../validators/validator.js')
const {
    contentCreate_ValidationRules,
    getSignedUrlForUpload_ValidationRules,
    getSignedUrlForDownload_ValidationRules,
    getStudentSignedUrlForDownload_ValidationRules,
    contentUpdate_ValidationRules,
    contentDetails_ValidationRules,
    contentGetContents_ValidationRules,
    contentDelete_ValidationRules
} = require('../validators/content.validator')
const {casl} = require('../utils/casl/casl.middleware')

router.get('/getsignedurlforupload',casl , getSignedUrlForUpload_ValidationRules(), validate,
 content_controller.content_getSignedURLForUpload);
router.get('/getsignedurlfordownload', getSignedUrlForDownload_ValidationRules(), validate,
 content_controller.content_getSignedURLForDownload);
router.get('/student/getsignedurlfordownload', getStudentSignedUrlForDownload_ValidationRules(), validate,
 content_controller.content_getStudentSignedURLForDownload);
router.get('/contents',casl , contentGetContents_ValidationRules(), validate,
 content_controller.content_contents);
router.get('/detail',casl , contentDetails_ValidationRules(), validate,
 content_controller.content_details);
router.post('/create',casl , contentCreate_ValidationRules(), validate,
 content_controller.content_create);
router.put('/update',casl , contentUpdate_ValidationRules(), validate,
 content_controller.content_update);
router.delete('/delete',casl , contentDelete_ValidationRules(), validate,
 content_controller.content_delete);

module.exports = router;