const express = require('express');
const router = express.Router();
const bbbRecordings_controller = require('../controllers/bbbRecordings.controller')
const {casl} = require('../utils/casl/casl.middleware');
const { validate } = require('../validators/validator');
const {
  bbbRecordingsGetRecordingInfoById_ValidationRules,
  bbbRecordingsDeleteRecordingById_ValidationRules,
  bbbRecordingsDeleteRecordingData_ValidationRules
} = require("../validators/bbbRecordings.validator");

router.post('/recordinginfo', bbbRecordingsGetRecordingInfoById_ValidationRules(), validate, bbbRecordings_controller.bbbRecordings_recordingInfoById)
router.post('/delete', bbbRecordingsDeleteRecordingById_ValidationRules(), validate, bbbRecordings_controller.bbbRecordings_deleteRecordingById)
router.get('/delete', bbbRecordingsDeleteRecordingData_ValidationRules(), validate, bbbRecordings_controller.bbbRecordings_DeleteRecordingData)

module.exports = router;