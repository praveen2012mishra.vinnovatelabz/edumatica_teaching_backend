const express = require('express');
const router = express.Router();
const assessmentquestion_controller = require('../controllers/assessmentquestion.controller');
const { validate } = require('../validators/validator.js')
const { getTopics_validationRules,getQuestions_validationRules } = require("../validators/assessmentquestion.validator")
const {casl} = require('../utils/casl/casl.middleware')



router.get('/getTopics' ,casl, getTopics_validationRules() ,validate, assessmentquestion_controller.topics_get)
router.post('/getQuestions',getQuestions_validationRules(), validate, assessmentquestion_controller.questions_get )


module.exports=router