const express = require('express');
const router = express.Router();
const { validate } = require('../validators/validator');
const {casl} = require('../utils/casl/casl.middleware');
const payments_controller = require('../controllers/payments.controller')
const {
    createCourseBundle_ValidationRules,
    fetchCourseBundle_ValidationRules,
    updateCourseBundle_ValidationRules,
    fetchCourseBundleById_ValidationRules,
    deleteCourseBundle_ValidationRules,
    fetchStudentAcc_ValidationRules,
    generatePayOrder_ValidationRules,
    generateOrgPayOrder_ValidationRules,
    fetchOrgAcc_ValidationRules,
    validateReferralCode_ValidationRules,
    validateOfflinePayment_ValidationRules,
    generateEdumacPackagepayOrder_ValidationRules,
    updatePayOrders_ValidationRules,
    paymentStatus_ValidationRules,
    fetchStatus_ValidationRules,
} = require("../validators/payments.validator");

router.get('/feestructure/:bundleId', fetchCourseBundleById_ValidationRules(), validate, payments_controller.payments_fetchCourseBundleById)
router.get('/feestructure', fetchCourseBundle_ValidationRules(), validate, payments_controller.payments_fetchCourseBundle)
router.post('/feestructure', createCourseBundle_ValidationRules(), validate, payments_controller.payments_createCourseBundle)
router.patch('/feestructure', updateCourseBundle_ValidationRules(), validate, payments_controller.payments_updateCourseBundle)
router.delete('/feestructure', deleteCourseBundle_ValidationRules(), validate, payments_controller.payments_deleteCourseBundle)
router.post('/studentpaymentaccount', fetchStudentAcc_ValidationRules(), validate, payments_controller.payments_fetchStudentAcc)
router.get('/orgpaymentaccount', fetchOrgAcc_ValidationRules(), validate, payments_controller.payments_fetchOrgAcc)
router.post('/payorder', generatePayOrder_ValidationRules(), validate, payments_controller.payments_generatePayOrder)
router.post('/orgpayorder', generateOrgPayOrder_ValidationRules(), validate, payments_controller.payments_generateOrgPayOrder)
router.post('/purchaseEdumacPackagepayOrder', generateEdumacPackagepayOrder_ValidationRules(), validate, payments_controller.payments_generateEdumacPackagepayOrder)
router.get('/validateReferral', validateReferralCode_ValidationRules(), validate, payments_controller.validateReferralCode)
router.get('/getMasterPackages', validate, payments_controller.getMasterPackages)
router.post('/validateOfflinePayment', validateOfflinePayment_ValidationRules(), validate, payments_controller.payments_validateOfflinePayment)
router.patch('/updatePayOrders', updatePayOrders_ValidationRules(), validate, payments_controller.payments_updatePayOrders)
router.get('/paymentStatus', paymentStatus_ValidationRules(), validate, payments_controller.payments_paymentStatus)
router.get('/getStatus', fetchStatus_ValidationRules(), validate, payments_controller.payments_checkStatus)

module.exports = router;