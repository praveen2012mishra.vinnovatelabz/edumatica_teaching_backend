const express = require('express');
const router = express.Router();
const admin_controller = require('../controllers/admin.controller');
const {permissionsHandler} = require('../controllers/admin.controller');
const { updateUserStatus_ValidationRules, verifyUser_ValidationRules, /*generateRegisterLink_ValidationRules, */ createAdmin_ValidationRules, editAdmin_ValidationRules, deleteAdmin_ValidationRules, createRole_ValidationRules, createPermissions_ValidationRules, editRole_ValidationRules, changePassword_ValidationRules, resetPassword_ValidationRules, sendNewPasswordMail_ValidaionRules, getUsers_ValidationRules, fetchKycStatus_ValidationRules, fetchKycData_ValidationRules } = require('../validators/admin.validator')
const {casl} = require('../utils/casl/casl.middleware')
const { validate } = require('../validators/validator');

// Common requests for all roles
// router.get('/getAdmin', casl,admin_controller.admin_details);
router.post('/changePassword', changePassword_ValidationRules(), validate, admin_controller.changePassword)

// Super Admin requests
// router.post('/generateRegisterLink', generateRegisterLink_ValidationRules(), validate, admin_controller.generateRegisterLink)
router.post('/resetPassword', permissionsHandler, resetPassword_ValidationRules(), validate, admin_controller.resetPassword)
router.post('/sendNewPasswordMail', permissionsHandler, sendNewPasswordMail_ValidaionRules(), validate, admin_controller.sendNewPasswordMail)

// CRUD admin
router.post('/createAdmin', permissionsHandler, createAdmin_ValidationRules(), validate, admin_controller.createAdmin)
router.get('/getAdmin', permissionsHandler, admin_controller.getAdmin)
router.post('/editAdmin', permissionsHandler, editAdmin_ValidationRules(), validate, admin_controller.editAdmin)
router.post('/deleteAdmin', permissionsHandler, deleteAdmin_ValidationRules(), validate, admin_controller.deleteAdmin)
router.get('/getAdmins', permissionsHandler, admin_controller.getAdmins)

// CRUD roles and permissions
router.post('/createPermissions', permissionsHandler, createPermissions_ValidationRules(), validate, admin_controller.createPermissions)
router.post('/createRole', permissionsHandler, createRole_ValidationRules(), validate, admin_controller.createRole)
router.post('/editRole', permissionsHandler, editRole_ValidationRules(), validate, admin_controller.editRole)
router.get('/getPermissions', permissionsHandler, admin_controller.getPermissions)
router.get('/getRoles', permissionsHandler, admin_controller.getRoles)

// Customer Management
router.get('/getUsers', permissionsHandler, getUsers_ValidationRules(), validate, admin_controller.getUsers)
router.post('/updateUserStatus', permissionsHandler, updateUserStatus_ValidationRules(), validate, admin_controller.updateUserStatus)
router.get('/fetchKycStatus', permissionsHandler, fetchKycStatus_ValidationRules(), validate, admin_controller.fetchKycStatus)
router.get('/fetchKycData', permissionsHandler, fetchKycData_ValidationRules(), validate, admin_controller.fetchKycData)

module.exports = router;