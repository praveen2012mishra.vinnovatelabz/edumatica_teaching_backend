const express = require('express');
const router = express.Router();
const bbbMeetings_controller = require('../controllers/bbbMeetings.controller');
const {casl} = require('../utils/casl/casl.middleware');
const { validate } = require('../validators/validator');
const {
  bbbMeetingsCreateMeeting_ValidationRules,
  bbbMeetingsJoinMeeting_ValidationRules,
  bbbMeetingsGetMeetingInfo_ValidationRules,
  bbbMeetingsIsMeetingRunning_ValidationRules,
  bbbMeetingsGetPostMeetingEventsInfo_ValidationRules,
  bbbMeetingsGetPostIndividualMeetingEventsInfo_ValidationRules,
  bbbMeetingsFeedback_ValidationRules
} = require("../validators/bbbMeetings.validator");

router.post('/create', bbbMeetingsCreateMeeting_ValidationRules(), validate, bbbMeetings_controller.bbbMeetings_createMeeting)
router.post('/join', bbbMeetingsJoinMeeting_ValidationRules(), validate, bbbMeetings_controller.bbbMeetings_joinMeeting)
router.post('/info', bbbMeetingsGetMeetingInfo_ValidationRules(), validate, bbbMeetings_controller.bbbMeetings_getMeetingInfo)
router.post('/ismeetingrunning', bbbMeetingsIsMeetingRunning_ValidationRules(), validate, bbbMeetings_controller.bbbMeetings_isMeetingRunning)
router.get('/getmeeting', bbbMeetingsGetPostMeetingEventsInfo_ValidationRules(), validate, bbbMeetings_controller.bbbMeetings_getPostMeetingEventsInfo)
router.get('/getindividualmeeting', bbbMeetingsGetPostIndividualMeetingEventsInfo_ValidationRules(), validate, bbbMeetings_controller.bbbMeetings_getPostIndividualMeetingEventsInfo)
router.post('/feedback', bbbMeetingsFeedback_ValidationRules(), validate, bbbMeetings_controller.bbbMeetings_feedback)

module.exports = router;