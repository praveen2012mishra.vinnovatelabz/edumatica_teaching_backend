const express = require('express');
const router = express.Router();
const otp_controller = require('../controllers/otp.controller');
const { generateOTP_ValidationRules, validateOTP_ValidationRules, validateForForgetPwd_ValidationRules } = require('../validators/otp.validator');
const { validate } = require('../validators/validator');

router.post('/generateOTP', generateOTP_ValidationRules(), validate, otp_controller.generateOTP);
router.post('/validateOTP', validateOTP_ValidationRules(), validate, otp_controller.validateOTP);
router.post('/validateOTP/password', validateForForgetPwd_ValidationRules(), validate, otp_controller.validateForForgetPwd);

module.exports = router;