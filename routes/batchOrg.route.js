const express = require('express');
const router = express.Router();
const batch_controller = require('../controllers/batchOrg.controller');
const { validate } = require('../validators/validator.js')
const { batchCreate_ValidationRules, batchUpdate_ValidationRules,
        batchDetails_ValidationRules,
        batchDelete_ValidationRules} = require('../validators/batch.validator')
const {casl} = require('../utils/casl/casl.middleware')

router.get('/batches',casl, batch_controller.batch_batches);
router.get('/bbborgbatches',batch_controller.batch_bbbbatches);
router.post('/create',casl, batchCreate_ValidationRules(), validate, batch_controller.batch_create);
router.get('/detail',casl, batchDetails_ValidationRules(), validate, batch_controller.batch_details);
router.put('/update',casl, batchUpdate_ValidationRules(), validate, batch_controller.batch_update);
router.delete('/delete',casl, batchDelete_ValidationRules(), validate, batch_controller.batch_delete);

module.exports = router;