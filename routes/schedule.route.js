const express = require('express');
const router = express.Router();
const schedule_controller = require('../controllers/schedule.controller');
const { validate } = require('../validators/validator.js')
const { scheduleCreate_ValidationRules, scheduleDetails_ValidationRules, getParentSchedules_ValidationRules,
        scheduleDelete_ValidationRules, scheduleGetTutorRoom_ValidationRules,
        scheduleGetStudentRoom_ValidationRules, scheduleUpdate_ValidationRules } 
            = require('../validators/schedule.validator')
const {casl} = require('../utils/casl/casl.middleware')

                       
router.get('/studentRoom',casl, scheduleGetStudentRoom_ValidationRules(), validate,
 schedule_controller.schedule_getStudentRoom);
router.get('/tutorRoom',casl, scheduleGetTutorRoom_ValidationRules(), validate,
 schedule_controller.schedule_getTutorRoom);
router.get('/schedules',casl, schedule_controller.schedule_schedules);
router.get('/studentSchedules',casl, schedule_controller.schedule_getStudentSchedules);
router.get('/studentAttendance',casl, schedule_controller.getStudentAttendance);
router.get('/parentSchedules',casl, getParentSchedules_ValidationRules(), schedule_controller.getParentSchedules);
router.get('/detail',casl, scheduleDetails_ValidationRules(), validate,
 schedule_controller.schedule_details);
router.post('/create',casl, scheduleCreate_ValidationRules(), validate,
 schedule_controller.schedule_create);
router.put('/update',casl, scheduleUpdate_ValidationRules(), validate,
 schedule_controller.schedule_update);
router.delete('/delete',casl, scheduleDelete_ValidationRules(), validate,
 schedule_controller.schedule_delete);
 
module.exports = router;