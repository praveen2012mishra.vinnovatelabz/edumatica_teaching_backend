const express = require('express');
const router = express.Router();
const assessment_controller = require('../controllers/assessment.controller');
const { validate } = require('../validators/validator.js')
const { 
    assessmentPatch_ValidationRules , assessmentDelete_ValidationRules, 
    assessmentsDelete_ValidationRules, assessmentDetails_ValidationRules,
    assessmentCreate_ValidationRules, assessmentCopy_ValidationRules,  
    assessmentAssign_ValidationRules, assessmentsDetails_ValidationRules,
    deleteAssessmentAssign_ValidationRules
} = require('../validators/assessment.validator')
const {casl} = require('../utils/casl/casl.middleware')


router.get('/assessments',casl, assessmentsDetails_ValidationRules() , validate,assessment_controller.assessment_getAssessments);

router.get('/assessment',casl, assessmentDetails_ValidationRules(), validate, assessment_controller.assessment_details);

router.patch('/assessment',casl,assessmentPatch_ValidationRules(),validate, assessment_controller.assessment_patchAssessment)

router.delete('/assessment',casl,assessmentDelete_ValidationRules(),validate,assessment_controller.assessment_deleteAssessment)
router.delete('/assessments',casl,assessmentsDelete_ValidationRules(),validate,assessment_controller.assessment_deleteAssessments)

router.post('/assessment', casl,assessmentCreate_ValidationRules(), validate,
 assessment_controller.assessment_createAssessment);

router.post('/assignassessment' ,casl, assessmentAssign_ValidationRules() ,validate ,assessment_controller.assessment_assignAssessment )
router.get('/assignassessment', assessment_controller.assessment_fetchAssignAssessment )
router.delete('/assignassessment', deleteAssessmentAssign_ValidationRules() ,validate, assessment_controller.assessment_deleteAssignAssessment )

router.post('/copyAssessment',casl, assessmentCopy_ValidationRules(),validate,assessment_controller.assessment_copyassessment)
module.exports = router;