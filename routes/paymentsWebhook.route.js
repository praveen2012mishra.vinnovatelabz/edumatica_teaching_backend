const express = require('express');
const router = express.Router();
const { validate } = require('../validators/validator');
const payments_controller = require('../controllers/payments.controller')

router.post('/payorder/authenticate', validate, payments_controller.payments_authenticatePaymentSuccess)

module.exports = router