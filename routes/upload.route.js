const express = require('express');
const router = express.Router();

const {uploadData,get_distinct_filenames_controller,delete_by_filename_controller}  = require("../controllers/upload.controller")
const { upload }  = require("../utils/multer.upload")
const {casl} = require('../utils/casl/casl.middleware')
const {delete_by_filename_ValidationRules} = require('../validators/upload.validator')


router.post('/uploadData',casl ,upload.array("files"),uploadData)

router.get('/filenames', get_distinct_filenames_controller)

router.delete('/deletebyfilename', delete_by_filename_ValidationRules(),delete_by_filename_controller)

module.exports = router