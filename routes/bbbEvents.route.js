const express = require('express');
const router = express.Router();
const bbbEvents_controller = require('../controllers/bbbEvents.controller')

router.post('/eventlistener', bbbEvents_controller.bbbEvents_hookeventlistener)
router.get('/eventlistener', bbbEvents_controller.bbbEvents_eventstreamer)

module.exports = router;