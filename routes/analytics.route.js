const express = require('express');
const router = express.Router();
const { validate } = require('../validators/validator');
const {casl} = require('../utils/casl/casl.middleware');
const analytics_controller = require('../controllers/analytics.controller')
const { analytics_studentAssessment, analytics_studentAssessmentGetAnswer } = require('../validators/analytics.validator')

router.get('/studentassessment', analytics_studentAssessment(), validate, analytics_controller.analytics_studentAssessment)
router.get('/studentassessmentanswers', analytics_studentAssessmentGetAnswer(), validate, analytics_controller.analytics_studentAssessmentGetAnswer)

module.exports = router;