const express = require('express');
const router = express.Router();
const tutor_controller = require('../controllers/tutor.controller');
const { tutorCreate_ValidationRules, addStudentsByTutor_ValidationRules,
    removeStudentsByTutor_ValidationRules, updateKYCDocsByTutor_ValidationRules,
    getUploadUrlForKYCDoc_ValidationRules, getDownloadUrlForKYCDoc_ValidationRules,
    updateStudent_ValidationRules, updateParent_ValidationRules,
    tutorCheckCourseCounte_ValidationRules, delUploadedKYCDoc_ValidationRules,
    changePassword_ValidationRules, uploadZip_ValidationRules, getParentForTutor_ValidationRules, generateVideoKycAwsLink_ValidationRules, updateVideoKycSuccessUuid_ValidationRules, getVideoKycDownloadLink_ValidationRules, fetchKycDetails_ValidationRules } = require('../validators/tutor.validator');
const {casl} = require('../utils/casl/casl.middleware')
const { validate } = require('../validators/validator');
const { upload } = require('../utils/multer.uploadzip');



router.put('/addTutor',casl, tutorCreate_ValidationRules(), validate, tutor_controller.tutor_create);
router.put('/onBoardTutor',casl, tutorCreate_ValidationRules(), validate, tutor_controller.tutor_onboard);
router.post('/checkcoursecount', tutorCheckCourseCounte_ValidationRules(), validate, tutor_controller.tutor_checkCourseCount);
router.get('/getTutor',casl, tutor_controller.tutor_details);
router.get('/getCoursesForTutor',casl, tutor_controller.tutor_courses);
router.post('/addStudentsByTutor',casl, addStudentsByTutor_ValidationRules(), validate,
 tutor_controller.tutor_addStudents);
router.delete('/removeStudentsForTutor',casl, removeStudentsByTutor_ValidationRules(), validate,
 tutor_controller.tutor_removeStudentsForTutor);
router.get('/getStudentsByTutor',casl, tutor_controller.tutor_getStudents);
router.get('/getStudentsByTutorid',casl, tutor_controller.tutor_getStudents_id);
router.get('/getStudentsByParent',casl, tutor_controller.tutor_getStudentsbyParent);
router.post('/updateKYCDocsByTutor',casl, updateKYCDocsByTutor_ValidationRules(), validate,
 tutor_controller.tutor_updateKYCDocs);
router.get('/getUploadUrlForKYCDoc', getUploadUrlForKYCDoc_ValidationRules(), validate,
 tutor_controller.tutor_getUploadUrlForKYCDoc);
router.delete('/uploadedKYCDoc', delUploadedKYCDoc_ValidationRules(), validate,
 tutor_controller.tutor_deleteUploadKYCDoc);
router.get('/getDownloadUrlForKYCDoc', getDownloadUrlForKYCDoc_ValidationRules(), validate,
 tutor_controller.tutor_getDownloadUrlForKYCDoc);
router.post('/uploadZip', upload.single('files'), tutor_controller.uploadZip);
router.get('/getStudent', tutor_controller.student_details);
router.put('/updateStudent', updateStudent_ValidationRules(), validate, tutor_controller.student_update);
router.put('/onboardStudent', updateStudent_ValidationRules(), validate, tutor_controller.student_onboard);
router.get('/getParent', tutor_controller.parent_details);
router.get('/getParentForTutor', getParentForTutor_ValidationRules(), tutor_controller.parent_details_for_tutor);
router.put('/updateParent', updateParent_ValidationRules(), validate, tutor_controller.parent_update);
router.put('/onboardParent', updateParent_ValidationRules(), validate, tutor_controller.parent_onboard);
router.post('/changePassword', changePassword_ValidationRules(), validate, tutor_controller.changePassword);
router.get('/videoKyc', validate, tutor_controller.getVideoKycToken);
router.post('/videoKyc', updateVideoKycSuccessUuid_ValidationRules(), validate, tutor_controller.updateVideoKycSuccessUuid);
router.post('/videoKycLink', generateVideoKycAwsLink_ValidationRules(), validate, tutor_controller.generateVideoKycAwsLink);
router.get('/videoKycLink', getVideoKycDownloadLink_ValidationRules(), validate, tutor_controller.getVideoKycDownloadLink);
router.get('/redoVideoKyc', validate, tutor_controller.redoVideoKyc);

router.get('/getProfilePercentage', tutor_controller.getProfilePercentage)

module.exports = router;