const express = require('express');
const router = express.Router();
const assignments_controller = require('../controllers/assignments.controller');
const { upload } = require('../utils/multer.uploadzip');
const { validate } = require('../validators/validator.js');
const { 
    createAssignment_ValidationRules, fetchStudentAssignment_ValidationRules,
    fetchAssignedAssignment_ValidationRules, getDownloadUrlForAssignmentDoc_ValidationRules,
    submitAssignmentDoc_ValidationRules, createAssignmentDraft_ValidationRules,
    getUploadUrlForAssignmentDoc_ValidationRules, evaluateAssignmentDoc_ValidationRules
} = require('../validators/assignments.validator');

router.post('/zipFilesAndUpload', upload.array('documents'), assignments_controller.zipFilesAndUpload)
router.post('/', createAssignment_ValidationRules(), validate, assignments_controller.createAssignment)
router.post('/draft', createAssignmentDraft_ValidationRules(), validate, assignments_controller.createAssignmentDraft)
router.get('/', assignments_controller.fetchAssignment)
router.get('/assigned', assignments_controller.fetchAssignedAssignment)
router.get('/student', fetchStudentAssignment_ValidationRules(), validate, assignments_controller.fetchStudentAssignment)
router.get('/uploadUrl', getUploadUrlForAssignmentDoc_ValidationRules(), validate, assignments_controller.getUploadUrlForAssignmentDoc);
router.get('/downloadUrl', getDownloadUrlForAssignmentDoc_ValidationRules(), validate, assignments_controller.getDownloadUrlForAssignmentDoc);
router.post('/submit', submitAssignmentDoc_ValidationRules(), validate, assignments_controller.submitAssignmentDoc);
router.post('/evaluate', evaluateAssignmentDoc_ValidationRules(), validate, assignments_controller.evaluateAssignmentDoc);
router.post('/topdf', upload.array('documents'), assignments_controller.topdf);

module.exports = router;
