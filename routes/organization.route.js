const express = require('express');
const router = express.Router();
const organization_controller = require('../controllers/organization.controller');
const { org_updateTutor } = require('../dbc/organization.dbc');
const { organizationCreate_ValidationRules, addTutorsForOrganization_ValidationRules, orgCheckCourseCounte_ValidationRules,
    removeTutors_ValidationRules, addStudents_ValidationRules, updateStudent_ValidationRules,
    removeStudents_ValidationRules, getStudentsForClass_ValidationRules, generateVideoKycAwsLink_ValidationRules, getVideoKycDownloadLink_ValidationRules, updateVideoKycSuccessUuid_ValidationRules, fetchKycDetails_ValidationRules } = require('../validators/organization.validator');
const { validate } = require('../validators/validator');
const {casl} = require('../utils/casl/casl.middleware')

router.get('/getOrganization', organization_controller.organization_details);
router.get('/getCourses', organization_controller.org_courses);
router.put('/addOrganization', organizationCreate_ValidationRules(), validate, organization_controller.organization_create);
router.put('/onboardOrganization', organizationCreate_ValidationRules(), validate, organization_controller.organization_onboard);
router.post('/checkcoursecount', orgCheckCourseCounte_ValidationRules(), validate, organization_controller.organization_checkCourseCount);
router.post('/addTutors', addTutorsForOrganization_ValidationRules(), validate, organization_controller.addTutorsForOrganization);
router.get('/tutors', organization_controller.getTutors);
router.delete('/removeTutors', removeTutors_ValidationRules(), validate, organization_controller.removeTutorsForOrganization);
router.put('/updateTutor', /*updateTutor_ValidationRules(), validate,*/ organization_controller.org_updateTutor);
router.post('/addStudents', addStudents_ValidationRules(), validate,
organization_controller.org_addStudents);
router.put('/updateStudent', updateStudent_ValidationRules(), validate, organization_controller.student_update);
router.delete('/removeStudents',casl, removeStudents_ValidationRules(), validate,
organization_controller.org_removeStudents);
router.get('/getStudents',casl, organization_controller.org_getStudents);
router.get('/videoKyc', validate, organization_controller.getVideoKycToken);
router.post('/videoKyc', updateVideoKycSuccessUuid_ValidationRules(), validate, organization_controller.updateVideoKycSuccessUuid);
router.post('/videoKycLink', generateVideoKycAwsLink_ValidationRules(), validate, organization_controller.generateVideoKycAwsLink);
router.get('/videoKycLink', getVideoKycDownloadLink_ValidationRules(), validate, organization_controller.getVideoKycDownloadLink);
router.get('/redoVideoKyc', validate, organization_controller.redoVideoKyc);
router.get('/getClassStudents',getStudentsForClass_ValidationRules(),validate,organization_controller.org_getStudentsForClass);



module.exports = router;