const express = require('express')
const router = express.Router()
const adminAuth_controller = require('../controllers/adminAuth.controller')
const pass_controller = require('../utils/adminPassport.utils')
const { adminAuthenticate_ValidationRules, generateRegisterLink_ValidationRules, register_ValidationRules, getOtp_ValidationRules, verifyOtp_ValidationRules, setPassword_ValidationRules } = require('../validators/adminAuth.validator')
const { validate } = require('../validators/validator');

// router.put('/register', register_ValidationRules(), validate, adminAuth_controller.register)
router.post('/authenticate', adminAuthenticate_ValidationRules(), validate, pass_controller.adminAuthenticate)
router.get('/getOtp', getOtp_ValidationRules(), validate, adminAuth_controller.getOtp)
router.post('/verifyOtp', verifyOtp_ValidationRules(), validate, adminAuth_controller.verifyOtp)
router.post('/setPassword', setPassword_ValidationRules(), validate, adminAuth_controller.setPassword)

module.exports = router