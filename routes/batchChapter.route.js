const express = require('express');
const router = express.Router();
const batchChapter_controller = require('../controllers/batchChapter.controller');
const { validate } = require('../validators/validator.js')
const {
    batchChapterSave_ValidationRules
} = require('../validators/batchChapter.validator')
const {casl} = require('../utils/casl/casl.middleware')

router.post('/create',casl, batchChapterSave_ValidationRules(), validate, batchChapter_controller.batchChapter_save);
router.get('/studentChapters',casl , batchChapter_controller.student_chapters);

module.exports = router;