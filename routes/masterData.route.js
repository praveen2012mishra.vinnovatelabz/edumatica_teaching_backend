const express = require('express');
const router = express.Router();
const masterData_controller = require('../controllers/masterData.controller');
const { validate } = require('../validators/validator.js')
const {
    getClassList_ValidationRules, getSubjectList_ValidationRules,
    getChapterList_ValidationRules, getSchoolsByCity_ValidationRules,
    getCitiesByPinCode_ValidationRules
} = require('../validators/masterData.validator')
const {casl} = require('../utils/casl/casl.middleware')


router.get('/getBoardsList',casl , masterData_controller.getBoardsList);
router.get('/getClassList',casl , getClassList_ValidationRules(), validate, masterData_controller.getClassList);
router.get('/getSubjectList',casl , getSubjectList_ValidationRules(), validate, masterData_controller.getSubjectList);
router.get('/getChapterList',casl , getChapterList_ValidationRules(), validate, masterData_controller.getChapterList);
router.get('/getCities',casl , masterData_controller.getCityList);
router.get('/getCitiesByPinCode',casl , getCitiesByPinCode_ValidationRules(), validate, masterData_controller.getCitiesByPinCode);
router.get('/getSchoolsByCity',casl , getSchoolsByCity_ValidationRules(), validate, masterData_controller.getSchoolsByCity);
router.get('/getQualificationList', masterData_controller.getQualificationList);

module.exports = router;