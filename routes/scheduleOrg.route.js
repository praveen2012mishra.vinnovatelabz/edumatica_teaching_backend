const express = require('express');
const router = express.Router();
const schedule_controller = require('../controllers/scheduleOrg.controller');
const { validate } = require('../validators/validator.js')
const { scheduleCreate_ValidationRules, scheduleDelete_ValidationRules,
    scheduleUpdate_ValidationRules
} = require('../validators/schedule.validator')
const {casl} = require('../utils/casl/casl.middleware')

router.post('/create',casl, scheduleCreate_ValidationRules(), validate, schedule_controller.schedule_create);
router.delete('/delete',casl, scheduleDelete_ValidationRules(), validate, schedule_controller.schedule_delete);
router.put('/',casl, scheduleUpdate_ValidationRules(), validate, schedule_controller.schedule_update);

module.exports = router;