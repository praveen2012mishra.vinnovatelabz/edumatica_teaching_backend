const express = require('express');
const router = express.Router();
const organization_controller = require('../controllers/organization.controller');
const {  fetchOrgKycDetails_ValidationRules } = require('../validators/organization.validator');
const { fetchTutorKycDetails_ValidationRules } = require('../validators/tutor.validator');
const tutor_controller = require('../controllers/tutor.controller');
const { validate } = require('../validators/validator.js')

router.get('/fetchOrgKycDetails', fetchOrgKycDetails_ValidationRules(), validate, organization_controller.fetchKycDetails)
router.get('/fetchTutorKycDetails', fetchTutorKycDetails_ValidationRules(), validate, tutor_controller.fetchKycDetails)




module.exports = router;