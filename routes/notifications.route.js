const express = require('express');
const router = express.Router();
const { validate } = require('../validators/validator');
const {casl} = require('../utils/casl/casl.middleware');
const notifications_controller = require('../controllers/notifications.controller')
const {
  pushNotificationsSaveDeviceTokens_ValidationRules,
  saveMsgTemplate_ValidationRules,
  savePushTemplate_ValidationRules,
  sendCustomPush_ValidationRules,
  sendMail_ValidationRules,
  getAllNotifications_ValidationRules,
  updateNotifications_ValidationRules,
  switchMsgTemplate_ValidationRules,
  switchPushTemplate_ValidationRules,
  announcementFileUpload_ValidationRules,
  announcementFileDownload_ValidationRules,
  fetchAnnouncement_ValidationRules,
  fetchAnnouncementById_ValidationRules,
} = require("../validators/notifications.validator");

router.post('/devicetoken', pushNotificationsSaveDeviceTokens_ValidationRules(), validate, notifications_controller.notifications_pushNotificationsSaveDeviceTokens)
router.post('/msgtemplate', saveMsgTemplate_ValidationRules(), validate, notifications_controller.save_notificationMsgTemplate)
router.post('/pushtemplate', savePushTemplate_ValidationRules(), validate, notifications_controller.save_notificationPushTemplate)

router.patch('/msgtemplate', switchMsgTemplate_ValidationRules(), validate, notifications_controller.switch_notificationMsgTemplate)
router.patch('/pushtemplate', switchPushTemplate_ValidationRules(), validate, notifications_controller.switch_notificationPushTemplate)

router.post('/sendcustompush', sendCustomPush_ValidationRules(), validate, notifications_controller.send_customPush)
router.post('/announcementFile', announcementFileUpload_ValidationRules(), validate, notifications_controller.generateAnnouncementFileAwsLink)
router.get('/announcementFile', announcementFileDownload_ValidationRules(), validate, notifications_controller.getAnnouncementFileDownloadLink)
router.get('/announcement/:announcementId', fetchAnnouncementById_ValidationRules(), validate, notifications_controller.fetchAnnouncementById)
router.get('/announcement', fetchAnnouncement_ValidationRules(), validate, notifications_controller.fetchAnnouncement)

router.get('/sendmail',sendMail_ValidationRules(), validate, notifications_controller.sendMail)
router.get('/', getAllNotifications_ValidationRules(), validate, notifications_controller.getAllNotifications)
router.patch('/', updateNotifications_ValidationRules(), validate, notifications_controller.updateNotifications)
module.exports = router;