const express = require('express');
const router = express.Router();
const batch_controller = require('../controllers/batch.controller');
const { validate } = require('../validators/validator.js')
const { batchCreate_ValidationRules, batchUpdate_ValidationRules,
        batchDetails_ValidationRules,
        batchDelete_ValidationRules
        ,batchesSearch_ValidationRules, getUploadUrlForSyllabus_ValidationRules,
        getDownloadUrlForSyllabus_ValidationRules,
        updateSyllabus_ValidationRules,
        batchDetails_id_ValidationRules
    } = require('../validators/batch.validator')
const {casl} = require('../utils/casl/casl.middleware')

router.get('/batches', casl ,batch_controller.batch_batches);
router.get('/bbbbatches',batch_controller.batch_bbbbatches);
router.post('/create', casl ,batchCreate_ValidationRules(), validate, batch_controller.batch_create);
router.get('/detail', casl ,batchDetails_ValidationRules(), validate, batch_controller.batch_details);
router.get('/detail/id', casl ,batchDetails_id_ValidationRules(), validate, batch_controller.batch_details_id);
router.put('/update', casl ,batchUpdate_ValidationRules(), validate, batch_controller.batch_update);
router.delete('/delete',casl, batchDelete_ValidationRules(), validate, batch_controller.batch_delete);

router.get('/batchsearch', batchesSearch_ValidationRules() , validate , batch_controller.batches_search)

router.get('/getUploadUrlForSyllabus', getUploadUrlForSyllabus_ValidationRules(), validate, batch_controller.getUploadUrlForSyllabus);
router.get('/getDownloadUrlForSyllabus', getDownloadUrlForSyllabus_ValidationRules(), validate, batch_controller.getDownloadUrlForSyllabus);
router.put('/syllabus', casl, updateSyllabus_ValidationRules(), validate, batch_controller.syllabusUpdate);

module.exports = router;