const express = require('express');
const router = express.Router();
const auth_controller = require('../controllers/auth.controller');
const pass_controller = require('../utils/passport.utils');
const { 
    register_ValidationRules, setPassword_ValidationRules, checkOrgCode_ValidationRules,
    authenticate_ValidationRules, verifyCaptcha_ValidationRules, refreshtoken_ValidationRules, verifyEmail_ValidationRules
} = require('../validators/auth.validator');
const { validate } = require('../validators/validator');

router.post('/checkOrgCode', checkOrgCode_ValidationRules(), validate, auth_controller.checkOrgCode);
router.put('/register', register_ValidationRules(), validate, auth_controller.register);
router.post('/setPassword', setPassword_ValidationRules(), validate, auth_controller.setPassword);
router.post('/authenticate', authenticate_ValidationRules(), validate, pass_controller.authenticate);
router.post('/refreshtoken', refreshtoken_ValidationRules(), validate, auth_controller.refreshtoken);
router.post('/verifyCaptcha', verifyCaptcha_ValidationRules(), validate, auth_controller.verifyCaptcha);
router.post('/verifyEmail', verifyEmail_ValidationRules(), validate, auth_controller.verifyEmail)

module.exports = router;