const express = require('express');
const router = express.Router();
const bbbWebHook_controller = require('../controllers/bbbWebHooks.controller')
const {casl} = require('../utils/casl/casl.middleware');
const { validate } = require('../validators/validator');
const {
  bbbWebHookCreateHook_ValidationRules,
  bbbWebHookDestroyHook_ValidationRules,
  bbbWebHookGethookslist_ValidationRules,
} = require("../validators/bbbWebHooks.validator");

router.post('/create', bbbWebHookCreateHook_ValidationRules(), validate, bbbWebHook_controller.bbbWebHook_createHook)
router.post('/destroy', bbbWebHookDestroyHook_ValidationRules(), validate, bbbWebHook_controller.bbbWebHook_destroyHook)
router.post('/lists', bbbWebHookGethookslist_ValidationRules(), validate, bbbWebHook_controller.bbbWebHook_gethookslist)

module.exports = router;