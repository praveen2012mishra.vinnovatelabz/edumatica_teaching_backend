const express = require('express');
const router = express.Router();
const {firstPatchassessment_ValidationRules, patchassessment_ValidationRules , getAttemptAssessment_ValidationRules,getAttemptAssessmentAnswers_ValidationRules, getAttemptAssessmentbyAssessmentId_ValidationRules} = require('../validators/attemptassessment.validator')
const { validate } = require('../validators/validator.js')
const attemptassessment_controller = require('../controllers/attemptassessment.controller');
const {casl} = require('../utils/casl/casl.middleware')




router.patch('/patchassessment',casl,patchassessment_ValidationRules(),validate,attemptassessment_controller.attemptassessment_patchAttemptedAssessment)

router.patch('/firstpatchassessment',casl,firstPatchassessment_ValidationRules(),validate,attemptassessment_controller.attemptassessment_patchAttemptedAssessment)

router.get('/checkStatus',casl,getAttemptAssessment_ValidationRules(), validate , attemptassessment_controller.attemptassessment_getAttemptAssessment)

router.get('/getAnswers',casl,getAttemptAssessmentAnswers_ValidationRules(),validate, attemptassessment_controller.attemptassessment_getAnswers)

router.get('/attemptassessments',casl,validate,attemptassessment_controller.attemptassessment_getAllAssessments)
router.get('/attemptassessments/child',casl,validate,attemptassessment_controller.attemptassessment_getAllChildAssessments)

router.get('/getStudents',casl,validate,attemptassessment_controller.attemptassessment_getassessmentstudentdata)


router.get('/getAssignedAssessments',casl,validate,attemptassessment_controller.attemptassessment_getAssignedAssessments)

router.get('/getAllAssignedAssessments',validate,attemptassessment_controller.attemptassessment_getAllAssignedAssessments)

router.get('/getAttemptAssessmentbyAssessmentId', getAttemptAssessmentbyAssessmentId_ValidationRules(),validate,attemptassessment_controller.attemptassessment_getAttemptAssessmentbyAssessmentId)


module.exports = router;
