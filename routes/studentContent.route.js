const express = require('express');
const router = express.Router();
const studentContent_controller = require('../controllers/studentContent.controller');
const { validate } = require('../validators/validator.js')
const {
    studentContentCreate_ValidationRules, studentContentAllstudentContents_ValidationRules,
    studentContentSaveAnswers_ValidationRules, studentContentViewResults_ValidationRules,
    studentContentGetContent_ValidationRules
} = require('../validators/studentContent.validator')
const {casl} = require('../utils/casl/casl.middleware')

router.get('/contents',casl, studentContentAllstudentContents_ValidationRules(), validate,
 studentContent_controller.studentContent_allContents);
router.get('/details',casl, studentContentGetContent_ValidationRules(), validate,
 studentContent_controller.studentContent_details);
router.post('/create',casl, studentContentCreate_ValidationRules(), validate,
 studentContent_controller.studentContent_create);
router.put('/saveAnswers',casl, studentContentSaveAnswers_ValidationRules(), validate,
 studentContent_controller.studentContent_saveAnswers);
router.post('/viewResults',casl, studentContentViewResults_ValidationRules(), validate,
 studentContent_controller.studentContent_viewResults);

module.exports = router;