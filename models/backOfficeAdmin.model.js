const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const { BACKOFFICE_ADMIN_TYPES } = require('../utils/constant.utils')

const BackOfficeAdminSchema = new Schema({
    mobileNo : {type: String},
    firstName : {type: String},
    lastName: {type: String},
    emailId : {type: String},
    adminRole: {type: Schema.Types.ObjectId, ref:'BackOfficeAdminRole'},
    isItDefaultPassword: {type: Boolean, default: true},
    status: {type: Number, default: 1},
    insertedon: {type: Date},
    insertedby: {type: String},
    updatedon: {type: Date},
    updatedby: {type: String},
    actionCount: {type: Number}
});

const model = mongoose.model("BackOfficeAdmin", BackOfficeAdminSchema);

module.exports = model;
