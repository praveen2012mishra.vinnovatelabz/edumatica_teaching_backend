const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let MasterSchoolSchema = new Schema({
    cityID: {type: Number},
    cityName: {type: String}, 
    schoolName: {type: String},
    schoolAddress: {type: String},
    status: {type: Number},
    updatedon: {type: Date},
    updatedby: {type: String}
});

//Export the model
module.exports = mongoose.model('MasterSchool', MasterSchoolSchema);