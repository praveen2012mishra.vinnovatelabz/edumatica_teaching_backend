const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let EventSchema = new Schema({
    _id: Schema.Types.ObjectId,
    type: {
      type: String,
    },
    userID: {
      type: String,
    },
    name: {
      type: String,
    },
    role: {
      type: String,
    },
    eventTriggerTime: {
      type: Number,
    },
  });

  module.exports = mongoose.model("Event", EventSchema);