const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let RoleSchema = new Schema({
    name: {type: String},
    status: {type: Number},
    updatedon: {type: Date},
    updatedby: {type: String}
});

module.exports = mongoose.model('Role', RoleSchema);