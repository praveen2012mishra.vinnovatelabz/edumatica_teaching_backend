const mongoose = require('mongoose');
const { ROLE_STATUSES, KYC_STATUSES } = require('../utils/constant.utils');

const Schema = mongoose.Schema;

const courseSchema = mongoose.Schema({
    board: {type: String}, 
    className: {type: String},
    subject: {type: String}
},{ _id : false });

const kycSchema = mongoose.Schema({
    kycDocType: {type: String}, 
    kycDocFormat: {type: String},
    kycDocLocation: {type: String},
    uuid: {type: String},
    url: {type: String}
},{ _id : false });

const OrganizationSchema = new Schema({
    ownerId: {type: String},
    mobileNo : {type: String},
    organizationName : {type: String},
    emailId : {type: String},
    dob: {type: String},
    businessType: { type: String},
    businessName: { type: String},
    ownerName: { type: String},
    image: {type: String},
    address: {type: String},
    city: { type: String},
    pinCode: { type: String},
    stateName: {type: String},
    ownerPAN: { type: String},  // from here
    businessPAN: { type: String},
    gstin: {type: String},
    aadhaar : {type: String},
    bankIfsc : {type: String},
    bankAccount : {type: String},
    aadhaarKycZip : {type: String}, 
    aadhaarKycShareCode : {type: String},
    aadhaarKycStatus : {type: String},
    aadhaarMobile : {type: String},
    videoKycToken: {type: String},
    videoKycUuid: {type: String},
    videoKycFileName: {type: String},
    isVideoKycDone: {type: Boolean, default: false},
    videoKycVerified: {type: Boolean, default: false},
    videoKycAttempts: {type: Number, default: 0},
    kycDetails: [kycSchema],    // to here kyc to add boolean switch kyc complete
    isAllKycProcessDone: {type: Boolean, default: false},
    allKycStatus: {type: String, enum : KYC_STATUSES, default: 'notChecked'},
    linkedAccount: {type: String},
    courseDetails: [courseSchema],
    tutorList: [{type: mongoose.Schema.Types.ObjectId, ref:'Tutor'}],
    studentList: [{type: mongoose.Schema.Types.ObjectId, ref:'Student'}],
    batchList: [{type: mongoose.Schema.Types.ObjectId, ref:'Batch'}],
    scheduleList: [{type: mongoose.Schema.Types.ObjectId, ref:'Schedule'}],
    contentSize: {type: Number},
    customChapter: {type: Number},
    quizCount: {type: Number},
    package: {type: mongoose.Schema.Types.ObjectId, ref:'MasterPackage'},
    status: {type: Number},
    updatedon: {type: Date},
    updatedby: {type: String},
    roleStatus: {type: String, enum : ROLE_STATUSES },
    emailVerificationToken: {type: String},
    expireEmailVerificationToken: {type: Date},
    isEmailVerified: {type: Boolean},
    userReferralCode: {type: String},
    canUseReferralTill: {type: Date},
    isReferralCodeUsed: {type: Boolean, default: false},
    platformStartDate: {type: Date, default: new Date()},
});

const model = mongoose.model("Organization", OrganizationSchema);

module.exports = model;