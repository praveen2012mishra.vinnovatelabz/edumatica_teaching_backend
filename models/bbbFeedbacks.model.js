const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let FeedbackSchema = new Schema({
    batchId: {
        type:Schema.Types.ObjectId,
        ref:'Batch',
        required:true
    },
    entityId: {
        type:Schema.Types.ObjectId,
        ref:'Student',
        required: true
    },
    rating: {
        type: Number,
        required: true
    },
    review: {
        type: String,
    }
})

module.exports = mongoose.model("Feedback", FeedbackSchema);