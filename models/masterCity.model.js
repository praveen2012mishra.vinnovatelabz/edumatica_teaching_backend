const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let MasterCitySchema = new Schema({
    pinCode: {type: Number},
    cityName: {type: String}, 
    stateName: {type: String},
    status: {type: Number},
    updatedon: {type: Date},
    updatedby: {type: String}
});

//Export the model
module.exports = mongoose.model('MasterCity', MasterCitySchema);