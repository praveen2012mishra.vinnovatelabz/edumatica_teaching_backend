const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let PaymentPackageSchema = new Schema({
  _id: Schema.Types.ObjectId,
  ownerId: { type: String },
  totalprice: { type: Number },
  discount: { type: Number },
  paymentplan: {
    type: String,
    enum: ["PREPAID", "POSTPAID"],
    default: "PREPAID",
  },
  paymentcycle: {
    type: String,
    enum: ["LUMPSUM", "HALFYEARLY", "QUARTERLY", "MONTHLY"], // LUMPSUM | QUARTERLY | HALFYEARLY | MONTHLY
    default: "MONTHLY",
  },
  graceperiod: { type: Number },
  updatedon: { type: Date },
  updatedby: { type: String },
});

//Export the model
module.exports = mongoose.model("PaymentPackage", PaymentPackageSchema);
