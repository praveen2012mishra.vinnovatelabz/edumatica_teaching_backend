const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let MeetingSchema = new Schema({
  _id: Schema.Types.ObjectId,
  meetingID: {
    type:Schema.Types.ObjectId,
    ref:'Batch',
  },
  internalMeetingID: {
    type: String,
  },
  scheduleID: {
    type:Schema.Types.ObjectId,
    ref:'Schedule',
  },
  meetingName: {
    type: String,
  },
  createDate: {
    type: String,
  },
  createTime: {
    type: Number,
  },
  tutorID: {
    type:Schema.Types.ObjectId,
    ref:'Tutor'
  },
  tutorName: {
    type: String,
  },
  isRecorded: {
    type: Boolean,
    default: false
  },
  isRecordingDeleted: {
    type: Boolean,
  },
  recordingURL: {
    type: String
  },
  events: [{type: Schema.Types.ObjectId, ref:"Event"}],
});

module.exports = mongoose.model("Meeting", MeetingSchema);
