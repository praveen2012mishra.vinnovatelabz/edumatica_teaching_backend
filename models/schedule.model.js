const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let ScheduleSchema = new Schema({
    ownerId: {type: String},
    tutorId: {type: mongoose.Schema.Types.ObjectId, ref:'Tutor'},
    batch: { type: mongoose.Schema.Types.ObjectId, ref:'Batch' },
    dayname: {type: String},  // master data service format Monday, Tuesday
    fromhour: {type: String}, // format 10:00  hh:mm
    tohour: {type: String},
    status: {type: Number},
    updatedon: {type: Date},
    updatedby: {type: String}
});


//Export the model
module.exports = mongoose.model('Schedule', ScheduleSchema);