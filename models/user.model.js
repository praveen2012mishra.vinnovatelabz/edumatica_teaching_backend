const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let UserSchema = new Schema({
    mobile: {type: String, unique: true},
    password: {type: String},
    failedAttemptCount: {type: Number},
    failedOn: {type: Date},
    owner: [{
        ownerId: {type: String},
        roles: [{ 
            role: {type: mongoose.Schema.Types.ObjectId, ref:'Role'},
            entityId: {type: mongoose.Schema.Types.ObjectId}
        }]
    }],
    status: {type: Number},
    updatedon: {type: Date},
    updatedby: {type: String}
});

module.exports = mongoose.model('User', UserSchema);