const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let AssessmentQuestionSchema = new Schema({
    questionSource : String,
    question: {type: Schema.Types.ObjectId, refPath:'questionSource'},
    percentageError:Number,
    negativeMarking:Number,
    marks: Number , 
    updatedon: {type: Date},
    updatedby: {type: String}
});

//Export the model
module.exports = mongoose.model('AssessmentQuestion', AssessmentQuestionSchema);