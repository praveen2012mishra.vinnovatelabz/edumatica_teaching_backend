const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const NotificationSchema = mongoose.Schema({
    _id: Schema.Types.ObjectId,
    title: { type: String },
    message: {type: String}, 
    data: {type: String},
    isViewedStatus: {type: Boolean},
});


// //Export the model
module.exports = mongoose.model('UserNotification', NotificationSchema)