const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let BatchChapterSchema = new Schema({
    ownerId: {type: String},  // tutor profile service
    batch:  { type: mongoose.Schema.Types.ObjectId, ref:'Batch' },
    chapter:  { type: mongoose.Schema.Types.ObjectId, ref:'Chapter' },
    startDate: {type: Date},
    endDate: {type: Date},
    status: {type: Number},
    updatedon: {type: Date},
    updatedby: {type: String}
});

//Export the model
module.exports = mongoose.model('BatchChapter', BatchChapterSchema);
