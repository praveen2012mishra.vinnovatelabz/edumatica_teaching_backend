const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let BatchInviteSchema = new Schema({
    ownerId: {type: String},
    batchId: { type: Schema.ObjectId, ref: "Batch" },
    studentId: { type: Schema.ObjectId, ref: "Student" },
    isAccepted: {
        type: String,
        enum: ["ACCEPT", "REJECT", "REMOVE", "DELETE"],
        default: "REJECT",
    }
});

//Export the model
module.exports = mongoose.model('BatchInvite', BatchInviteSchema);
