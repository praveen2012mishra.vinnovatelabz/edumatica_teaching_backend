const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const OtpSchema = new Schema({
    mobileNo : {type: String},
    otp : {type: String},
    generatedOn : {type: Date},    
    sentCount: {type: Number},
    failedCount: {type: Number},
    status: {type: Number},
    updatedon: {type: Date},
    updatedby: {type: String}
});

const model = mongoose.model("Otp", OtpSchema);

module.exports = model;