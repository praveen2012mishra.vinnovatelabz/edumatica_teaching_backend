const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const MasterPackageSchema = new Schema({
    name : {type: String},
    planId : {type: String},               // can be trial | Edumatica_Basic | Edumatica_Plus
    graceperiod : {type : Number},         
    paymentplan: {
        type: String,
        enum: ["PREPAID", "POSTPAID"],
        default: "PREPAID",
    },
    paymentcycle: {
        type: String,
        enum: ["LUMPSUM", "HALFYEARLY", "QUARTERLY", "MONTHLY"], // LUMPSUM | QUARTERLY | HALFYEARLY | MONTHLY
        default: "MONTHLY",
    },    //discount not included , Discount will be kept seperate. 
    cost: {type: Number},
    perstudentcost : {type: Number},
    recordingQuota : {type : Number},   //In hours 
    courseCount: {type: Number},
    studentCount: {type: Number},
    tutorCount: {type: Number},
    batchCount: {type: Number},
    sessionCount: {type: Number},
    contentSize: {type: Number},
    customChapter: {type: Number},
    status: {type: Number},
    // max session hour per day, 
    isActive: {type: Boolean, default:true},
    activeFrom: {type: Date, default: new Date()},
    activeTill: {type: Date},
    icon: {type: String},
    updatedon: {type: Date},
    updatedby: {type: String}
});

const model = mongoose.model("MasterPackage", MasterPackageSchema);

module.exports = model;