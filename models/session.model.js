const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let SessionSchema = new Schema({
    ownerId: {type: String},
    tutorId: {type: mongoose.Schema.Types.ObjectId, ref:'Tutor'},
    schedule: { type: mongoose.Schema.Types.ObjectId, ref:'Schedule' },
    dayname: {type: String},  // master data service format Monday, Tuesday
    fromhour: {type: String}, // format 10:00  hh:mm
    tohour: {type: String},   
    date: {type: String}, //'YYYY-MM-DD'
    roomid: {type: String},
    status: {type: Number},
    updatedon: {type: Date},
    updatedby: {type: String}
});


//Export the model
module.exports = mongoose.model('Session', SessionSchema);