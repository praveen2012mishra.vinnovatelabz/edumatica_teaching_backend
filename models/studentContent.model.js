const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let StudentContentSchema = new Schema({
    ownerId: {type: String},  // tutor profile service
    student:  { type: mongoose.Schema.Types.ObjectId, ref:'Student' },
    content:  { type: mongoose.Schema.Types.ObjectId, ref:'Content' },
    batch:  { type: mongoose.Schema.Types.ObjectId, ref:'Batch' },
    duration: {type: Number},
    isFinished: {type : Boolean},
    total: {type: Number}, 
	attempted: {type: Number},
    correct: {type: Number},
    marks: {type: Number},
    answers: [{
                serialNo: {type: Number},
                selectedOption: {type: String}, 
                isCorrect: {type: Boolean}
             }],
    status: {type: Number},
    updatedon: {type: Date},
    updatedby: {type: String}
});

//Export the model
module.exports = mongoose.model('StudentContent', StudentContentSchema);
