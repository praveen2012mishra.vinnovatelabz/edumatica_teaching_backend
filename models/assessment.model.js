const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let AssessmentSchema = new Schema({
    ownerId: {type: String},  // tutor profile service
    userId : { type: String }, // for specific tutor service 
    boardname: {type: String},  // master service
	classname: {type: String},  // master service
	subjectname: {type: String}, // master service 
    assessmentname: {type: String}, // string with length 5 - 50
    totalMarks: {type: Number}, // string with length 5 - 50
    duration: {type: Number}, // in minutes
    instructions: {type: String},
    // published : {type: Boolean},
    sections : [{type:Schema.Types.ObjectId,ref:'Section'}],
    status: {type: Number},
    updatedon: {type: Date},
    updatedby: {type: String}
});

//Export the model
module.exports = mongoose.model('Assessment', AssessmentSchema);