const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let BatchSchema = new Schema({
    ownerId: {type: String}, 
    tutorId: { type: Schema.ObjectId, ref:'Tutor' },
    boardname: {type: String}, 
	classname: {type: String},
	subjectname: {type: String}, 
    batchfriendlyname: {type: String}, 
    syllabus: {type: String}, 
    batchstartdate: {type: Date}, 
    batchenddate: {type: Date},
    batchicon: {type: String},
    students: [{ type: Schema.ObjectId, ref:'Student' }],
    status: {type: Number},
    updatedon: {type: Date},
    updatedby: {type: String}
});

//Export the model
module.exports = mongoose.model('Batch', BatchSchema);
