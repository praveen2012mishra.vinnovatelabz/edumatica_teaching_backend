const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let QuizDetailSchema = new Schema({
    serialNo: {type: Number}, 
    questiontext: {type: String},
	option1: {type: String}, 
	option2: {type: String},
	option3: {type: String}, 
    option4: {type: String},
    answer: {type: String},
    answerDescription:  {type: String},
    status: {type: Number},
    updatedon: {type: Date},
    updatedby: {type: String}
});

//Export the model
module.exports = mongoose.model('QuizDetail', QuizDetailSchema);
