const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let AnnouncementSchema = new Schema({
  ownerId: { type: String },
  accountType : {type : String, enum:['Tutor', 'Organization']},                                          // Whether a independent teacher or Organization
  entityId: {type: Schema.Types.ObjectId, refPath: 'accountType'},
  title: { type: String },
  brief: { type: String },
  fileUuid: { type: String },
  batchArr: [{type: Schema.Types.ObjectId, ref: 'Batch'}],
  students: [{type: Schema.Types.ObjectId, ref: 'Student'}]
}, {timestamps : true});

//Export the model
module.exports = mongoose.model("Announcement", AnnouncementSchema);
