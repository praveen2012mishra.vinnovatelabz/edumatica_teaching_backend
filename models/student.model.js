const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const StudentSchema = new Schema({
    ownerId : {type: String},
    mobileNo : {type: String},
    enrollmentId: {type: String},
    studentName : {type: String},
    emailId : {type: String},
    boardName : {type: String},
    className : {type: String},
    image: {type: String},
    location: {type: String},
    pinCode: {type: String},
    stateName: {type: String},
    cityName: {type: String},
    schoolName : {type: String},
    batches: [{type: mongoose.Schema.Types.ObjectId, ref:'Batch'}],
    parent: { type: mongoose.Schema.Types.ObjectId, ref:'Parent' },
    status: {type: Number},
    updatedon: {type: Date},
    updatedby: {type: String},
    roleStatus: {type: String, enum : ['FRESHER', 'ACTIVE', 'DEACTIVATE'] },
    disabledBatches: [{type: mongoose.Schema.Types.ObjectId, ref:'Batch'}],
    dob: {type: Date},
    gender: {type: String},
    country:{type: String},
    emailVerificationToken: {type: String},
    expireEmailVerificationToken: {type: Date},
    isEmailVerified: {type: Boolean},
    platformStartDate: {type: Date, default: new Date()},
});

const model = mongoose.model("Student", StudentSchema);

module.exports = model;