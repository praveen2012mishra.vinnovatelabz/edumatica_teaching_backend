const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let AttemptedAssessmentSchema = new Schema({
  ownerId: { type: String },
  studentId: { type: Schema.Types.ObjectId, ref: "Student" },
  batchId: {type: mongoose.Schema.Types.ObjectId, ref: 'Batch'},
  startTime: { type: Date },
  endTime: { type: Date },
  startDate: { type: Date },
  endDate: { type: Date },
  currentQuestion: { type: Number },
  answers: { type: [[[String]]] },
  attemptedQuestions: { type: [[Number]] },
  assessment: { type: Schema.Types.ObjectId, ref: "Assessment" },
  assessmentData: {
    unfocussed: { type: Number },
    unfocussedTime: { type: Number },
    sectionsData: {
      visits: { type: Number },
      questions: {
        visits: { type: Number },
        attempts: { type: Number },
        timeSpent: { type: Number },
      },
    },
  },
  solutionTime: { type: Date },
  isSubmitted: { type: Boolean },
  isStarted: { type: Boolean },
  updatedby: { type: String },
  updatedon: { type: Date },
});

module.exports = mongoose.model(
  "AttemptedAssessment",
  AttemptedAssessmentSchema
);
