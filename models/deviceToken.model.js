const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let DeviceTokenSchema = new Schema({
    ownerId: {type: String},
    userId: {type: String},
    mobileNo: {type: String},
    deviceToken: [{type: String}]
});

//Export the model
module.exports = mongoose.model('DeviceToken', DeviceTokenSchema);