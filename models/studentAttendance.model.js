const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let StudentAttendanceSchema = new Schema({
    student: { type: mongoose.Schema.Types.ObjectId, ref:'Student' },
    session: { type: mongoose.Schema.Types.ObjectId, ref:'Schedule' },
    isPresent: { type: Boolean},
    status: {type: Number},
    updatedon: {type: Date},
    updatedby: {type: String}
});


//Export the model
module.exports = mongoose.model('StudentAttendance', StudentAttendanceSchema);