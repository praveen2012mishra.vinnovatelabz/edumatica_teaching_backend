const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ParentSchema = new Schema({
    ownerId : {type: String},
    mobileNo : {type: String},
    parentName : {type: String},
    emailId : {type: String},
    status: {type: Number},
    updatedon: {type: Date},
    updatedby: {type: String},
    roleStatus: {type: String, enum : ['FRESHER','ACTIVE', 'DEACTIVATE'] },
    dob: {type: Date},
    gender: {type: String},
    country:{type: String}
});

const model = mongoose.model("Parent", ParentSchema);

module.exports = model;