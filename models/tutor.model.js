const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const { ROLE_STATUSES, KYC_STATUSES } = require('../utils/constant.utils')

const courseSchema = mongoose.Schema({
    board: {type: String}, 
    className: {type: String},
    subject: {type: String}
},{ _id : false });

const kycSchema = mongoose.Schema({
    kycDocType: {type: String}, 
    kycDocFormat: {type: String},
    kycDocLocation: {type: String},
    uuid: {type: String},
    url: {type: String}
},{ _id : false });

const TutorSchema = new Schema({
    ownerId: {type: String},
    mobileNo : {type: String},
    enrollmentId: {type: String},
    tutorName : {type: String},
    emailId : {type: String},
    qualifications : [{type: String}],
    schoolName : {type: String},
    address : {type: String},
    otherQualifications : {type: String},
    almaMater : {type: String},
    location: {type: String},
    pinCode: {type: String},
    stateName: {type: String},
    cityName: {type: String},
    image: {type: String},
    pan: {type: String},    // from here
    aadhaar : {type: String},
    bankIfsc : {type: String},
    bankAccount : {type: String},
    aadhaarKycZip : {type: String},
    aadhaarKycShareCode : {type: String},
    aadhaarKycStatus : {type: String},
    aadhaarMobile : {type: String},
    videoKycToken: {type: String},
    videoKycUuid: {type: String},
    gstin: {type: String},
    videoKycFileName: {type: String},
    isVideoKycDone: {type: Boolean, default: false},
    videoKycVerified: {type: Boolean, default: false},
    videoKycAttempts: {type: Number, default: 0},
    kycDetails: [kycSchema],     // to here kyc to add boolean switch kyc complete
    isAllKycProcessDone: {type: Boolean, default: false},
    allKycStatus: {type: String, enum : KYC_STATUSES, default: 'notChecked'},
    linkedAccount: {type: String},
    courseDetails: [courseSchema],
    studentList: [{type: mongoose.Schema.Types.ObjectId, ref:'Student'}],
    batchList: [{type: mongoose.Schema.Types.ObjectId, ref:'Batch'}],
    scheduleList: [{type: mongoose.Schema.Types.ObjectId, ref:'Schedule'}],
    dob: {type:Date},
    contentSize: {type: Number},
    customChapter: {type: Number},
    quizCount: {type: Number},
    package: {type: mongoose.Schema.Types.ObjectId, ref:'MasterPackage'},
    status: {type: Number},
    updatedon: {type: Date},
    updatedby: {type: String},
    roleStatus: {type: String, enum : ROLE_STATUSES}, // For what?
    emailVerificationToken: {type: String},
    expireEmailVerificationToken: {type: Date},
    isEmailVerified: {type: Boolean},
    userReferralCode: {type: String},
    canUseReferralTill: {type: Date},
    isReferralCodeUsed: {type: Boolean, default: false},
    platformStartDate: {type: Date, default: new Date()},
});

const model = mongoose.model("Tutor", TutorSchema);

module.exports = model;