const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const sectionSchema = new Schema({
  title: { type: String },
  questions: [{type:Schema.Types.ObjectId,ref:'AssessmentQuestion'}],
  totalMarks: { type: Number },
  duration: { type: Number },
  instructions: { type: String }
});

module.exports = mongoose.model("Section", sectionSchema);
