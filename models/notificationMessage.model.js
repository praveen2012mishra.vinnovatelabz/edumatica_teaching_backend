const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const NotificationMessageSchema = new Schema({
    _id: Schema.Types.ObjectId,
    messageType: {type: String},
    messageBatch_id: {type: String},
    recipient: [{type: Number}],
    message: {type: String},
    status: {type: String},
    code: {type: Number}
}, {timestamps : true});

const model = mongoose.model("NotificationMessage", NotificationMessageSchema);

module.exports = model;