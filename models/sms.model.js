const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let SmsSchema = new Schema({
    mobileNo: {type: String}, 
    otp: {type: String}, 
    message: {type: String},
    sentOn: {type: String},
    status: {type: Number},
    updatedon: {type: Date},
    updatedby: {type: String}
});


//Export the model
module.exports = mongoose.model('Sms', SmsSchema);