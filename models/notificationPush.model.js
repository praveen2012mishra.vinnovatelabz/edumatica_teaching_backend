const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const NotificationPushSchema = new Schema({
    deviceTokens: [{type: String}],
    pushMessage: {type: String},
    status: {type: String},
    response: {type: String}
}, {timestamps : true});


//Export the model
module.exports = mongoose.model('NotificationPush', NotificationPushSchema);