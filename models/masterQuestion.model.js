const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let MasterQuestionSchema = new Schema({
    filename:{type:String},
    srno:{type:Number},
    boardname:{type: String},
    classname: {type: String},  
	subjectname: {type: String}, 
	chaptername: {type: String},
	subTopic: {type: String}, 
    questionDescription:  {type: String},
    commonQuestionPart :{type:String},
    option1: {type: String},
    option2: {type: String},
    option3:  {type: String},
    option4:  {type: String},
    answer:  {type: [String]},
    answerDescription:  {type: String},
    nestedQuestions : [{type: Schema.Types.ObjectId, refPath : 'source'}],
    marks:  {type: Number},
    complexity:  'easy'|'medium'|'high',
    imageLinks:  [{filename:String,encoding:String}],
    status: {type: Number},
    source:"CustomQuestion" | "MasterQuestion" ,
    type:"single"|"multiple"|"numeric",
    percentageError:Number,
    negativeMarking:Number,
    checksum:{type:String},
    updatedon: {type: Date},
    updatedby: {type: String}
});

//Export the model
module.exports = mongoose.model('MasterQuestion', MasterQuestionSchema);