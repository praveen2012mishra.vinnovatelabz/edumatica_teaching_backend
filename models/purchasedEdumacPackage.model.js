const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const PurchasedEdumacPackageSchema = new Schema({
    ownerId : {type: String},
    accountType : {type : String, enum:['Tutor', 'Organization']},                                          // Whether a independent teacher or Organization
    entityId: {type: Schema.Types.ObjectId, refPath: 'accountType'},
    name : {type: String},
    planId : {type: String},               // can be trial | Edumatica_Basic | Edumatica_Plus
    graceperiod : {type : Number},         
    paymentplan: {
        type: String,
        enum: ["PREPAID", "POSTPAID"],
        default: "PREPAID",
    },
    paymentcycle: {
        type: String,
        enum: ["LUMPSUM", "HALFYEARLY", "QUARTERLY", "MONTHLY"], // LUMPSUM | QUARTERLY | HALFYEARLY | MONTHLY
        default: "MONTHLY",
    },    //discount not included , Discount will be kept seperate. 
    cost: {type: Number},
    perstudentcost : {type: Number},
    recordingQuota : {type : Number},   //In hours 
    courseCount: {type: Number},
    studentCount: {type: Number},
    tutorCount: {type: Number},
    batchCount: {type: Number},
    sessionCount: {type: Number},
    contentSize: {type: Number},
    customChapter: {type: Number},
});

//Export the model
module.exports = mongoose.model("PurchasedEdumacPackage", PurchasedEdumacPackageSchema);
