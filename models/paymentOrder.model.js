const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let PaymentOrderSchema = new Schema({
    ownerId: { type: String },
    entityId: { type: String },
    orderId: { type: String },
    accountType : {type : String, default: "Student"},   
    purchasedPackageId: { type: Schema.Types.ObjectId, ref: "PurchasedCoursePackage" },
    batchId: { type: Schema.Types.ObjectId, ref: "Batch" },
    paymentcycle: {
        type: String,
        enum: ["LUMPSUM", "HALFYEARLY", "QUARTERLY", "MONTHLY"],
    },
    authenticationDetail: { type: Schema.Types.ObjectId, ref: "PaymentOrderAuthentication" },
    entity: {
        type: String,
        enum: ["order"],
      },
    amount: { type: Number }, // in Rupees - paisa to rupee for razorpay
    amount_paid: { type: Number },
    gatewayServiceCharge: { type: Number },
    serviceChargeTax: { type: Number },
    settlementAmount: { type: Number },
    settlementUTR:  { type: String },
    convenience_fee: { type: Number },
    convenience_fee_tax: { type: Number },
    edumatica_earning: { type: Number },
    edumatica_earning_tax: { type: Number },
    tds: { type: Number },
    studentPayableAmt: { type: Number },
    netAmountToBene: { type: Number },
    // amount_due: { type: Number },
    currency: {
        type: String,
        enum: ["INR"],
      },
    receipt: { type: String },
    // offer_id: { type: String },
    status: {
        type: String,
        enum: ["created", "attempted", "paid", "failed"],
      },
    attempts: { type: Number },
    notes: { type: String },
    paymentType: { type: String },
    isTransferMade: {type:Boolean},
    packageUpdate: {type:Boolean, default: false},
    newPackagePlanId: { type: String },
    created_at: { type: Number },
});

//Export the model
module.exports = mongoose.model("PaymentOrder", PaymentOrderSchema);