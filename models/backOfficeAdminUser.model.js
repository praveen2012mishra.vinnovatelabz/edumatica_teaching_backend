const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { BACKOFFICE_ADMIN_TYPES } = require('../utils/constant.utils')

const BackOfficeAdminUserSchema = new Schema({
    entityId: {type: Schema.Types.ObjectId, ref:'BackOfficeAdmin'},
    username: {type: String},
    password: {type: String},
    adminRole: {type: Schema.Types.ObjectId, ref:'BackOfficeAdminRole'},
    otp: {type: String},
    otpGeneratedOn: {type: Date},
    isOtpVerified: {type: Boolean, default: false},
    failedAttemptCount: {type: Number},
    failedOn: {type: Date},
    status: {type: Number, default: 1},
    updatedon: {type: Date},
    updatedby: {type: String}
})

module.exports = mongoose.model('BackOfficeAdminUser', BackOfficeAdminUserSchema);