const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const NotificationPullSchema = new Schema({
    userId: {type: String},
    mobileNo: {type: String},
    notifications: [{type: mongoose.Schema.Types.ObjectId, ref:'UserNotification'}],
}, {timestamps : true});


// //Export the model
// module.exports = mongoose.model('UserNotification', NotificationSchema)
module.exports = mongoose.model('NotificationPull', NotificationPullSchema);