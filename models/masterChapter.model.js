const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let MasterChapterSchema = new Schema({
    boardID: {type: Number},
    boardName: {type: String}, 
    boardDescriptions: {type: String},
    classID: {type: Number},
    className: {type: String}, 
    subjectID: {type: Number},
	subjectName: {type: String}, 
	chapter: {
        name: {type: String},
        topic: [{type: String}]
     },
    status: {type: Number},
    updatedon: {type: Date},
    updatedby: {type: String}
    
});

//Export the model
module.exports = mongoose.model('MasterChapter', MasterChapterSchema);