const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let ChapterSchema = new Schema({
    ownerId: {type: String},  // tutor profile service
    batchId: {type: Schema.ObjectId, ref:'Batch'},
    batchfriendlyname: {type: String},
    chaptername: {type: String}, // string with length 5 - 50
    boardname: {type: String},  // master service
	classname: {type: String},  // master service
	subjectname: {type: String}, // master service 
	contents: [{ type: mongoose.Schema.Types.ObjectId, ref:'Content' }],
    status: {type: Number},
    updatedon: {type: Date},
    updatedby: {type: String}
});

//Export the model
module.exports = mongoose.model('Chapter', ChapterSchema);
