const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let StudentPayableSchema = new Schema({
  ownerId: { type: String },
  accountType : {type : String, enum:['Tutor', 'Organization']},                                          // Whether a independent teacher or Organization
  entityId: {type: Schema.Types.ObjectId, refPath: 'accountType'}, 
  student:{ type: Schema.Types.ObjectId, ref: "Student" },
  batch: {type: Schema.Types.ObjectId, ref: "Batch"}, 
  addedOn: {type: Date},
  renewalDate: {type: Date}, //batchstartdate + 1 month batch already started then grace date now + 7   // in cron job check if tutor renewalDate+30days =< this renewalDate if true move to next
  graceperiod: {type: Number}, //7days
  currentPaybleAmountToEdumac: {type: Number}, //from masterplan
  // payed for no. of days
  currentStatus: {type: String, enum : ['ACTIVE', 'DEACTIVATED']}, // Payment Process
  currentStatusChangeDate: {type: Date}, // Deactivate activate date 
  updatedon: { type: Date },
  updatedby: { type: String },
});

//Export the model
module.exports = mongoose.model("StudentPayable", StudentPayableSchema);
