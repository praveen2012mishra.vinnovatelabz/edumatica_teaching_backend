const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let StudentPaymentAccountSchema = new Schema({
  ownerId: { type: String },
  entityId: { type: String },
  student:  { type: Schema.Types.ObjectId, ref: "Student" },
  purchasedPackage:[{ type: Schema.Types.ObjectId, ref: "PurchasedCoursePackage" }],
  vpaId: { type: String },
  invoices : [{ type: Schema.Types.ObjectId, ref: 'Invoice' }],
  status: { type: Number, default:1 },
  updatedon: { type: Date },
  updatedby: { type: String },
});

//Export the model
module.exports = mongoose.model("StudentPaymentAccount", StudentPaymentAccountSchema);
