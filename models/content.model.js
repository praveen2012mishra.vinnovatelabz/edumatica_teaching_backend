const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let ContentSchema = new Schema({
    ownerId: {type: String}, 
    chapter: { type: mongoose.Schema.Types.ObjectId, ref:'Chapter' },
    contenttype: {type: String}, // known types slide, embedlink, image, doc, video, quiz
    contentsubtype: {type: String},  // Class Content , Extra Material
    contentlength: {type: Number},
    contentname: {type: String}, 
    title: {type: String},
    description: {type: String},
    tags: [{type: String}],
    duration: {type: Number},
    thumbnail:  {type: String},
    marks: {type: Number},
    uuid: {type: String}, // match uuid type
    questions: [{ type: mongoose.Schema.Types.ObjectId, ref:'QuizDetail' }],
    status: {type: Number},
    updatedon: {type: Date},
    updatedby: {type: String},
    studentviewcount: [{ type: Schema.ObjectId, ref:'Student' }] // student vsiting content array
});

//Export the model
module.exports = mongoose.model('Content', ContentSchema);

