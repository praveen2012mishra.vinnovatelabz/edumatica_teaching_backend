const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const OrganizationPaymentAccountSchema  = new Schema({                              //Independent teacher and Organizzation will have this account 
  ownerId : {type: String},
  accountType : {type : String, enum:['Tutor', 'Organization']},                                          // Whether a independent teacher or Organization
  entityId: {type: Schema.Types.ObjectId, refPath: 'accountType'},                                               //If an independent tutor , then tutor Id, otherwise OrganizationId
  purchasedEdumacPackage : {type: Schema.Types.ObjectId, ref: 'PurchasedEdumacPackage'},
  retainerShipFeeBalance : {type:Number},
  studentPaybleFeeBalance: {type: Number}, //or add to retainershipFeeBalance
  totaldebitBalance: {type: Number},
  currentgraceperiod: {type:Number},
  renewalDate : {type:Date},
  vpaId: {type: String}, 
  currentStudentCount : {type:Number}, 
  currentRecordingQuota : {type:Number},
  currentTutorCount : {type:Number},
  currentBatchCount : {type:Number}, 
  currentCourseCount : {type:Number}, 
  paymentOrder : [{type: Schema.Types.ObjectId, ref: 'OrgPaymentOrder'}],
  totalcreditBalance : {type:Number},
  studentPayable : [{type: Schema.Types.ObjectId, ref: 'StudentPayable'}],
})

//Export the model
module.exports = mongoose.model("OrganizationPaymentAccount", OrganizationPaymentAccountSchema);
