const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BackOfficeAdminRoleSchema = new Schema({
    name: {type: String},
    permissions: [{type: Schema.Types.ObjectId, ref:'BackOfficeAdminPermission'}],
    status: {type: Number, default: 1},
    insertedon: {type: Date},
    insertedby: {type: String},
    updatedon: {type: Date},
    updatedby: {type: String}
});

module.exports = mongoose.model('BackOfficeAdminRole', BackOfficeAdminRoleSchema);