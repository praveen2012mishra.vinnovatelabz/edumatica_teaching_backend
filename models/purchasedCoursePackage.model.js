const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const paymentPlanSchema = mongoose.Schema({
    totalprice: { type: Number },
    discount: { type: Number },
    paymentplan: {
        type: String,
        enum: ["PREPAID", "POSTPAID"],
        default: "PREPAID",
    },
    paymentcycle: {
        type: String,
        enum: ["LUMPSUM", "HALFYEARLY", "QUARTERLY", "MONTHLY"], // LUMPSUM | QUARTERLY | HALFYEARLY | MONTHLY
        default: "MONTHLY",
    },
    graceperiod: { type: Number },
},{ _id : false });

let PurchasedCoursePackageSchema = new Schema({
    _id: Schema.Types.ObjectId,
    ownerId: {type: String},
    studentId: { type: Schema.Types.ObjectId, ref: "Student" },
    batch: { type: Schema.Types.ObjectId, ref: "Batch" },
    paymentPlans: [paymentPlanSchema],
    paymentTransactions: [{type: Schema.Types.ObjectId, ref: 'PaymentOrder'}],
    currentGracePeriod: {type: Number, default: 3},
    renewalDate: {type: Date},
    paymentComplete: {type: Boolean, default: false},
});

//Export the model
module.exports = mongoose.model("PurchasedCoursePackage", PurchasedCoursePackageSchema);
