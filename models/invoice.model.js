const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let InvoiceSchema = new Schema({
  ownerId: { type: String },
  userId: { type: Schema.Types.ObjectId, ref: "User" }, // entityId
  pdflink: { type: String },
  orderId: { type: Schema.Types.ObjectId, ref: "Order" },
});

//Export the model
module.exports = mongoose.model("Invoice", InvoiceSchema);
