const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Assignment - Tutor
const taskDocSchema = Schema({
    taskDocType: {type: String}, 
    taskDocFormat: {type: String},
    taskDocLocation: {type: String},
    uuid: {type: String},
},{ _id : false });

const answerDocSchema = Schema({
    answerDocType: {type: String}, 
    answerDocFormat: {type: String},
    answerDocLocation: {type: String},
    uuid: {type: String}
},{ _id : false });

const assignedStudentSchema = Schema({
    studentId: {type: mongoose.Schema.Types.ObjectId, ref:'Student'},
    batchId: {type: mongoose.Schema.Types.ObjectId, ref: 'Batch'},
    uploadedAnswerDocs: answerDocSchema,
    marks: {type: Number},
    feedback: {type: String},
    isSubmitted: {type: Boolean, default: false},
    submittedOn: {type: Date},
    isEvaluated: {type: Boolean, default: false},
    evaluatedOn: {type: Date},
    tutorHighlights: Schema.Types.Mixed,
}, {_id: false})

const AssignedAssignmentSchema = new Schema({
    ownerId: {type: String},
    tutorId: [{type: mongoose.Schema.Types.ObjectId, ref:'Tutor'}],
    assignment: {type: mongoose.Schema.Types.ObjectId, ref:'Assignment'},
    students: [assignedStudentSchema],
    status: {type: Number, default: 1}
})

const AssignmentSchema = new Schema({
    ownerId: {type: String},
    tutorId: [{type: mongoose.Schema.Types.ObjectId, ref:'Tutor'}],
    boardname: {type: String},
	classname: {type: String},
	subjectname: {type: String},
    assignmentname: {type: String},
    marks: {type: Number},
    instructions: {type: String},
    batches: [{type: mongoose.Schema.Types.ObjectId, ref: 'Batch'}],
    students: [{type: mongoose.Schema.Types.ObjectId, ref: 'Student'}],
    taskDocs: taskDocSchema,
    brief: {type: String},
    startDate: {type: Date},
    endDate: {type: Date},
    isPublished: {type: Boolean},
    publishedDate: {type: Date},
    status: {type: Number, default: 1}
});

const Assignment = mongoose.model('Assignment', AssignmentSchema);
const AssignedAssignment = mongoose.model('AssignedAssignment', AssignedAssignmentSchema);

module.exports = {
    Assignment: Assignment,
    AssignedAssignment: AssignedAssignment,
    // AttemptAssignment: AttemptAssignment,
}