const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const NotificationEmailSchema = new Schema({
    mailID: [{type: String}],
    email: {type: String},
    status: {type: String},
    response: {type: String}
}, {timestamps : true});


//Export the model
module.exports = mongoose.model('NotificationEmail', NotificationEmailSchema);