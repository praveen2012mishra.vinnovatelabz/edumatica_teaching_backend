const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let PaymentOrderAuthenticationSchema = new Schema({
  orderId: {type: String},
  orderAmount: {type: String},
  referenceId: {type: String},
  txStatus: {type: String},
  paymentMode: {type: String},
  txMsg: {type: String},
  txTime: {type: String},
  signature: {type: String},
});

//Export the model
module.exports = mongoose.model("PaymentOrderAuthentication", PaymentOrderAuthenticationSchema);