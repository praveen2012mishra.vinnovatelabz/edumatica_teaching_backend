const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let CourseBundleSchema = new Schema({
  ownerId: { type: String },
  batch: { type: Schema.Types.ObjectId, ref: "Batch" },
  participants:[{ type: Schema.Types.ObjectId, ref: "Student" }],
  paymentpackages:[{ type: Schema.Types.ObjectId, ref: "PaymentPackage" }],
  status: { type: Number },
  updatedon: { type: Date },
  updatedby: { type: String },
});

//Export the model
module.exports = mongoose.model("CourseBundle", CourseBundleSchema);
