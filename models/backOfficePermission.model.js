const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BackOfficeAdminPermissionSchema = new Schema({
    name: {type: String},
    group: {type: String},
    status: {type: Number, default: 1},
})

module.exports = mongoose.model('BackOfficeAdminPermission', BackOfficeAdminPermissionSchema);

/*
permission
_id
permission name
status 1 || 0

*/