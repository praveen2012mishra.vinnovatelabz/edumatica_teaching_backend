const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const MasterNotificationTemplateSchema = new Schema({
    _id: Schema.Types.ObjectId,
    messageType: {type: String},
    msgIdentifierText: {type: String},
    pushIdentifierText: {type:String},
    htmlIdentifierText: {type: String},
    icon: {type: String},
    deepLink: {type: String},
    msgSwitch: {type:Boolean, default: true},
    pushSwitch: {type:Boolean, default: true},
    htmlSwitch: {type:Boolean, default: true},
}, {timestamps : true});

const model = mongoose.model("MasterNotificationTemplate", MasterNotificationTemplateSchema);

module.exports = model;