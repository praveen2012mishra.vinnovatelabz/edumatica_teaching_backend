const logger = require("../utils/logger.utils");
const { ResponseCode } = require("../utils/response.utils");
const NotificationEmail = require('../models/notificationEmail.model')

exports.notifications_auditEmail = (mailID, status, msg, response) => {
    var newNotification = new NotificationEmail({
        mailID: JSON.stringify(mailID),
        email: msg,
        status: status,
        response: JSON.stringify(response)
      });
    newNotification.save().then(result => {
        logger.info(
            "notifications_auditEmail - emailAudit.save - result - " +
              result
          );
    }).catch(err => {
        logger.error(
            "notifications_auditEmail - emailAudit.save - Error - " +
              err
          );
    })
}