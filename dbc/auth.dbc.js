const uuid = require('uuid');
var bcrypt = require('bcryptjs');
const User = require('../models/user.model');
const Role = require('../models/role.model');
const Tutor = require('../models/tutor.model');
const Organization = require('../models/organization.model');
const Student = require('../models/student.model');
const Parent = require('../models/parent.model');
const { validateOTP, validateForForgetPwd } = require('../dbc/otp.dbc');
const { ResponseCode } = require('../utils/response.utils');
const logger = require('../utils/logger.utils');
const blockDuration = 60 * 60 * 1000 // 1 hour
const maxAllowedFailedAttemptCount = 3;
const AbilityStore = require('../models/abilityStore.model')

exports.checkOwnerId  = (ownerId, callback)=> {    
    User.findOne({'owner.ownerId' : ownerId})
    .exec( function (err, userData) {
        if (err) {
            logger.error('checkOwnerId - User.findOne - Error - ' + err);
            return callback(err);
        }
        if(userData == null){                
            logger.info('checkOwnerId - ownerId:' + ownerId
                                    + ' New Code');
            return callback(null, ResponseCode.Success);                    
        }
        else{
            logger.warn('checkOwnerId - ownerId:' + ownerId
            + ' Code Already Exists');
            return callback(null, ResponseCode.CodeAlreadyExists);
        }
    })
}

const freshRegister = (ownerId, mobileNo, password, userType, callback) => {
    let returnUserType = userType;
    
        Role.findOne({name : userType}, async (err,role)=>{

        if(err){
            logger.error('register - Role.findOne - Error - ' + err);
            callback(err);
        }

        if(role == null) {
            logger.warn('register - mobileNo:' + mobileNo + 'Role' + userType + ' Role Not Found');             
            return callback(null, ResponseCode.RoleNotFound);
        } else {
            let userData;
                    
            const Model = (userType == 'ROLE_STUDENT') ? Student : ((userType == 'ROLE_PARENT') ? Parent : Tutor);
            if(userType == 'ROLE_STUDENT' || userType == 'ROLE_PARENT' || userType == 'ROLE_TUTOR') {
                userData = await new Promise(resolve => {
                    Model.findOne({$and:[{ ownerId:ownerId, mobileNo : mobileNo}]}, (err, data) => {
                        if(err){
                            resolve({status: 'error', data: err})
                        } else if(data === null) {
                            resolve({status: 'empty'})
                        } else {
                            resolve({status: 'success', data: data})
                        }
                    })
                });

                if(userData.status === 'error') {
                    logger.error('register - ' + userType + '.findOne - Error - ' + userData.data);
                    callback(userData.data);

                } else if(userData.status === 'empty' && userType != 'ROLE_TUTOR') {
                    logger.warn('register - mobileNo:' + mobileNo + ' ' + userType +' Not Found');               
                    const response = (userType == 'ROLE_STUDENT') ? ResponseCode.StudentNotFound : ResponseCode.ParentNotFound;
                    return callback(null, response);
                }
            }

            User.findOne({mobile: mobileNo}, (err, user) => {
                if(err) {
                    logger.error('register - User.findOne - Error - ' + err);
                    callback(err);
                }
                if(user != null) {
                    logger.warn('register - Flow Corrupt');               
                    callback('Flow Corrupt')
                } else {
                    var salt = bcrypt.genSaltSync(10);
                    var passwordHash = bcrypt.hashSync(password, salt);
                    let user = new User ({
                        mobile: mobileNo,
                        password: passwordHash,
                        status: 1,
                        owner:[{
                            ownerId: ownerId,
                            roles: [{
                                role: role._id
                            }]
                        }]
                    })

                    user.owner[0].roles[0].entityId = ((userType == 'ROLE_STUDENT' || userType == 'ROLE_PARENT') ? 
                        userData.data._id : user._id
                    )

                    logger.debug(user)
                    user.save(function (err) {
                        if (err) {
                            logger.error('register - user.save - Error - ' + err);
                            return callback(err);
                        }

                        if(userType == 'ROLE_TUTOR' && userData.status !== 'empty') {
                            Role.findOne({name : 'ROLE_ORGANIZATION_TUTOR'},(err,orgRole)=>{
                                if(err){
                                    logger.error('register - Role.findOne - Error - ' + err);
                                    callback(err);
                                }

                                if(orgRole != null){
                                    user.owner[0].roles[0] = {
                                        role: orgRole._id,
                                        entityId: userData.data._id
                                    }
                                    user.save(function (err) {
                                        if (err) {
                                            logger.error('register - user.save - Error - ' + err);
                                            return callback(err);
                                        }

                                        Tutor.findOneAndUpdate({$and:[{ ownerId:ownerId, mobileNo : mobileNo}]}, {$set: {roleStatus: 'ACTIVE'}}, (err, upd) => {
                                            returnUserType = 'ROLE_ORGANIZATION_TUTOR'
                                            logger.info('register - mobileNo:' + mobileNo + ' Tutor registered successfully');
                                            return callback(null, ResponseCode.Success, returnUserType);
                                        })
                                    });
                                } else {
                                    logger.error('Organization Tutor Not Avaiable')
                                    callback(null, ResponseCode.RoleNotFound)
                                }
                            })
                        } else {
                            logger.info('register - mobileNo:' + mobileNo + ' ' + userType + ' registered successfully');
                            return callback(null, ResponseCode.Success, returnUserType);
                        }
                    })
                }
            })
        }
    })
}

const addRoleRegister = (ownerId, mobileNo, password, userType, callback) => {
    let returnUserType = userType;

    Role.findOne({name : userType}, async (err,role)=>{
        if(err){
            logger.error('register - Role.findOne - Error - ' + err);
            callback(err);
        }

        if(role == null) {
            logger.warn('register - mobileNo:' + mobileNo + 'Role' + userType + ' Role Not Found');             
            return callback(null, ResponseCode.RoleNotFound);
        } else {
            let userData;
                    
            const Model = (userType == 'ROLE_STUDENT') ? Student : ((userType == 'ROLE_PARENT') ? Parent : Tutor);
            if(userType == 'ROLE_STUDENT' || userType == 'ROLE_PARENT' || userType == 'ROLE_TUTOR') {
                userData = await new Promise(resolve => {
                    Model.findOne({$and:[{ ownerId:ownerId, mobileNo : mobileNo}]}, (err, data) => {
                        if(err){
                            resolve({status: 'error', data: err})
                        } else if(data === null) {
                            resolve({status: 'empty'})
                        } else {
                            resolve({status: 'success', data: data})
                        }
                    })
                });

                if(userData.status === 'error') {
                    logger.error('register - ' + userType + '.findOne - Error - ' + userData.data);
                    callback(userData.data);

                } else if(userData.status === 'empty' && userType != 'ROLE_TUTOR') {
                    logger.warn('register - mobileNo:' + mobileNo + ' ' + userType +' Not Found');               
                    const response = (userType == 'ROLE_STUDENT') ? ResponseCode.StudentNotFound : ResponseCode.ParentNotFound;
                    return callback(null, response);
                }
            }

            User.findOne({mobile: mobileNo}, (err, user) => {
                if(err) {
                    logger.error('register - User.findOne - Error - ' + err);
                    callback(err);
                }

                if(user == null) {
                    logger.warn('register - Flow Corrupt');               
                    callback('Flow Corrupt')
                } else {
                    if(!bcrypt.compareSync(password, user.password)) {
                        return callback(null, ResponseCode.IncorrectUserOrPassword)
                    }

                    if(!(user.owner.some(elem => elem.ownerId === ownerId))) {
                        let newOwner = {
                            ownerId: ownerId,
                            roles: [{
                                role: role._id,
                                entityId: ((userType == 'ROLE_STUDENT' || userType == 'ROLE_PARENT') ? 
                                    userData.data._id : user._id
                                )
                            }]
                        }
                        let newOwnerUser = {
                            _id: user._id,
                            mobile: user.mobile,
                            owner: user.owner,
                            status: user.status
                        };
                        newOwnerUser.owner.push(newOwner)
                        logger.debug(newOwnerUser)

                        if(userType == 'ROLE_TUTOR' && userData.status !== 'empty') {
                            Role.findOne({name : 'ROLE_ORGANIZATION_TUTOR'},(err,orgRole)=>{
                                if(err){
                                    logger.error('register - Role.findOne - Error - ' + err);
                                    callback(err);
                                }

                                if(orgRole != null){
                                    let newOwner = {
                                        ownerId: ownerId,
                                        roles: [{
                                            role: orgRole._id,
                                            entityId: userData.data._id
                                        }]
                                    }
                                    returnUserType = 'ROLE_ORGANIZATION_TUTOR'
                                    User.findOneAndUpdate({mobile: mobileNo}, { $addToSet: { owner: newOwner } }, (err, data) => {
                                        if (err) {
                                            logger.error('register - user.findOneAndUpdate - Error - ' + err);
                                            return callback(err);
                                        }

                                        Tutor.findOneAndUpdate({$and:[{ ownerId:ownerId, mobileNo : mobileNo}]}, {$set: {roleStatus: 'ACTIVE'}}, (err, upd) => {
                                            logger.info('register - mobileNo:' + mobileNo + ' ' + userType + ' registered successfully');
                                            return callback(null, ResponseCode.Success, returnUserType);
                                        })
                                    })
                                } else {
                                    logger.error('Organization Tutor Not Avaiable')
                                    callback(null, ResponseCode.RoleNotFound)
                                }
                            })
                        } else {
                            User.findOneAndUpdate({mobile: mobileNo}, { $addToSet: { owner: newOwner } }, (err, data) => {
                                if (err) {
                                    logger.error('register - user.findOneAndUpdate - Error - ' + err);
                                    return callback(err);
                                }

                                logger.info('register - mobileNo:' + mobileNo + ' ' + userType + ' registered successfully');
                                return callback(null, ResponseCode.Success, returnUserType);
                            })
                        }
                    } else {
                        let newRole = {
                            role: role._id,
                            entityId: ((userType == 'ROLE_STUDENT' || userType == 'ROLE_PARENT') ? 
                                userData.data._id : user._id
                            )
                        }

                        if(userType == 'ROLE_TUTOR' && userData.status !== 'empty') {
                            Role.findOne({name : 'ROLE_ORGANIZATION_TUTOR'},(err,orgRole)=>{
                                if(err){
                                    logger.error('register - Role.findOne - Error - ' + err);
                                    callback(err);
                                }

                                if(orgRole != null){
                                    newRole = {
                                        role: orgRole._id,
                                        entityId: userData.data._id
                                    }

                                    returnUserType = 'ROLE_ORGANIZATION_TUTOR'
                                    User.findOneAndUpdate({ $and: [{mobile :mobileNo}, {"owner.ownerId": ownerId}]}, {
                                        $addToSet: { "owner.$.roles": newRole } }, (err, data) => {
                                        if (err) {
                                            logger.error('register - user.findOneAndUpdate - Error - ' + err);
                                            return callback(err);
                                        }

                                        Tutor.findOneAndUpdate({$and:[{ ownerId:ownerId, mobileNo : mobileNo}]}, {$set: {roleStatus: 'ACTIVE'}}, (err, upd) => {
                                            logger.info('register - mobileNo:' + mobileNo + ' ' + userType + ' registered successfully');
                                            return callback(null, ResponseCode.Success, returnUserType);
                                        })
                                    })
                                } else {
                                    logger.error('Organization Tutor Not Avaiable')
                                    callback(null, ResponseCode.RoleNotFound)
                                }
                            })
                        } else {
                            User.findOneAndUpdate({ $and: [{mobile :mobileNo}, {"owner.ownerId": ownerId}]}, {
                                $addToSet: { "owner.$.roles": newRole } }, (err, data) => {
                                if (err) {
                                    logger.error('register - user.findOneAndUpdate - Error - ' + err);
                                    return callback(err);
                                }

                                logger.info('register - mobileNo:' + mobileNo + ' ' + userType + ' registered successfully');
                                return callback(null, ResponseCode.Success, returnUserType);
                            })
                        }
                    }
                }
            })
        }
    })
}

exports.register = (ownerId, mobileNo, otp, password, userType, callback)=>{
    validateOTP(ownerId, mobileNo, otp, userType, false, async (err, code)=>{
        if(err){
            logger.error('register - isOtpValidated - Error - ' + err);
            callback(err);
        }

        if(code === ResponseCode.Success) {
            freshRegister(ownerId, mobileNo, password, userType, callback)
        } else if(code === ResponseCode.UserRegistered) {
            addRoleRegister(ownerId, mobileNo, password, userType, callback)
        } else {
            logger.warn('register - mobileNo:' + mobileNo + ' Otp not verified');
            callback(null, ResponseCode.OtpNotVerified);
        }
    })
}

exports.authenticate = async (username, password, callback)=>{
    try {
        User.aggregate([
            { $match : { $and: [{ mobile: username }] } },
            { $unwind: "$owner" },
            { $unwind: "$owner.roles" },
            { 
                $lookup: {
                    "from": "roles",
                    "localField": "owner.roles.role",
                    "foreignField": "_id",
                    "as": "owner.roles.role"
                }
            },
            { $unwind: "$owner.roles.role" },
            { 
                $lookup: {
                    "from": "abilityStores",
                    "localField": "owner.roles.entityId",
                    "foreignField": "consumerId",
                    "as": "owner.roles.permissions.accesspermissions"
                }
            },
            { 
                $lookup: {
                    "from": "abilityStores",
                    "localField": "_id",
                    "foreignField": "userId",
                    "as": "owner.roles.permissions.controlpermissions"
                }
            },
            {
                $group: {
                    _id: { _id : "$_id" , password: "$password", mobile: "$mobile",
                        status: "$status", failedAttemptCount: "$failedAttemptCount", 
                        failedOn: "$failedOn", __v: "$__v", updatedon: "$updatedon",
                        updatedby: "$updatedby", owner_id: "$owner._id",
                        owner_ownerId: "$owner.ownerId"
                    },
                    roles: { "$push": "$owner.roles" }
                }
            },
            {
                $group: {
                    _id: { _id : "$_id._id" , password: "$_id.password", mobile: "$_id.mobile",
                        status: "$_id.status", failedAttemptCount: "$_id.failedAttemptCount", 
                        failedOn: "$_id.failedOn", __v: "$_id.__v", updatedon: "$_id.updatedon",
                        updatedby: "$_id.updatedby"
                    },
                    owner: { "$push": {_id: "$_id.owner_id", ownerId: "$_id.owner_ownerId", roles: "$roles" } }
                }
            },
            {
                $project: { 
                    _id: "$_id._id", owner: 1, password: "$_id.password", mobile: "$_id.mobile",
                    status: "$_id.status", updatedon: "$_id.updatedon", updatedby: "$_id.updatedby",
                    failedAttemptCount: "$_id.failedAttemptCount", failedOn: "$_id.failedOn", __v: "$_id.__v"
                }
            },
            { $sort: { _id: 1 } }
        ], (err, data) => {
            data = data[0]
            
            if (!data) {
                logger.warn('authenticate - user:' + username + ' User not found.');
                return callback(null, ResponseCode.UserNotExist, data );
            }

            const currentTime = new Date();
            let failedAttemptCount = data.failedAttemptCount;
            let failedOn = data.failedOn;
            if (!failedOn || currentTime.getTime() - failedOn.getTime() > blockDuration) {
                failedAttemptCount = 0;
            }

            if (failedAttemptCount < maxAllowedFailedAttemptCount) {
                if (bcrypt.compareSync(password, data.password)) {
                    failedAttemptCount = 0;
                    failedOn = null;
                    User.updateOne({ $and: [{ mobile: data.mobile }] },
                        { $set:
                            {
                                failedAttemptCount: failedAttemptCount,
                                failedOn: failedOn
                            }
                        }, (err, update) => {
                        if (err) {
                            logger.error('authenticate - user.save -  Error - ' + err);
                            return callback(err);
                        }
                        logger.info('authenticate - user:' + username + ' User logged in successfully');
                        return callback(null, ResponseCode.Success, data);
                    })
                } else {
                    failedAttemptCount = failedAttemptCount + 1;
                    failedOn = new Date();
                    User.updateOne({ $and: [{ mobile: data.mobile }] }, 
                        { $set:
                            {
                                failedAttemptCount: failedAttemptCount,
                                failedOn: failedOn
                            }
                        }, (err, update) => {
                        if (err) {
                            logger.error('authenticate - user.save -  Error - ' + err);
                            return callback(err);
                        }
                        logger.warn('authenticate - user:' + username + ' Incorrect user or password.');
                        return callback(null, ResponseCode.IncorrectUserOrPassword);
                    })
                }
            } else {
                logger.warn('authenticate - user:' + username + ' Max attempt count exceeded');
                return callback(null, ResponseCode.MaxLoginAttemptCountExceeded, null);
            }
        })
    } catch (err_2) {
        logger.error('authenticate - User.findOne - Error - ' + err_2);
        return callback(err_2);
    }
}
exports.setPassword = (ownerId, mobileNo, otp, password, userType, callback)=>{
    validateOTP(ownerId, mobileNo, otp, userType, true , (err, code)=>{

        if(err){
            logger.error('setPassword - isOtpValidated - Error - ' + err);
            callback(err);
        }
        if(code === ResponseCode.Success){
            User.findOne({$and: [{mobile : mobileNo}]},(err,user)=>{
                if(err){
                    logger.error('setPassword - User.findOne - Error - ' + err);
                    callback(err);
                }
                if(user.password) {
                    if(bcrypt.compareSync(password, user.password)) {
                        return callback(null, ResponseCode.SamePasswordAlreadyExist)
                    }
                }
                if(user != null){   
                    var salt = bcrypt.genSaltSync(10);
                    var passwordHash = bcrypt.hashSync(password, salt);
                    user.password = passwordHash;
                    user.save(function (err) {
                        if (err) {
                            logger.error('setPassword - user.save - Error - ' + err);
                            return callback(err);
                        }
                        logger.info('setPassword - mobileNo:' + mobileNo + ' Password changed successfully');
                        callback(null, ResponseCode.Success);
                    });              
                    
                }
                else{
                    logger.warn('setPassword - mobileNo:' + mobileNo + ' User doesn\'t exist');
                    return callback(null, ResponseCode.UserNotExist);
                }
            })
        }
        else{
            logger.warn('setPassword - mobileNo:' + mobileNo + ' Otp not verified');
            return callback(null, ResponseCode.OtpNotVerified);
        }
    });
}

exports.verifyEmail = (role, entityId, verificationtoken, callback) => {
    if(role === "ROLE_TUTOR") {
        Tutor.findOne({$and: [{_id:entityId},{emailVerificationToken:verificationtoken},{expireEmailVerificationToken:{$gt:Date.now()}}]}).exec((err, userDb) => {
            if(err) {
                logger.error('verifyEmail - Tutor.findOne - Error - ' + err);
                return callback(err)
            }
            if(userDb) {
                if(userDb.isEmailVerified === true && userDb.emailVerificationToken === null) {
                    return callback(null, ResponseCode.Success)
                }
                Tutor.findOneAndUpdate({$and: [{_id:entityId},{emailVerificationToken:verificationtoken},{expireEmailVerificationToken:{$gt:Date.now()}}]}, {emailVerificationToken:null,expireEmailVerificationToken:null,isEmailVerified:true,}).exec((err, response) => {
                    if(err) {
                        logger.error('verifyEmail - Tutor.findOneAndUpdate - Error - ' + err);
                        return callback(err)
                    }
                    if(response) {
                        return callback(null, ResponseCode.Success)
                    }
                })
            }
            if(!userDb) {
                logger.warn('verifyEmail - Tutor.findOne - No tutor found');
                return callback(null, ResponseCode.UserNotExist)
            }
        })
    }
    if(role === "ROLE_ORGANIZATION") {
        Organization.findOne({$and: [{_id:entityId},{emailVerificationToken:verificationtoken},{expireEmailVerificationToken:{$gt:Date.now()}}]}).exec((err, userDb) => {
            if(err) {
                logger.error('verifyEmail - Organization.findOne - Error - ' + err);
                return callback(err)
            }
            if(userDb) {
                if(userDb.isEmailVerified === true && userDb.emailVerificationToken === null) {
                    return callback(null, ResponseCode.Success)
                }
                Organization.findOneAndUpdate({$and: [{_id:entityId},{emailVerificationToken:verificationtoken},{expireEmailVerificationToken:{$gt:Date.now()}}]}, {emailVerificationToken:null,expireEmailVerificationToken:null,isEmailVerified:true,}).exec((err, response) => {
                    if(err) {
                        logger.error('verifyEmail - Organization.findOneAndUpdate - Error - ' + err);
                        return callback(err)
                    }
                    if(response) {
                        return callback(null, ResponseCode.Success)
                    }
                })
            }
            if(!userDb) {
                logger.warn('verifyEmail - Organization.findOne - No Organization found');
                return callback(null, ResponseCode.UserNotExist)
            }
        })
    }
    if(role === "ROLE_STUDENT") {
        Student.findOne({$and: [{_id:entityId},{emailVerificationToken:verificationtoken},{expireEmailVerificationToken:{$gt:Date.now()}}]}).exec((err, userDb) => {
            if(err) {
                logger.error('verifyEmail - Student.findOne - Error - ' + err);
                return callback(err)
            }
            if(userDb) {
                if(userDb.isEmailVerified === true && userDb.emailVerificationToken === null ) {
                    return callback(null, ResponseCode.Success)
                }
                Student.findOneAndUpdate({$and: [{_id:entityId},{emailVerificationToken:verificationtoken},{expireEmailVerificationToken:{$gt:Date.now()}}]}, {emailVerificationToken:null,expireEmailVerificationToken:null,isEmailVerified:true,}).exec((err, response) => {
                    if(err) {
                        logger.error('verifyEmail - Student.findOneAndUpdate - Error - ' + err);
                        return callback(err)
                    }
                    if(response) {
                        return callback(null, ResponseCode.Success)
                    }
                })
            }
            if(!userDb) {
                logger.warn('verifyEmail - Student.findOne - No Student found');
                return callback(null, ResponseCode.UserNotExist)
            }
        })
    }
}