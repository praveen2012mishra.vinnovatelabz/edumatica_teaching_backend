const StudentContent = require('../models/studentContent.model');
const Student = require('../models/student.model');
const Batch = require('../models/batch.model');
const Content = require('../models/content.model');
const logger = require('../utils/logger.utils');
const { ResponseCode } = require('../utils/response.utils');

exports.studentContent_create = (ownerId, userId, studentContentReq, callback)=> {
    Batch.findOne({$and: [{ownerId : ownerId}, 
        {batchfriendlyname: studentContentReq.batchfriendlyname}] })
        .exec( function (err, batch) {
        if (err) {
            logger.error('studentContent_create - Batch.findOne - Error - ' + err);
            return callback(err);
        }
        if(batch == null) {
            logger.warn('studentContent_create - entityId:' + entityId + ', batchfriendlyname:' + batchfriendlyname
                                    + ' No batch found');
            return callback(null, ResponseCode.BatchNotFound);
        }
        else{            
            studentsArray = batch.students;
            if(studentsArray.length > 0){
                Content.findOne({$and: [{ownerId : ownerId},
                    {contentname : studentContentReq.contentname}]})
                    .exec( function (err, content) {
                    if (err){
                        logger.error('studentContent_create - Content.findOne - Error - ' + err);
                        return callback(err);
                    }
                    if(content == null)  {
                        logger.warn('studentContent_create - ownerId:' + ownerId
                            + ', contentname:' + studentContentReq.contentname
                            + ', No content found');
                        return callback(null, ResponseCode.ContentNotFound); 
                    }
                    studentsArray.forEach(student => {
                        StudentContent.findOne({$and: [{ownerId: ownerId},
                            {student :student}, {content :content._id}]})
                            .exec( function (err, studentContent) {
                            if (err) {
                                logger.error('studentContent_create - StudentContent.findOne - Error - ' + err);
                                return callback(err);
                            }
                            if(studentContent == null) {
                                let studentContent = new StudentContent({
                                        ownerId: ownerId,
                                        student: student,
                                        content: content._id,
                                        batch: batch._id,
                                        duration: studentContentReq.duration,
                                        isFinished: false,
                                        total: content.questions.length, 
                                        attempted: 0,
                                        correct: 0,
                                        marks: 0,
                                        answers: null,
                                        status: 1,
                                        updatedon: new Date(),
                                        updatedby: userId
                                });
                                studentContent.save(function (err) {
                                    if (err) {
                                        logger.error('studentContent_create - studentContent.save - Error - ' + err);
                                        return callback(err);
                                    } 
                                })
                            }
                        })
                    });
                    logger.info('studentContent_create - ownerId:' + ownerId
                    + ', contentname:' + studentContentReq.contentname
                    + ', Assigned Content(s) created successfully');
                    callback(null, ResponseCode.Success); 
                });
            }
            else{
                logger.warn('studentContent_create - ownerId:' + ownerId
                            + ', contentname:' + studentContentReq.contentname
                            + ', No student found');
                return callback(null, ResponseCode.StudentNotFound);
            }
        }
    });
}

exports.studentContent_allContents = (ownerId, callback)=> {
    Student.findOne({ownerId : ownerId}).select({"_id" : 1}).exec((err,studentId)=>{
        if(err){
            logger.error('studentContent_allContents - Student.findOne - Error - ' + err);
            return callback(err);
        }
        if(studentId != null){            
            StudentContent.find({$and: [{ownerId: ownerId},
                        {student :studentId}]})
                        .populate('content')
                        .select({"_id": 0,"__v":0})
                        .exec( function (err, studentContents) {
                if (err) {
                    logger.error('studentContent_allContents - Student.findOne - Error - ' + err);
                    return callback(err);
                }
                logger.info('studentContent_allContents - ownerId:' + ownerId
                + ', Assigned Content(s) by student retrieved successfully');
                callback(null, ResponseCode.Success, studentContents);
            });            
        }
    });
};

exports.studentContent_details = (ownerId, contentname, callback)=> {
    Student.findOne({ownerId : ownerId})
        .select({"_id" : 1}).exec((err,studentId)=>{
        if(err){
            logger.error('studentContent_details - Student.findOne - Error - ' + err);
            return callback(err);
        }
        if(studentId != null){             
            Content.findOne({$and: [{ownerId : ownerId},
                {contentname : contentname}]})
                .populate('questions')
                .exec( function (err, content) {
                if (err){
                    logger.error('studentContent_details - Content.findOne - Error - ' + err);
                    return callback(err);
                }
                if(content == null)  {
                    logger.warn('studentContent_details - ownerId:' + ownerId
                        + ', contentname:' + contentname
                        + ', No content found');
                    return callback(null, ResponseCode.ContentNotFound);     
                }
                StudentContent.findOne({$and: [{ownerId: ownerId},
                            {student :studentId}, {content :content._id}]})
                            .select({"_id": 0,"__v":0})
                            .exec( function (err, studentContent) {
                    if (err) {
                        logger.error('studentContent_details - StudentContent.findOne - Error - ' + err);
                        return callback(err);
                    }
                    if(studentContent == null) {
                        logger.warn('studentContent_details - ownerId:' + ownerId
                        + ', contentname:' + contentname
                        + ', No quiz found for student');
                    return callback(null, ResponseCode.ContentNotFound)
                    }
                    logger.info('studentContent_details - ownerId:' + ownerId
                    + ', contentname:' + contentname
                    + ', Content retrieved successfully');
                    callback(null, ResponseCode.Success, content);
                });
            });
        }
        else{
            logger.warn('studentContent_details - ownerId:' + ownerId
                        + ', contentname:' + contentname
                        + ', No student found');
            return callback(null, ResponseCode.StudentNotFound);
        }
    });
};

exports.studentContent_saveAnswers = (ownerId, userId, studentContentReq, callback)=> {
    Student.findOne({ownerId : ownerId}).select({"_id" : 1}).exec((err,studentId)=>{
        if(err){
            logger.error('studentContent_saveAnswers - Student.findOne - Error - ' + err);
            return callback(err);
        }
        if(studentId != null){           
            Content.findOne({$and: [{ownerId : ownerId},
                {contentname : studentContentReq.contentname}]})
                .populate('questions').exec( function (err, content) {
                if (err){
                    logger.error('studentContent_saveAnswers - Content.findOne - Error - ' + err);
                    return callback(err);
                }
                if(content == null)  {
                    logger.warn('studentContent_saveAnswers - ownerId:' + ownerId
                        + ', contentname:' + studentContentReq.contentname
                        + ', No content found');
                    return callback(null, ResponseCode.ContentNotFound); 
                }
                StudentContent.findOne({$and: [{ownerId: ownerId},
                            {student :studentId}, {content :content._id}]})
                            .populate('student')
                            .exec( function (err, studentContent) {
                    if (err) {
                        logger.error('studentContent_saveAnswers - StudentContent.findOne - Error - ' + err);
                        return callback(err);       
                    }
                    if(studentContent == null) {
                        logger.warn('studentContent_saveAnswers - ownerId:' + ownerId
                        + ', contentname:' + studentContentReq.contentname
                        + ', No quiz found');
                        return callback(null, ResponseCode.ContentNotFound);
                    }
                    if(studentContent.isFinished == true) {
                        logger.info('studentContent_saveAnswers - ownerId:' + ownerId
                        + ', contentname:' + studentContentReq.contentname
                        + ', Assigned Content(s) retrieved successfully');
                        return callback(null, ResponseCode.Success, studentContent);
                    }
                    var answers = studentContentReq.answers;
                    var quizAnswers = [];
                    var correct = 0;
                    answers.forEach(answer => {
                        var isCorrect = false;
                        for (let i = 0; i < content.questions.length; i++) {
                            if(content.questions[i].serialNo == answer.serialNo
                                && content.questions[i].answer == answer.selectedOption){
                                    isCorrect = true;
                                    break; 
                            }                             
                        }                        
                        if(isCorrect == true) correct++; 
                        let quizAnswer = {
                            serialNo: answer.serialNo,
                            selectedOption: answer.selectedOption,
                            isCorrect: isCorrect
                        };
                        quizAnswers.push(quizAnswer);
                    })
                    let studentContentToUpdate = {};
                    studentContentToUpdate.total = content.questions.length;
                    studentContentToUpdate.attempted = answers.length;
                    studentContentToUpdate.correct = correct;
                    studentContentToUpdate.marks = correct * content.marks;
                    studentContentToUpdate.isFinished = true;
                    studentContentToUpdate.answers = quizAnswers;
                    studentContentToUpdate.updatedon = new Date();
                    studentContentToUpdate.updatedby = userId;   
                    studentContentToUpdate.student = studentContent.student;       
                    StudentContent.findOneAndUpdate({"_id" : studentContent._id},
                            {$set: studentContentToUpdate},(err,data)=>{
                        if(err){
                            logger.error('studentContent_saveAnswers - StudentContent.findOneAndUpdate - Error - ' + err);
                            return callback(err);
                        }
                        logger.info('studentContent_saveAnswers - ownerId:' + ownerId
                        + ', contentname:' + studentContentReq.contentname
                        + ', Content updated successfully');
                        callback(null, ResponseCode.Success, studentContentToUpdate);
                    })          
                });
            });
        
        }
        else{
            logger.warn('studentContent_saveAnswers - ownerId:' + ownerId
                        + ', contentname:' + studentContentReq.contentname
                        + ', No student found');
            return callback(null, ResponseCode.StudentNotFound); 
        }
    });
};

exports.studentContent_viewResults = (ownerId, studentContentReq, callback)=> {
    Batch.findOne({$and: [{ownerId : ownerId}, 
        {batchfriendlyname: studentContentReq.batchfriendlyname}] })
        .exec( function (err, batch) {
        if (err) {
            logger.error('studentContent_viewResults - Batch.findOne - Error - ' + err);
            return callback(err);
        }
        if(batch == null) {
            logger.warn('studentContent_viewResults - entityId:' + entityId + ', batchfriendlyname:' + batchfriendlyname
                                    + ' No batch found');
            return callback(null, ResponseCode.BatchNotFound);
        }
        else{
            Content.findOne({$and: [{ownerId : ownerId},
                {contentname : studentContentReq.contentname}]}).populate('questions')
                .exec( function (err, content) {
                if (err){
                    logger.error('studentContent_viewResults - Content.findOne - Error - ' + err);
                    return callback(err);
                }
                if(content == null)  {
                    logger.warn('studentContent_viewResults - ownerId:' + ownerId
                        + ', contentname:' + studentContentReq.contentname
                        + ', No content found');
                    return callback(null, ResponseCode.ContentNotFound); 
                }
                
                StudentContent.find({$and: [{ownerId: ownerId},
                    {batch: batch._id}, {content :content._id}]})
                    .select({"_id": 0,"__v":0})
                    .populate('student')
                    .exec( function (err, studentContents) {
                    if (err) {
                        logger.error('studentContent_viewResults - StudentContent.find - Error - ' + err);
                        return callback(err);
                    }
                    if(studentContents == null) {
                        logger.warn('studentContent_viewResults - ownerId:' + ownerId
                        + ', contentname:' + studentContentReq.contentname
                        + ', No content found for student');
                        return callback(null, ResponseCode.ContentNotFound)
                    }
                    logger.info('studentContent_viewResults - ownerId:' + ownerId
                        + ', contentname:' + studentContentReq.contentname
                        + ', Assigned Content(s) by tutor retrieved successfully');
                    callback(null, ResponseCode.Success, studentContents);
                });
            }); 
        }
    });
};