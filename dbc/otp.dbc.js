const Otp = require('../models/otp.model');
const Sms = require('../models/sms.model');
const User = require('../models/user.model');
const Role = require('../models/role.model');
const Student = require('../models/student.model');
const Parent = require('../models/parent.model');
const Tutor = require('../models/tutor.model');
const logger = require('../utils/logger.utils');
const SmsFactory = require('../utils/smsFactory.utils');
const SmsProvider = SmsFactory.create(process.env.SMSPROVIDER || 'Pinpoint');
const cache = require('memory-cache');
const { ResponseCode } = require('../utils/response.utils');
const otpExpiryTime = 30 * 60 * 1000 // 30 mins
const otpSentMaxCount = 10
const otpFailedMaxCount = 3;

function getNewOtp() {
    var digits = '0123456789'; 
    let otp = ''; 
    for (let i = 0; i < 6; i++ ) { 
        otp += digits[Math.floor(Math.random() * 10)]; 
    }
    return otp;
  }


exports.generateOTP = (mobileNo, callback)=> {   
    var otpCache = cache.get(mobileNo);
    if(otpCache == null){        
        Otp.findOne({mobileNo : mobileNo})
            .exec( function (err, otpData) {
            if (err) {
                logger.error('generateOTP - Otp.findOne - Error - ' + err);
                return callback(err);
            }
            var currentTime = new Date();
            if(otpData == null) {
                otpData = new Otp({
                    mobileNo: mobileNo,
                    otp: getNewOtp(),
                    sentCount: 1,
                    generatedOn: new Date(),
                    failedCount: 0
                })   
            }
            else if(!otpData.generatedOn
                || currentTime.getTime() - otpData.generatedOn.getTime() > otpExpiryTime) {
                otpData.otp = getNewOtp();
                otpData.generatedOn = new Date();
                otpData.sentCount = 1;
                otpData.failedCount = 0;
            }
            else if(otpData.sentCount < otpSentMaxCount){
                    otpData.sentCount = otpData.sentCount + 1;
            }
            else {
                logger.warn('generateOTP - mobileNo:' + mobileNo
                + ', Max Otp sent limit reached');
                return callback(null, ResponseCode.MaxOtpSentCountExceeded);
            }
            otpData.save(function (err) {
                if (err) {
                    logger.error('generateOTP - otpData.save -  Error - ' + err);
                    return callback(err);
                }
                cache.put(mobileNo, otpData, otpExpiryTime);
                if(process.env.GLOBALNOTIFICATIONSWITCH === "true" && process.env.SENDSMS === "true") {
                    const args = {type: "OTP" ,otp: otpData.otp}
                    SmsProvider.sendSms(mobileNo, args, (err, code, data) => {
                        if(err){
                            logger.error('generateOTP - sendSms - Error - ' + err);
                            return callback(err);
                        }
                        if(code === ResponseCode.Success) {
                            logger.info('generateOTP - mobileNo:' + mobileNo
                                + ', SMS sent successfully');
                                return callback(null, ResponseCode.Success);
                        }
                        else{
                            logger.warn('generateOTP - mobileNo:' + mobileNo
                            + ', SMS sending failed');
                            return callback(null, ResponseCode.SmsSendingFailed);
                        }
                    })
                    // SmsProvider.sendTransactionSms(mobileNo, 
                    //     'Your verification OTP code is ' + otpData.otp, (err, data)=>{
                    //     if(err){
                    //         logger.error('generateOTP - sendTransactionSms - Error - ' + err);
                    //         return callback(err);
                    //     }
                    //     if(code === ResponseCode.Success){
                    //         sms = new sms({ 
                    //             mobileNo: mobileNo, 
                    //             otp: otpCache.otp, 
                    //             message: 'Your verification OTP code is ' + otpCache.otp,
                    //             sentOn: new Date(),
                    //             status: data
                    //             });
                    //         sms.save(function (err) {
                    //             if (err) {
                    //                 logger.error('generateOTP - sms.save -  Error - ' + err);
                    //                 return callback(err);
                    //             }
                    //             logger.info('generateOTP - mobileNo:' + mobileNo
                    //             + ', SMS sent successfully');
                    //             return callback(null, ResponseCode.Success);
                    //         });
                    //     }
                    //     else{
                    //         logger.warn('generateOTP - mobileNo:' + mobileNo
                    //         + ', SMS sending failed');
                    //         return callback(null, ResponseCode.SmsSendingFailed);
                    //     }
                    // });
                }
                else{
                    logger.info('generateOTP - mobileNo:' + mobileNo
                    + ', Otp saved successfully');
                    return callback(null, ResponseCode.Success, otpData.otp);
                }
            })
        })
    }
    else{        
        var currentTime = new Date();
        if(!otpCache.generatedOn
            || currentTime.getTime() - otpCache.generatedOn.getTime() > otpExpiryTime) {
                otpCache.otp = getNewOtp();
                otpCache.sentCount = 1;
                otpCache.generatedOn = new Date();
                otpCache.failedCount = 0;
                otpCache.lastFailedOn = null;
        }
        else if(otpCache.sentCount < otpSentMaxCount){
            otpCache.sentCount = otpCache.sentCount + 1;
        }
        else {
            logger.warn('generateOTP - mobileNo:' + mobileNo
            + ', Max Otp sent limit reached');
            return callback(null, ResponseCode.MaxOtpSentCountExceeded);
        }
        Otp.findOneAndUpdate({mobileNo:otpCache.mobileNo},{$set: otpCache},
            function (err) {
            if (err) {
                logger.error('generateOTP - otpData.save -  Error - ' + err);
                return callback(err);
            }
            cache.put(mobileNo, otpCache, otpExpiryTime);
            if(process.env.GLOBALNOTIFICATIONSWITCH === "true" && process.env.SENDSMS === "true"){
                const args = {type: "OTP" ,otp: otpCache.otp}
                SmsProvider.sendSms(mobileNo, args, (err, code, data) => {
                    if(err){
                        logger.error('generateOTP - sendSms - Error - ' + err);
                        return callback(err);
                    }
                    if(code === ResponseCode.Success) {
                        logger.info('generateOTP - mobileNo:' + mobileNo
                            + ', SMS sent successfully');
                            return callback(null, ResponseCode.Success);
                    }
                    else{
                        logger.warn('generateOTP - mobileNo:' + mobileNo
                        + ', SMS sending failed');
                        return callback(null, ResponseCode.SmsSendingFailed);
                    }
                })
                // SmsProvider.sendTransactionSms(mobileNo, 
                //     'Your verification OTP code is ' + otpCache.otp, (err, code, data)=>{
                //     if(err){
                //         logger.error('generateOTP - sendTransactionSms - Error - ' + err);
                //         return callback(err);
                //     }
                //     if(code === ResponseCode.Success){
                //         sms = new sms({ 
                //             mobileNo: mobileNo, 
                //             otp: otpCache.otp, 
                //             message: 'Your verification OTP code is ' + otpCache.otp,
                //             sentOn: new Date(),
                //             status: data
                //             });
                //         sms.save(function (err) {
                //             if (err) {
                //                 logger.error('generateOTP - sms.save -  Error - ' + err);
                //                 return callback(err);
                //             }
                //             logger.info('generateOTP - mobileNo:' + mobileNo
                //             + ', SMS sent successfully');
                //             return callback(null, ResponseCode.Success);
                //         });
                //     }
                //     else{
                //         logger.warn('generateOTP - mobileNo:' + mobileNo
                //         + ', SMS sending failed');
                //         return callback(null, ResponseCode.SmsSendingFailed);
                //     }
                // });
            }
            else{
                logger.info('generateOTP - mobileNo:' + mobileNo
                + ', Otp saved successfully');
                return callback(null, ResponseCode.Success, otpCache.otp);
            }
        })
    }
}

exports.validateOTP = (ownerId, mobileNo, otp, userType, forgetPassword, callback) => {
    var otpObj = cache.get(mobileNo);
    if(otpObj == null){
        Otp.findOne({mobileNo : mobileNo}).exec( function (err, otpData) {
            if (err) {
                logger.error('validateOTP - Otp.findOne - Error - ' + err);
                return callback(err);
            }

            if(otpData == null) {
                logger.warn('validateOTP - mobileNo:' + mobileNo + ', Otp not found');
                callback(null, ResponseCode.OtpNotGenerated);
            } else {
                var currentTime = new Date();
                if(currentTime.getTime() - otpData.generatedOn.getTime() <= otpExpiryTime && 
                    otpData.failedCount < otpFailedMaxCount
                ) {
                    if(otpData.otp == otp) {
                        if(forgetPassword) {
                            User.findOne({mobile:mobileNo}).exec()
                            .then((user)=>{
                                if(user==null){
                                    logger.error('Forget Password. User with mobileNo:'+mobileNo+' not found.')
                                    callback(null,ResponseCode.UserNotExist)
                                }
                                else{
                                    if(user.owner.length>0){
                                        logger.debug('User with mobileNo : '+mobileNo)
                                        callback(null, ResponseCode.Success)

                                    }
                                    else{
                                        logger.error('User with mobileNo : '+mobileNo+ ' has no roles added.')
                                        callback(null, ResponseCode.RoleNotFound)
                                    }
                                }
                            })
                            .catch((err)=>{
                                logger.error('Error finding user. Error :'+err)
                                callback(err)
                            })
                        } else {
                            Role.findOne({name : userType}, async (err,role)=>{
                                if(err){
                                    logger.error('register - Role.findOne - Error - ' + err);
                                    callback(err);
                                }

                                if(role == null) {
                                    logger.warn('register - mobileNo:' + mobileNo + 'Role' + userType + ' Role Not Found');             
                                    return callback(null, ResponseCode.RoleNotFound);
                                } else {
                                    let userData;

                                    const Model = (userType == 'ROLE_STUDENT') ? Student : ((userType == 'ROLE_PARENT') ? Parent : Tutor);
                                    if(userType == 'ROLE_STUDENT' || userType == 'ROLE_PARENT' || userType == 'ROLE_TUTOR') {
                                        userData = await new Promise(resolve => {
                                            Model.findOne({$and:[{ ownerId:ownerId, mobileNo : mobileNo}]}, (err, data) => {
                                                if(err){
                                                    resolve({status: 'error', data: err})
                                                } else if(data === null) {
                                                    resolve({status: 'empty'})
                                                } else {
                                                    resolve({status: 'success'})
                                                }
                                            })
                                        });
                
                                        if(userData.status === 'error') {
                                            logger.error('register - ' + userType + '.findOne - Error - ' + userData.data);
                                            callback(userData.data);
                
                                        } else if(userData.status === 'empty' && userType != 'ROLE_TUTOR') {
                                            logger.warn('register - mobileNo:' + mobileNo + ' ' + userType +' Not Found');               
                                            const response = (userType == 'ROLE_STUDENT') ? ResponseCode.StudentNotFound : ResponseCode.ParentNotFound;
                                            return callback(null, response);
                                        }
                                    }

                                    if(userType == 'ROLE_ORGANIZATION' || (userType == 'ROLE_TUTOR' && userData.status === 'empty')) {
                                        const ownerIdUnavailalbe = await new Promise(resolve => {
                                            User.exists({'owner.ownerId': ownerId}, (err, existBool) => {
                                                if(err){
                                                    resolve({status: 'error', data: err})
                                                } else if(existBool) {
                                                    resolve({status: 'unavailalbe'})
                                                } else {
                                                    resolve({status: 'available'})
                                                }
                                            })
                                        })
                
                                        if(ownerIdUnavailalbe.status === 'error') {
                                            logger.error('register - User.exists - Error - ' + ownerIdUnavailalbe.data);
                                            callback(ownerIdUnavailalbe.data);
                
                                        } else if(ownerIdUnavailalbe.status === 'unavailalbe') {
                                            logger.warn('register - User.exists OwnerId ' + ownerId + ' unavailable');               
                                            callback(null,ResponseCode.OrganizationAlreadyRegistered)
                                        }
                                    }

                                    User.findOne({mobile: mobileNo}, async (err, user) => {
                                        if(err) {
                                            logger.error('register - User.findOne - Error - ' + err);
                                            callback(err);
                                        }

                                        if(user == null) {
                                            logger.info('validateOTP - mobileNo:' + mobileNo+ ', userType:' + userType + ', ownerId ' + ownerId + ' Otp validated successfully');
                                            return callback(null, ResponseCode.Success);
                                        } else {
                                            if(!(user.owner.some(elem => elem.ownerId === ownerId))) {
                                                if(userType === 'ROLE_ORGANIZATION') {
                                                    const owners = [...user.owner]
                                                        const everyRoles = owners.map(elem => {
                                                        return(elem.roles.map(eleme => {
                                                            return eleme['role'].toString()
                                                        }))
                                                    })

                                                    Role.findOne({name : 'ROLE_TUTOR'},(err,tutorRole)=>{
                                                        if(everyRoles.flat().includes(role._id.toString()) || everyRoles.flat().includes(tutorRole._id.toString())) {
                                                            return callback(null, ResponseCode.RegistrationRestriction)
                                                        } else {
                                                            logger.info('validateOTP - mobileNo:' + mobileNo+ ', userType:' + userType + ', ownerId ' + ownerId + ' Otp validated successfully');
                                                            return callback(null, ResponseCode.UserRegistered);
                                                        }
                                                    })
                                                } else if(userType == 'ROLE_TUTOR' && userData.status === 'empty') {
                                                    const owners = [...user.owner]
                                                        const everyRoles = owners.map(elem => {
                                                        return(elem.roles.map(eleme => {
                                                            return eleme['role'].toString()
                                                        }))
                                                    })

                                                    Role.findOne({name : 'ROLE_ORGANIZATION'},(err,orgRole)=>{
                                                        if(everyRoles.flat().includes(role._id.toString()) || everyRoles.flat().includes(orgRole._id.toString())) {
                                                            return callback(null, ResponseCode.RegistrationRestriction)
                                                        } else {
                                                            logger.info('validateOTP - mobileNo:' + mobileNo+ ', userType:' + userType + ', ownerId ' + ownerId + ' Otp validated successfully');
                                                            return callback(null, ResponseCode.UserRegistered);
                                                        }
                                                    })
                                                } else {
                                                    logger.info('validateOTP - mobileNo:' + mobileNo+ ', userType:' + userType + ', ownerId ' + ownerId + ' Otp validated successfully');
                                                    return callback(null, ResponseCode.UserRegistered);
                                                }
                                            } else {
                                                const ownerTbu = user.owner.find(elem => elem.ownerId === ownerId)
                                                const rolesOwnerTbu = ownerTbu.roles.map((item) =>  {return item['role'].toString()});
                                                
                                                if(rolesOwnerTbu.includes(role._id)) {
                                                    logger.warn('register - mobileNo:' + mobileNo + ' ' + userType + ' already registered');
                                
                                                    let response;
                                                    switch(userType) {
                                                        case 'ROLE_ORGANIZATION':
                                                            response = ResponseCode.OrganizationAlreadyRegistered;
                                                            break;
                                                        case 'ROLE_TUTOR':
                                                            response = ResponseCode.TutorAlreadyRegistered;
                                                            break;
                                                        case 'ROLE_STUDENT':
                                                            response = ResponseCode.StudentAlreadyRegistered;
                                                            break;
                                                        case 'ROLE_PARENT':
                                                            response = ResponseCode.ParentAlreadyRegistered;
                                                            break;
                                                    } 

                                                    return callback(null, response);
                                                } else {
                                                    Role.find({ _id : {$in: rolesOwnerTbu}}, (err, roldata) => {
                                                        if(err) {
                                                            logger.error('register - user.findOne - Error - ' + err);
                                                            return callback(err);
                                                        } else if(!roldata[0]){
                                                            logger.error('register - user.findOne - Error - ');
                                                            return callback('emptyRole');
                                                        } else {
                                                            let allRolesName = roldata.map(el => el.name)
                        
                                                            if(userType == 'ROLE_ORGANIZATION') {
                                                                return callback(null, ResponseCode.RegistrationRestriction);
                                                            } else if(userType == 'ROLE_TUTOR' && (allRolesName.includes('ROLE_ORGANIZATION') || allRolesName.includes('ROLE_STUDENT') || allRolesName.includes('ROLE_ORGANIZATION_TUTOR'))) {
                                                                return callback(null, ResponseCode.RegistrationRestriction);
                                                            } else if(userType == 'ROLE_STUDENT') {
                                                                return callback(null, ResponseCode.RegistrationRestriction);
                                                            } else if(userType == 'ROLE_PARENT' && (allRolesName.includes('ROLE_ORGANIZATION') || allRolesName.includes('ROLE_STUDENT'))) {
                                                                return callback(null, ResponseCode.RegistrationRestriction);
                                                            } else if(userType == 'ROLE_ORGANIZATION_TUTOR' && (allRolesName.includes('ROLE_ORGANIZATION') || allRolesName.includes('ROLE_STUDENT') || allRolesName.includes('ROLE_TUTOR'))) {
                                                                return callback(null, ResponseCode.RegistrationRestriction);
                                                            }
        
                                                            logger.info('validateOTP - mobileNo:' + mobileNo+ ', userType:' + userType + ', ownerId ' + ownerId + ' Otp validated successfully');
                                                            return callback(null, ResponseCode.UserRegistered);
                                                        }
                                                    })
                                                }
                                            }
                                        }
                                    })
                                }
                            })
                        }
                    } else {
                        otpData.failedCount = otpData.failedCount + 1;
                        otpData.save(function (err) {
                            if (err) {
                                logger.error('generateOTP - otpData.save -  Error - ' + err);
                                return callback(err);
                            }
                            cache.put(mobileNo, otpData, otpExpiryTime);
                            logger.warn('validateOTP - mobileNo:' + mobileNo + ' Incorrect Otp entered');
                            return callback(null, ResponseCode.IncorrectOtp);
                        })
                    }
                } else{
                    logger.warn('validateOTP - mobileNo:' + mobileNo + ' Max Otp attempt limit reached');
                    return callback(null, ResponseCode.MaxOtpAttemptCountExceeded);
                }
            }
        })
    } else {
        var currentTime = new Date();
        if(currentTime.getTime() - otpObj.generatedOn.getTime() <= otpExpiryTime && 
            otpObj.failedCount < otpFailedMaxCount
        ) {
            if(otpObj.otp == otp) {
                if(forgetPassword) {
                    User.findOne({mobile:mobileNo}).exec()
                    .then((user)=>{
                        if(user==null){
                            logger.error('Forget Password. User with mobileNo:'+mobileNo+' not found.')
                            callback(null,ResponseCode.UserNotExist)
                        }
                        else{
                            if(user.owner.length>0){
                                logger.debug('User with mobileNo : '+mobileNo)
                                callback(null, ResponseCode.Success)
                            }
                            else{
                                logger.error('User with mobileNo : '+mobileNo+ ' has no roles added.')
                                callback(null, ResponseCode.RoleNotFound)
                            }
                        }
                    })
                } else {
                    Role.findOne({name : userType}, async (err,role)=>{
                        if(err){
                            logger.error('register - Role.findOne - Error - ' + err);
                            callback(err);
                        }

                        if(role == null) {
                            logger.warn('register - mobileNo:' + mobileNo + 'Role' + userType + ' Role Not Found');             
                            return callback(null, ResponseCode.RoleNotFound);
                        } else {
                            let userData;

                            const Model = (userType == 'ROLE_STUDENT') ? Student : ((userType == 'ROLE_PARENT') ? Parent : Tutor);
                            if(userType == 'ROLE_STUDENT' || userType == 'ROLE_PARENT' || userType == 'ROLE_TUTOR') {
                                userData = await new Promise(resolve => {
                                    Model.findOne({$and:[{ ownerId:ownerId, mobileNo : mobileNo}]}, (err, data) => {
                                        if(err){
                                            resolve({status: 'error', data: err})
                                        } else if(data === null) {
                                            resolve({status: 'empty'})
                                        } else {
                                            resolve({status: 'success'})
                                        }
                                    })
                                });
        
                                if(userData.status === 'error') {
                                    logger.error('register - ' + userType + '.findOne - Error - ' + userData.data);
                                    callback(userData.data);
        
                                } else if(userData.status === 'empty' && userType != 'ROLE_TUTOR') {
                                    logger.warn('register - mobileNo:' + mobileNo + ' ' + userType +' Not Found');               
                                    const response = (userType == 'ROLE_STUDENT') ? ResponseCode.StudentNotFound : ResponseCode.ParentNotFound;
                                    return callback(null, response);
                                }
                            }

                            if(userType == 'ROLE_ORGANIZATION' || (userType == 'ROLE_TUTOR' && userData.status === 'empty')) {
                                const ownerIdUnavailalbe = await new Promise(resolve => {
                                    User.exists({'owner.ownerId': ownerId}, (err, existBool) => {
                                        if(err){
                                            resolve({status: 'error', data: err})
                                        } else if(existBool) {
                                            resolve({status: 'unavailalbe'})
                                        } else {
                                            resolve({status: 'available'})
                                        }
                                    })
                                })
        
                                if(ownerIdUnavailalbe.status === 'error') {
                                    logger.error('register - User.exists - Error - ' + ownerIdUnavailalbe.data);
                                    callback(ownerIdUnavailalbe.data);
        
                                } else if(ownerIdUnavailalbe.status === 'unavailalbe') {
                                    logger.warn('register - User.exists OwnerId ' + ownerId + ' unavailable');               
                                    callback(null,ResponseCode.OrganizationAlreadyRegistered)
                                }
                            }

                            User.findOne({mobile: mobileNo}, async (err, user) => {
                                if(err) {
                                    logger.error('register - User.findOne - Error - ' + err);
                                    callback(err);
                                }

                                if(user == null) {
                                    logger.info('validateOTP - mobileNo:' + mobileNo+ ', userType:' + userType + ', ownerId ' + ownerId + ' Otp validated successfully');
                                    return callback(null, ResponseCode.Success);
                                } else {
                                    if(!(user.owner.some(elem => elem.ownerId === ownerId))) {
                                        if(userType === 'ROLE_ORGANIZATION') {
                                            const owners = [...user.owner]
                                                const everyRoles = owners.map(elem => {
                                                return(elem.roles.map(eleme => {
                                                    return eleme['role'].toString()
                                                }))
                                            })

                                            Role.findOne({name : 'ROLE_TUTOR'},(err,tutorRole)=>{
                                                if(everyRoles.flat().includes(role._id.toString()) || everyRoles.flat().includes(tutorRole._id.toString())) {
                                                    return callback(null, ResponseCode.RegistrationRestriction)
                                                } else {
                                                    logger.info('validateOTP - mobileNo:' + mobileNo+ ', userType:' + userType + ', ownerId ' + ownerId + ' Otp validated successfully');
                                                    return callback(null, ResponseCode.UserRegistered);
                                                }
                                            })
                                        } else if(userType == 'ROLE_TUTOR' && userData.status === 'empty') {
                                            const owners = [...user.owner]
                                                const everyRoles = owners.map(elem => {
                                                return(elem.roles.map(eleme => {
                                                    return eleme['role'].toString()
                                                }))
                                            })

                                            Role.findOne({name : 'ROLE_ORGANIZATION'},(err,orgRole)=>{
                                                if(everyRoles.flat().includes(role._id.toString()) || everyRoles.flat().includes(orgRole._id.toString())) {
                                                    return callback(null, ResponseCode.RegistrationRestriction)
                                                } else {
                                                    logger.info('validateOTP - mobileNo:' + mobileNo+ ', userType:' + userType + ', ownerId ' + ownerId + ' Otp validated successfully');
                                                    return callback(null, ResponseCode.UserRegistered);
                                                }
                                            })
                                        } else {
                                            logger.info('validateOTP - mobileNo:' + mobileNo+ ', userType:' + userType + ', ownerId ' + ownerId + ' Otp validated successfully');
                                            return callback(null, ResponseCode.UserRegistered);
                                        }
                                    } else {
                                        const ownerTbu = user.owner.find(elem => elem.ownerId === ownerId)
                                        const rolesOwnerTbu = ownerTbu.roles.map((item) =>  {return item['role'].toString()});
                                        
                                        if(rolesOwnerTbu.includes(role._id)) {
                                            logger.warn('register - mobileNo:' + mobileNo + ' ' + userType + ' already registered');
                        
                                            let response;
                                            switch(userType) {
                                                case 'ROLE_ORGANIZATION':
                                                    response = ResponseCode.OrganizationAlreadyRegistered;
                                                    break;
                                                case 'ROLE_TUTOR':
                                                    response = ResponseCode.TutorAlreadyRegistered;
                                                    break;
                                                case 'ROLE_STUDENT':
                                                    response = ResponseCode.StudentAlreadyRegistered;
                                                    break;
                                                case 'ROLE_PARENT':
                                                    response = ResponseCode.ParentAlreadyRegistered;
                                                    break;
                                            } 

                                            return callback(null, response);
                                        } else {
                                            Role.find({ _id : {$in: rolesOwnerTbu}}, (err, roldata) => {
                                                if(err) {
                                                    logger.error('register - user.findOne - Error - ' + err);
                                                    return callback(err);
                                                } else if(!roldata[0]){
                                                    logger.error('register - user.findOne - Error - ');
                                                    return callback('emptyRole');
                                                } else {
                                                    let allRolesName = roldata.map(el => el.name)
                
                                                    if(userType == 'ROLE_ORGANIZATION') {
                                                        return callback(null, ResponseCode.RegistrationRestriction);
                                                    } else if(userType == 'ROLE_TUTOR' && (allRolesName.includes('ROLE_ORGANIZATION') || allRolesName.includes('ROLE_STUDENT') || allRolesName.includes('ROLE_ORGANIZATION_TUTOR'))) {
                                                        return callback(null, ResponseCode.RegistrationRestriction);
                                                    } else if(userType == 'ROLE_STUDENT') {
                                                        return callback(null, ResponseCode.RegistrationRestriction);
                                                    } else if(userType == 'ROLE_PARENT' && (allRolesName.includes('ROLE_ORGANIZATION') || allRolesName.includes('ROLE_STUDENT'))) {
                                                        return callback(null, ResponseCode.RegistrationRestriction);
                                                    } else if(userType == 'ROLE_ORGANIZATION_TUTOR' && (allRolesName.includes('ROLE_ORGANIZATION') || allRolesName.includes('ROLE_STUDENT') || allRolesName.includes('ROLE_TUTOR'))) {
                                                        return callback(null, ResponseCode.RegistrationRestriction);
                                                    }

                                                    logger.info('validateOTP - mobileNo:' + mobileNo+ ', userType:' + userType + ', ownerId ' + ownerId + ' Otp validated successfully');
                                                    return callback(null, ResponseCode.UserRegistered);
                                                }
                                            })
                                        }
                                    }
                                }
                            })
                        }
                    })
                }
            } else {
                otpObj.failedCount = otpObj.failedCount + 1;
                Otp.findOneAndUpdate({mobileNo : otpObj.mobileNo},
                    {$set: otpObj},function (err) {
                    if (err) {
                        logger.error('validateOTP - Otp.findOneAndUpdate -  Error - ' + err);
                        return callback(err);
                    }
                    cache.put(mobileNo, otpObj, otpExpiryTime);
                    logger.warn('validateOTP - mobileNo:' + mobileNo
                    + ' Incorrect Otp entered');
                    return callback(null, ResponseCode.IncorrectOtp);
                })
            }
        } else{
            logger.warn('validateOTP - mobileNo:' + mobileNo + ' Max Otp attempt limit reached');
            return callback(null, ResponseCode.MaxOtpAttemptCountExceeded);
        }
    }
}

exports.validateForForgetPwd = (mobileNo, otp, callback) => {
    var otpObj = cache.get(mobileNo);
    if(otpObj) {
        var currentTime = new Date();
        if(currentTime.getTime() - otpObj.generatedOn.getTime() <= otpExpiryTime
            && otpObj.failedCount < otpFailedMaxCount) {
            if(otpObj.otp == otp) {
                User.findOne({mobile: mobileNo}, (err, user) => {
                    if(err) {
                        logger.error('register - User.findOne - Error - ' + err);
                        callback(err);
                    }
                    if(user == null) { 
                        logger.warn('validateForForgetPwd - mobileNo:' + mobileNo + ' User doesn\'t exist');
                        return callback(null, ResponseCode.UserNotExist);
                    } else {
                        logger.info('validateForForgetPwd - mobileNo:' + mobileNo);
                        return callback(null, ResponseCode.Success);
                    }
                }) 
            } else {
                otpObj.failedCount = otpObj.failedCount + 1;
                Otp.findOneAndUpdate({mobileNo : otpObj.mobileNo},
                    {$set: otpObj},function (err) {
                    if (err) {
                        logger.error('validateForForgetPwd - Otp.findOneAndUpdate -  Error - ' + err);
                        return callback(err);
                    }
                    cache.put(mobileNo, otpObj, otpExpiryTime);
                    logger.warn('validateForForgetPwd - mobileNo:' + mobileNo
                    + ' Incorrect Otp entered');
                    return callback(null, ResponseCode.IncorrectOtp);
                })
            }
        } else {
            logger.warn('validateForForgetPwd - mobileNo:' + mobileNo + ' Max Otp attempt limit reached');
            return callback(null, ResponseCode.MaxOtpAttemptCountExceeded);
        }
    } else {
        Otp.findOne({mobileNo : mobileNo}).exec( function (err, otpData) {
            if (err) {
                logger.error('validateOTP - Otp.findOne - Error - ' + err);
                return callback(err);
            }
            if(otpData != null){
                var currentTime = new Date();
                if(currentTime.getTime() - otpData.generatedOn.getTime() <= otpExpiryTime
                    && otpData.failedCount < otpFailedMaxCount){
                    if(otpData.otp == otp){
                        User.findOne({mobile: mobileNo}, (err, user) => {
                            if(err) {
                                logger.error('register - User.findOne - Error - ' + err);
                                callback(err);
                            }
                            if(user == null) { 
                                logger.warn('validateForForgetPwd - mobileNo:' + mobileNo + ' User doesn\'t exist');
                                return callback(null, ResponseCode.UserNotExist);
                            } else {
                                logger.info('validateForForgetPwd - mobileNo:' + mobileNo);
                                return callback(null, ResponseCode.Success);
                            }
                        }) 
                    } else {
                        otpObj.failedCount = otpObj.failedCount + 1;
                        Otp.findOneAndUpdate({mobileNo : otpObj.mobileNo},
                            {$set: otpObj},function (err) {
                            if (err) {
                                logger.error('validateOTP - Otp.findOneAndUpdate -  Error - ' + err);
                                return callback(err);
                            }
                            cache.put(mobileNo, otpObj, otpExpiryTime);
                            logger.warn('validateOTP - mobileNo:' + mobileNo
                            + ' Incorrect Otp entered');
                            return callback(null, ResponseCode.IncorrectOtp);
                        })
                    }
                } else{
                    logger.warn('validateOTP - mobileNo:' + mobileNo + ' Max Otp attempt limit reached');
                    return callback(null, ResponseCode.MaxOtpAttemptCountExceeded);
                }
            } else {
                logger.warn('validateOTP - mobileNo:' + mobileNo + ', Otp not found');
                callback(null, ResponseCode.OtpNotGenerated);
            }
        })
    }
}