const Content = require('../models/content.model');
const QuizDetail = require('../models/quizDetail.model');
const ContentType_Quiz = 'quiz';
const Chapter = require('../models/chapter.model');
const logger = require('../utils/logger.utils');
const Tutor = require('../models/tutor.model');
const { deleteObject } = require('../utils/aws.utils');
const { ResponseCode } = require('../utils/response.utils');

exports.content_create = (ownerId, userId, contentReq, callback)=>{
    Chapter.findOne({$and: [{ownerId : ownerId}, {boardname : contentReq.boardname}, 
        {classname : contentReq.classname}, {subjectname: contentReq.subjectname},
         {chaptername: contentReq.chaptername}]})
         .exec( function (err, chapter) {
        if (err) {
            logger.error('content_create - Chapter.findOne - Error - ' + err);
            return callback(err);
        }
        if(chapter == null)  {
            let chapter = new Chapter(
                {
                    ownerId : ownerId,
                    boardname: contentReq.boardname,
                    classname: contentReq.classname,
                    subjectname: contentReq.subjectname,           
                    chaptername: contentReq.chaptername,
                    status: 1,
                    updatedon: new Date(),
                    updatedby: userId
                }
            );
            chapter.save(function (err) {
                if (err) {
                    logger.error('content_create - chapter.save - Error - ' + err);
                    return callback(err);
                }
                logger.info('content_create - ownerId:' + ownerId
                + ', boardname:' + contentReq.boardname + ', classname:' + contentReq.classname
                + ', subjectname:' + contentReq.subjectname + ', chaptername:' + contentReq.chaptername
                + ', Chapter saved successfully');                                            
                Content.findOne({$and: [{ownerId : ownerId},
                    {contentname : contentReq.contentname}]})
                    .exec( function (err, content) {
                     if (err){
                        logger.error('content_create - Content.findOne - Error - ' + err);
                        return callback(err);
                     }
                     if(content == null) {
                        if( contentReq.contenttype == ContentType_Quiz
                            && contentReq.questions != null){
                            let quizDetailsId = [];
                            let quizDetails = [];
                            let questions = contentReq.questions;
                            var serialNo = 1;
                            questions.forEach(question => {
                                let quizDetail = new QuizDetail({
                                        serialNo: serialNo++,
                                        questiontext: question.questiontext, 
                                        option1: question.option1, 
                                        option2: question.option2,
                                        option3: question.option3,
                                        option4: question.option4,
                                        answer: question.answer,
                                        answerDescription: question.answerDescription,
                                        status: 1,
                                        updatedon: new Date(),
                                        updatedby: userId
                                });
                                quizDetailsId.push(quizDetail._id);
                                quizDetails.push(quizDetail);
                            });
        
                            QuizDetail.insertMany(quizDetails,(err,data)=>{
                                if(err){
                                    logger.error('content_create - QuizDetail.insertMany - Error - ' + err);
                                    return callback(err);
                                }
        
                                let content = new Content({
                                    ownerId : ownerId,
                                    chapter: chapter._id,
                                    contentname: contentReq.contentname,
                                    uuid: contentReq.uuid,
                                    contenttype: contentReq.contenttype,
                                    title: contentReq.title,
                                    description: contentReq.description,
                                    tags: contentReq.tags,
                                    duration: contentReq.duration,
                                    marks: contentReq.marks,
                                    questions: quizDetailsId,
                                    status: 1,
                                    updatedon: new Date(),
                                    updatedby: userId
                                });
                                content.save((err)=>{
                                    if(err){
                                        logger.error('content_create - content.save - Error - ' + err);
                                        return callback(err);
                                    }
                                    else {                
                                        Chapter.findOneAndUpdate({"_id":chapter._id},
                                        {$push: {contents: content._id}},(err,data)=>{
                                            if(err){
                                                logger.error('content_create - Error - ' + err);
                                                return callback(err);
                                            }
                                            logger.info('content_create - ownerId:' + ownerId
                                            + ', contentname:' + contentReq.contentname
                                            + ', Content saved successfully');
                                            callback(null, ResponseCode.Success);
                                        });
                                        // Push above content id in chapter                 
                                    }        
                                })
                            });
                        }
                        else{
                            let newContent = {
                                ownerId : ownerId,
                                chapter: chapter._id,
                                contentname: contentReq.contentname,
                                uuid: contentReq.uuid,
                                contenttype: contentReq.contenttype,
                                title: contentReq.title,
                                description: contentReq.description,
                                thumbnail:contentReq.thumbnail,
                                contentlength: contentReq.contentlength,
                                status: 1,
                                updatedon: new Date(),
                                updatedby: userId
                            };
                            const allowSubtype = (
                                contentReq.contenttype === 'pdf' || contentReq.contenttype === 'document' || 
                                contentReq.contenttype === 'image' || contentReq.contenttype === 'video'
                            )
                            const thumbnailType =(
                                contentReq.contenttype === 'image' || contentReq.contenttype === 'video'
                            )
                            
                            if(allowSubtype && contentReq.contentsubtype) {
                                newContent = {...newContent, ...{contentsubtype: contentReq.contentsubtype}}
                                
                                if(thumbnailType){
                                    newContent = {...newContent,...{thumbnail: contentReq.thumbnail}}
                                }
                            }

                            const content = new Content(newContent);
                            content.save((err)=>{
                                if(err){
                                    logger.error('content_create - content.save - Error - ' + err);
                                    return callback(err);
                                }
                                else {                
                                    Chapter.findOneAndUpdate({"_id":chapter._id},
                                        {$push: {contents: content._id}},(err,data)=>{
                                        if(err){
                                            logger.error('content_create - Chapter.findOneAndUpdate - Error - ' + err);
                                            return callback(err);
                                        }
                                        if(contentReq.contenttype === 'embed-link') {
                                            logger.info('content_create - ownerId:' + ownerId
                                                + ', contentname:' + contentReq.contentname
                                                + ', Content saved successfully');
                                            return callback(null, ResponseCode.Success);
                                        } else {
                                            Tutor.findOne({ownerId : ownerId},(err,tutor)=>{
                                                if(err){
                                                    logger.error('content_create - Tutor.findOne - Error - ' + err);
                                                    return callback(err);
                                                }
                                                tutor.contentSize = tutor.contentSize + contentReq.contentlength;
                                                tutor.save((err)=>{
                                                    if(err){
                                                        logger.error('content_create - tutor.save - Error - ' + err);
                                                        return callback(err);
                                                    }
                                                    logger.info('content_create - ownerId:' + ownerId
                                                    + ', contentname:' + contentReq.contentname
                                                    + ', Content saved successfully');
                                                    return callback(null, ResponseCode.Success);
                                                })                                        
                                            });                                        
                                        }
                                    });          
                                }        
                            })
                        }
                    }
                    else{
                        logger.warn('content_create - ownerId:' + ownerId
                        + ', contentname:' + contentReq.contentname
                        + ', Content already exists');
                        return callback(null, ResponseCode.ContentAlreadyExists);
                    }
                })
            })
        }
        else{
            Content.findOne({$and: [{ownerId : ownerId},
                {contentname : contentReq.contentname}]})
                .exec( function (err, content) {
                 if (err){
                    logger.error('content_create - Content.findOne - Error - ' + err);
                     return callback(err);
                 }
                 if(content == null) {
                    if(contentReq.contenttype == ContentType_Quiz
                        && contentReq.questions != null){
                        let quizDetailsId = [];
                        let quizDetails = [];
                        let questions = contentReq.questions;
                        var serialNo = 1;
                        questions.forEach(question => {
                            let quizDetail = new QuizDetail({
                                    serialNo: serialNo++,
                                    questiontext: question.questiontext, 
                                    option1: question.option1, 
                                    option2: question.option2,
                                    option3: question.option3,
                                    option4: question.option4,
                                    answer: question.answer,
                                    answerDescription: question.answerDescription,
                                    status: 1,
                                    updatedon: new Date(),
                                    updatedby: userId
                            });
                            quizDetailsId.push(quizDetail._id);
                            quizDetails.push(quizDetail);
                        });
    
                        QuizDetail.insertMany(quizDetails,(err,data)=>{
                            if(err){
                                logger.error('content_create - QuizDetail.insertMany - Error - ' + err);
                                return callback(err);
                            }
    
                            let content = new Content({
                                ownerId : ownerId,
                                chapter: chapter._id,
                                contentname: contentReq.contentname,
                                uuid: contentReq.uuid,
                                contenttype: contentReq.contenttype,
                                title: contentReq.title,
                                description: contentReq.description,
                                tags: contentReq.tags,
                                duration: contentReq.duration,
                                marks: contentReq.marks,
                                questions: quizDetailsId,
                                status: 1,
                                updatedon: new Date(),
                                updatedby: userId
                            });
                            content.save((err)=>{
                                if(err){
                                    logger.error('content_create - content.save - Error - ' + err);
                                    return callback(err);
                                }
                                else {                
                                    Chapter.findOneAndUpdate({"_id":chapter._id},{$push: {contents: content._id}},(err,data)=>{
                                        if(err){
                                            logger.error('content_create - Chapter.findOneAndUpdate - Error - ' + err);
                                            return callback(err);
                                        }
                                        logger.info('content_create - ownerId:' + ownerId
                                            + ', contentname:' + contentReq.contentname
                                            + ', Content saved successfully');
                                        callback(null, ResponseCode.Success);
                                    });
                                }        
                            })
                        });
                    }
                    else{
                        let newContent = {
                            ownerId : ownerId,
                            chapter: chapter._id,
                            contentname: contentReq.contentname,
                            uuid: contentReq.uuid,
                            title: contentReq.title,
                            description: contentReq.description,
                            contenttype: contentReq.contenttype,
                            thumbnail:contentReq.thumbnail,
                            contentlength: contentReq.contentlength,
                            status: 1,
                            updatedon: new Date(),
                            updatedby: userId
                        };
                        const allowSubtype = (
                            contentReq.contenttype === 'pdf' || contentReq.contenttype === 'document' || 
                            contentReq.contenttype === 'image' || contentReq.contenttype === 'video'
                        )
                        const thumbnailType =(
                            contentReq.contenttype === 'image' || contentReq.contenttype === 'video'
                        )
                        
                        if(allowSubtype && contentReq.contentsubtype) {
                            newContent = {...newContent, ...{contentsubtype: contentReq.contentsubtype}}
                        
                            if(thumbnailType){
                                newContent = {...newContent,...{thumbnail: contentReq.thumbnail}}
                            }
                        }
                        const content = new Content(newContent)
                        content.save((err)=>{
                            if(err){
                                logger.error('content_create - content.save - Error - ' + err);
                                return callback(err);
                            }
                            else {                
                                Chapter.findOneAndUpdate({"_id":chapter._id},
                                    {$push: {contents: content._id}},(err,data)=>{
                                    if(err){
                                        logger.error('content_create - Chapter.findOneAndUpdate - Error - ' + err);
                                        return callback(err);
                                    }
                                    if(contentReq.contenttype === 'embed-link') {
                                        logger.info('content_create - ownerId:' + ownerId
                                            + ', contentname:' + contentReq.contentname
                                            + ', Content saved successfully');
                                        return callback(null, ResponseCode.Success);
                                    } else {
                                        Tutor.findOne({ownerId : ownerId},(err,tutor)=>{
                                            if(err){
                                                logger.error('content_create - Tutor.findOne - Error - ' + err);
                                                return callback(err);
                                            }
                                            tutor.contentSize = tutor.contentSize + contentReq.contentlength;
                                            tutor.save((err)=>{
                                                if(err){
                                                    logger.error('content_create - tutor.save - Error - ' + err);
                                                    return callback(err);
                                                }
                                                logger.info('content_create - ownerId:' + ownerId
                                                + ', contentname:' + contentReq.contentname
                                                + ', Content saved successfully');
                                                return callback(null, ResponseCode.Success);
                                            })                                        
                                        });
                                    }
                                });          
                            }        
                        })
                    }
                }
                else{
                    logger.warn('content_create - ownerId:' + ownerId
                    + ', contentname:' + contentReq.contentname
                    + ', Content already exists');
                    return callback(null, ResponseCode.ContentAlreadyExists);
                }
            })
        }        
    });
}

exports.content_contents = (ownerId, chapterReq, contenttype, callback)=> {
    Chapter.findOne({$and: [{ownerId : ownerId}, {boardname : chapterReq.boardname}, 
        {classname : chapterReq.classname}, {subjectname: chapterReq.subjectname},
         {chaptername: chapterReq.chaptername}]}).lean()
         .exec( function (err, chapter) {
        if (err) {
            logger.error('content_contents - Chapter.findOne - Error - ' + err);
            return callback(err);
        }
        if(chapter == null)  {
            logger.warn('content_contents - ownerId:' + ownerId
            + ', boardname:' + chapterReq.boardname + ', classname:' + chapterReq.classname
            + ', subjectname:' + chapterReq.subjectname + ', chaptername:' + chapterReq.chaptername
            + ', No chapter found');
            return callback(null, ResponseCode.ChapterNotFound); 
        }
        Content.find({$and: [{chapter : chapter._id}, {contenttype: contenttype}]})
        .select({"_id": 0,"__v":0})
        .populate({path:'questions',options:{lean:true}}).lean()
        .exec( function (err, contents) {
            if (err) {
                logger.error('content_contents - Content.find - Error - ' + err);
                return callback(err);
            }
            logger.info('content_contents - ownerId:' + ownerId
            + ', boardname:' + chapterReq.boardname + ', classname:' + chapterReq.classname
            + ', subjectname:' + chapterReq.subjectname + ', chaptername:' + chapterReq.chaptername
            + ', Content(s) retrieved successfully');
            callback(null,  ResponseCode.Success, contents);
        })
    });
};

exports.content_details = (ownerId, contentname, callback)=> {
    Content.findOne({$and: [{ownerId : ownerId},
        {contentname : contentname}]}).populate('questions')
        .select({"_id": 0,"__v":0}).exec( function (err, content) {
        if (err){
            logger.error('content_details - Content.findOne - Error - ' + err);
            return callback(err);
        }
        logger.info('content_details - ownerId:' + ownerId
            + ', contentname:' + contentname
            + ', Content retrieved successfully');
        callback(null, ResponseCode.Success, content);
    })
};

exports.content_update = (ownerId, userId, contentReq, callback)=> {

    /****** Validations for update request ***/
    // Chapter Model
    Chapter.find({$and: [{ownerId : ownerId}, {boardname : contentReq.boardname}, {classname : contentReq.classname}, {subjectname: contentReq.subjectname}, {chaptername: contentReq.chaptername}]}).exec((err, chapters) => {
        if(err) {
            logger.error('content_update - Chapter.findOne - Error - ' + err);
            return callback(err);
        }
        if(chapters === null) {
            return callback(null, ResponseCode.ChapterNotFound);
        }
    });
    
    /************************* */
    let content = {};
    content.updatedon = new Date();
    content.updatedby = userId; 
    content.title = contentReq.title;
    content.description = contentReq.description,
    content.tags = contentReq.tags,
    content.duration = contentReq.duration;
    content.marks = contentReq.marks;
    if(contentReq.questions){
        let quizDetails = [];
        let quizDetailsId = [];
        var serialNo = 1;
        contentReq.questions.forEach( question => { 
            let quizDetail = new QuizDetail({
                    serialNo: serialNo++,
                    questiontext: question.questiontext, 
                    option1: question.option1, 
                    option2: question.option2,
                    option3: question.option3,
                    option4: question.option4,
                    answer: question.answer,
                    answerDescription: question.answerDescription,
                    status: 1,
                    updatedon: new Date(),
                    updatedby: userId
            });
            quizDetailsId.push(quizDetail._id);
            quizDetails.push(quizDetail);
            
        });
        content.questions = quizDetailsId;
        QuizDetail.insertMany(quizDetails,(err,data)=>{
            if(err){
                logger.error('content_update - QuizDetail.insertMany - Error - ' + err);
                return callback(err);
            }
            Content.findOneAndUpdate({$and: [{ownerId : ownerId},
                {contentname : contentReq.contentname}]}, {$set: content},
                function (err, data) {
                    if (err) {
                        logger.error('content_update - Content.findOneAndUpdate - Error - ' + err);
                        return callback(err);
                    }
                    logger.info('content_update - ownerId:' + ownerId
                    + ', contentname:' + contentReq.contentname
                    + ', Content updated successfully');
                    callback(null, ResponseCode.Success);
            });
        });
    }  
    else
    {
        Content.findOneAndUpdate({$and: [{ownerId : ownerId},
            {contentname : contentReq.contentname}]}, {$set: content},
            function (err, data) {
                if (err){
                    logger.error('content_update - Content.findOneAndUpdate - Error - ' + err);
                    return callback(err);
                }
                logger.info('content_update - ownerId:' + ownerId
                + ', contentname:' + contentReq.contentname
                + ', Content updated successfully');
                callback(null, ResponseCode.Success);
        });
    }  
};

exports.content_delete = (ownerId, contentname, callback)=> {
    Content.findOne({$and: [{ownerId : ownerId},
        {contentname : contentname}]}).populate('chapter')
        .select({"_id": 0,"__v":0}).exec( function (err, content) {
        if (err) {
            logger.error('content_delete - Content.findOneAndDelete - Error - ' + err);
            return callback(err);
        }
        if(content != null){
            if(content.contenttype == 'pdf'
            || content.contenttype == 'video'
            || content.contenttype == 'image'
            || content.contenttype == 'document'){
                var folder = content.chapter.boardname + '-' 
                                + content.chapter.classname + '-' 
                                + content.chapter.subjectname + '-'
                                + content.chapter.chaptername;
                var fileName = ownerId + '/' + folder + '/' + content.uuid;

                deleteObject(fileName, (err, code)=>{
                    if(err){
                        logger.error('content_delete - Error - ' + err);
                        return next(err);
                    }
                    logger.info('content_delete - ownerId:' + ownerId
                    + ', contentname:' + contentname
                    + ', Content deleted from S3.');
                });
            }
            Content.findOneAndDelete({$and: [{ownerId : ownerId},
                {contentname : contentname}]}, function (err) {
                if (err) {
                    logger.error('content_delete - Content.findOneAndDelete - Error - ' + err);
                    return callback(err);
                }
                if(content.contenttype == 'pdf'
                || content.contenttype == 'video'
                || content.contenttype == 'image'
                || content.contenttype == 'document'){
                    Tutor.findOne({ownerId : ownerId},(err,tutor)=>{
                        if(err){
                            logger.error('content_delete - Tutor.findOne - Error - ' + err);
                            return callback(err);
                        }
                        tutor.contentSize = tutor.contentSize - content.contentlength;
                        tutor.save((err)=>{
                            if(err){
                                logger.error('content_delete - tutor.save - Error - ' + err);
                                return callback(err);
                            }
                            logger.info('content_delete - ownerId:' + ownerId
                            + ', contentname:' + contentname
                            + ', Content deleted successfully');
                            callback(null, ResponseCode.Success)
                        })                                        
                    });
                }
                else{
                    logger.info('content_delete - ownerId:' + ownerId
                            + ', contentname:' + contentname
                            + ', Content deleted successfully');
                            callback(null, ResponseCode.Success)
                }
                
            })
        }
        else{
            logger.warn('content_delete - ownerId:' + ownerId
                    + ', contentname:' + contentname
                    + ', Content not found');
                callback(null, ResponseCode.ContentNotFound)
        }
    })
};
