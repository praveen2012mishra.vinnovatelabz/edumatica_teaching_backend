const logger = require('../utils/logger.utils');
const { ResponseCode } = require('../utils/response.utils');
const Meeting = require('../models/bbbMeetings.model')
const Feedback = require('../models/bbbFeedbacks.model');
const { response } = require('express');

exports.bbbMeetings_getPostMeetingEventsInfo = (bbbMeetingsreq, callback) => {
        Meeting.find({
          meetingID: bbbMeetingsreq.meetingID,
        }).populate("events", "_id type userID name role eventTriggerTime").exec(function (err, meetingData) {
          if(err){
            logger.error('bbbMeetings_getPostMeetingEventsInfo - PostMeetingEventsInfo.find - Error - ' + err);
            return callback(err);
          }
          if (meetingData.length > 0) {
            return callback(null, ResponseCode.Success, meetingData);
          } else {
              logger.warn('bbbMeetings_getPostMeetingEventsInfo - PostMeetingEventsInfo.InfoNotAvailable - Warning - ' + meetingData);
              return callback(null, ResponseCode.MeetingInfoNotAvailable, meetingData);
          }
        })
}

exports.bbbMeetings_getPostIndividualMeetingEventsInfo = (internalMeetingID, callback) => {
  Meeting.findOne({
    internalMeetingID: internalMeetingID,
  }).populate("events", "_id type userID name role eventTriggerTime").exec(function (err, individualMeetingData) {
    if(err) {
      logger.error('bbbMeetings_getPostIndividualMeetingEventsInfo - DBC PostIndividualMeetingEventsInfo.find - Error - ' + err);
            return callback(err);
    }
    logger.info("DBC bbbMeetings PostIndividualMeetingEventsInfo.find Result:" + individualMeetingData)
    return callback(null, ResponseCode.Success, individualMeetingData);
  }) 
}

exports.bbbMeetings_feedback = (entityId,params, callback) => {
  const batchId = params.batchId
  const feedback = new Feedback({
    batchId:batchId,
    entityId:entityId,
    rating:params.rating,
    review:params.review,
  })
  feedback.save().then(response => {
    logger.info("bbbMeetings_feedback Feedback.save Result:" + response)
    callback(null, ResponseCode.Success)
  }).catch(err => {
    logger.error('bbbMeetings_feedback - DBC Feedback.save - Error - ' + err);
    return callback(err);
  })
  }