const { Assignment, AssignedAssignment } = require('../models/assignments.model');
const Batch = require('../models/batch.model');
const logger = require('../utils/logger.utils');
const { ResponseCode } = require('../utils/response.utils');

exports.createAssignment = (ownerId, assignment, callback) => {
    if(assignment.batches.length < 1) {
        callback(null, ResponseCode.BatchNotSelected);
    } else if(assignment.students.length < 1) {
        callback(null, ResponseCode.StudentNotSelected);
    } else {
        const promise0 = new Promise(async(resolve, reject) => {
            if(assignment._id) {
                Assignment.findOneAndUpdate({_id: assignment._id}, 
                    {$set: {...assignment, ...{isPublished: true, publishedDate: new Date()}}},
                    (err, data) => {
                        if(err) {
                            resolve({success: false, data: err})
                        } else {
                            resolve({success: true, data: assignment})
                        }
                    }
                )
            } else {
                const newAssignment = new Assignment ({
                    ownerId: ownerId,
                    tutorId: assignment.tutorId,
                    boardname: assignment.boardname,
                    classname: assignment.classname,
                    subjectname: assignment.subjectname,
                    assignmentname: assignment.assignmentname,
                    marks: assignment.marks,
                    instructions: assignment.instructions,
                    batches: assignment.batches,
                    students: assignment.students,
                    taskDocs: assignment.taskDocs,
                    brief: assignment.brief,
                    startDate: assignment.startDate,
                    endDate: assignment.endDate,
                    isPublished: true,
                    publishedDate: new Date()
                })

                newAssignment.save(function (err) {
                    if(err) {
                        resolve({success: false, data: err})
                    } else {
                        resolve({success: true, data: newAssignment})
                    }
                })
            }
        })

        promise0.then(async(value0) => {
            if(!value0.success) {
                logger.error('createAssignment - Batch.findById - Error - ' + value0.data);
                return callback(value0.data);
            } else {
                logger.error('createAssignment - Assignment.save - Success');

                const promise1 = new Promise(async(resolve, reject) => {
                    const students = [];
                    const stuStringArr = value0.data.students.map(list => String(list));
                    assignment.batches.forEach((element, count) => {
                        Batch.findById(element, (err1, batch) => {
                            if(err1) {
                                resolve({success: false, data: err1})
                            } else {
                                if(batch && batch.students.length > 0) {
                                    batch.students.forEach(stu => {
                                        if(stuStringArr.includes(String(stu))) {
                                            students.push({
                                                studentId: stu,
                                                batchId: batch._id
                                            })
                                        }
                                    })
                                }
                            }

                            if(assignment.batches.length === count + 1){
                                resolve({success: true, data: students})
                            }
                        })
                    })
                })
    
                promise1.then(async(value) => {
                    if(!value.success) {
                        logger.error('createAssignment - Batch.findById - Error - ' + value.data);
                        return callback(value.data);
                    } else {
                        const newAssignedAssignment = new AssignedAssignment ({
                            ownerId: ownerId,
                            tutorId: assignment.tutorId,
                            assignment: value0.data._id,
                            students: value.data,
                        })
    
                        newAssignedAssignment.save(function (err2) {
                            if(err2) {
                                logger.error('createAssignment - AssignedAssignment.save - Error - ' + err2);
                                return callback(err2);
                            } else {
                                logger.error('createAssignment - AssignedAssignment.save - Success');
                                return callback(null, ResponseCode.Success);
                            }
                        })
                    }
                });
            }
        })
    }
}

exports.createAssignmentDraft = (ownerId, assignment, callback) => {
    if(assignment.batches.length < 1) {
        callback(null, ResponseCode.BatchNotSelected);
    } else if(assignment.students.length < 1) {
        callback(null, ResponseCode.StudentNotSelected);
    } else {
        const promise0 = new Promise(async(resolve, reject) => {
            if(assignment._id) {
                Assignment.findOneAndUpdate({_id: assignment._id}, 
                    {$set: {...assignment}},
                    (err, data) => {
                        if(err) {
                            resolve({success: false, data: err})
                        } else {
                            resolve({success: true, data: assignment})
                        }
                    }
                )
            } else {
                const newAssignment = new Assignment ({
                    ownerId: ownerId,
                    tutorId: assignment.tutorId,
                    boardname: assignment.boardname,
                    classname: assignment.classname,
                    subjectname: assignment.subjectname,
                    assignmentname: assignment.assignmentname,
                    marks: assignment.marks,
                    instructions: assignment.instructions,
                    batches: assignment.batches,
                    students: assignment.students,
                    taskDocs: assignment.taskDocs,
                    brief: assignment.brief,
                    startDate: assignment.startDate,
                    endDate: assignment.endDate,
                    isPublished: false
                })

                newAssignment.save(function (err) {
                    if(err) {
                        resolve({success: false, data: err})
                    } else {
                        resolve({success: true, data: newAssignment})
                    }
                })
            }
        })

        promise0.then(async(value0) => {
            if(!value0.success) {
                logger.error('createAssignmentDraft - Assignment.save - Error - ' + value0.data);
                return callback(value0.data);
            } else {
                logger.error('createAssignmentDraft - Assignment.save - Success');
                return callback(null, ResponseCode.Success);
            }
        })
    }
}

exports.fetchAssignment = (ownerId, query, callback) => {
    const mongoQuery = {$and:[{ownerId: ownerId}, {status: 1}]};
    if(query.assignmentId) {
        mongoQuery['$and'].push({_id: query.assignmentId})
    }
    if(query.tutorId) {
        mongoQuery['$and'].push({tutorId: query.tutorId})
    }
    Assignment.find(mongoQuery, (err, data) => {
        if(err) {
            logger.error('fetchAssignment - Assignment.find - Error - ' + err);
            return callback(err); 
        } else {
            logger.error('fetchAssignment - Assignment.find - Success');
            return callback(null, ResponseCode.Success, data);
        }
    })
}

exports.fetchAssignedAssignment = (ownerId, query, callback) => {
    const studentId = query.studentId;
    const assignmentId = query.assignmentId;
    let mongoQuery;

    if(assignmentId && studentId) {
        mongoQuery = {$and:[{ownerId: ownerId}, {status: 1}, {assignment: assignmentId}, {students:{$elemMatch:{studentId: studentId}}}]}
    } else if(assignmentId) {
        mongoQuery = {$and:[{ownerId: ownerId}, {status: 1}, {assignment: assignmentId}]}
    } else {
        mongoQuery = {$and:[{ownerId: ownerId}, {status: 1}]}
    }

    AssignedAssignment.find(mongoQuery).populate({path:"assignment", model:"Assignment"}).exec((err, data) => {
        if(err) {
            logger.error('fetchAssignment - Assignment.find - Error - ' + err);
            return callback(err); 
        } else {
            logger.error('fetchAssignment - Assignment.find - Success');
            return callback(null, ResponseCode.Success, data);
        }
    })
}

exports.fetchStudentAssignment = (ownerId, query, callback) => {
    const studentId = query.studentId;

    AssignedAssignment.find({$and:[{ownerId: ownerId}, {status: 1}, {students:{$elemMatch:{studentId: studentId}}}]}, (err, data) => {
        if(err) {
            logger.error('fetchStudentAssignment - AssignedAssignment.find - Error - ' + err);
            return callback(err); 
        } else {
            const assignmentArr = data.map(ele => {return ele.assignment.toString()})
            Assignment.find({$and:[{ownerId: ownerId}, {status: 1}, {_id: {$in: assignmentArr}}]}, (err1, data1) => {
                if(err1) {
                    logger.error('fetchStudentAssignment - Assignment.find - Error - ' + err1);
                    return callback(err1); 
                } else {
                    const assignmentWithStatus = data1.map(list => {
                        const thisassignedassignment = data.find(elem => String(elem.assignment) === String(list._id));
                        const thisstudentstatus = thisassignedassignment.students.find(elem => String(elem.studentId) === String(studentId));
                        return (
                            {
                                ...list._doc,
                                ...{
                                    isSubmitted: thisstudentstatus.isSubmitted,
                                    isEvaluated: thisstudentstatus.isEvaluated
                                }
                            }
                        )
                    })
                    logger.error('fetchStudentAssignment - Assignment.find - Success');
                    return callback(null, ResponseCode.Success, assignmentWithStatus);
                }
            })
        }
    })
}

exports.submitAssignmentDoc = (ownerId, submitData, callback) => {
    AssignedAssignment.findOneAndUpdate({ $and: [{ownerId: ownerId}, {status: 1}, {assignment: submitData.assignmentId}, {"students.studentId": submitData.studentId}]}, {
        $set: { "students.$.uploadedAnswerDocs": submitData.answerDocs, "students.$.isSubmitted": true, "students.$.submittedOn": new Date() } }, (err, update) => {
        if(err) {
            logger.error('submitAssignmentDoc - AssignedAssignment.findOneAndUpdate - Error - ' + err);
            return callback(err); 
        } else {
            logger.error('submitAssignmentDoc - AssignedAssignment.findOneAndUpdate - Success');
            return callback(null, ResponseCode.Success);
        }
    })
}

exports.evaluateAssignmentDoc = (ownerId, evalData, callback) => {
    AssignedAssignment.findOneAndUpdate({ $and: [{ownerId: ownerId}, {status: 1}, {assignment: evalData.assignmentId}, {"students.studentId": evalData.studentId}]}, {
        $set: { 
            "students.$.marks": evalData.tutorfeedback.marks,
            "students.$.feedback": evalData.tutorfeedback.feedback,
            "students.$.tutorHighlights": evalData.tutorfeedback.tutorHighlights,
            "students.$.isEvaluated": true, 
            "students.$.evaluatedOn": new Date(), 
        } }, (err, update) => {
        if(err) {
            logger.error('evaluateAssignmentDoc - AssignedAssignment.findOneAndUpdate - Error - ' + err);
            return callback(err); 
        } else {
            logger.error('evaluateAssignmentDoc - AssignedAssignment.findOneAndUpdate - Success');
            return callback(null, ResponseCode.Success);
        }
    })
}
