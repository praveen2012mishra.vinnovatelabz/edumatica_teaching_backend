const DeviceToken = require("../models/deviceToken.model");
const NotificationPush = require("../models/notificationPush.model");
const NotificationPull = require("../models/notificationPull.model");
const UserNotification = require('../models/userNotifications.model');
const Announcement = require("../models/announcement.model");
const logger = require("../utils/logger.utils");
const { ResponseCode } = require("../utils/response.utils");
const mongoose = require('mongoose');

exports.notifications_pushNotificationsSaveDeviceTokens = (
  ownerId,
  params,
  callback
) => {
  const userId = params.userId;
  const mobileNo = params.mobileNo;
  const deviceToken = params.deviceToken;
  DeviceToken.findOne({ userId: userId }).exec((err, tokenDb) => {
    if (err) {
      logger.error(
        "notifications_pushNotificationsSaveDeviceTokens - DeviceToken.findOne - Error - " +
          err
      );
      callback(err);
    }
    if (tokenDb) {
      if (tokenDb.deviceToken.includes(deviceToken)) {
        callback(null, ResponseCode.Success);
        return;
      }
      DeviceToken.findOneAndUpdate(
        { userId: userId },
        { $push: { deviceToken: deviceToken } },
        { new: true }
      ).exec((err, response) => {
        if (err) {
          logger.error(
            "notifications_pushNotificationsSaveDeviceTokens - DeviceToken.findOneAndUpdate - Error - " +
              err
          );
          callback(err);
        }
        if (response) {
          logger.info(
            "notifications_pushNotificationsSaveDeviceTokens - DeviceToken.findOneAndUpdate - Response - " +
              response
          );
          callback(null, ResponseCode.Success);
        }
      });
    }
    if (!tokenDb) {
      let newEntry = new DeviceToken({
        ownerId: ownerId,
        userId: userId,
        mobileNo: mobileNo,
        deviceToken: deviceToken,
      });
      newEntry
        .save()
        .then((result) => {
          logger.info(
            "notifications_pushNotificationsSaveDeviceTokens - DeviceToken.save - result - " +
              result
          );
          callback(null, ResponseCode.Success);
        })
        .catch((err) => {
          logger.error(
            "notifications_pushNotificationsSaveDeviceTokens - DeviceToken.save - Error - " +
              err
          );
          callback(err);
        });
    }
  });
};

exports.notifications_pushNotificationsFindDeviceTokens = (
  userIds,
  callback
) => {
  let tokens = [];
  DeviceToken.find({ userId: {$in: userIds} }).exec((err, tokenDbs) => {
    // console.log(userId)
    if (err) {
      logger.error(
        "notifications_pushNotificationsFindDeviceTokens - DeviceToken.find - Error - " +
          err
      );
      callback(err);
      return;
    }
    if (tokenDbs) {
      logger.info(
        "notifications_pushNotificationsFindDeviceTokens - DeviceToken.find - Response - " +
          tokenDbs
      );
      
      tokenDbs.forEach(el => {
        tokens = [...tokens, ...el.deviceToken]
      })
      // console.log(tokens)
      callback(null, ResponseCode.Success, tokens);
    }
  });
};

exports.notifications_auditPush = (deviceTokens, status, msg, response) => {
  var newNotification = new NotificationPush({
    deviceTokens: deviceTokens,
    pushMessage: msg,
    status: status,
    response: JSON.stringify(response),
  });
  newNotification
    .save()
    .then((result) => {
      logger.info(
        "notifications_auditPush - NotificationPush.save - result - " + result
      );
    })
    .catch((err) => {
      logger.error(
        "notifications_auditPush - NotificationPush.save - Error - " + err
      );
    });
};

exports.notification_processAnnouncement = (ownerId, entityId, accountType, students, batchArr, title, brief, uuid, callback) => {
  // console.log(ownerId, entityId, accountType, students, batchArr, title, brief, uuid)
  var announcement = new Announcement({
    ownerId:ownerId,
    accountType:accountType,
    entityId:entityId,
    title: title,
    brief: brief,
    fileUuid: uuid,
    batchArr: batchArr,
    students: students
  })
  announcement.save().then(response => {
    return callback(null, ResponseCode.Success, announcement._id)
  }).catch(err => {
    logger.error(
        "notification_processAnnouncement - Announcement.save - Error - " + err
      );
  })
}

exports.fetchAnnouncement = (ownerId, entityId, callback) => {
  Announcement.find({$and: [{ownerId:ownerId}, {entityId:entityId}]}).populate("batchArr").exec((err, announcements) => {
    if(err) {
      logger.error(
        "fetchAnnouncement - Announcement.find - Error - " +
          err
      );
      return;
    }
    if(announcements) {
      callback(null, ResponseCode.Success, announcements)
    }
  })
}

exports.fetchAnnouncementById = (announcementId, callback) => {
  Announcement.findById(announcementId).populate("batchArr entityId").exec((err, announcement) => {
    if(err) {
      logger.error(
        "fetchAnnouncementById - Announcement.findById - Error - " +
          err
      );
      return;
    }
    if(announcement) {
      callback(null, ResponseCode.Success, announcement)
    }
  })
}

exports.notification_saveToUserPullDb = (userIds, title, msg, data) => {
  userIds.map((userId) => {
    NotificationPull.findOne({ userId: userId }).exec((err, notificationDb) => {
      if (err) {
        logger.error(
          "notification_saveToUserPullDb - NotificationPull.findOne - Error - " +
            err
        );
        return;
      }
      const UserNotificationId = new mongoose.Types.ObjectId();
      if (!notificationDb) {
        if (data) {
          var userNotifDb = new UserNotification({
            _id: UserNotificationId,
            title: title,
            message: msg,
            data: JSON.stringify(data),
            isViewedStatus: false,
          })
        }
        if (!data) {
          var userNotifDb = new UserNotification({
            _id: UserNotificationId,
            title: title,
            message: msg,
            isViewedStatus: false,
          })
        }

        var notifdb = new NotificationPull({
          userId: userId,
          notifications: [UserNotificationId],
        });

        userNotifDb
          .save()
          .then((result) => {
            logger.info(
              "notification_saveToUserPullDb - UserNotification.save - Response - " +
              result
            );
            // console.log(result)
            notifdb.save().then( result => {
              logger.info(
                "notification_saveToUserPullDb - NotificationPull.save - Response - " +
                result
              );
              // console.log(result)
            }).catch(err => {
              logger.error(
                "notification_saveToUserPullDb - NotificationPull.save - Error - " +
                err
              );
              // console.log(err)
            })
          })
          .catch((err) => {
            logger.error(
              "notification_saveToUserPullDb - UserNotification.save - Error - " +
              err
            );
            // console.log(err)
          });
      }
      if (notificationDb) {
        if (data) {
          var newNotification = new UserNotification({
            _id: UserNotificationId,
            title: title,
            message: msg,
            data: JSON.stringify(data),
            isViewedStatus: false,
          });
        }
        if (!data) {
          var newNotification = new UserNotification({
            _id: UserNotificationId,
            title: title,
            message: msg,
            isViewedStatus: false,
          });
        }
        newNotification.save().then(result=> {
          logger.info(
            "notification_saveToUserPullDb - UserNotification.save - Response - " +
            result
          );
          NotificationPull.findOneAndUpdate(
            { userId: userId },
            { $push: { notifications: UserNotificationId } },
            { new: true }
          ).exec((err, result) => {
            if (err) {
              logger.error(
                "notification_saveToUserPullDb - NotificationPull.update - Error - " +
                err
              );
              // console.log(err);
              return;
            }
            logger.info(
              "notification_saveToUserPullDb - NotificationPull.update - Response - " +
              result
            );
            // console.log(result);
          });
        }).catch(err => {
          logger.error(
            "notification_saveToUserPullDb - UserNotification.save - Error - " +
            err
          );
          // console.log(err)
        })
        // console.log(newNotification)
        
      }
    });
  });
};

exports.getAllNotifications = (userId, callback) => {
  NotificationPull.findOne({ userId: userId }).populate("notifications").exec((err, notificationDb) => {
    if (err) {
      logger.error(
        "notification_saveToUserPullDb - NotificationPull.findOne - Error - " +
          err
      );
      callback(err);
    }
    if (!notificationDb) {
      callback(null, ResponseCode.Success);
      return;
    }
    if (notificationDb) {
      const notifications = notificationDb.notifications;
      logger.info(
        "getAllNotifications - NotificationPull.findOne - Notifications - " +
          notifications
      );
      callback(null, ResponseCode.Success, notifications);
    }
  });
};

exports.updateNotifications = (userId, notificationIdArr, callback) => {
  UserNotification.updateMany({'_id':{$in : notificationIdArr}}, {$set : {'isViewedStatus':true}}).then(response => {
    logger.info(
      "updateNotifications - NotificationPull.updated - Response - " +
      JSON.stringify(response)
    );
    callback(null, ResponseCode.Success);
  }).catch(err => {
    logger.error(
      "notification_updateNotifications - NotificationPull.findOne - Error - " +
        err
    );
    callback(err);
  })
};


exports.notificationCron_deleteOldPullNotifications = () => {
  NotificationPull.find().exec((err, notifDb) => {
    if(err) {
      logger.error(
        "notificationCron_deleteOldPullNotifications - NotificationPull.find - Error - " +
          err
      );
      return
    }
    if(notifDb) {
      notifDb.forEach(el => {
        const pull = el.notifications
        if(pull.length > 100){
          const pullIdsToRemove = []
          for (let index = 0; index < pull.length - 100 ; index++) {
            pullIdsToRemove.push(pull[index])
          }
          UserNotification.deleteMany({_id : {$in : pullIdsToRemove}}, (err) => {
            if (err) {
              logger.error(
                "notificationCron_deleteOldPullNotifications - UserNotification.deleteMany - Error - " +
                  err
              );
            }
            NotificationPull.findByIdAndUpdate(el._id, { $pull: { notifications: { $in: pullIdsToRemove } } },{ multi: true }).exec(err => {
              if(err) {
                logger.error(
                  "notificationCron_deleteOldPullNotifications - NotificationPull.findByIdAndUpdate - Error - " +
                    err
                );
              }
            })
          })
        }
      })
    }
  })
}