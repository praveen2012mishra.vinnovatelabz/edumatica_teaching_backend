const Assessment = require('../models/assessment.model');
const AssessmentQuestion = require('../models/assessmentQuestion.model');
const Student = require('../models/student.model');
const AttemptedAssessment = require('../models/attemptedassessment.model')
const logger = require('../utils/logger.utils');
const { ResponseCode } = require('../utils/response.utils');
const Section = require('../models/section.model');
const { checkAnswer } = require('../utils/helpers.utils');

exports.analytics_studentAssessment = (ownerId, studentId, callback) => {
    AttemptedAssessment.find({ownerId:ownerId, studentId:studentId}).populate("assessment").exec()
    .then((attemptAssessmentDocs)=>{
        if(attemptAssessmentDocs===null){
            logger.warn("analytics_studentAssessment: No assessment found studentId:" + studentId)
            callback(null,ResponseCode.AssessmentNotFound)
        }
        else{
            logger.info("analytics_studentAssessment: attemptAssessmentDocs:" + attemptAssessmentDocs)
            callback(null,ResponseCode.Success,attemptAssessmentDocs)
        }
    })
    .catch((err)=>{
        logger.error("analytics_studentAssessment: find:error studentId:" + studentId)
        callback(err)
    })
}

exports.analytics_studentAssessmentGetAnswer = (ownerId, studentId, attemptAssessmentId, callback) => {
    AttemptedAssessment.findOne({_id:attemptAssessmentId,studentId:studentId,ownerId:ownerId}).exec()
    .then((attemptedAssessmentDoc)=>{
        if(attemptedAssessmentDoc===null){
            logger.info('Attempt Assessment not found. attemptassessmentId:'+attemptAssessmentId)
            callback(null,ResponseCode.AttemptedAssessmentNotFound)
        }
        else{
            if(!attemptedAssessmentDoc.isSubmitted){
                logger.info('Attempt Assessment not submitted attemptassessmentId:'+attemptAssessmentId)
                callback(null,ResponseCode.AttemptedAssessmentNotSubmitted)
            }
            else if(new Date()<new Date(attemptedAssessmentDoc.solutionTime)){
                callback(null,ResponseCode.SolutionsNotAccesible)
            }
            else{
                AttemptedAssessment.findOne({_id:attemptAssessmentId,studentId:studentId,ownerId:ownerId}).populate({path:'assessment',options:{lean:true}, select:'sections answers',populate:{path:'sections',options:{lean:true}, populate:{path:'questions',options:{lean:true}, populate:{path:'question',select:'answer answerDescription type' , option:{lean:true}}}}}).lean().exec()
                .then((attemptedAssessmentDoc)=>{
                    var tempQues
                    logger.debug(JSON.stringify(attemptedAssessmentDoc))
                    const reorderAttemptedAssessmentDoc =  {
                        ...attemptedAssessmentDoc,
                        assessment:{
                            ...attemptedAssessmentDoc.assessment,
                            sections: attemptedAssessmentDoc.assessment.sections.map((section)=>{
                                return {
                                    ...section,
                                    questions: section.questions.map(ques=>{
                                        tempQues=ques.question
                                        delete ques.question
                                        delete ques._id
                                        return {
                                        ...ques,
                                        ...tempQues
                                         }
                                    })
                                }
                            })
                        }
                    }
                    logger.debug(JSON.stringify(reorderAttemptedAssessmentDoc))
                    const answersData = reorderAttemptedAssessmentDoc.assessment.sections.map((section,index)=>{
                        return section.questions.map((question,ind)=>{
                            return {index:ind+1,correct:question.answer.join(","),answerDescription:question.answerDescription,marks:checkAnswer(question.answer,attemptedAssessmentDoc.answers[index][ind],question.type,question.percentageError,question.negativeMarking,question.marks).marks,result:checkAnswer(question.answer,attemptedAssessmentDoc.answers[index][ind],question.type,question.percentageError,question.negativeMarking,question.marks).result,your:(attemptedAssessmentDoc.answers[index][ind].length===0 || attemptedAssessmentDoc.answers[index][ind][0]==="" ?"Unanswered":attemptedAssessmentDoc.answers[index][ind].join(","))}
                        })
                    })
                    logger.debug(answersData)
                    callback(null,ResponseCode.Success,answersData)
                })
                .catch((err)=>{
                    logger.info('Unable to find answers for assessmentId:' + attemptAssessmentId)
                    callback(err)
                })
            }
        }
        
    })
    .catch((err)=>{
        callback(err)
    })
}