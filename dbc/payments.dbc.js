const mongoose = require('mongoose');
const logger = require('../utils/logger.utils');
const { ResponseCode } = require('../utils/response.utils');
const PaymentPackage = require('../models/paymentPackage.model');
const CourseBundle = require('../models/courseBundle.model');
const BatchInvite = require('../models/batchInvite.model');
const PurchasedCoursePackage = require('../models/purchasedCoursePackage.model');
const StudentPaymentAccount = require('../models/studentPaymentAccount.model');
const PaymentOrder = require("../models/paymentOrder.model");
const PaymentOrderAuthentication = require("../models/paymentOrderAuthentication.model");
const OrganizationPaymentOrder = require('../models/orgPaymentOrder.model')
const OrganizationPaymentAccount = require('../models/orgPaymentAccount.model');
const PurchasedEdumacPackage = require('../models/purchasedEdumacPackage.model');
const StudentPayable = require('../models/studentPayble.model');
const Tutor = require("../models/tutor.model");
const Organization = require("../models/organization.model");
const Student = require("../models/student.model");
const Batch = require("../models/batch.model");
const User = require("../models/user.model");
const MasterPackage = require("../models/masterPackage.model");
const {renewalCalculator} = require("../utils/helpers.utils");
const shortid = require('shortid');
const datefns = require("date-fns");
const { decryptWithAES } = require('../utils/referralGenerator.utils');
const { default: axios } = require('axios');
var FormData = require('form-data');

exports.payments_createCourseBundle = (ownerId, userId, entityId, batchId, packageArr, callback)=> {
    CourseBundle.findOne({$and: [{ownerId:ownerId},{batch:batchId }]}).exec((err, bundle) => {
        if(err) {
            logger.error('payments_createCourseBundle CourseBundle.FindOne - Error - ' + err);
            return callback(err)
        }
        if(bundle) {
            logger.warn('payments_createCourseBundle dbc CourseBundle.FindOne - Bundle Already Eists')
            return(null, ResponseCode.CourseBundleAlreadyExists)
        }
        if(!bundle) {
            function createData(
              _id,
              ownerId,
              totalprice,
              discount,
              paymentplan,
              paymentcycle,
              graceperiod,
              updatedon,
              updatedby
            ) {
              return { _id, ownerId, totalprice, discount, paymentplan, paymentcycle, graceperiod, updatedon, updatedby };
            }
            var packages = []
            var packageIdArr = [];
            packageArr.map(ele => {
                let packageId = new mongoose.Types.ObjectId();
                packages.push(createData(packageId,ownerId,ele.totalprice,ele.discount,ele.paymentplan,ele.paymentcycle,ele.graceperiod,new Date(),userId))
                packageIdArr.push(packageId)
            })
            PaymentPackage.insertMany(packages).then(response => {
                logger.info('payments_createCourseBundle PaymentPackage.insertMany - Success - Response: ' + response);
                BatchInvite.find({$and: [{ownerId:ownerId},{batchId:batchId}]}).populate("batchId").exec((err, inviteDb) => {
                    if(err) {
                        logger.error('payments_createCourseBundle BatchInvite.find - Error - ' + err);
                        return callback(err);
                    }
                    if(!inviteDb) var confirmStudentArr = [];
                    if(inviteDb) {
                        var confirmStudentArr = [];
                        inviteDb.map(el => {
                            if(confirmStudentArr.includes(el.studentId) !== true && el.isAccepted === "ACCEPT"){
                                confirmStudentArr.push(el.studentId)
                            }
                        })
                    }
                    const newCourseBundle = new CourseBundle({
                        ownerId: ownerId,
                        batch: batchId,
                        participants: confirmStudentArr,
                        paymentpackages: packageIdArr,
                        status: 1,
                        updatedon: new Date(),
                        updatedby: userId,
                    })
                    newCourseBundle.save().then(response => {   
                        logger.info('payments_createCourseBundle CourseBundle.Create - Success - Response: ' + response);
                        // Should be multiple purchased course package each for individual students
                        PurchasedCoursePackage.find({$and: [{studentId:{$in : confirmStudentArr}},{ownerId:ownerId},{batch:batchId}]}).exec((err, purchaseDb) => {
                            if(err) {
                                logger.error('payments_createCourseBundle PurchasedCoursePackage.find - Error - ' + err);
                                return callback(err);
                            }
                            if(purchaseDb.length !== 0) {
                                // console.log(purchaseDb)
                                logger.warn('payments_createCourseBundle dbc PurchasedCoursePackage.Find - PurchasedCoursePackage Already Exists' + purchaseDb)
                                return(null, ResponseCode.PurchasedCoursePackageAlreadyExists)
                            }
                            if(purchaseDb.length === 0) {   // change to batch wise renewal date and grace to be decremented upon missing renewal date
                                Batch.findOne({$and: [{ownerId:ownerId},{_id:batchId}]}).exec((err,batchDetails) => {
                                    // console.log(batchDetails)
                                    if(err) {
                                        logger.error('payments_createCourseBundle Batch.findOne - Error - ' + err);
                                        return callback(err);
                                    }
                                    const batch_startDate = batchDetails.batchstartdate;
                                    const batch_endDate = batchDetails.batchenddate;
                                    // case batch update will check if student added in between cycle
                                    // var renewalDate = datefns.add(batch_startDate, {
                                    //     days: process.env.INITIAL_GRACE_PERIOD,          // old format
                                    // })
                                    if(batch_startDate >= new Date()) {
                                        var renewalDate = batch_startDate;
                                    } else {
                                        var renewalDate = new Date()
                                    }
                                    let purchasedpackagestosave = []
                                    confirmStudentArr.forEach(ele =>  {
                                        // console.log(renewalDate)
                                        const newDocId = new mongoose.Types.ObjectId();
                                        const newPurchasedCoursePackage = new PurchasedCoursePackage({
                                            _id: newDocId,
                                            ownerId: ownerId,
                                            studentId: ele,
                                            batch: batchId,
                                            paymentPlans: packageArr,
                                            currentGracePeriod: process.env.INITIAL_GRACE_PERIOD,
                                            renewalDate: renewalDate,
                                        })
                                        purchasedpackagestosave.push(newPurchasedCoursePackage)
                                        StudentPaymentAccount.findOneAndUpdate({$and: [{ownerId:ownerId},{student:ele}]},{ $push: { purchasedPackage:newDocId}},{new:true}).exec((err, accresponse) => {
                                            console.log(accresponse)
                                            if(err) {
                                                logger.error('payments_createCourseBundle StudentPaymentAccount.findOneAndUpdate - Error - ' + err);
                                                return callback(err) 
                                            }
                                            logger.info('payments_createCourseBundle StudentPaymentAccount.findOneAndUpdate - Success - Response: ' + accresponse);
                                        })
                                    })
                                    PurchasedCoursePackage.insertMany(purchasedpackagestosave).then(response => {
                                        logger.info('payments_createCourseBundle PurchasedCoursePackage.insertMany - Success - Response: ' + response);
                                        return callback(null, ResponseCode.Success)
                                    }).catch(err => {
                                        logger.error('payments_createCourseBundle PurchasedCoursePackage.insertMany - Error - ' + err);
                                        return callback(err);
                                    })
                                    // newPurchasedCoursePackage.save().then(response => {
                                    //     var purchasedCoursePackageId = response._id;
                                    //     StudentPaymentAccount.updateMany({student:{$in : confirmStudentArr}}, {$push : {purchasedPackage:purchasedCoursePackageId}}).then(response => {
                                    //         logger.info('payments_createCourseBundle StudentPaymentAccount.updateMany - Success - Response: ' + response);
                                    //         return callback(null, ResponseCode.Success)
                                    //     }).catch(err => {
                                    //         logger.error('payments_createCourseBundle StudentPaymentAccount.updateMany - Error - ' + err);
                                    //         return callback(err) 
                                    //     })
                                    // }).catch(err => {
                                    //     logger.error('payments_createCourseBundle PurchasedCoursePackage.Create - Error - ' + err);
                                    //     return callback(err);
                                    // })
                                })
                            }
                        })
                    }).catch(err => {
                        logger.error('payments_createCourseBundle CourseBundle.Create - Error - ' + err);
                        return callback(err);
                    })
                })
            }).catch(err => {
                logger.error('payments_createCourseBundle PaymentPackage.insertMany - Error - ' + err);
                return callback(err);
            })
        }
    }) 
}

//for tutors or Org for edit handler
exports.payments_fetchCourseBundleById = (ownerId, userId, bundleId, callback) => {
    CourseBundle.findOne({$and: [{ownerId:ownerId},{_id:bundleId },{status:1}]}).populate("batch paymentpackages").exec((err, courseBundle) => {
        if(err) {
            logger.error('payments_fetchCourseBundleById CourseBundle.FindOne - Error - ' + err);
            return callback(err)
        }
        if(courseBundle) {
            logger.info('payments_fetchCourseBundleById CourseBundle.FindOne - Respone - ' + JSON.stringify(courseBundle));
            return callback(null, ResponseCode.Success, courseBundle)
        }
        if(!courseBundle) {
            logger.warn('payments_fetchCourseBundleById CourseBundle.FindOne Null Warn - Respone - ' + JSON.stringify(courseBundle));
            return callback(null, ResponseCode.EmptyCourseBundle)
        }
    })
}

//for tutors or Org for view handler
exports.payments_fetchCourseBundle = (ownerId, userId, callback) => {
    CourseBundle.find({$and: [{ownerId:ownerId},{status:1}]}).populate({path: "batch", model:"Batch", populate: [{path:"tutorId", model:"Tutor"}]}).populate("paymentpackages").exec((err, bundleDb) => {
        if(err) {
            logger.error('payments_fetchCourseBundle CourseBundle.Find - Error - ' + err);
            return callback(err)
        }
        if(bundleDb) {
            logger.info('payments_fetchCourseBundle CourseBundle.Find - Respone - ' + JSON.stringify(bundleDb));
            return callback(null, ResponseCode.Success, bundleDb)
        }
        if(!bundleDb) {
            logger.warn('payments_fetchCourseBundle CourseBundle.Find Null Warn - Respone - ' + JSON.stringify(bundleDb));
            return callback(null, ResponseCode.EmptyCourseBundle)
        }
    })
}

//for tutors or Org
exports.payments_updateCourseBundle = (ownerId, userId, bundleId, paymentPackageId, newfees, newdiscount, callback) => {
    CourseBundle.findOne({$and: [{ownerId:ownerId},{_id:bundleId },{status:1}]}).populate("batches").exec((err, courseBundle) => {
        if(err) {
            logger.error('payments_updateCourseBundle CourseBundle.FindOne - Error - ' + err);
            return callback(err)
        }
        if(courseBundle) {
            PaymentPackage.findOne({$and: [{ownerId:ownerId},{_id:paymentPackageId }]}).exec((err, feeDoc) => {
                if(err) {
                    logger.error('payments_updateCourseBundle FeeStructure.FindOne - Error - ' + err);
                    return callback(err)
                }
                if(feeDoc) {
                    PaymentPackage.findOneAndUpdate({$and: [{ownerId:ownerId},{_id:feeDoc._id }]}, {totalprice:newfees, discount:newdiscount}, {new: true}).exec((err, response) => {
                        if(err) {
                            logger.error('payments_updateCourseBundle FeeStructure.FindOneAndUpdate - Error - ' + err);
                            return callback(err)
                        }
                        if(response) {
                            logger.info('payments_updateCourseBundle CourseBundle.FindOneAndUpdate Success - Response - ' + response);
                            return callback(null, ResponseCode.Success);
                        }
                    })
                }
            })
        }
        if(!courseBundle) {
            logger.warn('payments_updateCourseBundle CourseBundle.FindOne Null Warn - Respone - ' + JSON.stringify(courseBundle));
            return callback(null, ResponseCode.EmptyCourseBundle)
        }
    })
}

//for tutors or Org
exports.payments_deleteCourseBundle = (ownerId, userId, bundleId, callback) => {
    CourseBundle.findOne({$and: [{ownerId:ownerId},{_id:bundleId },{status:1}]}).exec((err, courseBundle) => {
        if(err) {
            logger.error('payments_deleteCourseBundle CourseBundle.FindOne - Error - ' + err);
            return callback(err)
        }
        if(courseBundle) {
            CourseBundle.findOneAndUpdate({$and: [{ownerId:ownerId},{_id:bundleId },{status:1}]},{$set:{status:0}}).exec((err, response) => {
                if(err) {
                    logger.error('payments_deleteCourseBundle CourseBundle.findOneAndUpdate - Error - ' + err);
                    return callback(err)
                }
                if(response) {
                    return callback(null, ResponseCode.Success)
                }
            })
        }
        if(!courseBundle) {
            logger.warn('payments_deleteCourseBundle CourseBundle.FindOne Null Warn - Respone - ' + JSON.stringify(courseBundle));
            return callback(null, ResponseCode.EmptyCourseBundle)
        }
    })
}



exports.payments_fetchStudentAcc = (ownerId, userId, studentList, callback) => {
    StudentPaymentAccount.find({$and: [{ownerId:ownerId},{student: {$in: studentList}},{status:1}]}).populate("student").populate("invoice").populate({path:"purchasedPackage", model:"PurchasedCoursePackage", populate: [{path:"batch", model:"Batch"}, {path:"paymentTransactions", model:"PaymentOrder", populate:[{path:"batchId", model:"Batch"}]}]}).exec((err, studentAcc) => {
        if(err) {
            logger.error('payments_fetchStudentAcc StudentPaymentAccount.findOne - Error - ' + err);
            return callback(err)
        }
        if(studentAcc) {
            logger.info('payments_fetchStudentAcc StudentPaymentAccount.findOne - Success');
            return callback(null, ResponseCode.Success, studentAcc)
        }
    })
}

exports.payments_fetchOrgAcc = (ownerId, userId, entityId, callback) => {
    OrganizationPaymentAccount.findOne({$and: [{ownerId:ownerId},{entityId:entityId}]}).populate("purchasedEdumacPackage studentPayable").exec((err, paymentAcc) => {
        if(err) {
            logger.error('payments_fetchOrgAcc OrganizationPaymentAccount.findOne - Error - ' + err);
            return callback(err)
        }
        if(paymentAcc) {
            logger.info('payments_fetchOrgAcc OrganizationPaymentAccount.findOne - Response - ' + JSON.stringify(paymentAcc));
            return callback(null, ResponseCode.Success, paymentAcc)
        }
    })
}

exports.payments_generatePayOrder = (ownerId, userId, entityId, purchasedPackageId, batchId, paymentcycle, payOrder, callback) => {
    PaymentOrder.findOne({$and: [{ownerId:ownerId},{entityId:entityId},{orderId:payOrder.id}]}).exec((err,order) => {
        if(err) {
            logger.error('payments_generatePayOrder PaymentOrder.findOne - Error - ' + err);
            return callback(err)
        }
        if(order) {
            logger.warn('payments_generatePayOrder PaymentOrder.findOne Duplicate order found - Error - ' + err);
            return callback(null, ResponseCode.UnprocessablePayOrder)
        }
        if(!order) {
            const amount = payOrder.amount
            const newPaymentOrder = new PaymentOrder({
                ownerId: ownerId,
                entityId: entityId,
                orderId: payOrder.orderId,
                accountType: payOrder.accountType,
                purchasedPackageId: purchasedPackageId,
                batchId: batchId,
                paymentcycle: paymentcycle,
                entity: "order",
                amount: amount,
                amount_paid: 0,
                currency: payOrder.currency,
                status: "created",
                attempts: 1,
                notes: JSON.stringify(payOrder.notes),
                packageUpdate: payOrder.packageUpdate,
                created_at: payOrder.created_at
            })
            newPaymentOrder.save().then(response => {
                const unprocessedOrder = {
                    appId:process.env.CASHFREE_APP_ID,
                    id: payOrder.orderId,
                    currency: payOrder.currency,
                    amount: payOrder.amount,
                    returnUrl: `${process.env.BASE_URL}/payments/status/${payOrder.orderId}`,
                    notifyUrl: `${process.env.BASE_URL}/paymenthooks/payorder/authenticate`
                }
                logger.info('payments_generatePayOrder PaymentOrder.Create - Response - ' + JSON.stringify(response));
                return callback(null, ResponseCode.Success, unprocessedOrder)
            }).catch(err => {
                logger.error('payments_generatePayOrder PaymentOrder.Create - Error - ' + err);
                return callback(err)
            })
        }
    })
}

exports.payments_generateOrgPayOrder = (ownerId, userId, entityId, purchasedEdumacPackageId, paymentcycle, payOrder, callback) => {
    OrganizationPaymentOrder.findOne({$and: [{ownerId:ownerId},{entityId:entityId},{orderId:payOrder.orderId}]}).exec((err,order) => {
        if(err) {
            logger.error('payments_generateOrgPayOrder PaymentOrder.findOne - Error - ' + err);
            return callback(err)
        }
        if(order) {
            logger.warn('payments_generateOrgPayOrder PaymentOrder.findOne Duplicate order found - Error - ' + err);
            return callback(null, ResponseCode.UnprocessablePayOrder)
        }
        if(!order) {
            const amount = payOrder.amount
            const newPaymentOrder = new OrganizationPaymentOrder({
                ownerId: ownerId,
                entityId: entityId,
                orderId: payOrder.orderId,
                accountType: payOrder.accountType,
                purchasedEdumacPackageId: purchasedEdumacPackageId,
                // batchId: batchId,    // not valid in case of org/admintutor
                paymentcycle: paymentcycle,
                entity: "order",
                amount: amount,
                amount_paid: 0,
                currency: payOrder.currency,
                status: "created",
                attempts: 1,
                notes: JSON.stringify(payOrder.notes),
                packageUpdate: payOrder.packageUpdate,
                created_at: payOrder.created_at
            })
            newPaymentOrder.save().then(response => {
                const unprocessedOrder = {
                    appId:process.env.CASHFREE_APP_ID,
                    id: payOrder.orderId,
                    currency: payOrder.currency,
                    amount: payOrder.amount,
                    returnUrl: `${process.env.BASE_URL}/payments/status/${payOrder.orderId}`,
                    notifyUrl: `${process.env.BASE_URL}/paymenthooks/payorder/authenticate`
                }
                logger.info('payments_generateOrgPayOrder PaymentOrder.Create - Response - ' + JSON.stringify(response));
                return callback(null, ResponseCode.Success, unprocessedOrder)
            }).catch(err => {
                logger.error('payments_generateOrgPayOrder PaymentOrder.Create - Error - ' + err);
                return callback(err)
            })
        }
    })
}

exports.payments_generateEdumacPackagepayOrder = (ownerId, userId, entityId, purchasedEdumacPackageId, paymentcycle, payOrder, callback) => {
    OrganizationPaymentOrder.findOne({$and: [{ownerId:ownerId},{entityId:entityId},{orderId:payOrder.orderId}]}).exec((err,order) => {
        if(err) {
            logger.error('payments_generateEdumacPackagepayOrder PaymentOrder.findOne - Error - ' + err);
            return callback(err)
        }
        if(order) {
            logger.warn('payments_generateEdumacPackagepayOrder PaymentOrder.findOne Duplicate order found - Error - ' + err);
            return callback(null, ResponseCode.UnprocessablePayOrder)
        }
        if(!order) {
            const amount = payOrder.amount
            const newPaymentOrder = new OrganizationPaymentOrder({
                ownerId: ownerId,
                entityId: entityId,
                orderId: payOrder.orderId,
                accountType: payOrder.accountType,
                purchasedEdumacPackageId: purchasedEdumacPackageId,
                paymentcycle: paymentcycle,
                entity: "order",
                amount: amount,
                amount_paid: 0,
                currency: payOrder.currency,
                status: "created",
                attempts: 1,
                notes: JSON.stringify(payOrder.notes),
                packageUpdate: payOrder.packageUpdate,
                newPackagePlanId: payOrder.newPackagePlanId,
                created_at: payOrder.created_at
            })
            newPaymentOrder.save().then(response => {
                const unprocessedOrder = {
                    appId:process.env.CASHFREE_APP_ID,
                    id: payOrder.orderId,
                    currency: payOrder.currency,
                    amount: payOrder.amount,
                    returnUrl: `${process.env.BASE_URL}/payments/status/${payOrder.orderId}`,
                    notifyUrl: `${process.env.BASE_URL}/paymenthooks/payorder/authenticate`
                }
                logger.info('payments_generateEdumacPackagepayOrder PaymentOrder.Create - Response - ' + JSON.stringify(response));
                return callback(null, ResponseCode.Success, unprocessedOrder)
            }).catch(err => {
                logger.error('payments_generateEdumacPackagepayOrder PaymentOrder.Create - Error - ' + err);
                return callback(err)
            })
        }
    })
}


exports.payments_authenticatePaymentSuccess = (paymentAuthenticationReq) => {
    const entity = paymentAuthenticationReq;
    const totalAmt = parseFloat(entity.orderAmount);
    // const amt = (totalAmt*1000)/1025;
    const amt = totalAmt;
    PaymentOrder.findOne({orderId:entity.orderId}).exec((err,unprocessedOrder) => {
        if(err) {
             logger.error('payments_authenticatePaymentSuccess PaymentOrder.findOne - Error - ' + err);
             return
        }
        if(!unprocessedOrder) {
            OrganizationPaymentOrder.findOne({orderId:entity.orderId}).exec((err,unprocessedOrgOrder) => {
                if(err) {
                    logger.error('payments_authenticatePaymentSuccess OrganizationPaymentOrder.findOne - Error - ' + err);
                    return
                }
                logger.info('payments_authenticatePaymentSuccess OrganizationPaymentOrder.findOne - unprocessed Org Order retrieved:  unprocessedOrgOrder - ' + JSON.stringify(unprocessedOrgOrder));


                // IF Edumatica Package is purchased 
                if(entity.txStatus === "SUCCESS" && unprocessedOrgOrder.packageUpdate === true) {
                    PurchasedEdumacPackage.findOne({_id:unprocessedOrgOrder.purchasedEdumacPackageId}).exec((err, purchasedEdumac) => {
                        if(err) {
                            logger.error('payments_authenticatePaymentSuccess PurchasedEdumacPackage.findOne - Error - ' + err);
                            return
                        }
                        if(!purchasedEdumac) {
                            logger.warn('payments_authenticatePaymentSuccess PurchasedEdumacPackage.findOne - Warning - ' + JSON.stringify(purchasedEdumac));
                            return
                        }
                        if(purchasedEdumac) {
                            const newPlanId = unprocessedOrgOrder.newPackagePlanId
                            MasterPackage.findOne({planId:newPlanId}).exec((err, masterpackage) => {
                                if(err) {
                                    logger.error('payments_authenticatePaymentSuccess MasterPackage.findOne - Error - ' + err);
                                    return
                                }
                                if (masterpackage.cost === amt) {
                                    const currentEdumacPackage = new PurchasedEdumacPackage({
                                        ownerId : ownerId,
                                        accountType : unprocessedOrgOrder.accountType,                                          // Whether a independent teacher or Organization
                                        entityId: data._id,
                                        name : masterpackage.name,
                                        planId : masterpackage.planId,               // can be trial | Edumatica_Basic | Edumatica_Plus
                                        graceperiod : masterpackage.graceperiod,         
                                        paymentplan: 'PREPAID',
                                        paymentcycle: 'MONTHLY',    //discount not included , Discount will be kept seperate. 
                                        cost: masterpackage.cost,
                                        perstudentcost : masterpackage.perstudentcost,
                                        recordingQuota : masterpackage.recordingQuota,   //In hours 
                                        courseCount: masterpackage.courseCount,
                                        studentCount: masterpackage.studentCount,
                                        tutorCount: purchasedPackage.tutorCount,     // not applicable for Tutor
                                        batchCount: masterpackage.batchCount,
                                        sessionCount: masterpackage.sessionCount,
                                        contentSize: masterpackage.contentSize,
                                        customChapter: masterpackage.customChapter,
                                    })
                                    currentEdumacPackage.save().then(packageDb => {
                                        OrganizationPaymentAccount.findOneAndUpdate({$and:[{accountType:unprocessedOrgOrder.accountType},{entityId:unprocessedOrgOrder.entityId}]}, {purchasedEdumacPackage:packageDb._id}, {new:true}).exec((err, paymentAcc) => {
                                            if(err) {
                                                logger.error('payments_authenticatePaymentSuccess OrganizationPaymentAccount.findOneAndUpdate - Error - ' + err);
                                                return
                                            }
                                            // Send notification to user
                                            logger.debug('payments_authenticatePaymentSuccess OrganizationPaymentAccount.findOneAndUpdate - Success')
                                        })
                                    }).catch(err => {
                                        logger.error('payments_authenticatePaymentSuccess PurchasedEdumacPackage.save - Error - ' + err);
                                        return
                                    })
                                }
                            })
                        }
                    })
                }


                // IF no package purchase | monthly payment
                if(entity.txStatus === "SUCCESS" && unprocessedOrgOrder.packageUpdate === false) {
                    PurchasedEdumacPackage.findOne({_id:unprocessedOrgOrder.purchasedEdumacPackageId}).exec((err, purchasedEdumac) => {
                        if(err) {
                            logger.error('payments_authenticatePaymentSuccess PurchasedEdumacPackage.findOne - Error - ' + err);
                            return
                        }
                        if(!purchasedEdumac) {
                            logger.warn('payments_authenticatePaymentSuccess PurchasedEdumacPackage.findOne - Warning - ' + JSON.stringify(purchasedEdumac));
                            return
                        }
                        if(purchasedEdumac) {
                            logger.info('payments_authenticatePaymentSuccess PurchasedEdumacPackage.findOne - PurchasedEdumacPackage retrieved:  PurchasedEdumacPackage - ' + JSON.stringify(purchasedEdumac));
                            OrganizationPaymentAccount.findOne({$and: [{ownerId:unprocessedOrgOrder.ownerId},{entityId:unprocessedOrgOrder.entityId}]}).populate("purchasedEdumacPackage").exec((err, orgAccDb) => {
                                if(err) {
                                    logger.error('payments_authenticatePaymentSuccess OrganizationPaymentAccount.findOne - Error - ' + err);
                                    return
                                }
                                if(!orgAccDb) {
                                    logger.warn('payments_authenticatePaymentSuccess OrganizationPaymentAccount.findOne - Warning - ' + JSON.stringify(purchasedEdumac));
                                    return
                                }
                                if(orgAccDb) {
                                    StudentPayable.find({$and: [{ownerId:orgAccDb.ownerId},{entityId:orgAccDb.entityId},{currentStatus: 'ACTIVE'}]}).exec((err, activeStudArr) => {
                                        if(err) {
                                            logger.error('payments_authenticatePaymentSuccess StudentPayable.find - Error - ' + err);
                                            return
                                        }
                                        // Calculate absolute amount to verify
                                        const payedAmt = unprocessedOrgOrder.amount;
                                        // const payedAmt = (payedOrderAmt*1000)/1025;
                                        console.log(amt, orgAccDb.totaldebitBalance, payedAmt,activeStudArr)
                                        if(orgAccDb.totaldebitBalance === payedAmt && payedAmt === amt) {
                                            var retainerShipFeeBalance = purchasedEdumac.cost;
                                            var studentPaybleFeeBalance = purchasedEdumac.perstudentcost*activeStudArr.length;    // purchasedEdumac.perstudentcost*activeStudArr.length
                                            var totaldebitBalance = retainerShipFeeBalance+studentPaybleFeeBalance;
                                            var totalcreditBalance = orgAccDb.totalcreditBalance;
                                            var currentgraceperiod = purchasedEdumac.graceperiod;
                                            var renewalDate = datefns.addDays(orgAccDb.renewalDate,30);
                                            // change renewalDate and graceperiod of each studentPayable
                                            StudentPayable.find({$and: [{ownerId:orgAccDb.ownerId},{entityId:orgAccDb.entityId},{currentStatus: 'ACTIVE'}]}).exec((err, allPayableDb)=> {
                                                if(err){
                                                    logger.error('payments_authenticatePaymentSuccess StudentPayable.find - Error - ' + err);
                                                    return
                                                }
                                                if(allPayableDb) {
                                                    allPayableDb.forEach(doc => {
                                                        doc.renewalDate = datefns.addDays(new Date(),30);
                                                        doc.graceperiod = purchasedEdumac.graceperiod;
                                                        doc.currentPaybleAmountToEdumac = purchasedEdumac.perstudentcost;
                                                        doc.save().then(updateRes => {
                                                            logger.info('payments_authenticatePaymentSuccess StudentPayable.find document update - Success - ' + updateRes);
                                                        }).catch(err => {
                                                            logger.error('payments_authenticatePaymentSuccess StudentPayable.find document update - Error - ' + err);
                                                            return
                                                        })

                                                    })
                                                }
                                            })
                                            // also check for disabled studentPayble
                                            // increase for disabled
                                            StudentPayable.updateMany({$and: [{ownerId:orgAccDb.ownerId},{entityId:orgAccDb.entityId},{currentStatus: 'DEACTIVATED'}]},
                                            {$inc: {currentPaybleAmountToEdumac:purchasedEdumac.perstudentcost}, currentStatus:'ACTIVE'}).then(result => {
                                                logger.info('payments_authenticatePaymentSuccess StudentPayable.updateMany  DEACTIVATED - Updated:  PurchasedEdumacPackage - ' + JSON.stringify(result));
                                            }).catch(err => {
                                                logger.error('payments_authenticatePaymentSuccess StudentPayable.updateMany DEACTIVATED - Error:  PurchasedEdumacPackage - ' + err);
                                            })

                                            // Check if organization account is deactivated if so reactivate it
                                            const Model = orgAccDb.accountType === "Tutor" ? Tutor : Organization
                                            Model.findOne({$and: [{_id:orgAccDb.entityId},{ownerId: orgAccDb.ownerId}]}).exec((err, userDoc) => {
                                                if(err) {
                                                    logger.error('payments_authenticatePaymentSuccess Tutor : Organization Model.findOne - Error - ' + err);
                                                }
                                                if(userDoc && userDoc.roleStatus === 'DEACTIVATE') {
                                                    userDoc.roleStatus = 'ACTIVE'
                                                    userDoc.save().catch(err => {
                                                       logger.error('payments_authenticatePaymentSuccess Tutor : Organization Model.save - Error - ' + err); 
                                                    })
                                                }
                                            })
                                        }
                                        if(orgAccDb.totaldebitBalance === (payedAmt + orgAccDb.totalcreditBalance) && payedAmt === amt) {
                                            var retainerShipFeeBalance = purchasedEdumac.cost;
                                            var studentPaybleFeeBalance = purchasedEdumac.perstudentcost*activeStudArr.length;
                                            var totaldebitBalance = retainerShipFeeBalance+studentPaybleFeeBalance;
                                            var totalcreditBalance = 0;
                                            var currentgraceperiod = purchasedEdumac.graceperiod;
                                            var renewalDate = datefns.addDays(orgAccDb.renewalDate,30);
                                            // change renewalDate and graceperiod of each studentPayable
                                            StudentPayable.find({$and: [{ownerId:orgAccDb.ownerId},{entityId:orgAccDb.entityId},{currentStatus: 'ACTIVE'}]}).exec((err, allPayableDb)=> {
                                                if(err){
                                                    logger.error('payments_authenticatePaymentSuccess StudentPayable.find - Error - ' + err);
                                                    return
                                                }
                                                if(allPayableDb) {
                                                    allPayableDb.forEach(doc => {
                                                        doc.renewalDate = datefns.addDays(new Date(),30);
                                                        doc.graceperiod = purchasedEdumac.graceperiod;
                                                        doc.currentPaybleAmountToEdumac = purchasedEdumac.perstudentcost;
                                                        doc.save().then(updateRes => {
                                                            logger.info('payments_authenticatePaymentSuccess StudentPayable.find document update - Success - ' + updateRes);
                                                        }).catch(err => {
                                                            logger.error('payments_authenticatePaymentSuccess StudentPayable.find document update - Error - ' + err);
                                                            return
                                                        })

                                                    })
                                                }
                                            })
                                            // StudentPayable.updateMany({$and: [{ownerId:orgAccDb.ownerId},{entityId:orgAccDb.entityId},{currentStatus: 'ACTIVE'}]},
                                            // {$set: { renewalDate: {$add: ["$renewalDate", 24*60*60000*30]}}, graceperiod:purchasedEdumac.graceperiod, currentPaybleAmountToEdumac:purchasedEdumac.perstudentcost}).then(result => {
                                            //     logger.info('payments_authenticatePaymentSuccess StudentPayable.updateMany ACTIVE - Updated:  PurchasedEdumacPackage - ' + JSON.stringify(result));
                                            // }).catch(err => {
                                            //     logger.error('payments_authenticatePaymentSuccess StudentPayable.updateMany ACTIVE - Error:  PurchasedEdumacPackage - ' + err);
                                            // })
                                            // also check for disabled studentPayble
                                            // increase for disabled
                                            StudentPayable.updateMany({$and: [{ownerId:orgAccDb.ownerId},{entityId:orgAccDb.entityId},{currentStatus: 'DEACTIVATED'}]},
                                            {$inc: {currentPaybleAmountToEdumac:purchasedEdumac.perstudentcost}, currentStatus:'ACTIVE'}).then(result => {
                                                logger.info('payments_authenticatePaymentSuccess StudentPayable.updateMany ACTIVE - Updated:  PurchasedEdumacPackage - ' + JSON.stringify(result));
                                            }).catch(err => {
                                                logger.error('payments_authenticatePaymentSuccess StudentPayable.updateMany DEACTIVATED - Error:  PurchasedEdumacPackage - ' + err);
                                            })
                                            // Check if organization account is deactivated if so reactivate it
                                            const Model = orgAccDb.accountType === "Tutor" ? Tutor : Organization
                                            Model.findOne({$and: [{_id:orgAccDb.entityId},{ownerId: orgAccDb.ownerId}]}).exec((err, userDoc) => {
                                                if(err) {
                                                    logger.error('payments_authenticatePaymentSuccess Tutor : Organization Model.findOne - Error - ' + err);
                                                }
                                                if(userDoc && userDoc.roleStatus === 'DEACTIVATE') {
                                                    userDoc.roleStatus = 'ACTIVE'
                                                    userDoc.save().catch(err => {
                                                       logger.error('payments_authenticatePaymentSuccess Tutor : Organization Model.save - Error - ' + err); 
                                                    })
                                                }
                                            })
                                        }
                                        if(orgAccDb.totaldebitBalance > payedAmt && (orgAccDb.retainerShipFeeBalance === payedAmt) && payedAmt === amt) {
                                            var retainerShipFeeBalance = 0;
                                            var studentPaybleFeeBalance = orgAccDb.studentPaybleFeeBalance
                                            var totaldebitBalance = orgAccDb.totaldebitBalance - orgAccDb.retainerShipFeeBalance
                                            var totalcreditBalance = orgAccDb.totalcreditBalance
                                            var currentgraceperiod = orgAccDb.currentgraceperiod;
                                            var renewalDate = orgAccDb.renewalDate;
                                            // decrease graceperiod number of tutor accordingly if new Date() > renewalDate         // this should be through cron job Check retainerShipFeeBalance === 0
                                        }
                                        if(orgAccDb.totaldebitBalance > payedAmt && (orgAccDb.studentPaybleFeeBalance === payedAmt) && payedAmt === amt) {
                                            var retainerShipFeeBalance = orgAccDb.retainerShipFeeBalance;
                                            var studentPaybleFeeBalance = 0;
                                            var totaldebitBalance = orgAccDb.totaldebitBalance - orgAccDb.retainerShipFeeBalance
                                            var totalcreditBalance = orgAccDb.totalcreditBalance
                                            var currentgraceperiod = orgAccDb.currentgraceperiod;
                                            var renewalDate = orgAccDb.renewalDate;
                                            // decrease graceperiod number of tutor accordingly if new Date() > renewalDate         // this should be through cron job Check studentPaybleFeeBalance === 0
                                            StudentPayable.find({$and: [{ownerId:orgAccDb.ownerId},{entityId:orgAccDb.entityId},{currentStatus: 'ACTIVE'}]}).exec((err, allPayableDb)=> {
                                                if(err){
                                                    logger.error('payments_authenticatePaymentSuccess StudentPayable.find - Error - ' + err);
                                                    return
                                                }
                                                if(allPayableDb) {
                                                    allPayableDb.forEach(doc => {
                                                        doc.renewalDate = datefns.addDays(new Date(),30);
                                                        doc.graceperiod = purchasedEdumac.graceperiod;
                                                        doc.currentPaybleAmountToEdumac = purchasedEdumac.perstudentcost;
                                                        doc.save().then(updateRes => {
                                                            logger.info('payments_authenticatePaymentSuccess StudentPayable.find document update - Success - ' + updateRes);
                                                        }).catch(err => {
                                                            logger.error('payments_authenticatePaymentSuccess StudentPayable.find document update - Error - ' + err);
                                                            return
                                                        })

                                                    })
                                                }
                                            })
                                        }
                                        if(orgAccDb.totaldebitBalance < payedAmt && payedAmt === amt) {
                                            var studentPaybleFeeBalance = purchasedEdumac.perstudentcost*activeStudArr.length;    // purchasedEdumac.perstudentcost*activeStudArr.length
                                            var totaldebitBalance = retainerShipFeeBalance+studentPaybleFeeBalance;
                                            var totaldebitBalance = retainerShipFeeBalance+studentPaybleFeeBalance;
                                            var totalcreditBalance = orgAccDb.totalcreditBalance + (payedAmt - orgAccDb.totaldebitBalance)
                                            var currentgraceperiod = purchasedEdumac.graceperiod;
                                            // var renewalDate = datefns.addDays(orgAccDb.renewalDate,30);
                                            // change renewalDate and graceperiod of each studentPayable
                                            StudentPayable.find({$and: [{ownerId:orgAccDb.ownerId},{entityId:orgAccDb.entityId},{currentStatus: 'ACTIVE'}]}).exec((err, allPayableDb)=> {
                                                if(err){
                                                    logger.error('payments_authenticatePaymentSuccess StudentPayable.find - Error - ' + err);
                                                    return
                                                }
                                                if(allPayableDb) {
                                                    allPayableDb.forEach(doc => {
                                                        doc.renewalDate = datefns.addDays(new Date(),30);
                                                        doc.graceperiod = purchasedEdumac.graceperiod;
                                                        doc.currentPaybleAmountToEdumac = purchasedEdumac.perstudentcost;
                                                        doc.save().then(updateRes => {
                                                            logger.info('payments_authenticatePaymentSuccess StudentPayable.find document update - Success - ' + updateRes);
                                                        }).catch(err => {
                                                            logger.error('payments_authenticatePaymentSuccess StudentPayable.find document update - Error - ' + err);
                                                            return
                                                        })

                                                    })
                                                }
                                            })
                                            // also check for disabled studentPayble
                                            // increase for disabled
                                            StudentPayable.updateMany({$and: [{ownerId:orgAccDb.ownerId},{entityId:orgAccDb.entityId},{currentStatus: 'DEACTIVATED'}]},
                                            {$inc: {currentPaybleAmountToEdumac:purchasedEdumac.perstudentcost}, currentStatus:'ACTIVE'}).then(result => {
                                                logger.info('payments_authenticatePaymentSuccess StudentPayable.updateMany DEACTIVATED - Updated:  PurchasedEdumacPackage - ' + JSON.stringify(result));
                                            }).catch(err => {
                                                logger.error('payments_authenticatePaymentSuccess StudentPayable.updateMany DEACTIVATED - Error:  PurchasedEdumacPackage - ' + err);
                                            })
                                            // Check if organization account is deactivated if so reactivate it
                                            const Model = orgAccDb.accountType === "Tutor" ? Tutor : Organization
                                            Model.findOne({$and: [{_id:orgAccDb.entityId},{ownerId: orgAccDb.ownerId}]}).exec((err, userDoc) => {
                                                if(err) {
                                                    logger.error('payments_authenticatePaymentSuccess Tutor : Organization Model.findOne - Error - ' + err);
                                                }
                                                if(userDoc && userDoc.roleStatus === 'DEACTIVATE') {
                                                    userDoc.roleStatus = 'ACTIVE'
                                                    userDoc.save().catch(err => {
                                                       logger.error('payments_authenticatePaymentSuccess Tutor : Organization Model.save - Error - ' + err); 
                                                    })
                                                }
                                            })
                                        }
                                        console.log(retainerShipFeeBalance,studentPaybleFeeBalance,totaldebitBalance,totalcreditBalance,currentgraceperiod,renewalDate)
                                        OrganizationPaymentAccount.findOneAndUpdate({$and: [{ownerId:unprocessedOrgOrder.ownerId},{entityId:unprocessedOrgOrder.entityId}]},
                                        {retainerShipFeeBalance:retainerShipFeeBalance,studentPaybleFeeBalance:studentPaybleFeeBalance,totaldebitBalance:totaldebitBalance,totalcreditBalance:totalcreditBalance,currentgraceperiod:currentgraceperiod,renewalDate:renewalDate},{new:true}).exec((err, updatedAcc) => {
                                            if(err) {
                                                logger.error('payments_authenticatePaymentSuccess OrganizationPaymentAccount.findOneAndUpdate - Error - ' + err);
                                                return
                                            }
                                            if(!updatedAcc) {
                                                logger.warn('payments_authenticatePaymentSuccess OrganizationPaymentAccount.findOneAndUpdate - Warning No updates happened - ' + JSON.stringify(purchasedEdumac));
                                                return
                                            }
                                            if(updatedAcc) {
                                                const newOrgPaymentAuthentication = new PaymentOrderAuthentication({
                                                    orderId: entity.orderId,
                                                    orderAmount: entity.orderAmount,
                                                    referenceId: entity.referenceId,
                                                    txStatus: entity.txStatus,
                                                    paymentMode: entity.paymentMode,
                                                    txMsg: entity.txMsg,
                                                    txTime: entity.txTime,
                                                    signature: entity.signature,
                                                })
                                                newOrgPaymentAuthentication.save().then(response => {
                                                    logger.info('payments_authenticatePaymentSuccess Organization PaymentOrderAuthentication.Create - Response - ' + JSON.stringify(response));
                                                    OrganizationPaymentOrder.findOneAndUpdate({orderId:entity.orderId},{authenticationDetail:response._id, status:"paid", amount_paid:amt, receipt:entity.referenceId},{new:true}).exec((err,response) => {     
                                                        console.log(response)
                                                        if(err) {
                                                            logger.error('payments_authenticatePaymentSuccess OrganizationPaymentOrder.findOneAndUpdate- Error - ' + err);
                                                            return
                                                        }
                                                        if(response) {
                                                            logger.info('payments_authenticatePaymentSuccess OrganizationPaymentOrder.findOneAndUpdate - updated:  Response - ' + JSON.stringify(response));
                                                        }
                                                    })
                                                }).catch(err => {
                                                    logger.error('payments_authenticatePaymentSuccess Organization PaymentOrderAuthentication.Create - Error - ' + err);
                                                    return
                                                })
                                            }
                                        })
                                    })
                                }
                            })
                        }
                    })
                }

                
                if(entity.txStatus !== "SUCCESS") {
                    const newOrgPaymentAuthentication = new PaymentOrderAuthentication({
                        orderId: entity.orderId,
                        orderAmount: entity.orderAmount,
                        referenceId: entity.referenceId,
                        txStatus: entity.txStatus,
                        paymentMode: entity.paymentMode,
                        txMsg: entity.txMsg,
                        txTime: entity.txTime,
                        signature: entity.signature,
                    })
                    newOrgPaymentAuthentication.save().then(response => {
                        logger.error('payments_authenticatePaymentSuccess Organization PaymentOrderAuthentication.Create Authentication Error - Error');
                        OrganizationPaymentOrder.findOneAndUpdate({orderId:entity.orderId},{authenticationDetail:response._id, status:"failed"},{new:true}).exec((err,response) => {
                            if(err) {
                                logger.error('payments_authenticatePaymentSuccess OrganizationPaymentOrder.findOneAndUpdate Authentication Error - Error - ' + err);
                                return
                            }
                            if(response) {
                                logger.error('payments_authenticatePaymentSuccess OrganizationPaymentOrder.findOneAndUpdate Authentication Error - updated:  Response - ' + JSON.stringify(response));
                            }
                        })
                    }).catch(err => {
                        logger.error('payments_authenticatePaymentSuccess Organization PaymentOrderAuthentication.Create - Error - ' + err);
                        return
                    })
                }
            })
        }
        if(unprocessedOrder) {
            logger.info('payments_authenticatePaymentSuccess PaymentOrder.findOne - unprocessed Order retrieved:  unprocessedOrder - ' + JSON.stringify(unprocessedOrder));
            if(entity.txStatus === "SUCCESS") {
                PurchasedCoursePackage.findOne({$and: [{_id:unprocessedOrder.purchasedPackageId},{batch:unprocessedOrder.batchId}]}).populate("batch").exec((err, purchasedDoc) => {
                    if(err) {
                        logger.error('payments_authenticatePaymentSuccess PurchasedCoursePackage.findOne - Error - ' + err);
                        return
                    }
                    if(!purchasedDoc) {
                        logger.warn('payments_authenticatePaymentSuccess PurchasedCoursePackage.findOne - Warning - ' + JSON.stringify(purchasedDoc));
                        return
                    }
                    if(purchasedDoc) {
                        logger.info('payments_authenticatePaymentSuccess PurchasedCoursePackage.findOne - PurchasedCoursePackage retrieved:  PurchasedCoursePackage - ' + JSON.stringify(purchasedDoc));
                        // Check if payment already made
                        if(purchasedDoc.paymentComplete === true) {
                            logger.error('payments_authenticatePaymentSuccess Discrepency in payment payment already cleared - Error - ' + err);
                            return
                        }

                        const currentPaymentPlan = purchasedDoc.paymentPlans.find(ele => {
                            return ele.paymentcycle === unprocessedOrder.paymentcycle
                        })
                        const batchEndDate = purchasedDoc.batch.batchenddate

                        const calculatedParams = renewalCalculator(purchasedDoc.renewalDate,currentPaymentPlan,batchEndDate)
                        const paymentComplete = calculatedParams.isFullfilled;
                        const currentGracePeriod = calculatedParams.newgrace;
                        const renewalDate = calculatedParams.renewalDate;
                        
                        PurchasedCoursePackage.findOneAndUpdate({$and: [{_id:unprocessedOrder.purchasedPackageId},{batch:unprocessedOrder.batchId}]},{paymentComplete:paymentComplete,currentGracePeriod:currentGracePeriod,renewalDate:renewalDate, $push:{paymentTransactions:unprocessedOrder._id}},{new: true}).exec((err, response) => {
                            if(err) {
                                logger.error('payments_authenticatePaymentSuccess PurchasedCoursePackage.findOneAndUpdate - Error - ' + err);
                                return
                            }
                            if(response) {
                                logger.info('payments_authenticatePaymentSuccess PurchasedCoursePackage.findOneAndUpdate - updated:  Response - ' + JSON.stringify(response));
                                const newPaymentAuthentication = new PaymentOrderAuthentication({
                                    orderId: entity.orderId,
                                    orderAmount: entity.orderAmount,
                                    referenceId: entity.referenceId,
                                    txStatus: entity.txStatus,
                                    paymentMode: entity.paymentMode,
                                    txMsg: entity.txMsg,
                                    txTime: entity.txTime,
                                    signature: entity.signature,
                                })
                                newPaymentAuthentication.save().then(response => {
                                    logger.info('payments_authenticatePaymentSuccess PaymentOrderAuthentication.Create - Response - ' + JSON.stringify(response));
                                    PaymentOrder.findOneAndUpdate({orderId:entity.orderId},{authenticationDetail:response._id, status:"paid", amount_paid:amt, receipt:entity.referenceId},{new:true}).exec((err,response) => {             // Change amt to totalAmt
                                        console.log(response)
                                        if(err) {
                                            logger.error('payments_authenticatePaymentSuccess PaymentOrder.findOneAndUpdate- Error - ' + err);
                                            return
                                        }
                                        if(response) {
                                            logger.info('payments_authenticatePaymentSuccess PaymentOrder.findOneAndUpdate - updated:  Response - ' + JSON.stringify(response));
                                            // If student course/batch is deactivated activate it
                                            Student.findOne({$and: [{ownerId:response.ownerId}, {_id:response.entityId}]}).exec((err, userStudent) => {
                                                if(err) {
                                                    logger.error('payments_authenticatePaymentSuccess Student.findOne- Error - ' + err);
                                                    return
                                                }
                                                const disabledBatches = userStudent.disabledBatches
                                                if(disabledBatches.includes(unprocessedOrder.batchId) === true){
                                                    Student.findOneAndUpdate({$and: [{ownerId:response.ownerId}, {_id:response.entityId}]},{$pull: {disabledBatches:unprocessedOrder.batchId}}).exec((err, result) => {
                                                        if(err) {
                                                            logger.error('payments_authenticatePaymentSuccess Student.findOneAndUpdate- Error - ' + err);
                                                            return
                                                        }
                                                        if(result) {
                                                            logger.info('payments_authenticatePaymentSuccess Student.findOneAndUpdate - updated:  Response - ' + JSON.stringify(result));
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }).catch(err => {
                                    logger.error('payments_authenticatePaymentSuccess PaymentOrderAuthentication.Create - Error - ' + err);
                                    return
                                })
                            }
                        })    
                    }
                })
            }

            
            if(entity.txStatus !== "SUCCESS") {
                const newPaymentAuthentication = new PaymentOrderAuthentication({
                    orderId: entity.orderId,
                    orderAmount: entity.orderAmount,
                    referenceId: entity.referenceId,
                    txStatus: entity.txStatus,
                    paymentMode: entity.paymentMode,
                    txMsg: entity.txMsg,
                    txTime: entity.txTime,
                    signature: entity.signature,
                })
                newPaymentAuthentication.save().then(response => {
                    logger.error('payments_authenticatePaymentSuccess PaymentOrderAuthentication.Create Authentication Error - Error');
                    PaymentOrder.findOneAndUpdate({orderId:entity.orderId},{authenticationDetail:response._id, status:"failed"},{new:true}).exec((err,response) => {
                        if(err) {
                            logger.error('payments_authenticatePaymentSuccess Authentication PaymentOrder.findOneAndUpdate- Error - ' + err);
                            return
                        }
                        if(response) {
                            logger.error('payments_authenticatePaymentSuccess Authentication PaymentOrder.findOneAndUpdate - updated:  Error - ' + JSON.stringify(response));
                        }
                    })
                }).catch(err => {
                    logger.error('payments_authenticatePaymentSuccess PaymentOrderAuthentication.Create - Error - ' + err);
                    return
                })       
            }
        }
    })
}


const payments_helperStudentPayableAdder = (entityId, accountType, callback) => {
    OrganizationPaymentAccount.findOne({$and: [{entityId:entityId},{accountType:accountType}]}).exec((err, orgPaymentAcc) => {
        if(err) {
            logger.error('payments_helperStudentPayableAdder OrganizationPaymentAccount.findOne - Error - ' + err);
            return callback(err)
        }
        if(orgPaymentAcc) {
            const currentOrgRenewalDate = orgPaymentAcc.renewalDate;
            // const matchFilter = datefns.addDays(currentOrgRenewalDate,10)
            StudentPayable.find({$and: [{_id:{$in : orgPaymentAcc.studentPayable}},{accountType:accountType}]}).exec((err, payableDb) => {
                if(err) {
                    logger.error('payments_helperStudentPayableAdder StudentPayable.find - Error - ' + err);
                    return callback(err)
                }
                if(payableDb) {
                    const retainerShipFeeBalance = orgPaymentAcc.retainerShipFeeBalance;
                    var totalStudentPayableBill = 0
                    payableDb.map(elem => {
                        if(datefns.isSameMonth(new Date(), elem.renewalDate) === true && datefns.isSameMonth(new Date(),currentOrgRenewalDate) === true) {
                            totalStudentPayableBill = totalStudentPayableBill + elem.currentPaybleAmountToEdumac
                        }
                    })
                    const totaldebitBalance = retainerShipFeeBalance+totalStudentPayableBill
                    OrganizationPaymentAccount.findOneAndUpdate({$and: [{entityId:entityId},{accountType:accountType}]},{studentPaybleFeeBalance:totalStudentPayableBill,totaldebitBalance:totaldebitBalance}).exec((err, response) => {
                        if(err) {
                            logger.error('payments_helperStudentPayableAdder OrganizationPaymentAccount.findOneAndUpdate - Error - ' + err);
                            return callback(err)
                        }
                        logger.info('payments_helperStudentPayableAdder OrganizationPaymentAccount.findOneAndUpdate - Response - ' + response);
                        return callback(null)
                    })
                }
            })
        }
    })
}


exports.payments_handleRouting = (paymentRoutingReq,callback) => {
    const entity = paymentRoutingReq;
    const orderId = entity.orderId;
    const totalAmt = parseFloat(entity.orderAmount);
    const amt = totalAmt;
    // const amt = (totalAmt*1000)/1025;
    // const amount_to_payment_partner = (1*amt)/100;
    // const convenience_fee = (1.5*amt)/100;
    // const convenience_fee_tax = (18*convenience_fee)/100;
    // const paymentId = entity.id;
    OrganizationPaymentOrder.findOne({orderId:orderId}).exec((err,processedOrgOrder) => {
        if(err) {
            logger.error('payments_handleRouting OrganizationPaymentOrder.findOne - Error - ' + err);
            return callback(err)
        }
        // if(processedOrgOrder) {
        //     // Process immediate routing to Edumatica Account and calculate taxes for both parties
        //     const ownerId = processedOrgOrder.ownerId;
        //     const batchId = processedOrgOrder.batchId;
        //     const orgEntityId = processedOrgOrder.entityId;
        //     if(processedOrgOrder.amount_paid === totalAmt) {
        //         const amountToTutor = 0;
        //         const amountToEdumac = totalAmt;  // or totalAmt - amount_to_payment_partner
        //         const tds = 0 // no tds for edumatica
        //         const edumatica_earning = amt
        //         const edumatica_earning_tax = (18*edumatica_earning)/100
        //         processedOrgOrder.tds = tds;
        //         processedOrgOrder.edumatica_earning = edumatica_earning;
        //         processedOrgOrder.edumatica_earning_tax = edumatica_earning_tax;
        //         processedOrgOrder.save().then(result => {
        //             return callback(null, null, amountToTutor, amountToEdumac)
        //         }).catch(err => {
        //             logger.error('payments_handleRouting OrganizationPaymentOrder.save - Error - ' + err);
        //             return callback(err)
        //         })
        //     }
        // }
        if(!processedOrgOrder) {
            PaymentOrder.findOne({orderId:orderId}).exec((err, orderDb) => {
                if(err) {
                    logger.error('payments_handleRouting PaymentOrder.findOne - Error - ' + err);
                    return callback(err)
                }
                if(!orderDb) {
                    logger.error('payments_handleRouting PaymentOrder.findOne not found in handleRouting Error - ' + JSON.stringify(orderDb));
                    return
                }
                if(orderDb) {
                    const ownerId = orderDb.ownerId;
                    const amount_paid = amt;
                    const batchId = orderDb.batchId;
                    const studententityId = orderDb.entityId;
                    StudentPayable.findOne({$and: [{ownerId:ownerId},{batch:batchId},{student:studententityId}]}).exec((err, studentpayableDb) => {
                        if(err) {
                            logger.error('payments_handleRouting StudentPayable.findOne - Error - ' + err);
                            return callback(err)
                        }
                        const currentPaybleAmountToEdumac = studentpayableDb.currentPaybleAmountToEdumac;
                        const accountType = studentpayableDb.accountType;
                        const Model = accountType === "Tutor" ? Tutor : Organization
                            Model.findOne({$and: [{_id:studentpayableDb.entityId},{ownerId:ownerId}]}).exec((err, userEntity) => {
                                if(err) {
                                    logger.error('payments_handleRouting Org/Tutor.findOne - Error - ' + err);
                                    return callback(err)
                                }
                                if(userEntity) {
                                    if(amount_paid>=currentPaybleAmountToEdumac) {
                                        var amountToEdumac = currentPaybleAmountToEdumac;
                                        StudentPayable.findOne({$and: [{ownerId:ownerId},{batch:batchId},{student:studententityId}]}).exec((err,document) => {
                                            if(err) {
                                                logger.error('payments_handleRouting StudentPayable.findOne - Error - ' + err);
                                                return callback(err)
                                            }
                                            if(document) {
                                                // document.currentPaybleAmountToEdumac = debitTowardsEdumac;
                                                document.renewalDate = datefns.addDays(new Date(),30);
                                                document.graceperiod = process.env.INITIAL_GRACE_PERIOD;
                                                document.save().then(updateRes => {
                                                    logger.info("payments_handleRouting  StudentPayable update-" + updateRes)
                                                    payments_helperStudentPayableAdder(studentpayableDb.entityId,accountType,(err) => {
                                                        if(err) {
                                                            return callback(err)
                                                        } else{
                                                            orderDb.studentPayableAmt = currentPaybleAmountToEdumac;
                                                            orderDb.save().then(result => {
                                                                return callback (null)
                                                            }).catch(err => {
                                                                logger.error('payments_handleRouting OrganizationPaymentOrder.save - Error - ' + err);
                                                                return callback(err)
                                                            })
                                                        }
                                                    })
                                                }).catch(err => {
                                                    logger.error('payments_handleRouting Org/Tutor.findOne - Error - ' + err);
                                                    return callback(err)
                                                })
                                            }
                                        })
                                        // StudentPayable.findOneAndUpdate({$and: [{ownerId:ownerId},{batch:batchId},{student:studententityId}]},
                                        //     {currentPaybleAmountToEdumac:debitTowardsEdumac, $set: { renewalDate: {$add: ["$renewalDate", new Date(24*60*60000*30)]}}, graceperiod:process.env.INITIAL_GRACE_PERIOD}).exec((err, response) => {
                                        //         if(err) {
                                        //             logger.error('payments_handleRouting StudentPayable.findOneAndUpdate - Error - ' + err);
                                        //             return callback(err)
                                        //         }
                                        //         payments_helperStudentPayableAdder(studentpayableDb.entityId,accountType,(err) => {
                                        //             if(err) {
                                        //                 return callback(err)
                                        //             } else{
                                        //                 return callback (null, linkedAccount, amountToTutor, amountToEdumac)
                                        //             }
                                        //         })
                                        //     })
                                    }
                                    if(amount_paid<currentPaybleAmountToEdumac) {
                                        var amountToTutor = 0;
                                        var amountToEdumac = amount_paid;
                                        var debitTowardsEdumac = currentPaybleAmountToEdumac - amount_paid;
                                        StudentPayable.findOneAndUpdate({$and: [{ownerId:ownerId},{batch:batchId},{student:studententityId}]},
                                            {currentPaybleAmountToEdumac:debitTowardsEdumac}).exec((err, response) => {
                                                if(err) {
                                                    logger.error('payments_handleRouting StudentPayable.findOneAndUpdate - Error - ' + err);
                                                    return callback(err)
                                                }
                                                payments_helperStudentPayableAdder(studentpayableDb.entityId,accountType,(err) => {
                                                    if(err) {
                                                        return callback(err)
                                                    } else{
                                                            orderDb.studentPayableAmt = amountToEdumac;
                                                            orderDb.save().then(result => {
                                                                return callback (null)
                                                            }).catch(err => {
                                                                logger.error('payments_handleRouting OrganizationPaymentOrder.save - Error - ' + err);
                                                                return callback(err)
                                                            })
                                                    }
                                                })
                                            })
                                    }
                                }
                            })
                    })
                }
            })
        }
    })
}


exports.paymentCron_calculateStudentPayableCron = () => {
    console.log("Cron Running at paymentCron_calculateStudentPayableCron")
    OrganizationPaymentAccount.find().select("ownerId accountType entityId studentPaybleFeeBalance purchasedEdumacPackage totaldebitBalance retainerShipFeeBalance currentgraceperiod renewalDate studentPayable").exec((err, paymentAcc) => {
        if(err) {
            logger.error('paymentCron_calculateStudentPayableCron OrganizationPaymentAccount.find - Error - ' + err);
        }
        if(paymentAcc) {
            console.log(JSON.stringify(paymentAcc))
            paymentAcc.forEach(acc => {
                logger.info('paymentCron_calculateStudentPayableCron OrganizationPaymentAccount.find' + JSON.stringify(acc))
                const currentOrgRenewalDate = acc.renewalDate;
                // const matchFilter = datefns.addDays(currentOrgRenewalDate,10)
                const entityId = acc.entityId;
                const accountType = acc.accountType;
                const Model = accountType === "Tutor" ? Tutor : Organization
                Model.findOne({$and:[{_id:entityId},{ownerId:acc.ownerId}]}).exec((err,user) => {
                    if(err) {
                        logger.error('paymentCron_calculateStudentPayableCron Tutor.find || Organization.findOne - Error - ' + err);
                    }
                    if(user && user.roleStatus === 'ACTIVE') {
                        // Find only studentpayable and disable student in case
                        StudentPayable.find({$and: [{_id:{$in : acc.studentPayable}}]}).exec((err, payableDb) => {
                            if (err) {
                                logger.error('paymentCron_calculateStudentPayableCron StudentPayable.find - Error - ' + err);
                            }
                            if(!payableDb) {
                                logger.error('paymentCron_calculateStudentPayableCron StudentPayable.find - Error No Documents found - ' + err);
                            }
                            if(payableDb) {
                                const retainerShipFeeBalance = acc.retainerShipFeeBalance;
                                var totalStudentPayableBill = 0;
                                payableDb.forEach(elem => {
                                    if(datefns.isSameMonth(new Date(), elem.renewalDate) === true && datefns.isSameMonth(new Date(),currentOrgRenewalDate) === true) {
                                        totalStudentPayableBill = totalStudentPayableBill + elem.currentPaybleAmountToEdumac
                                    }
                                })
                                const totaldebitBalance = retainerShipFeeBalance+totalStudentPayableBill
                                OrganizationPaymentAccount.findOneAndUpdate({$and: [{entityId:entityId},{accountType:accountType}]},{studentPaybleFeeBalance:totalStudentPayableBill,totaldebitBalance:totaldebitBalance}).exec((err, response) => {
                                    if(err) {
                                        logger.error('paymentCron_calculateStudentPayableCron OrganizationPaymentAccount.findOneAndUpdate - Error - ' + err);
                                    }
                                    console.log(response);
                                    payableDb.forEach(payableDoc => {
                                        if(new Date() >= payableDoc.renewalDate && payableDoc.graceperiod > 0) {
                                            // set grace--
                                            payableDoc.graceperiod = payableDoc.graceperiod - 1;
                                            payableDoc.save().catch(err => {
                                                logger.error('paymentCron_calculateStudentPayableCron StudentPayable.save - Error - ' + err);
                                            })
                                        }
                                        if(new Date() >= payableDoc.renewalDate && payableDoc.graceperiod <= 0) {
                                            // Disable Student and send notification to student and tutor
                                            Student.findOneAndUpdate({$and: [{ownerId: payableDoc.ownerId},{_id: payableDoc.student}]},{roleStatus:"DEACTIVATE"}).exec((err, disabledRes) => {
                                                if(err) {
                                                    logger.error('paymentCron_calculateStudentPayableCron Student.findOneAndUpdate - Error - ' + err);
                                                }
                                                if(disabledRes) {
                                                    // Send Notifications Here

                                                }
                                            })
                                        }
                                    })
                                })
                            }
                        })
                    }
                })
            })
        }
    })
}


exports.paymentCron_calculateStudentPaymentAcc = () => {
    console.log("Cron Running at paymentCron_calculateStudentPaymentAcc")
    StudentPaymentAccount.find().select("entityId student purchasedPackage").exec((err,paymentAcc) => {
        if(err) {
            logger.error('paymentCron_calculateStudentPaymentAcc StudentPaymentAccount.find - Error - ' + err);
            // return callback(err)
        }
        if(paymentAcc) {
            paymentAcc.forEach(eachAcc => {
                const entityId = eachAcc.entityId;
                const studentId = eachAcc.student;
                const purchasedPackageArr = eachAcc.purchasedPackage;
                // Find Student First needed for roleStatus & notifications
                Student.findById(studentId).exec((err, currentStudent) => {
                    if(err) {
                        logger.error('paymentCron_calculateStudentPaymentAcc Student.findById - Error - ' + err);
                        // return callback(err)
                    }
                    if(currentStudent && currentStudent.roleStatus === 'ACTIVE') {
                            PurchasedCoursePackage.find({_id:{$in : purchasedPackageArr}}).exec((err,purchasedArr) => {
                                if(err) {
                                    logger.error('paymentCron_calculateStudentPaymentAcc PurchasedCoursePackage.find - Error - ' + err);
                                    // return callback(err)
                                }
                                if(purchasedArr) {
                                    purchasedArr.forEach(elem => {
                                        if(elem.paymentComplete !== true) {
                                            const currentGrace = elem.currentGracePeriod;
                                            const currentRenewalDate = elem.renewalDate;
                                            const notificationStartDate = datefns.subDays(elem.renewalDate,3)
                                            const today = new Date()
                                            if (datefns.isSameMonth(today,currentRenewalDate) === true) {
                                                if(today < currentRenewalDate && today > notificationStartDate) {
                                                    // Send notification
                                                }
                                                if(today > currentRenewalDate && currentGrace > 0) {
                                                    // Send notification and set grace--
                                                    elem.currentGracePeriod = elem.currentGracePeriod - 1;
                                                    elem.save().catch(err => {
                                                        logger.error('paymentCron_calculateStudentPaymentAcc PurchasedCoursePackage save to Update - Error - ' + err);
                                                    })
                                                }
                                                if(today > currentRenewalDate && currentGrace <= 0) {
                                                    // Send notification and Disble Account also disable student payable
                                                    Student.findOneAndUpdate({_id:studentId},{$push: {disabledBatches:elem.batch}}).exec((err,response) => {
                                                        if(err) {
                                                            logger.error('paymentCron_calculateStudentPaymentAcc Student.findOneAndUpdate Student Disable Payment Defaulter - Error - ' + err);
                                                        }
                                                        logger.info('paymentCron_calculateStudentPaymentAcc Student.findOneAndUpdate Student Course Disabled Payment Defaulter - Success - ' + response);
                                                        // Enable code if billing of student to be stopped during temporary deactivation
                                                        // StudentPayable.findOneAndUpdate({student:studentId}, {currentStatus:"DEACTIVATE"}).exec((res,err) => {
                                                        //     if(err) {
                                                        //         logger.error('paymentCron_calculateStudentPaymentAcc StudentPayable.findOneAndUpdate - Error - ' + err);
                                                        //     }
                                                        // })
                                                    })
                                                }
                                            }
                                        }
                                    })
                                }
                            })
                    }
                })
            })
        }
    })
}

exports.paymentCron_calculateRetainershipFeeCron = () => {
    console.log("Cron Running at paymentCron_calculateRetainershipFeeCron")
    OrganizationPaymentAccount.find().select("ownerId accountType entityId retainerShipFeeBalance currentgraceperiod renewalDate").exec((err, accDb) => {
        if(err) {
            logger.error('paymentCron_calculateRetainershipFeeCron OrganizationPaymentAccount.find - Error - ' + err);
            return callback(err)
        }
        if(accDb) {
            accDb.forEach(acc => {
                const currentOrgRenewalDate = acc.renewalDate;
                const currentgraceperiod = acc.currentgraceperiod;
                const matchFilter = datefns.subDays(currentOrgRenewalDate,3);
                const entityId = acc.entityId;
                const accountType = acc.accountType;
                const Model = accountType === "Tutor" ? Tutor : Organization
                Model.findOne({$and:[{_id:entityId},{ownerId:acc.ownerId}]}).exec((err,user) => {
                    if(err) {
                        logger.error('paymentCron_calculateRetainershipFeeCron Tutor.find || Organization.findOne - Error - ' + err);
                        // return callback(err)
                    }
                    if(user && user.roleStatus === 'ACTIVE') {
                        if (datefns.isSameMonth(new Date(),currentOrgRenewalDate) === true) {
                            if(new Date() < currentOrgRenewalDate && new Date() > matchFilter && currentgraceperiod > 0 && acc.retainerShipFeeBalance > 0) {
                                // Send notification
                            }
                            if(datefns.isSameDay(new Date(),currentOrgRenewalDate) === true && currentgraceperiod > 0 && acc.retainerShipFeeBalance > 0) {
                                // Send notification    Decrease grace--
                                acc.currentgraceperiod = acc.currentgraceperiod - 1;
                                acc.save().catch(err => {
                                    logger.error('paymentCron_calculateRetainershipFeeCron OrganizationPaymentAccount.find and then update - Error - ' + err);
                                    // return callback(err)
                                })
                            }
                            if(new Date() > currentOrgRenewalDate && currentgraceperiod <= 0 && acc.retainerShipFeeBalance > 0) {
                                // Send notification Disable all acc with ownerId
                                user.roleStatus = "DEACTIVATE"
                                user.save().then(updatedUser => {
                                    // Enable code if student needs to be blocked to view uploaded contents
                                    // Student.updateMany({ownerId:user.ownerId},{roleStatus:"DEACTIVATE"}).then(result => {
                                    //     // Enable code if billing of student to be stopped during temporary deactivation
                                    //     // StudentPayable.updateMany({ownerId:user.ownerId},{currentStatus:"DEACTIVATE"}).catch(err => {
                                    //     //     logger.error('paymentCron_calculateRetainershipFeeCron StudentPayable.updateMany - Error - ' + err);
                                    //     // })
                                    // }).catch(err => {
                                    //     logger.error('paymentCron_calculateRetainershipFeeCron Student.updateMany - Error - ' + err);
                                    // })
                                }).catch(err => {
                                    logger.error('paymentCron_calculateRetainershipFeeCron Tutor | Organization.find and then update - Error - ' + err);
                                })
                            }
                        }
                    }
                })
            })
        }
    })
    
}

exports.paymentCron_batchEndCheckerCron = () => {
    console.log("Cron Running at paymentCron_batchEndCheckerCron")
    Batch.find().select("ownerId students batchenddate").exec((err, batchDb) => {
        if(err) {
            logger.error('paymentCron_batchEndCheckerCron Batch.find - Error - ' + err); 
        }
        if(batchDb) {   
            batchDb.forEach(batch => {
                const batchenddate = batch.batchenddate;
                const studentsArr = batch.students;
                if(datefns.isSameDay(new Date, batchenddate) === true) {
                    let payableIdsToRemove = [];
                    let purchaseIdsToRemove = [];
                    StudentPayable.find({$and: [{ownerId:batch.ownerId},{batch:batch._id}]}).select("batch student").exec((err, payableDb) => {
                        if(err) {
                            logger.error('paymentCron_batchEndCheckerCron - StudentPayable.find - Error - ' + err);
                        }
                        if(payableDb) {
                            payableDb.forEach(each => {
                                payableIdsToRemove.push(each._id)
                            })
                        }
                    })
                    PurchasedCoursePackage.find({$and:[{ownerId:ownerId},{batch:batch._id}]}).select("batch studentId").exec((err, purchaseDb) => {
                        if(err) {
                            logger.error('paymentCron_batchEndCheckerCron - PurchasedCoursePackage.find - Error - ' + err);
                        }
                        if(purchaseDb) {
                            purchaseDb.forEach(each => {
                                purchaseIdsToRemove.push(each._id)
                            })
                        }
                    })
                    StudentPayable.deleteMany({$and: [{ownerId:batch.ownerId},{batch:batch._id}]}).exec((err, deleteResponse) => {
                        if(err) {
                            logger.error('paymentCron_batchEndCheckerCron - StudentPayable.findOneAndDelete - Error - ' + err);
                        }
                        if(deleteResponse) {
                            OrganizationPaymentAccount.findOneAndUpdate({ownerId:batch.ownerId},{$pull: { studentPayable: {$in : payableIdsToRemove} }}).exec((err, result) => {
                                if(err) {
                                    logger.error('paymentCron_batchEndCheckerCron - OrganizationPaymentAccount.findOneAndUpdate - Error - ' + err);
                                }
                                if(result) {
                                    PurchasedCoursePackage.deleteMany({$and:[{ownerId:batch.ownerId},{batch:batch._id}]}).then(response => {
                                        if(response) {
                                            StudentPaymentAccount.updateMany({$and:[{ownerId:batch.ownerId},{student: {$in: studentsArr}}]},{$pull: { purchasedPackage: {$in : purchaseIdsToRemove} }}).then(response => {
                                                logger.info('paymentCron_batchEndCheckerCron -  StudentPaymentAccount.updateMany - Success' + response)
                                            }).catch(err => {
                                                logger.error('paymentCron_batchEndCheckerCron -  StudentPaymentAccount.updateMany - Error - ' + err);
                                            })
                                        }
                                    }).catch(err => {
                                        logger.error('paymentCron_batchEndCheckerCron - PurchasedCoursePackage.deleteMany - Error - ' + err);
                                    })
                                }
                            })
                        }
                    })
                }
            })
        }
    })
}


exports.validateReferralCode = (ownerId, entityId, accountType, referralCode, callback) => {
    const Model = accountType === "Tutor" ? Tutor : Organization;
    const decodedReferral = decryptWithAES(referralCode);
    console.log(decodedReferral)
    Model.findOne({$and:[{ownerId:ownerId},{_id:entityId}]}).exec((err, userDb) => {
        if(err) {
            logger.error('validateReferralCode - Tutor : Organization Model.findOne - Error - ' + err);
            return callback(err)
        }
        if(userDb) {
            if(new Date() < userDb.canUseReferralTill) {
                if(userDb.isReferralCodeUsed === false) {
                    const refererUserAccountType = decodedReferral.substring(0, decodedReferral.indexOf('_')); 
                    const refererEntityId = decodedReferral.substring(decodedReferral.indexOf('_')+1, decodedReferral.length);
                    console.log(refererUserAccountType, refererEntityId)
                    const RefererUser = refererUserAccountType === "Tutor" ? Tutor : Organization;
                    RefererUser.findById(refererEntityId).exec((err, referer) => {
                        console.log(entityId, refererEntityId)
                        if(err) {
                            logger.error('validateReferralCode - Referer Tutor : Organization Model.findOne - Error - ' + err);
                            return callback(err)
                        }
                        if(referer && refererEntityId.toString() !== entityId.toString()) {
                            OrganizationPaymentAccount.findOne({$and:[{ownerId:referer.ownerId},{entityId:refererEntityId}]}).exec((err, paymentAcc) => {
                                if(err) {
                                    logger.error('validateReferralCode - Referer Tutor : Organization OrganizationPaymentAccount.findOne - Error - ' + err);
                                    return callback(err)
                                }
                                if(paymentAcc) {
                                    OrganizationPaymentAccount.findOneAndUpdate({$and:[{ownerId:referer.ownerId},{entityId:refererEntityId}]},{$inc: {totalcreditBalance:process.env.REFERRAL_CREDIT}}).exec((err, response) => {
                                        if(err) {
                                            logger.error('validateReferralCode - Referer Tutor : Organization OrganizationPaymentAccount.findOneAndUpdate - Error - ' + err);
                                            return callback(err)
                                        }
                                        userDb.isReferralCodeUsed = true;
                                        userDb.save().then(response => {
                                            return callback(null, ResponseCode.Success)
                                        }).catch(err => {
                                            logger.error('validateReferralCode - User Tutor : Organization Model.save - Error - ' + err);
                                            return callback(err)
                                        })
                                    })
                                }
                            })
                        }else {
                            // invalid ReferralCode
                            logger.warn('validateReferralCode - Invalid Referral Code - Error - ' + err);
                            return callback(null, ResponseCode.InvalidReferralCode)
                        }
                    })
                }else {
                    logger.warn('validateReferralCode - Referral already used - Error - ' + err);
                    return callback(null, ResponseCode.ReferralAlreadyUsed)
                }
            }else {
                logger.warn('validateReferralCode - Referral Time exceeded - Error - ' + err);
                return callback(null, ResponseCode.ReferralTimeExceeded)
            }
        }
    })
}


exports.getMasterPackages = (ownerId, callback) => {
    MasterPackage.find().exec((err, packagesArr) => {
        if(err) {
            logger.error('getMasterPackages MasterPackage.find - Error - ' + err);
            return callback(err)
        }
        return callback(null, ResponseCode.Success, packagesArr)
    })
}


exports.payments_validateOfflinePayment = (ownerId, entityId, accountType, amount, purchasedCoursePackageId, paymentcycle, callback) => {
    PurchasedCoursePackage.findOne({$and: [{ownerId:ownerId}, {_id: purchasedCoursePackageId}]}).populate("batch").exec((err, purchasedDoc) => {
        if(err) {
            logger.error('payments_validateOfflinePayment PurchasedCoursePackage.findOne - Error - ' + err);
            return callback(err)
        }
        if(!purchasedDoc) {
            logger.warn('payments_validateOfflinePayment PurchasedCoursePackage.findOne - Warning - ' + JSON.stringify(purchasedDoc));
            return callback(null, ResponseCode.PurchasedCoursePackageAlreadyExists)
        }
        if(purchasedDoc) {
            logger.info('payments_validateOfflinePayment PurchasedCoursePackage.findOne - PurchasedCoursePackage retrieved:  PurchasedCoursePackage - ' + JSON.stringify(purchasedDoc));
            // Check if payment already made
            if(purchasedDoc.paymentComplete === true) {
                logger.error('payments_validateOfflinePayment Discrepency in payment payment already cleared - Error - ' + err);
                return callback(null, ResponseCode.PaymentAlreadyCompleted)
            }
            const currentPaymentPlan = purchasedDoc.paymentPlans.find(ele => {
                return ele.paymentcycle === paymentcycle
            })
            const batchEndDate = purchasedDoc.batch.batchenddate

            const calculatedParams = renewalCalculator(purchasedDoc.renewalDate,currentPaymentPlan,batchEndDate)
            const paymentComplete = calculatedParams.isFullfilled;
            const currentGracePeriod = calculatedParams.newgrace;
            const renewalDate = calculatedParams.renewalDate;
            const orderId = `${entityId}${shortid.generate()}`;
            const newPaymentOrder = new PaymentOrder({
                ownerId: ownerId,
                entityId: entityId,
                orderId: orderId,
                accountType: "Student",
                purchasedPackageId: purchasedCoursePackageId,
                batchId: purchasedDoc.batch,
                paymentcycle: paymentcycle,
                entity: "order",
                amount: amount,
                amount_paid: amount,
                currency: "INR",
                status: "paid",
                attempts: 1,
                paymentType: "OFFLINE",
                created_at: new Date()
            })
            newPaymentOrder.save().then(processedOrder => {
                PurchasedCoursePackage.findOneAndUpdate({$and: [{_id:purchasedDoc._id},{ownerId:ownerId}]},{paymentComplete:paymentComplete,currentGracePeriod:currentGracePeriod,renewalDate:renewalDate, $push:{paymentTransactions:processedOrder._id}},{new: true}).exec((err, response) => {
                    if(err) {
                        logger.error('payments_authenticatePaymentSuccess PurchasedCoursePackage.findOneAndUpdate - Error - ' + err);
                        return callback(err)
                    }
                    // If student account is deactivated activate it
                    Student.findOne({$and: [{ownerId:ownerId}, {_id:purchasedDoc.studentId}]}).exec((err, userStudent) => {
                        if(userStudent.roleStatus === 'DEACTIVATE'){
                            userStudent.roleStatus = 'ACTIVE';
                            userStudent.save().catch(err => {
                                logger.error('payments_authenticatePaymentSuccess Student.findOne and save Student REACTIVATION - Error - ' + err);
                            })
                        }
                    })
                    return callback(null, ResponseCode.Success)
                })
            }).catch(err => {
                logger.error('payments_validateOfflinePayment PaymentOrder.save - Error - ' + err);
                return callback(err)
            })
        }   
    })
}


exports.payments_updatePayOrders = (orderArr, callback) => {
    orderArr.map(eachOrder => {
        OrganizationPaymentOrder.findOne({orderId:eachOrder.orderId}).exec((err, payorder) => {
            if(err) {
                logger.error('payments_updatePayOrders OrganiztionPaymentOrder.findOne - Error - ' + err);
                return callback(err)
            }
            if(payorder && eachOrder.amount === payorder.amount) {
                // Confirm calculations
                const amount = payorder.amount
                const total_convenience_fee = (2.5*amount)/100
                const convenience_fee = total_convenience_fee - (eachOrder.gatewayServiceCharge + eachOrder.serviceChargeTax)
                const convenience_fee_tax = (18*convenience_fee)/100
                payorder.gatewayServiceCharge = eachOrder.gatewayServiceCharge
                payorder.serviceChargeTax = eachOrder.serviceChargeTax
                payorder.settlementAmount = eachOrder.settlementAmount
                payorder.settlementUTR = eachOrder.settlementUTR
                payorder.convenience_fee = convenience_fee
                payorder.convenience_fee_tax = convenience_fee_tax
                payorder.edumatica_earning = eachOrder.settlementAmount - (convenience_fee + convenience_fee_tax)
                payorder.edumatica_earning_tax = (18*(eachOrder.settlementAmount - (convenience_fee + convenience_fee_tax)))/100
                payorder.tds = 0
                payorder.save().then(success => {

                }).catch(err => {
                    
                })
            }
            if(!payorder) {
                PaymentOrder.findOne({orderId:eachOrder.orderId}).exec((err, payorderStudent) => {
                    if(err) {
                        logger.error('payments_updatePayOrders PaymentOrder.findOne - Error - ' + err);
                        return callback(err)
                    }
                    if(payorderStudent && eachOrder.amount === payorderStudent.amount) {
                        const amount = payorderStudent.amount
                        const amountToEdumac = payorderStudent.studentPayableAmt
                        const total_convenience_fee = (2.5*amount)/100
                        const total_convenience_fee_tax = (18*total_convenience_fee)/100
                        const convenience_fee = total_convenience_fee - eachOrder.gatewayServiceCharge + eachOrder.serviceChargeTax
                        const convenience_fee_tax = (18*convenience_fee)/100
                        const amountToBene = amount - (amountToEdumac + total_convenience_fee + total_convenience_fee_tax)
                        const tds = (10*amountToBene)/100
                        payorderStudent.gatewayServiceCharge = eachOrder.gatewayServiceCharge
                        payorderStudent.serviceChargeTax = eachOrder.serviceChargeTax
                        payorderStudent.settlementAmount = eachOrder.settlementAmount
                        payorderStudent.settlementUTR = eachOrder.settlementUTR
                        payorderStudent.convenience_fee = convenience_fee
                        payorderStudent.convenience_fee_tax = convenience_fee_tax
                        payorderStudent.edumatica_earning = amountToEdumac
                        payorderStudent.edumatica_earning_tax = (18*amountToEdumac)/100
                        payorderStudent.tds = tds
                        payorderStudent.netAmountToBene = amountToBene - tds
                        payorderStudent.save().then(success => {

                        }).catch(err => {
                            
                        })
                    }
                })
            }
        })
    })
}


exports.payments_paymentStatus = (ownerId, entityId, accountType, callback) => {
    const OrderModel = accountType === "Student" ? PaymentOrder : OrganizationPaymentOrder
    OrderModel.find({$and: [{ownerId:ownerId}, {entityId:entityId}]}).exec((err, orderDb) => {
        if(err) {

        }
        if(orderDb) {
            const length = orderDb.length
            const newestOrder = orderDb[length]
            const orderId = newestOrder.orderId
            const appId = process.env.CASHFREE_APP_ID
            const secretKey = process.env.CASHFREE_SECRET
            var data = new FormData();
            const newData = {
            appId:appId,
            secretKey:secretKey,
            orderId:orderId,
            }
            Object.keys(newData).map(el => {
            data.append(el, newData[el])
            })

            var config = {
            method: 'post',
            url: 'https://test.cashfree.com/api/v1/order/info/status',
            headers: { 
                ...data.getHeaders()
            },
            data : data
            };

            axios(config)
            .then(function (result) {
                const res = JSON.stringify(result.data);
                const response = JSON.parse(res)

                if (response.txStatus === "SUCCESS" && newestOrder.status === "paid") {

                }
                if (response.txStatus !== "SUCCESS" && newestOrder.status !== "paid") {

                }
                if (response.txStatus === "SUCCESS" && newestOrder.status !== "paid") {

                    const txObj = {
                        orderId: orderId,
                        orderAmount: response.orderAmount,
                        referenceId: response.referenceId,
                        txStatus: response.txStatus,
                        paymentMode: response.paymentMode,
                        txMsg: response.txMsg,
                        txTime: response.txTime,
                    };

                    const crypto = require('crypto')

                    const secretKey = process.env.CASHFREE_SECRET;
                    const sortedkeys = Object.keys(txObj);
                    var signatureData = "";
                
                    for (var i = 0; i < sortedkeys.length; i++) {
                        var k = sortedkeys[i];
                        if(k === "signature") continue
                        signatureData +=txObj[k];
                    }
                    var signature = crypto.createHmac('sha256',secretKey).update(signatureData).digest('base64');

                    axios.post(`${process.env.BASE_URL}/paymenthooks/payorder/authenticate`, {
                        orderId: orderId,
                        orderAmount: response.orderAmount,
                        referenceId: response.referenceId,
                        txStatus: response.txStatus,
                        paymentMode: response.paymentMode,
                        txMsg: response.txMsg,
                        txTime: response.txTime,
                        signature: signature,
                    })
                }
            })
            .catch(function (err) {
                logger.error('payments_paymentStatus PaymentOrder.status - Error - ' + err);
                return callback(err)
            });
        }
    })
}


exports.payments_checkStatus = (ownerId, entityId, orderId, callback) => {
    const appId = process.env.CASHFREE_APP_ID
    const secretKey = process.env.CASHFREE_SECRET
    
    var data = new FormData();
    const newData = {
    appId:appId,
    secretKey:secretKey,
    orderId:orderId,
    }
    Object.keys(newData).map(el => {
    data.append(el, newData[el])
    })

    var config = {
    method: 'post',
    url: 'https://test.cashfree.com/api/v1/order/info/status',
    headers: { 
        ...data.getHeaders()
    },
    data : data
    };

    axios(config)
    .then(function (response) {
        const res = JSON.stringify(response.data);
        callback(null, ResponseCode.Success, JSON.parse(res))
    })
    .catch(function (err) {
        logger.error('payments_checkStatus PaymentOrder.status - Error - ' + err);
        return callback(err)
    });
}