const logger = require("../utils/logger.utils");
const { ResponseCode } = require("../utils/response.utils");
const mongoose = require("mongoose");
const NotificationMessage = require("../models/notificationMessage.model")

exports.notification_message = (type, mobileNo, msg, response) => {
        //console.log(response)
        var newMessageNotificationID = new mongoose.Types.ObjectId()
        if(response.status === "success") {
          logger.debug("notificationMessage.dbc success")
            var newNotification = new NotificationMessage({
                _id: newMessageNotificationID,
                messageType: type,
                messageBatch_id: response.batch_id,
                recipient: mobileNo,
                message: msg,
                status: response.status,
                code: 0
              });
        }
        if(response.status === "failure"){
          logger.debug("notificationMessage.dbc failure")
            var newNotification = new NotificationMessage({
                _id: newMessageNotificationID,
                messageType: type,
                recipient: mobileNo,
                message: msg,
                status: response.status,
                code: response.errors.code
              });
        }
        if(response.status === "Internal Error") {
          logger.debug("notificationMessage.dbc Internal Error")
            var newNotification = new NotificationMessage({
                _id: newMessageNotificationID,
                messageType: type,
                recipient: mobileNo,
                message: msg,
                status: response.status,
              });
        }
        
          newNotification.save().then(function() {
            NotificationMessage.findOne({_id:newMessageNotificationID}).then(response => {
                logger.info("Notification saved for Audit" + response)
            })
          })
}