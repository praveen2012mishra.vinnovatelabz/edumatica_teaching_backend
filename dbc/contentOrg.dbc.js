const Content = require('../models/content.model');
const QuizDetail = require('../models/quizDetail.model');
const ContentType_Quiz = 'quiz';
const Chapter = require('../models/chapter.model');
const logger = require('../utils/logger.utils');
const Organization = require('../models/organization.model');
const { deleteObject } = require('../utils/aws.utils');
const { ResponseCode } = require('../utils/response.utils');

exports.content_create = (ownerId, userId, contentReq, callback)=>{
    Chapter.findOne({$and: [{ownerId : ownerId}, {boardname : contentReq.boardname}, 
        {classname : contentReq.classname}, {subjectname: contentReq.subjectname},
         {chaptername: contentReq.chaptername}]})
         .exec( function (err, chapter) {
        if (err) {
            logger.error('content_create - Chapter.findOne - Error - ' + err);
            return callback(err);
        }
        if(chapter == null)  {
            let chapter = new Chapter(
                {
                    ownerId : ownerId,
                    boardname: contentReq.boardname,
                    classname: contentReq.classname,
                    subjectname: contentReq.subjectname,           
                    chaptername: contentReq.chaptername,
                    status: 1,
                    updatedon: new Date(),
                    updatedby: userId
                }
            );
            chapter.save(function (err) {
                if (err) {
                    logger.error('content_create - chapter.save - Error - ' + err);
                    return callback(err);
                }
                logger.info('content_create - ownerId:' + ownerId
                + ', boardname:' + contentReq.boardname + ', classname:' + contentReq.classname
                + ', subjectname:' + contentReq.subjectname + ', chaptername:' + contentReq.chaptername
                + ', Chapter saved successfully');                                            
                Content.findOne({$and: [{ownerId : ownerId},
                    {contentname : contentReq.contentname}]})
                    .exec( function (err, content) {
                     if (err){
                        logger.error('content_create - Content.findOne - Error - ' + err);
                        return callback(err);
                     }
                     if(content == null) {
                        if(contentReq.contenttype == ContentType_Quiz
                            && contentReq.questions != null){
                            let quizDetailsId = [];
                            let quizDetails = [];
                            let questions = contentReq.questions;
                            var serialNo = 1;
                            questions.forEach(question => {
                                let quizDetail = new QuizDetail({
                                        serialNo: serialNo++,
                                        questiontext: question.questiontext, 
                                        option1: question.option1, 
                                        option2: question.option2,
                                        option3: question.option3,
                                        option4: question.option4,
                                        answer: question.answer,
                                        answerDescription: question.answerDescription,
                                        status: 1,
                                        updatedon: new Date(),
                                        updatedby: userId
                                });
                                quizDetailsId.push(quizDetail._id);
                                quizDetails.push(quizDetail);
                            });
        
                            QuizDetail.insertMany(quizDetails,(err,data)=>{
                                if(err){
                                    logger.error('content_create - QuizDetail.insertMany - Error - ' + err);
                                    return callback(err);
                                }
        
                                let content = new Content({
                                    ownerId : ownerId,
                                    chapter: chapter._id,
                                    contentname: contentReq.contentname,
                                    uuid: contentReq.uuid,
                                    contenttype: contentReq.contenttype,
                                    title: contentReq.title,
                                    description: contentReq.description,
                                    tags: contentReq.tags,
                                    duration: contentReq.duration,
                                    marks: contentReq.marks,
                                    questions: quizDetailsId,
                                    status: 1,
                                    updatedon: new Date(),
                                    updatedby: userId
                                });
                                content.save((err)=>{
                                    if(err){
                                        logger.error('content_create - content.save - Error - ' + err);
                                        return callback(err);
                                    }
                                    else {                
                                        Chapter.findOneAndUpdate({"_id":chapter._id},
                                        {$push: {contents: content._id}},(err,data)=>{
                                            if(err){
                                                logger.error('content_create - Error - ' + err);
                                                return callback(err);
                                            }
                                            logger.info('content_create - ownerId:' + ownerId
                                            + ', contentname:' + contentReq.contentname
                                            + ', Content saved successfully');
                                            callback(null, ResponseCode.Success);
                                        });
                                        // Push above content id in chapter                 
                                    }        
                                })
                            });
                        }
                        else{
                            let newContent = {
                                ownerId : ownerId,
                                chapter: chapter._id,
                                contentname: contentReq.contentname,
                                uuid: contentReq.uuid,
                                title: contentReq.title,
                                description: contentReq.description,
                                contenttype: contentReq.contenttype,
                                contentlength: contentReq.contentlength,
                                thumbnail:contentReq.thumbnail,
                                status: 1,
                                updatedon: new Date(),
                                updatedby: userId
                            };

                            const allowSubtype = (
                                contentReq.contenttype === 'pdf' || contentReq.contenttype === 'document' || 
                                contentReq.contenttype === 'image' || contentReq.contenttype === 'video' || contentReq.contenttype==='embed-link'
                            )

                            const thumbnailType =(
                                contentReq.contenttype === 'image' || contentReq.contenttype === 'video'
                            )
                            
                            if(allowSubtype && contentReq.contentsubtype) {
                                newContent = {...newContent, ...{contentsubtype: contentReq.contentsubtype}}
                            
                                if(thumbnailType){
                                    newContent = {...newContent,...{thumbnail: contentReq.thumbnail}}
                                }
                            }
                            const content = new Content(newContent);
                            content.save((err)=>{
                                if(err){
                                    logger.error('content_create - content.save - Error - ' + err);
                                    return callback(err);
                                }
                                else {                
                                    Chapter.findOneAndUpdate({"_id":chapter._id},
                                        {$push: {contents: content._id}},(err,data)=>{
                                        if(err){
                                            logger.error('content_create - Chapter.findOneAndUpdate - Error - ' + err);
                                            return callback(err);
                                        }
                                        if(contentReq.contenttype === 'embed-link') {
                                            logger.info('content_create - ownerId:' + ownerId
                                                + ', contentname:' + contentReq.contentname
                                                + ', Content saved successfully');
                                            return callback(null, ResponseCode.Success);
                                        } else {
                                            Organization.findOne({ownerId : ownerId},(err, organization)=>{
                                                if(err){
                                                    logger.error('content_create - Organization.findOne - Error - ' + err);
                                                    return callback(err);
                                                }
                                                organization.contentSize = organization.contentSize + contentReq.contentlength;
                                                organization.save((err)=>{
                                                    if(err){
                                                        logger.error('content_create - organization.save - Error - ' + err);
                                                        return callback(err);
                                                    }
                                                    logger.info('content_create - ownerId:' + ownerId
                                                    + ', contentname:' + contentReq.contentname
                                                    + ', Content saved successfully');
                                                    return callback(null, ResponseCode.Success);
                                                })                                        
                                            });
                                        }                                      
                                    });          
                                }        
                            })
                        }
                    }
                    else{
                        logger.warn('content_create - ownerId:' + ownerId
                        + ', contentname:' + contentReq.contentname
                        + ', Content already exists');
                        return callback(null, ResponseCode.ContentAlreadyExists);
                    }
                })
            })
        }
        else{
            Content.findOne({$and: [{ownerId : ownerId},
                {contentname : contentReq.contentname}]})
                .exec( function (err, content) {
                 if (err){
                    logger.error('content_create - Content.findOne - Error - ' + err);
                     return callback(err);
                 }
                 if(content == null) {
                    if(contentReq.contenttype == ContentType_Quiz
                        && contentReq.questions != null){
                        let quizDetailsId = [];
                        let quizDetails = [];
                        let questions = contentReq.questions;
                        var serialNo = 1;
                        questions.forEach(question => {
                            let quizDetail = new QuizDetail({
                                    serialNo: serialNo++,
                                    questiontext: question.questiontext, 
                                    option1: question.option1, 
                                    option2: question.option2,
                                    option3: question.option3,
                                    option4: question.option4,
                                    answer: question.answer,
                                    status: 1,
                                    updatedon: new Date(),
                                    updatedby: userId
                            });
                            quizDetailsId.push(quizDetail._id);
                            quizDetails.push(quizDetail);
                        });
    
                        QuizDetail.insertMany(quizDetails,(err,data)=>{
                            if(err){
                                logger.error('content_create - QuizDetail.insertMany - Error - ' + err);
                                return callback(err);
                            }
    
                            let content = new Content({
                                ownerId : ownerId,
                                chapter: chapter._id,
                                contentname: contentReq.contentname,
                                uuid: contentReq.uuid,
                                contenttype: contentReq.contenttype,
                                title: contentReq.title,
                                description: contentReq.description,
                                tags: contentReq.tags,
                                duration: contentReq.duration,
                                marks: contentReq.marks,
                                questions: quizDetailsId,
                                status: 1,
                                updatedon: new Date(),
                                updatedby: userId
                            });
                            content.save((err)=>{
                                if(err){
                                    logger.error('content_create - content.save - Error - ' + err);
                                    return callback(err);
                                }
                                else {                
                                    Chapter.findOneAndUpdate({"_id":chapter._id},{$push: {contents: content._id}},(err,data)=>{
                                        if(err){
                                            logger.error('content_create - Chapter.findOneAndUpdate - Error - ' + err);
                                            return callback(err);
                                        }
                                        logger.info('content_create - ownerId:' + ownerId
                                            + ', contentname:' + contentReq.contentname
                                            + ', Content saved successfully');
                                        callback(null, ResponseCode.Success);
                                    });
                                }        
                            })
                        });
                    }
                    else{
                        let newContent = {
                            ownerId : ownerId,
                            chapter: chapter._id,
                            contentname: contentReq.contentname,
                            uuid: contentReq.uuid,
                            title: contentReq.title,
                            description: contentReq.description,
                            contenttype: contentReq.contenttype,
                            contentlength: contentReq.contentlength,
                            thumbnail:contentReq.thumbnail,
                            status: 1,
                            updatedon: new Date(),
                            updatedby: userId
                        };
                        const allowSubtype = (
                            contentReq.contenttype === 'pdf' || contentReq.contenttype === 'document' || 
                            contentReq.contenttype === 'image' || contentReq.contenttype === 'video' || contentReq.contenttype==='embed-link'
                        )

                        const thumbnailType =(
                            contentReq.contenttype === 'image' || contentReq.contenttype === 'video'
                        )
                        
                        if(allowSubtype && contentReq.contentsubtype) {
                            newContent = {...newContent, ...{contentsubtype: contentReq.contentsubtype}}
                            
                            if(thumbnailType){
                                newContent = {...newContent,...{thumbnail: contentReq.thumbnail}}
                            }
                        }
                        const content = new Content(newContent)
                        
                        content.save((err)=>{
                            if(err){
                                logger.error('content_create - content.save - Error - ' + err);
                                return callback(err);
                            }
                            else {                
                                Chapter.findOneAndUpdate({"_id":chapter._id},
                                    {$push: {contents: content._id}},(err,data)=>{
                                    if(err){
                                        logger.error('content_create - Chapter.findOneAndUpdate - Error - ' + err);
                                        return callback(err);
                                    }
                                    if(contentReq.contenttype === 'embed-link') {
                                        logger.info('content_create - ownerId:' + ownerId
                                            + ', contentname:' + contentReq.contentname
                                            + ', Content saved successfully');
                                        return callback(null, ResponseCode.Success);
                                    } else {
                                        Organization.findOne({ownerId : ownerId},(err, organization)=>{
                                            if(err){
                                                logger.error('content_create - Organization.findOne - Error - ' + err);
                                                return callback(err);
                                            }
                                            organization.contentSize = organization.contentSize + contentReq.contentlength;
                                            organization.save((err)=>{
                                                if(err){
                                                    logger.error('content_create - organization.save - Error - ' + err);
                                                    return callback(err);
                                                }
                                                logger.info('content_create - ownerId:' + ownerId
                                                + ', contentname:' + contentReq.contentname
                                                + ', Content saved successfully');
                                                return callback(null, ResponseCode.Success);
                                            })                                        
                                        });
                                    }
                                });          
                            }        
                        })
                    }
                }
                else{
                    logger.warn('content_create - ownerId:' + ownerId
                    + ', contentname:' + contentReq.contentname
                    + ', Content already exists');
                    return callback(null, ResponseCode.ContentAlreadyExists);
                }
            })
        }        
    });
}

exports.content_delete = (ownerId, contentname, callback)=> {
    Content.findOne({$and: [{ownerId : ownerId},
        {contentname : contentname}]}).populate('chapter')
        .select({"_id": 0,"__v":0}).exec( function (err, content) {
        if (err) {
            logger.error('content_delete - Content.findOneAndDelete - Error - ' + err);
            return callback(err);
        }
        if(content != null){
            if(content.contenttype == 'pdf'
            || content.contenttype == 'video'
            || content.contenttype == 'image'
            || content.contenttype == 'document'){
                var folder = content.chapter.boardname + '-' 
                                + content.chapter.classname + '-' 
                                + content.chapter.subjectname + '-'
                                + content.chapter.chaptername;
                var fileName = ownerId + '/' + folder + '/' + content.uuid;

                deleteObject(fileName, (err, code)=>{
                    if(err){
                        logger.error('content_delete - Error - ' + err);
                        return next(err);
                    }
                    logger.info('content_delete - ownerId:' + ownerId
                    + ', contentname:' + contentname
                    + ', Content deleted from S3.');
                });
            }
            Content.findOneAndDelete({$and: [{ownerId : ownerId},
                {contentname : contentname}]}, function (err) {
                if (err) {
                    logger.error('content_delete - Content.findOneAndDelete - Error - ' + err);
                    return callback(err);
                }
                if(content.contenttype === 'embed-link') {
                    logger.info('content_delete - ownerId:' + ownerId
                            + ', contentname:' + contentname
                            + ', Content deleted successfully');
                    callback(null, ResponseCode.Success)
                }  else {
                    Organization.findOne({ownerId : ownerId},(err, organization)=>{
                        if(err){
                            logger.error('content_delete - Organization.findOne - Error - ' + err);
                            return callback(err);
                        }
                        organization.contentSize = organization.contentSize - content.contentlength;
                        organization.save((err)=>{
                            if(err){
                                logger.error('content_delete - organization.save - Error - ' + err);
                                return callback(err);
                            }
                            logger.info('content_delete - ownerId:' + ownerId
                            + ', contentname:' + contentname
                            + ', Content deleted successfully');
                            callback(null, ResponseCode.Success)
                        })                                        
                    });
                }
                
            })
        }
        else{
            logger.warn('content_delete - ownerId:' + ownerId
                    + ', contentname:' + contentname
                    + ', Content not found');
                callback(null, ResponseCode.ContentNotFound)
        }
    })
};
