const Schedule = require('../models/schedule.model');
const Session = require('../models/session.model');
const StudentAttendance = require('../models/studentAttendance.model');
const Batch = require('../models/batch.model');
const Student = require('../models/student.model');
const Parent = require('../models/parent.model');
const logger = require('../utils/logger.utils');
const uuid = require('uuid');
const { ResponseCode } = require('../utils/response.utils');
const Tutor = require('../models/tutor.model');
const mongoose = require('mongoose');

function timeToDecimal(t) {
    var arr = t.split(':');
    var dec = parseInt((arr[1]/6)*10, 10);

    return parseFloat(parseInt(arr[0], 10) + '.' + (dec<10?'0':'') + dec);
}

exports.saveSchedulesInBatch = (ownerId, userId, batchReq, callback)=>{
    Tutor.findOne({ownerId : ownerId}).populate('package').exec((err, tutor)=>{
        if(err){
            logger.error('saveSchedulesInBatch - Tutor.findOne - Error - ' + err);
            return next(err);
        }
        if(batchReq.schedules.length + tutor.scheduleList.length <= tutor.package.sessionCount){
            Batch.findOne({$and: [{ownerId : ownerId}, 
                {batchfriendlyname: batchReq.batchfriendlyname}] })
                .exec( function (err, batch) {
                if (err) {
                    logger.error('saveSchedulesInBatch - Batch.findOne - Error - ' + err);
                    return callback(err);
                }
                if(batch == null)  {
                    logger.warn('saveSchedulesInBatch - ownerId:' + ownerId
                                            + ' No batch found');
                    return callback(null, ResponseCode.BatchNotFound); 
                }
                else {
                    Schedule.find({ownerId : ownerId})
                    .exec( function (err, dbSchedules) {
                        if (err) {
                            logger.error('saveSchedulesInBatch - Schedule.find - Error - ' + err);
                            return callback(err);
                        }
                        let scheduleOverlaps = false;
                        let schedulesArray = batchReq.schedules;
                        let scheduleIds = [];
                        let schedules = [];
                        let msg = '';
                        for (var i = 0; i < schedulesArray.length; i++){
                                let a = timeToDecimal(schedulesArray[i].fromhour);
                                let b = timeToDecimal(schedulesArray[i].tohour);
                                for (var j = 0; j < dbSchedules.length; j++){
                                let x = timeToDecimal(dbSchedules[j].fromhour);
                                let y = timeToDecimal(dbSchedules[j].tohour);
                                if (schedulesArray[i].dayname == dbSchedules[j].dayname) {
                                    if (!((a  < x && b <= x) || (y <= a && y < b))){
                                            scheduleOverlaps = true;
                                            msg = 'Invalid Schedule- dayname:' 
                                            + schedulesArray[i].dayname + ' fromhour:' + schedulesArray[i].fromhour 
                                            + ' tohour:' + schedulesArray[i].tohour 
                                            + ' overlaps with existing schedule dayname:' 
                                            + dbSchedules[j].dayname + ' fromhour:' + dbSchedules[j].fromhour 
                                            + ' tohour:' + dbSchedules[j].tohour;
                                            break;
                                    }
                                }
                            }
                        }
                        
                        if(scheduleOverlaps == false){
                            schedulesArray.forEach(schedule => {
                                let dbSchedule = new Schedule(
                                {
                                    ownerId : ownerId,
                                    tutorId: batch.tutorId,
                                    batch: batch._id, 
                                    dayname: schedule.dayname,
                                    fromhour: schedule.fromhour,
                                    tohour: schedule.tohour,
                                    status: 1,
                                    updatedon: new Date(),
                                    updatedby: userId
                                });
                                schedules.push(dbSchedule);
                                scheduleIds.push(dbSchedule._id); 
                            })
                            Schedule.insertMany(schedules,function (err,data) {
                                if (err) {
                                    logger.error('saveSchedulesInBatch - Schedule.insertMany - Error - ' + err);
                                    return callback(err);
                                }
                                tutor.scheduleList = tutor.scheduleList.concat(scheduleIds);
                                tutor.save((err)=>{
                                    if(err){
                                        logger.error('saveSchedulesInBatch - tutor.save - Error - ' + err);
                                        return callback(err);
                                    }
                                    logger.info('saveSchedulesInBatch - ownerId:' + ownerId
                                    + ' Schedule(s) Created successfully');
                                    return callback(null, ResponseCode.Success);
                                });
                            })   
                        }
                        else{
                            logger.warn('saveSchedulesInBatch - ownerId:' + ownerId
                                            + ' ' + msg);
                            return callback(null, ResponseCode.SchedulesOverlap, msg); 
                        }
                    });
                } 
            });
        }
        else{
            logger.warn('saveSchedulesInBatch - ownerId:' + ownerId
                            + ' Tutor Schedule limit exceeded');
            return callback(null, ResponseCode.ScheduleLimitExceeded); 
        }
    });
}

exports.getSchedules = (ownerId, callback)=>{
    Schedule.find({ownerId : ownerId}).select({"__v":0})
    .populate('batch')
    .populate('tutorId')
    .exec( function (err, schedules) {
        if (err) {
            logger.error('getSchedules - Schedule.find - Error - ' + err);
            return callback(err);
        }
        logger.info('getSchedules - ownerId:' + ownerId
        + ' Tutor Schedules(s) retrieved successfully');
        callback(null, ResponseCode.Success, schedules);
    });
}

exports.getStudentSchedules = (entityId, callback)=>{
    Student.findOne({_id : entityId}).exec((err,student)=>{
        if(err){
            logger.error('getStudentSchedules - Student.findOne - Error - ' + err);
            return callback(err);
        }
        if(student == null){
            logger.warn('getStudentSchedules - entityId:' + entityId
                                    + ' No student found');
            return callback(null, ResponseCode.StudentNotFound);
        }
        if(student.batches.length <=0){
            logger.warn('getStudentSchedules - entityId:' + entityId
                                    + ' No batch found');
            return callback(null, ResponseCode.Success, [])
        }

        Schedule.find({batch: {$in: student.batches}})
        .select({"_id": 0,"__v":0})
        .populate('batch').populate('tutorId','tutorName').exec((err,schedules)=>{
            if(err){
                logger.error('getStudentSchedules - Schedule.find - Error - ' + err);
                return callback(err);
            }
            logger.info('getStudentSchedules - entityId:' + entityId
            + ' Student Schedules(s) retrieved successfully');
            callback(null, ResponseCode.Success, schedules);
        })            
    })
}

exports.getParentSchedules = (entityId, studentId, callback)=>{
    Parent.findOne({_id : entityId}).exec((err, parent)=>{
        if(err){
            logger.error('getParentSchedules - Parent.findOne - Error - ' + err);
            return callback(err);
        }
        if(parent != null){
            Student.findOne({$and: [{parent : parent._id}, {_id: studentId}]}).exec((err, student)=>{
                if(err){
                    logger.error('getParentSchedules - Student.findOne - Error - ' + err);
                    return callback(err);
                }
                if(!student){
                    logger.warn('getParentSchedules - entityId:' + entityId
                                            + ' No student found');
                    return callback(null, ResponseCode.StudentNotFound);
                }
                
                // var allBatches = [];
                // students.forEach(student => {
                //     allBatches.push(student.batches);
                // })

                Schedule.find({batch: {$in: student.batches}})
                .select({"_id": 0,"__v":0})
                .populate('batch').exec((err, schedules)=>{
                    if(err){
                        logger.error('getParentSchedules - Schedule.find - Error - ' + err);
                        return callback(err);
                    }
                    logger.info('getParentSchedules - entityId:' + entityId
                    + ' Schedules(s) retrieved successfully');
                    callback(null, ResponseCode.Success, schedules);
                })
            })
        }
        else{
            logger.warn('getParentSchedules - entityId:' + entityId + ' Parent not found');
            callback(null, ResponseCode.ParentNotFound);
        }
    })
}

exports.getScheduleDetails = (ownerId, dayname, fromhour, callback)=>{
    Schedule.findOne({$and: [{ownerId : ownerId},
        {dayname: dayname},{fromhour: fromhour}]})
        .populate('batch').select({"__v":0}).exec( function (err, schedule) {
        if (err) {
            logger.error('getScheduleDetails - Schedule.findOne - Error - ' + err);
            return callback(err);
        }
        logger.info('getScheduleDetails - ownerId:' + ownerId
        + ', dayname:' + dayname + ' , fromhour:' + fromhour
        + ' Schedule retrieved successfully');
        callback(null, ResponseCode.Success, schedule);
    });
}

exports.getTutorRoom = (ownerId, tutorId, userId, dayname, fromhour, callback)=>{
    Schedule.findOne({$and: [{tutorId : tutorId},
        {dayname: dayname},{fromhour: fromhour}]})
        .exec( function (err, schedule) {
        if (err) {
            logger.error('getTutorRoom - Schedule.findOne - Error - ' + err);
            return callback(err);
        }
        if(schedule == null){
            logger.warn('getTutorRoom - ownerId:' + ownerId
            + ', dayname:' + dayname + ' , fromhour:' + fromhour
                                    + ' Schedule not found');
            return callback(null, ResponseCode.ScheduleNotFound);
        }
        
        var today = new Date();
        var date = today.toISOString().slice(0, 10);
        Session.findOne({$and: [{schedule:schedule._id},
            {date: date}]})
            .exec( function (err, session) {
            if (err) {
                logger.error('getTutorRoom - Session.findOne - Error - ' + err);
                return callback(err);
            }
            if(session != null) {
                logger.info('getTutorRoom - ownerId:' + ownerId
                + ', dayname:' + dayname + ' , fromhour:' + fromhour
                + ' Tutor session retrieved successfully');
                return callback(null, ResponseCode.Success, session.roomid);
            }
            var roomid = uuid.v4();
            let newsession = new Session({
                ownerId : ownerId,
                tutorId: tutorId,
                schedule: schedule._id,
                dayname: schedule.dayname, 
                fromhour: schedule.fromhour,
                tohour: schedule.tohour,
                date: date,
                roomid: roomid,
                status: 1,
                updatedon: new Date(),
                updatedby: userId
            })
            newsession.save(function (err) {
                if (err) {
                    logger.error('getTutorRoom - newsession.save - Error - ' + err);
                    return callback(err);
                }
                logger.info('getTutorRoom - ownerId:' + ownerId
                + ', dayname:' + dayname + ' , fromhour:' + fromhour
                + ' Tutor session created successfully');
                callback(null, ResponseCode.Success, roomid);
            });       
        });
    });
}

exports.getStudentRoom = (entityId, userId, dayname, fromhour, tutorId, callback)=>{
    Student.findOne({_id : entityId}).exec((err,student)=>{
        if(err){
            logger.error('getStudentRoom - Student.findOne - Error - ' + err);
            return callback(err);
        }
        if(student == null) {
            logger.warn('getStudentRoom - entityId:' + entityId
            + ', dayname:' + dayname + ' , fromhour:' + fromhour
                                    + ' Student not found');
            return callback(null, ResponseCode.StudentNotFound);
        }
        if(student.batches.length == 0) {
            logger.warn('getStudentRoom - entityId:' + entityId
            + ', dayname:' + dayname + ' , fromhour:' + fromhour
                                    + ' Student is not assigned any batch');
            return callback(null, ResponseCode.BatchNotFound);
        }    
        var today = new Date();      
        var date = today.toISOString().slice(0, 10);
        Session.findOne({$and: [{tutorId : tutorId},
            {dayname: dayname},
            {fromhour: fromhour}, {date: date}]})
            .exec( function (err, session) {
            if (err) {
                logger.error('getStudentRoom - Session.findOne - Error - ' + err);
                return callback(err);
            }
            if(session != null) {
                StudentAttendance.findOne({$and: [{student: student._id},
                    {session: session._id}]})
                    .exec( function (err, studentAttendance) {
                    if (err) {
                        logger.error('getStudentRoom - Session.findOne - Error - ' + err);
                        return callback(err);
                    }
                    if(studentAttendance == null) {
                        let newStudentAttendance = new StudentAttendance({
                            student : student._id,
                            session: session._id,
                            isPresent: true,
                            status: 1,
                            updatedon: new Date(),
                            updatedby: userId
                        })
                        newStudentAttendance.save(function (err) {
                            if (err) {
                                logger.error('getStudentRoom - newStudentAttendance.save - Error - ' + err);
                                return callback(err);
                            }                    
                            logger.info('getStudentRoom - entityId:' + entityId
                            + ', dayname:' + dayname + ' , fromhour:' + fromhour
                            + ' Student session retrieved successfully');
                            return callback(null, ResponseCode.Success, session.roomid);
                        });
                    }
                    else{
                        logger.info('getStudentRoom - entityId:' + entityId
                        + ', dayname:' + dayname + ' , fromhour:' + fromhour
                        + ' Student session retrieved successfully');
                        return callback(null, ResponseCode.Success, session.roomid);
                    }
                })
            }
            else{
                logger.warn('getStudentRoom - entityId:' + entityId
                + ', dayname:' + dayname + ' , fromhour:' + fromhour
                                    + ' Session not started');
                return callback(null, ResponseCode.SessionNotStarted);
            }
        }); 
       
    })
}

exports.getStudentAttendance = (entityId, callback)=>{
    Student.findOne({_id : entityId}).exec((err, student)=>{
        if(err){
            logger.error('getStudentAttendance - Student.findOne - Error - ' + err);
            return callback(err);
        }
        if(student == null) {
            logger.warn('getStudentAttendance - entityId:' + entityId
                                    + ' Student not found');
            return callback(null, ResponseCode.StudentNotFound);
        }
        StudentAttendance.find({student: student._id})
            .exec( function (err, studentAttendances) {
            if (err) {
                logger.error('getStudentAttendance - StudentAttendance.findOne - Error - ' + err);
                return callback(err);
            }               
            logger.info('getStudentAttendance - entityId:' + entityId
            + ' Student Attendance retrieved successfully');
            return callback(null, ResponseCode.Success, studentAttendances);                   
        })
    })
}

exports.schedule_update = (ownerId, userId, scheduleReq, callback)=> {
    Schedule.findOne({$and: [ {ownerId : ownerId},
        {_id: mongoose.Types.ObjectId(scheduleReq.scheduleId)}] }, (err, sched) => {
        if(err){
            logger.error('findSchedule - Schedule.findOne - Error - ' + err);
            return next(err);
        }
        if(sched) {
            let schedule = {};

            schedule.dayname = scheduleReq.dayname;
            schedule.batch = scheduleReq.batch ? mongoose.Types.ObjectId(scheduleReq.batch) : sched.batch;
            schedule.fromhour = scheduleReq.fromhour;
            schedule.tohour = scheduleReq.tohour;
            schedule.updatedon = new Date();
            schedule.updatedby = userId;

            Schedule.findOneAndUpdate({$and: [ {ownerId : ownerId},
                {_id: mongoose.Types.ObjectId(scheduleReq.scheduleId)}] },
                {$set: schedule}, (err, data) => {
                if (err) {
                    logger.error('schedule_update - Schedule.findOneAndUpdate - Error - ' + err);
                    return callback(err);
                }
                logger.info('schedule_update - ownerId:' + ownerId + ' Schedule updated successfully');
                callback(null, ResponseCode.Success);
            });

        } else {
            logger.warn('findSchedule - ownerId:' + ownerId + ' Schedule not found');      
            callback(null, ResponseCode.ScheduleNotFound);  
        }
    })
}

exports.deleteSchedule = (ownerId, dayname, fromhour, callback)=>{
    Schedule.findOneAndRemove({$and:[{ownerId : ownerId},{dayname: dayname},{fromhour: fromhour}]},
        function (err, schedule) {
        if (err) {
            logger.error('deleteSchedule - Schedule.findOneAndRemove - Error - ' + err);
            return callback(err);
        }
        Tutor.findOne({ownerId : ownerId}, function (err, tutor) {
            if (err) {
                logger.error('deleteSchedule - Tutor.findOne - Error - ' + err);
                return callback(err);
            }
            tutor.scheduleList = tutor.scheduleList.filter(function(item) {
                return item != schedule._id.toString()
            })
            tutor.save(function (err) {
                if (err) {
                    logger.error('deleteSchedule - tutor.save - Error - ' + err);
                    return callback(err);
                }
                logger.info('deleteSchedule - ownerId:' + ownerId
                + ', dayname:' + dayname + ' , fromhour:' + fromhour
                + ' Deleted Schedule successfully');
                callback(null, ResponseCode.Success);
            });
        });
    });
}