const logger = require('../utils/logger.utils');
const { ResponseCode } = require('../utils/response.utils');
const MasterQuestion = require("../models/masterQuestion.model")



exports.topics_get_dbc = (ownerId,userId,boardname,classname,subjectname,callback) =>{
    MasterQuestion.aggregate([
        {
            $match:{
                boardname:boardname,
                classname:classname,
                subjectname:subjectname,
                source:"MasterQuestion"
            }
        },
        {
            $lookup: {
                from: 'masterquestions',
                localField: 'nestedQuestions',
                foreignField: '_id',
                as: 'nestedQuestions'
            }
        },
        {
            $redact : {
                "$cond": [
                              { $and: [
                              {"$gt": [ { "$strLenCP": "$commonQuestionPart" }, 10]}, 
                              {"$ifNull": [ "$questionDescription", false ] } 
                              ]},
                              "$$PRUNE",
                              "$$KEEP"
                          ]
              }
        },
        {
            $group:{
                "_id": {
                    "topic": "$chaptername",
                    "questions": "$complexity",
                    
                },
                "questions": { 
                  '$push':{_id:'$_id',checksum: '$checksum', marks: '$marks', subTopic: '$subTopic'}
                  }
            },
        },
        {
            $sort:{
                
                    "_id.topic": 1,
                    "_id.questions":1
                  
            }
        }
        
    ])
        .then((aggDocs)=>{
           if(aggDocs===null){
               logger.warn("No questions found for this query")
               callback(null,ResponseCode.TopicNotFound)
           } 
           else{
               callback(null,ResponseCode.Success,aggDocs)
           }
        })
        .catch((err)=>{
            callback(err)
        })
}

exports.get_questions_dbc = (questionIds, callback) =>{
    MasterQuestion.find({_id:{$in:questionIds}}).populate('nestedQuestions').exec()
    .then((questions)=>{
        if(questions==null){
            callback(null,ResponseCode.QuestionNotFound)
        }
        else{
            callback(null,ResponseCode.Success,questions)
        }
    })
    .catch((err)=>{
        callback(err)
    })
}