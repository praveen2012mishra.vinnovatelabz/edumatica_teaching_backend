const AbilityStore = require("../models/abilityStore.model")
const { ResponseCode } = require('../utils/response.utils');

exports.getAbilities_dbc = (ownerId,userId,entityId,callback) =>{
    AbilityStore.find({ownerId:ownerId,consumerId:entityId}).exec()
    .then((accessAbilities)=>{
        if(accessAbilities===null){
            callback(null,ResponseCode.AbilitiesNotFound)
        }
        else{
            AbilityStore.find({ownerId:ownerId,userId:userId}).exec()
            .then((controlAbilities)=>{
                if(controlAbilities===null){
                    callback(null,ResponseCode.AbilitiesNotFound)
                }
                else{
                    const ap=accessAbilities.map((ap)=>{
                        return {
                            _id:ap._id,
                            abilityJSON:JSON.parse(ap.abilityJSON),
                            consumerId:ap.consumerId,
                            userId:ap.userId,
                            ownerId:ap.ownerId
                        }
                    })
                    const cp=controlAbilities.map((cp)=>{
                        return {
                            _id:cp._id,
                            abilityJSON:JSON.parse(cp.abilityJSON),
                            consumerId:cp.consumerId,
                            userId:cp.userId,
                            ownerId:cp.ownerId
                        }
                    })
                    callback(null,ResponseCode.Success,{accessAbilities:ap,controlAbilities:cp})
                }
            })
            .catch((err)=>{
                callback(err)
            })
        }
    })
    .catch((err)=>{
        callback(err)
    })
}

exports.create_ability_dbc=  (ownerId, pageId, userId , consumerId , abilityJSON , callback) =>{
    const newAbility = new AbilityStore({
        ownerId:ownerId,
        userId:userId,
        pageId:pageId,
        consumerId:consumerId,
        abilityJSON:JSON.stringify(abilityJSON)
    })

    newAbility.save()
    .then((doc)=>{
        if(doc===null){
            callback(null,ResponseCode.AbilityNotCreated)
        }
        else{
            callback(null,ResponseCode.Success)
        }
    })
    .catch((err)=>{
        callback(err)
    })
}


exports.patch_ability_dbc= (ownerId,userId,abilityId, consumerId , abilityJSON, callback) =>{
    AbilityStore.find({ownerId:ownerId,userId:userId,_id:abilityId}).exec()
    .then((abilityDoc)=>{
        if(abilityDoc===null){
            callback(null,ResponseCode.AbilitiesNotFound)
        }
        else{
            AbilityStore.findByIdAndUpdate(abilityId,{consumerId:consumerId,abilityJSON:JSON.stringify(abilityJSON)},{new:true}).exec()
            .then((newAbilityDoc)=>{
                callback(null,ResponseCode.Success,newAbilityDoc)
            })
            .catch((err)=>{
                callback(err)
            })
        }
    })
    .catch((err)=>{
        callback(err)
    })
}