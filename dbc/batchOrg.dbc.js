const Organization = require('../models/organization.model');
const Batch = require('../models/batch.model');
const Tutor = require('../models/tutor.model');
const Student = require('../models/student.model');
const Schedule = require('../models/schedule.model');
const BatchInvite = require('../models/batchInvite.model');
const StudentPaymentAccount = require('../models/studentPaymentAccount.model');
const OrganizationPaymentAccount = require('../models/orgPaymentAccount.model');
const PurchasedEdumacPackage = require('../models/purchasedEdumacPackage.model');
const StudentPayable = require('../models/studentPayble.model');
const PurchasedCoursePackage = require('../models/purchasedCoursePackage.model');
const CourseBundle = require('../models/courseBundle.model');
const logger = require('../utils/logger.utils');
const { ResponseCode } = require('../utils/response.utils');
const datefns = require('date-fns');
const mongoose = require('mongoose');
const { call } = require('file-loader');

exports.batch_create = (ownerId, entityId, userId, batch, callback)=>{
    Organization.findOne({ownerId : ownerId})
    .populate('courseDetails')
    .populate('studentList')
    .populate('package').exec((err, organization)=>{
        if(err){
            logger.error('batch_create - Organization.findOne - Error - ' + err);
            callback(err);
        }
        if(organization != null && organization.courseDetails != null 
        && organization.courseDetails.filter(function (el) {
            return el.board == batch.boardname 
            && el.className == batch.classname && el.subject == batch.subjectname;}).length > 0
        && organization.studentList != null && batch.students.length > 0){
            if(!organization.package || organization.batchList.length < organization.package.batchCount){
                Batch.findOne({$and: [{ownerId : ownerId},
                        {batchfriendlyname: batch.batchfriendlyname}] })
                        .exec( function (err, batchDb) {
                    if (err) {
                        logger.error('batch_create - Batch.findOne - Error - ' + err);
                        return callback(err);
                    }
                    if(batchDb == null) {
                        Tutor.findOne({$and: [{ownerId:ownerId,mobileNo : batch.tutor}] })
                            .exec( function (err, tutor) {
                            if (err) {
                                logger.error('batch_create - Tutor.findOne - Error - ' + err);
                                return callback(err);
                            }
                            if(tutor) {
                                Student.find({$and: [{ownerId:ownerId},{mobileNo: {$in: batch.students}}]}).select({"_id" : 1}).exec((err,data)=>{
                                    if(err){
                                        logger.error('batch_create - Student.find - Error - ' + err);
                                        return callback(err);
                                    }
                                    data = Object.values(data).filter(ele=>{if(ele) return ele})
                                    if(data.length == batch.students.length){
                                        var flag = 0;
                                        Batch.find({$and: [{ownerId : ownerId}, {boardname: batch.boardname}, {classname: batch.classname}, {subjectname: batch.subjectname}] }).exec((err, batches) => {
                                            if(err) {
                                                logger.error('Organization batch_create - Batch.find Check for student duplication - Error - ' + err);
                                                return callback(err);
                                            }
                                            if(!batches) {
                                                flag = 0;
                                            }
                                            if(batches) {
                                                const studentArray = data.map(ele => {
                                                    return ele._id
                                                })
                                                console.log(studentArray)
                                                studentArray.map(el => {
                                                    console.log(el)
                                                    batches.map(each => {
                                                        console.log(each.students)
                                                        if (each.students.includes(el) === true) flag = 1
                                                    })
                                                })
                                            }
                                            if(flag !== 1) {
                                                let batchDb = new Batch(
                                                    {
                                                        ownerId: ownerId,
                                                        tutorId: tutor._id,
                                                        boardname: batch.boardname,
                                                        classname: batch.classname,
                                                        subjectname: batch.subjectname,
                                                        batchfriendlyname: batch.batchfriendlyname,
                                                        batchenddate: batch.batchenddate,
                                                        batchstartdate: batch.batchstartdate,
                                                        students: data,
                                                        status: 1,
                                                        updatedon: new Date(),
                                                        updatedby: userId
                                                    }
                                                );
                                                batchDb.save(function (err) {
                                                    if (err) {
                                                        logger.error('batch_create - batch.save - Error - ' + err);
                                                        return callback(err);
                                                    }
                                                    
                                                    Tutor.findOneAndUpdate({$and: [{ownerId:ownerId,mobileNo : batch.tutor}]}, { $addToSet: { batchList: batchDb._id } }, (err, ret) => {})
                                                    Tutor.findOneAndUpdate({$and: [{ownerId:ownerId,mobileNo : batch.tutor}]}, { $addToSet: { studentList: {$each: data} } }, (err, ret) => {})

                                                    data.forEach(ele=>{
                                                        Student.findOneAndUpdate({"_id": ele},{$push: {batches: batchDb._id}},(err,data)=>{
                                                            if(err){
                                                                logger.error('batch_create - Student.findOneAndUpdate - Error - ' + err);
                                                                return callback(err);
                                                            }
                                                            
                                                        })
                                                    });
                                                    organization.batchList.push(batchDb._id);
                                                    organization.save(function (err) {
                                                        if (err) {
                                                            logger.error('batch_create - organization.save - Error - ' + err);
                                                            return callback(err);
                                                        }
                                                        logger.info('batch_create - ownerId:' + ownerId + ', batchfriendlyname:' + batch.batchfriendlyname
                                                                    + ' Batch Created successfully');
                                                        function createData(
                                                          ownerId,
                                                          batchId,
                                                          studentId,
                                                          isAccepted,
                                                        ) {
                                                          return { ownerId, batchId, studentId, isAccepted };
                                                        }
                                                        var invites = []
                                                        data.map(el => {
                                                            invites.push(createData(
                                                                ownerId,
                                                                batchDb._id,
                                                                el,
                                                                "ACCEPT"
                                                            ))
                                                        })
                                                        BatchInvite.insertMany(invites).then(result => {
                                                            PurchasedEdumacPackage.findOne({$and: [{ownerId: ownerId}, {accountType: 'Organization'}, {entityId: entityId}]}).exec((err, purchasedDb) => {
                                                                if(err) {
                                                                    logger.error('batch_create Organization - PurchasedEdumacPackage.findOne - Error - ' + err);
                                                                    return callback(err);
                                                                }
                                                                if(purchasedDb) {
                                                                    // console.log(purchasedDb)
                                                                    // student payble batch start day + example 7 day(grace) for 1st time for organization to pay to platform
                                                                    const batch_startDate = batchDb.batchstartdate;
                                                                    const batch_endDate = batchDb.batchenddate;
                                                                    if(batch_startDate >= new Date()) {
                                                                        var renewalDate = batch_startDate;
                                                                    } else {
                                                                        var renewalDate = new Date();
                                                                    }
                                                                    let studentPaybleToSaveArr = [];
                                                                    let studentPayableIdToSaveArr = [];
                                                                    var totalStudentPayableBill = 0;
                                                                    data.forEach(el => {
                                                                        newStudentPayble = new StudentPayable({
                                                                            ownerId: ownerId,
                                                                            accountType : 'Organization',                                          // Whether a independent teacher or Organization
                                                                            entityId: organization._id, 
                                                                            student:el,
                                                                            batch: batchDb._id,
                                                                            addedOn: new Date(),
                                                                            renewalDate: renewalDate, //batchstartdate + 1 month batch already started then grace date now + 7
                                                                            graceperiod: purchasedDb.graceperiod, //7days
                                                                            currentPaybleAmountToEdumac: purchasedDb.perstudentcost, //from masterpackage   //Check for full payment or partial payment
                                                                            currentStatus: 'ACTIVE', // Payment Process
                                                                            currentStatusChangeDate: new Date(), // Deactivate activate date 
                                                                            updatedon: new Date(),
                                                                            updatedby: userId,
                                                                        })
                                                                        totalStudentPayableBill = totalStudentPayableBill + purchasedDb.perstudentcost
                                                                        studentPaybleToSaveArr.push(newStudentPayble)
                                                                        studentPayableIdToSaveArr.push(newStudentPayble._id)
                                                                    })
                                                                    console.log(studentPaybleToSaveArr,studentPayableIdToSaveArr,totalStudentPayableBill)
                                                                    if(studentPaybleToSaveArr.length > 0 && studentPaybleToSaveArr.length === studentPayableIdToSaveArr.length && totalStudentPayableBill > 0) {
                                                                        StudentPayable.insertMany(studentPaybleToSaveArr).then(result => {
                                                                            OrganizationPaymentAccount.findOneAndUpdate({$and: [{ownerId: ownerId}, {accountType: 'Organization'}, {entityId: entityId}]}, {$push: {studentPayable:studentPayableIdToSaveArr}, $inc : {totaldebitBalance:totalStudentPayableBill,studentPaybleFeeBalance:totalStudentPayableBill} }).exec((err, paymentAccDb) => {
                                                                                if (err) {
                                                                                    logger.error('batch_create Organization - OrganizationPaymentAccount.findOneAndUpdate - Error - ' + err);
                                                                                    return callback(err);
                                                                                }
                                                                                logger.info('batch_create Organization Updated paymentAccDb:' + paymentAccDb)
                                                                                logger.info('batch_create Organization - ownerId:' + ownerId + ', batchfriendlyname:' + batch.batchfriendlyname
                                                                                        + ' invites sent' + ' Response: ' + result);
                                                                                return callback(null, ResponseCode.Success);
                                                                            })
                                                                        }).catch(err => {
                                                                            logger.error('batch_create Organization - StudentPayable.insertMany - Error - ' + err);
                                                                            return callback(err);
                                                                        })
                                                                    }
                                                                }
                                                            })                                                          
                                                        }).catch(err => {
                                                            logger.error('batch_create Organization - BatchInvite.insertMany - Error - ' + err);
                                                            return callback(err);
                                                        })
                                                    });
                                                });
                                            }else {
                                                logger.warn('batch_create - ownerId:' + ownerId + ', batchfriendlyname:' + batch.batchfriendlyname
                                                + ' Student(s) Duplicated, Student(s) present in existing batch of same course');
                                                return callback(null, ResponseCode.StudentPresentExistingBatch);
                                            }
                                        })
                                    }
                                    else{
                                        logger.warn('batch_create - ownerId:' + ownerId + ', batchfriendlyname:' + batch.batchfriendlyname
                                                        + ' Student(s) not found');
                                        return callback(null, ResponseCode.StudentNotFound);
                                    }
                                });
                            }
                            else{
                                logger.warn('batch_create - ownerId:' + ownerId + ', batchfriendlyname:' + batch.batchfriendlyname
                                                        + ', tutorId:' + batch.tutorId + ' Tutor Not found');
                                return callback(null, ResponseCode.TutorNotFound);
                            }
                        });
                    }
                    else{
                        logger.warn('batch_create - ownerId:' + ownerId + ', batchfriendlyname:' + batch.batchfriendlyname
                                                + ' Batch already exists');
                        return callback(null, ResponseCode.BatchAlreadyExists);
                    }
                });
            }
            else{
                logger.warn('batch_create - ownerId:' + ownerId + ', batchfriendlyname:' + batch.batchfriendlyname
                                                + ' Organization batch limit exceeded');
                return callback(null, ResponseCode.BatchLimitExceeded);
            }
        }
        else{
            logger.warn('batch_create - ownerId:' + ownerId + ', batchfriendlyname:' + batch.batchfriendlyname
                                            + ' Invalid course details');
            return callback(null, ResponseCode.InvalidInput);
        }
    });
}

exports.batch_batches = (ownerId, request, callback)=> {
    const query = (request.usertype === 'student') ? {students: request.studentId} : {ownerId : ownerId}
    Batch.find(query)
    .populate('tutorId')
    .select({"__v":0,"schedules":0})
    .exec( function (err, batches) {
        if (err) {
            logger.error('batch_batches - Batch.find - Error - ' + err);
            return callback(err);
        }
        logger.info('batch_batches - ownerId:' + ownerId
                                    + ' Batch(s) retrieved successfully');
        callback(null, ResponseCode.Success, batches);
    });
} 

exports.batch_bbbbatches = (ownerId, request, callback)=> {
    const query = (request.usertype === 'student') ? {students: request.studentId} : {ownerId : ownerId}
    Batch.find(query)
    .populate('tutorId')
    .select({"__v":0,"schedules":0})
    .populate("students", "mobileNo studentName")
    .exec( function (err, batches) {
        if (err) {
            logger.error('batch_bbbbatches - Batch.find - Error - ' + err);
            return callback(err);
        }
        logger.info('batch_bbbbatches - ownerId:' + ownerId
                                    + ' Batch(s) retrieved successfully');
        callback(null, ResponseCode.Success, batches);
    });
} 

exports.batch_details = (ownerId, batchfriendlyname, callback)=> {
    Batch.findOne({$and: [{ownerId : ownerId}, 
         {batchfriendlyname: batchfriendlyname}] })
         .populate('students')
         .populate('schedules')
         .select({"__v":0}).exec( function (err, batch) {
        if (err) {
            logger.error('batch_details - Batch.findOne - Error - ' + err);
            return callback(err);
        }
        if(batch == null) {
            logger.warn('batch_details - ownerId:' + ownerId + ', batchfriendlyname:' + batchfriendlyname
                                    + ' No batch found');
            return callback(null, ResponseCode.BatchNotFound);
        }
        logger.info('batch_details - ownerId:' + ownerId + ', batchfriendlyname:' + batchfriendlyname
                                    + ' Batch retrieved successfully');
        callback(null, ResponseCode.Success, batch);
    });
}

// Create batchInvites to new students, with isAccepted:ACCEPT
// Create StudentPayable and update OrgPaymentAcc
// Check courseBundle already created if true Create purchasedCourseBundle for new Students
exports.batch_update = (ownerId, entityId, userId, batchReq, callback)=> {
    let batch = {};
    batch.boardname = batchReq.boardname;
    batch.classname = batchReq.classname;
    batch.subjectname = batchReq.subjectname;
    batch.batchfriendlyname = batchReq.batchfriendlyname;
    batch.batchstartdate = batchReq.batchstartdate;
    batch.batchenddate = batchReq.batchenddate;
    Student.find({$and: [{ownerId:ownerId},{mobileNo: {$in: batchReq.students}}]}).select({"_id" : 1}).exec((err,studentData)=>{
        if(err){
            logger.error('batch_update - Student.find - Error - ' + err);
            return callback(err);
        }
        const studentArray = studentData.map(ele => {
            return ele._id.toString()
        })
        studentData = Object.values(studentData).filter(ele=>{if(ele) return ele})
        if(studentData.length>0){
            batch.students = studentData;
        }
        Tutor.find({$and: [{ownerId: ownerId}, {mobileNo: batchReq.tutor}]}).select('id').exec((err, tuto) => {
            if(err){
                logger.error('batch_update - Tutor.find - Error - ' + err);
                return callback(err);
            }
            data = Object.values(tuto).filter(ele => ele)

            const studentListCurrent = studentData.map(elem => String(elem._id));
            Batch.findById(batchReq.batchId, (err, bat) => {
                if(err){
                    logger.error('batch_update - Batch.findOne - Error - ' + err);
                    return callback(err);
                }

                if(!bat) {
                    logger.warn('batch_details - ownerId:' + ownerId + ', id:' + batchReq.batchId + ' No batch found');
                    return callback(null, ResponseCode.BatchNotFound);
                }

                if(String(bat.tutorId) === String(data[0]._id)) {
                    Tutor.findOneAndUpdate({_id: bat.tutorId}, { $addToSet: { studentList: {$each: studentData} } }, (err, data) => {}) 

                } else {
                    Tutor.findOneAndUpdate({$and: [{ownerId: ownerId}, {mobileNo: batchReq.tutor}]}, { $addToSet: { batchList: bat._id } }, (err, data) => {})
                    Tutor.findOneAndUpdate({$and: [{ownerId: ownerId}, {mobileNo: batchReq.tutor}]}, { $addToSet: { studentList: {$each: studentData} } }, (err, data) => {})  
                    
                    Tutor.findOneAndUpdate({_id: bat.tutorId}, { $pull: { batchList: bat._id } }, (err, data) => {})
                }

                const studentListPrev = thisbatch.students.map(elem => String(elem))

                const newEntries = studentListCurrent.filter(elem => !studentListPrev.includes(elem)).map(elem => mongoose.Types.ObjectId(elem));
                const remEntries = studentListPrev.filter(elem => !studentListCurrent.includes(elem)).map(elem => mongoose.Types.ObjectId(elem));

                const studentArray = newEntries.map(ele => {
                    return ele._id.toString()
                })
                const studentToRemove = remEntries.map(ele => {
                    return ele._id.toString()
                })

                if(data.length > 0) batch.tutorId = data[0];
                batch.updatedon = new Date();
                batch.updatedby = userId;
                logger.debug(studentData)
                Batch.findOneAndUpdate({$and: [{ownerId : ownerId},
                    {_id: batchReq.batchId}] },
                    {$set: batch},
                    function (err,batchDb) {
                        if (err) {
                            logger.error('batch_update - Batch.findOneAndUpdate - Error - ' + err);
                            return callback(err);
                        }
                        studentData.forEach(ele=>{
                            logger.debug(ele)
                            Student.findOneAndUpdate({"_id": ele},{$addToSet: {batches: batchDb._id}},(err)=>{
                                if(err){
                                    logger.error('batch_create - Student.findOneAndUpdate - Error - ' + err);
                                    return callback(err);
                                }
                            })
                        });
                        logger.info('batch_update - ownerId:' + ownerId + ', batchfriendlyname:' + batchReq.batchfriendlyname
                        + ' Batch updated successfully');
                        if (studentToRemove.length > 0) {
                            CourseBundle.findOneAndUpdate({$and: [{ownerId : ownerId}, {batch:batchReq.batchId}]}, {$pull : {participants: {$each: studentArray}}}).exec((err, result) => {
                                if (err) {
                                    logger.error('batch_update - CourseBundle.findOneAndUpdate - Error - ' + err);
                                }
                                PurchasedCoursePackage.find({batch:batchReq.batchId}).exec((err, packageArr) => {
                                    if(err) {
                                        logger.error('batch_update - PurchasedCoursePackage.find - Error - ' + err);
                                        // return callback(err);
                                    }
                                    if(packageArr) {
                                        // PurchasedCoursePackage.deleteMany({$and: [{ownerId:ownerId}, {batch:thisbatch._id}, {studentId: {$in:studentToRemove}}]}, (err, data) => {})
                                        studentToRemove.forEach(ele => {
                                            PurchasedCoursePackage.findOneAndDelete({$and: [{ownerId:ownerId}, {batch:batchReq.batchId}, {studentId:ele}]}).exec((err, data) => {
                                                if(err) {
                                                    logger.error('batch_update - PurchasedCoursePackage.findOneAndDelete - Error - ' + err);
                                                }
                                                if(data) {
                                                    StudentPaymentAccount.findOneAndUpdate({$and: [{ownerId:ownerId}, {student:ele}]}, {$pull: {purchasedPackage:data._id}}, (err, data) => {})
                                                }
                                            })
                                        })
                                        StudentPayable.find({$and: [{ownerId:ownerId}, {batch:batchReq.batchId}, {student: {$in:studentToRemove}}]}).exec((err, payables) => {
                                            let payableIdsToRemove = payables.map(each => {
                                                return each._id
                                            })
                                            StudentPayable.deleteMany({$and: [{ownerId:ownerId}, {batch:thisbatch._id}, {student: {$in:studentToRemove}}]}, (err, data) => {})
                                            OrganizationPaymentAccount.findOneAndUpdate({$and: [{ownerId:ownerId},{accountType:"Organization"},{entityId: entityId}]},{$pull: { studentPayable: {$in : payableIdsToRemove} }}, (err, data) => {})
                                        })
                                    }
                                })
                            })
                        }
                        if (studentArray.length > 0) {
                            CourseBundle.findOneAndUpdate({$and: [{ownerId : ownerId}, {batch:batchReq.batchId}]}, {$push : {participants: {$in: studentArray}}}).exec((err, result) => {
                                if (err) {
                                    logger.error('batch_update - CourseBundle.findOneAndUpdate - Error - ' + err);
                                }
                                PurchasedCoursePackage.find({batch:batchReq.batchId}).exec((err, packageArr) => {
                                    if(err) {
                                        logger.error('batch_update - PurchasedCoursePackage.find - Error - ' + err);
                                        // return callback(err);
                                    }
                                    if(packageArr) {
                                        let prevPackageStudentsArr = [];
                                        StudentPayable.find({batch: batchReq.batchId}).exec((err, existingPayabledb) => {
                                            // console.log(existingPayabledb)
                                            if(err) {
                                                logger.error('batch_update - StudentPayable.find - Error - ' + err);
                                                // return callback(err);
                                            }
                                            if(existingPayabledb) {
                                                existingPayabledb.map(ele => {
                                                    prevPackageStudentsArr.push(ele.student.toString())
                                                })
                
                                                const newPurchasePackageStudentArr = studentArray.filter(el => {
                                                    return prevPackageStudentsArr.includes(el) === false
                                                })
                                                
                                                console.log(prevPackageStudentsArr)
                                                console.log(newPurchasePackageStudentArr)
                        
                                                let invites = [];
                                                newPurchasePackageStudentArr.forEach(elem => {
                                                    const newInvite = new BatchInvite({
                                                        ownerId: ownerId,
                                                        batchId: batchReq.batchId,
                                                        studentId: elem,
                                                        isAccepted: 'ACCEPT'
                                                    })
                                                    invites.push(newInvite)
                                                })
                                                BatchInvite.insertMany(invites).then(result => {
                                                    PurchasedEdumacPackage.findOne({$and: [{ownerId: ownerId}, {accountType: 'Organization'}, {entityId: entityId}]}).exec((err, purchasedDb) => {
                                                        // console.log(entityId, purchasedDb)
                                                        if(err) {
                                                            logger.error('batch_update - PurchasedEdumacPackage.findOne - Error - ' + err);
                                                            // return callback(err);
                                                        }
                                                        if(purchasedDb) {
                                                            // student payble batch start day + example 7 day(grace) for 1st time for tutor to pay to platform
                                                            const batch_startDate = thisbatch.batchstartdate;
                                                            const batch_endDate = thisbatch.batchenddate;
                                                            if(batch_startDate >= new Date()) {
                                                                var renewalDate = batch_startDate;
                                                            } else {
                                                                var renewalDate = new Date();
                                                            }
                                                            let studentPaybleToSaveArr = [];
                                                            let studentPayableIdToSaveArr = [];
                                                            var totalStudentPayableBill = 0;
                                                            newPurchasePackageStudentArr.forEach(el => {
                                                                newStudentPayble = new StudentPayable({
                                                                    ownerId: ownerId,
                                                                    accountType : 'Organization',                                          // Whether a independent teacher or Organization
                                                                    entityId: entityId, 
                                                                    student:el,
                                                                    batch: batchReq.batchId,
                                                                    addedOn: new Date(),
                                                                    renewalDate: renewalDate, //batchstartdate + 1 month batch already started then grace date now + 7
                                                                    graceperiod: purchasedDb.graceperiod, //7days
                                                                    currentPaybleAmountToEdumac: purchasedDb.perstudentcost, //from masterpackage   //Check for full payment or partial payment
                                                                    currentStatus: 'ACTIVE', // Payment Process
                                                                    currentStatusChangeDate: new Date(), // Deactivate activate date 
                                                                    updatedon: new Date(),
                                                                    updatedby: userId,
                                                                })
                                                                totalStudentPayableBill = totalStudentPayableBill + purchasedDb.perstudentcost
                                                                studentPaybleToSaveArr.push(newStudentPayble)
                                                                studentPayableIdToSaveArr.push(newStudentPayble._id)
                                                            })
                                                            if(studentPaybleToSaveArr.length > 0 && studentPaybleToSaveArr.length === studentPayableIdToSaveArr.length && totalStudentPayableBill > 0) {
                                                                StudentPayable.insertMany(studentPaybleToSaveArr).then(result => {
                                                                    OrganizationPaymentAccount.findOneAndUpdate({$and: [{ownerId: ownerId}, {accountType: 'Organization'}, {entityId: entityId}]}, {$push: {studentPayable:studentPayableIdToSaveArr}, $inc : {totaldebitBalance:totalStudentPayableBill,studentPaybleFeeBalance:totalStudentPayableBill} }).exec((err, paymentAccDb) => {
                                                                        if (err) {
                                                                            logger.error('batch_update - OrganizationPaymentAccount.findOneAndUpdate - Error - ' + err);
                                                                            // return callback(err);
                                                                        }
                                                                        logger.info('batch_update - ownerId:' + ownerId + ', batchfriendlyname:' + batch.batchfriendlyname
                                                                                + ' invites sent' + ' Response: ' + result);
                                                                        // if batch started and coursebundle exists - make purchased course package
                                                                        CourseBundle.findOne({batch:batchReq.batchId}).exec((err, bundle) => {
                                                                            if(err) {
                                                                                logger.error('batch_update - CourseBundle.findOne - Error - ' + err);
                                                                                // return callback(err);
                                                                            }
                                                                            if(bundle) {
                                                                                // make purchased course package
                                                                                let purchasedpackagestosave = []
                                                                                newPurchasePackageStudentArr.forEach(ele =>  {
                                                                                    // console.log(renewalDate)
                                                                                    const newDocId = new mongoose.Types.ObjectId();
                                                                                    const newPurchasedCoursePackage = new PurchasedCoursePackage({
                                                                                        _id: newDocId,
                                                                                        ownerId: ownerId,
                                                                                        studentId: ele,
                                                                                        batch: batchReq.batchId,
                                                                                        paymentPlans: packageArr[0].paymentPlans,
                                                                                        currentGracePeriod: process.env.INITIAL_GRACE_PERIOD,
                                                                                        renewalDate: renewalDate,
                                                                                    })
                                                                                    purchasedpackagestosave.push(newPurchasedCoursePackage)
                                                                                    StudentPaymentAccount.findOneAndUpdate({$and: [{ownerId:ownerId},{student:ele}]},{ $push: { purchasedPackage:newDocId}},{new:true}).exec((err, accresponse) => {
                                                                                        // console.log(accresponse)
                                                                                        if(err) {
                                                                                            logger.error('batch_update StudentPaymentAccount.findOneAndUpdate - Error - ' + err);
                                                                                            // return callback(err) 
                                                                                        }
                                                                                        logger.info('batch_update StudentPaymentAccount.findOneAndUpdate - Success - Response: ' + accresponse);
                                                                                    })
                                                                                })
                                                                                PurchasedCoursePackage.insertMany(purchasedpackagestosave).then(response => {
                                                                                    logger.info('batch_update PurchasedCoursePackage.insertMany - Success - Response: ' + response);
                                                                                    // return callback(null, ResponseCode.Success)
                                                                                }).catch(err => {
                                                                                    logger.error('batch_update PurchasedCoursePackage.insertMany - Error - ' + err);
                                                                                    // return callback(err);
                                                                                })
                                                                            }
                                                                        })
                                                                    })
                                                                }).catch(err => {
                                                                    logger.error('batch_update - StudentPayable.insertMany - Error - ' + err);
                                                                    return callback(err);
                                                                })
                                                            }
                                                        }
                                                    })
                                                }).catch(err => {
                                                    logger.error('batch_update - BatchInvite.insertMany - Error - ' + err);
                                                    return callback(err);
                                                })
                                            }
                                        })
                                    }
                                    
                                })
                            })
                        }
                        callback(null, ResponseCode.Success);
                    }
                );
            })
        });
    });
}

exports.batch_delete = (ownerId, batchfriendlyname, callback)=> {
    Batch.findOne({$and: [{ownerId : ownerId}, 
    {batchfriendlyname: batchfriendlyname}] })
    .exec( function (err, batch) {
        if (err) {
            logger.error('batch_delete - Batch.findOne - Error - ' + err);
            return callback(err);
        }
        if(batch != null){            
            Schedule.deleteMany({batch : batch._id},function (err,data) {
                if (err) {
                    logger.error('batch_delete - Schedule.deleteMany - Error - ' + err);
                    return callback(err);
                }
                Batch.findOneAndDelete({$and: [{ownerId : ownerId}, 
                    {batchfriendlyname: batchfriendlyname}]}, function (err, batch) {
                    if (err) {
                        logger.error('batch_delete - Batch.findOneAndDelete - Error - ' + err);
                        return callback(err);
                    }
                    Organization.findOne({ownerId : ownerId}, function (err, organization) {
                        if (err) {
                            logger.error('batch_delete - Organization.findOne - Error - ' + err);
                            return callback(err);
                        }
                        organization.batchList = organization.batchList.filter(function(item) {
                            return item != batch._id.toString()
                        })
                        organization.save(function (err) {
                            if (err) {
                                logger.error('batch_delete - organization.save - Error - ' + err);
                                return callback(err);
                            }
                            Student.find({batches : batch._id}, (err, batchStudent) => {
                                if (err) {
                                    logger.error('batch_delete - Student.find - Error - ' + err);
                                    return callback(err);
                                }

                                const studentsIds = batchStudent.map((item) =>  {return item['_id'].toString()});

                                Tutor.findOneAndUpdate({batchList : batch._id}, { $pull: { batchList: batch._id } }, (err, data) => {})
                                Tutor.findOneAndUpdate({batchList : batch._id}, { $pullAll: { studentList: studentsIds } }, (err, data) => {})  
                                Student.updateMany({batches : batch._id}, { $pull: { batches: batch._id } }, (err, data) => {})

                                logger.info('batch_delete - ownerId:' + ownerId + ', batchfriendlyname:' + batchfriendlyname
                                        + ' Deleted Batch successfully');
                            
                            // Delete StudentPayable and StudentPurchasedCoursePackage, pull StudentPayable from OrgPaymentAcc and StudentPurchasedCoursePackage from StudentAcc
                            let payableIdsToRemove = [];
                            let purchaseIdsToRemove = [];
                            StudentPayable.find({$and: [{ownerId:ownerId},{accountType:"Organization"},{batch:batch._id}]}).exec((err, payableDb) => {
                                if(err) {
                                    logger.error('batch_delete - StudentPayable.find - Error - ' + err);
                                    return callback(err);
                                }
                                if(payableDb) {
                                    payableDb.forEach(each => {
                                        payableIdsToRemove.push(each._id)
                                    })
                                    PurchasedCoursePackage.find({$and:[{ownerId:ownerId},{batch:batch._id}]}).exec((err, purchaseDb) => {
                                        if(err) {
                                            logger.error('batch_delete - PurchasedCoursePackage.find - Error - ' + err);
                                            return callback(err);
                                        }
                                        if(purchaseDb) {
                                            purchaseDb.forEach(each => {
                                                purchaseIdsToRemove.push(each._id)
                                            })
                                            StudentPayable.deleteMany({$and: [{ownerId:ownerId},{accountType:"Organization"},{batch:batch._id}]}).exec((err, deleteResponse) => {
                                                if(err) {
                                                    logger.error('batch_delete - StudentPayable.deleteMany - Error - ' + err);
                                                    return callback(err);
                                                }
                                                if(deleteResponse) {
                                                    OrganizationPaymentAccount.findOneAndUpdate({$and: [{ownerId:ownerId},{accountType:"Organization"},{entityId: organization._id}]},{$pull: { studentPayable: {$in : payableIdsToRemove} }}).exec((err, result) => {
                                                        if(err) {
                                                            logger.error('batch_delete - OrganizationPaymentAccount.findOneAndUpdate - Error - ' + err);
                                                            return callback(err);
                                                        }
                                                        if(result) {
                                                            PurchasedCoursePackage.deleteMany({$and:[{ownerId:ownerId},{batch:batch._id}]}).then(response => {
                                                                if(response) {
                                                                    StudentPaymentAccount.updateMany({$and:[{ownerId:ownerId},{student: {$in: batch.students}}]},{$pull: { purchasedPackage: {$in : purchaseIdsToRemove} }}).then(response => {
                                                                        CourseBundle.findOneAndDelete({$and:[{ownerId:ownerId}, {batch:batch._id}]}).exec((err, result) => {
                                                                            if(err) {
                                                                                logger.error('batch_delete - CourseBundle.findOneAndDelete - Error - ' + err);
                                                                                return callback(err);
                                                                            }
                                                                            callback(null, ResponseCode.Success);
                                                                        })
                                                                    }).catch(err => {
                                                                        logger.error('batch_delete -  StudentPaymentAccount.updateMany - Error - ' + err);
                                                                        return callback(err);
                                                                    })
                                                                }
                                                            }).catch(err => {
                                                                logger.error('batch_delete - PurchasedCoursePackage.deleteMany - Error - ' + err);
                                                                return callback(err);
                                                            })
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        })
                        });
                    });
                    // Student.updateMany({batches : batch._id}, { $pull: { batches: batch._id } }, (err, data) => {})
                })
            })
        }
        else{
            logger.warn('batch_delete - ownerId:' + ownerId + ', batchfriendlyname:' + batchfriendlyname
                                    + ' Batch not found');
            callback(null, ResponseCode.BatchNotFound);
        }
   });   
}