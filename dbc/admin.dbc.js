const BackOfficeAdminUser = require('../models/backOfficeAdminUser.model')
const BackOfficeAdminRole = require('../models/backOfficeAdminRole.model')
const BackOfficeAdminPermission = require('../models/backOfficePermission.model')
const BackOfficeAdmin = require('../models/backOfficeAdmin.model')
const Tutor = require('../models/tutor.model')
const Organization = require('../models/organization.model')
const Parent = require('../models/parent.model')
const Student = require('../models/student.model')
const OrganizationPaymentOrder = require('../models/orgPaymentOrder.model')
const logger = require('../utils/logger.utils');
const { ResponseCode } = require('../utils/response.utils');
const { Template } = require('../utils/notificationTemplate.utils');
const backOfficeAdminRoleModel = require('../models/backOfficeAdminRole.model');
const bcrypt = require('bcryptjs')
const { ObjectID } = require('bson')
const EmailFactory = require('../utils/emailFactory.utils');
const EmailProvider = EmailFactory.create(process.env.EMAILPROVIDER || 'SendInBlue');

const registerUser = (adminUserReq, username, entityId, callback) => {
    BackOfficeAdminUser.findOne({username: adminUserReq.username}).exec((err, user) => {
        if(err) return callback(err)
        if(user == null) {
            const salt = bcrypt.genSaltSync(10)
            const passwordHash = bcrypt.hashSync(adminUserReq.password, salt)

            Object.assign(adminUserReq, {
                entityId,
                password: passwordHash,
                updatedby: username,
                updatedon: new Date()
            })
            const newAdminUser = new BackOfficeAdminUser(adminUserReq)
            newAdminUser.save((err) => {
                if(err) return callback(err)
                return callback(null, ResponseCode.Success)
            })
        } else {
            return callback(null, ResponseCode.UserRegistered)
        }
    })
}

exports.createAdmin = (adminReq, adminUserReq, username, callback) => {
    BackOfficeAdmin.findOne({emailId: adminReq.emailId}).exec((err, doc) => {
        if(err) return callback(err)
        if(doc) return callback(null, ResponseCode.UserRegistered)

        BackOfficeAdminRole.findOne({name: adminUserReq.role}).exec((err, role) => {
            if(err) return callback(err)
            if(!role) return callback(null, ResponseCode.RoleNotFound)
            
            const newDate = new Date()
            Object.assign(adminReq, {
                adminRole: role._id,
                insertedon: newDate,
                insertedby: username,
                actionCount: 1
            })
            const newAdmin = new BackOfficeAdmin(adminReq)
            newAdmin.save((err, admin) => {
                if(err) return callback(err)
                adminUserReq.adminRole = role._id
                registerUser(adminUserReq, username, admin._id, callback)
            })
        })
    })
}

exports.getAdmin = (emailId, callback) => {
    BackOfficeAdmin.findOne({$and: [{emailId}, {status: 1}]}).exec((err, doc) => {
        if(err) return callback(err)
        if(!doc) return callback(null, ResponseCode.AdminNotFound)
        return callback(null, ResponseCode.Success, doc)
    }) 
}

const getRoleId = async (role) => {
    try {
        const dbRole = await BackOfficeAdminRole.findOne({name: role})
        return dbRole._id
    } catch (err) {
        return 'err'
    }
}

exports.editAdmin = (adminReq, username, callback) => {
    BackOfficeAdmin.findOne({$and: [{emailId: adminReq.oldEmailId}, {status: 1}]}).exec(async (err, doc) => {
        if(err) return callback(err)
        if(!doc) return callback(null, ResponseCode.AdminNotFound)

        if(adminReq.role) {
            adminReq.adminRole = await getRoleId(adminReq.role)
            if(adminReq.adminRole === 'err' || !adminReq.adminRole) return callback(null, ResponseCode.RoleNotFound)
        }
        const newDate = new Date()
        Object.assign(doc, adminReq, {
            updatedby: username,
            updatedon: newDate,
            actionCount: doc.actionCount + 1
        })
        doc.save((err, doc) => {
            if(err) return callback(err)
            if(adminReq.role || adminReq.emailId) {
                const userUpdate = {
                    updatedon: newDate,
                    updatedby: username
                }
                if(adminReq.emailId) userUpdate.username = adminReq.emailId
                if(adminReq.role) userUpdate.adminRole = adminReq.adminRole

                BackOfficeAdminUser.findOneAndUpdate({username: adminReq.oldEmailId}, userUpdate).exec(err => {
                    if(err) return callback(err)
                    return callback(null, ResponseCode.Success)
                })
            } else return callback(null, ResponseCode.Success)
        })
    })
}

exports.deleteAdmin = (username, emailId, callback) => {
    const softDeleter = {
        status: 0,
        updatedby: username,
        updatedon: new Date()
    }
    BackOfficeAdmin.findOne({emailId}).exec((err, doc) => {
        if(err) return callback(err)
        if(!doc) return callback(null, ResponseCode.AdminNotFound)

        Object.assign(doc, softDeleter, {actionCount: doc.actionCount + 1})
        doc.save((err) => {
            if(err) return callback(err)
            
            BackOfficeAdminUser.findOne({username: emailId}).exec((err, user) => {
                if(err) return callback(err)
                if(!user) return callback(null, ResponseCode.UserNotExist)

                Object.assign(user, softDeleter)
                user.save(err => {
                    if(err) return callback(err)
                    return callback(null, ResponseCode.Success)
                })
            })
        })
    })
}

exports.createPermissions = (username, permissions, callback) => {
    const permissionNames = permissions.map(cur => cur.name)
    BackOfficeAdminPermission.find({name: {$in: permissionNames}}).exec((err, docs) => {
        if(err) return callback(err)
        if(docs.length === permissions.length) return callback(null, ResponseCode.PermissionAlreadyExists)

        const newDate = new Date()
        const oldPermissions = docs.map(cur => cur.name)
        const newPermissions = []
        permissions.forEach(cur => {
            if(!oldPermissions.includes(cur.name)) newPermissions.push({
                name: cur.name,
                group: cur.group
            })
        })

        BackOfficeAdminPermission.insertMany(newPermissions, err => {
            if(err) return callback(err)
            return callback(null, ResponseCode.Success)
        })
    })
}

exports.createRole = (name, permissions, username, callback) => {
    BackOfficeAdminRole.findOne({name}).exec(async (err, role) => {
        if(err) return callback(err)
        if(role) return callback(null, ResponseCode.RoleAlreadyExists)

        const dbPermissions = await BackOfficeAdminPermission.find({name: {$in: permissions}})
        if(dbPermissions.length !== permissions.length) {
            return callback(null, ResponseCode.PermissionNotFound)
        }

        const newRole = new BackOfficeAdminRole({
            name,
            permissions: dbPermissions.map(cur => cur._id),
            insertedby: username,
            insertedon: new Date()
        })
        newRole.save((err) => {
            if(err) return callback(err)
            return callback(null, ResponseCode.Success)
        })
    })
}

exports.editRole = (name, permissions, username, callback) => {
    BackOfficeAdminRole.findOne({name}).exec(async (err, role) => {
        if(err) return callback(err)
        if(!role) return callback(null, ResponseCode.RoleNotFound)

        const dbPermissions = await BackOfficeAdminPermission.find({name: {$in: permissions}})
        if(dbPermissions.length !== permissions.length) {
            return callback(null, ResponseCode.PermissionNotFound)
        }

        Object.assign(role, {
            name,
            permissions: dbPermissions.map(cur => cur._id),
            updatedby: username,
            updatedon: new Date()
        })
        role.save(err => {
            if(err) return callback(err)
            return callback(null, ResponseCode.Success)
        })
    })
}

exports.changePassword = (username, entityId, oldPassword, newPassword, isForced, callback) => {
    BackOfficeAdminUser.findOne({username}).exec((err, user) => {
        if(err) return callback(err)
        if(!user) return callback(null, ResponseCode.UserNotExist)
        if(!bcrypt.compareSync(oldPassword, user.password)) return callback(null, ResponseCode.IncorrectPassword) 

        const salt = bcrypt.genSaltSync(10)
        const passwordHash = bcrypt.hashSync(newPassword, salt)
        user.password = passwordHash
        user.save(err => {
            if(err) return callback(err)
            if(isForced){
                BackOfficeAdmin.findByIdAndUpdate(entityId, {isItDefaultPassword: false}).exec((err) => {
                    if(err) return callback(err)
                    return callback(null, ResponseCode.Success)
                })
            } else return callback(null, ResponseCode.Success)
        })
    })
}

exports.getAdmins = (callback) => {
    BackOfficeAdmin.find({status: 1}).exec((err, admins) => {
        if(err) return callback(err)
        return callback(null, ResponseCode.Success, admins)
    })
}

exports.getPermissions = (callback) => {
    BackOfficeAdminPermission.aggregate([
        {$match: {status: 1}},
        {$group: {
            _id: '$group',
            permissions: {$push: '$name'}
        }}
    ]).exec((err, permissions) => {
        if(err) return callback(err)
        return callback(null, ResponseCode.Success, permissions)
    })
}

exports.getRoles = (callback) => {
    BackOfficeAdminRole.find({status: 1}).populate('permissions').exec((err, roles) => {
        if(err) return callback(err)
        return callback(null, ResponseCode.Success, roles.map(role => ({
            name: role.name,
            permissions: role.permissions.map(cur => cur.name)
        })))
    })
}

exports.resetPassword = (username, usernameToReset, newPassword, callback) => {
    BackOfficeAdminUser.findOne({username: usernameToReset}).exec((err, doc) => {
        if(err) return callback(err)
        if(!doc) return callback(null, ResponseCode.UserNotExist)

        const salt = bcrypt.genSaltSync(10)
        const passwordHash = bcrypt.hashSync(newPassword, salt)
        const toUpdate = {
            password: passwordHash,
            isItDefaultPassword: true,
            updatedby: username,
            updatedon: new Date()
        }
        Object.assign(doc, toUpdate)
        doc.save(err => {
            if(err) return callback(err)
            return callback(null, ResponseCode.Success)
        })
    })
}

exports.sendNewPasswordMail = (name, email, newPassword, callback) => {
    const mail = [{
        name,
        email,
    }]
    const argsResetPassEmail = {type: Template.adminResetPassword, newPassword}
    EmailProvider.sendMail(mail, argsResetPassEmail, (err, code, data) => {
        return callback(err, code)
    })
}

const getModel = (userType) => {
    if(userType === 'tutor') return Tutor
    if(userType === 'org') return Organization
    if(userType === 'student') return Student
    if(userType === 'bothTutorOrg') return null
    return Parent
}

const getUserName = (userType, doc) => {
    if(userType === 'tutor') return {tutorName: doc.tutorName}
    if(userType === 'org') return {organizationName: doc.organizationName}
    if(userType === 'student') return {studentName: doc.studentName}
    return {parentName: doc.parentName}
}

exports.getUsers = (userType, query, callback) => {
    const Model = getModel(userType)
    Model.find(query).exec((err, docs) => {
        if(err) return callback(err)
        const userDetails = docs.map(cur => {
            const detail = {
                _id: cur._id,
                emailId: cur.emailId,
                mobileNo: cur.mobileNo
            }
            Object.assign(detail, getUserName(userType, cur))
            return detail
        })
        return callback(null, ResponseCode.Success, userDetails)
    })
}

exports.updateUserStatus = (username, userType, userId, roleStatus, callback) => {
    const disabler = {
        roleStatus,
        updatedby: username,
        updatedon: new Date()
    }
    const Model = getModel(userType)
    Model.findByIdAndUpdate(userId, disabler, (err, res) => {
        if(err) return callback(err)
        if(res == null) return callback(null, ResponseCode.UserNotExist)
        return callback(null, ResponseCode.Success)
    })
}

const fetchKycStatusDb = (Model, userType, query, tutorKycStatus, callback) => {
    Model.find(query).exec((err, users) => {
        if(err) return callback(err)
        const kycStatus = users.map(cur => {
            const userStatus = {
                _id: cur._id,
                isAllKycProcessDone: cur.isAllKycProcessDone,
                allKycStatus: cur.allKycStatus,
                aadhaarKycStatus: cur.aadhaarKycStatus,
                isVideoKycDone: cur.isVideoKycDone,
                videoKycVerified: cur.videoKycVerified,
            }
            Object.assign(userStatus, getUserName(userType, cur))
            return userStatus
        })
        return callback(null, ResponseCode.Success, [...kycStatus, ...tutorKycStatus])
    })
}

exports.fetchKycStatus = (userType, query, callback) => {
    const Model = getModel(userType)
    if(!Model) {
        fetchKycStatusDb(Tutor, 'tutor', query, [], (err, code, kycStatus) => {
            if(err) return callback(err)
            fetchKycStatusDb(Organization, 'org', query, kycStatus, callback)
        })
    } else fetchKycStatusDb(Model, userType, query, [], callback)
}

exports.fetchKycData = (userType, userId, callback) => {
    const Model = getModel(userType)
    Model.findById(userId).exec((err, user) => {
        if(err) return callback(err)
        const kycData = {
            _id: user._id,
            aadhaar: user.aadhaar,
            pan: user.pan,
            bankIfsc: user.bankIfsc,
            bankAccount: user.bankAccount,
            videoKycToken: user.videoKycToken,
            videoKycUuid: user.videoKycUuid,
            videoKycFileName: user.videoKycFileName,
            kycDetails: user.kycDetails,
            randomNo: Math.floor(1000 + Math.random() * 9000)
        }
        Object.assign(kycData, getUserName(userType, user))
        return callback(null, ResponseCode.Success, kycData)
    })
}

// exports.generateRegisterLink = (mobileNo, emailId, adminType, callback) => {
//     const query = mobileNo ? {mobileNo} : {emailId}
//     const username = mobileNo ? mobileNo : emailId

//     BackOfficeAdmin.findOne(query).exec((err, data) => {
//         if(err) {
//             return callback(err)
//         }
//         if(data == null) {
//             const newAdmin = new BackOfficeAdmin({
//                 mobileNo,
//                 emailId,
//                 adminType,
//                 status: 1,
//                 updatedon: new Date()
//             })
//             newAdmin.save().then(() => {
//                 // Generate a link with username, adminType and registerId
//                 const registerLink = new URL("https://www.edumatica.tk/")

//                 registerLink.searchParams.append("adminType", adminType)
                
//                 if(mobileNo) {
//                     registerLink.searchParams.append("mobileNo", mobileNo)
//                     // send sms
//                 }
//                 if(emailId) {
//                     registerLink.searchParams.append("emailId", emailId)
//                     // send email
//                 }

//                 return callback(null, ResponseCode.Success, registerLink)
//             })
//         }
//         if(data) {
//             return callback(null, ResponseCode.UserRegistered)
//         }
//     })
// }

exports.getOrgPaymentOrders = (accountType, paymentCycle, status, callback) => {
    // OrganizationPaymentOrder.find({status}).exec((err, res) => {
    //     if(err) return callback(err)
    //     return callback(null, ResponseCode.Success, res)
    // })
    OrganizationPaymentOrder.aggregate([
        {$match: {accountType, paymentCycle, status}},
        {$group: {
            _id: '$ownerId',
            amount: {$sum: 1},
            amount_paid: {$sum: 1},
            amount_to_payment_partner: {$sum: 1},
            convenience_fee: {$sum: 1},
            convenience_fee_tax: {$sum: 1},
        }}
    ]).exec((err, res) => {
        if(err) return callback(err)
        return callback(null, ResponseCode.Success, res)
    })
}