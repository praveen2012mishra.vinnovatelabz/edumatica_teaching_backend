const logger = require('../utils/logger.utils');
const { ResponseCode } = require('../utils/response.utils');
const Meeting = require('../models/bbbMeetings.model')

exports.bbbRecordings_DeleteRecordingData = (internalMeetingID, callback) => {
    const filter = { internalMeetingID: internalMeetingID };
    Meeting.findOneAndUpdate(
        filter,
        { isRecordingDeleted: true },
        { new: true }
      ).then(function () {
        Meeting.findOne(filter)
          .populate("events", "_id type userID name role eventTriggerTime")
          .then((result) => {
            logger.info("isRecording Deleted set to True" + result)
            return callback(null, ResponseCode.Success, result);
          }).catch(err => {
            logger.error("Error in updating Meeting Data with Recording Deleted info")
            return callback(err)
          })
      });
}