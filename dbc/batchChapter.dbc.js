const BatchChapter = require('../models/batchChapter.model');
const Batch = require('../models/batch.model');
const Chapter = require('../models/chapter.model');
const logger = require('../utils/logger.utils');
const Student = require('../models/student.model');
const { ResponseCode } = require('../utils/response.utils');

exports.batchChapter_save = (ownerId, userId, batchChapterReq, callback)=> {
    Batch.findOne({$and: [{ownerId : ownerId},
        {batchfriendlyname: batchChapterReq.batchfriendlyname}] })
        .exec( function (err, batch) {
        if (err){
            logger.error('batchChapter_save - Batch.findOne - Error - ' + err);
            return callback(err);
        }
        if(batch == null){
            logger.warn('batchChapter_save - ownerId:' + ownerId
                        + ', batchfriendlyname:' + batchChapterReq.batchfriendlyname
                        + ', No batch found');
            return callback(null, ResponseCode.BatchNotFound); 
        }   
        Chapter.findOne({$and: [{ownerId : ownerId}, {boardname : batchChapterReq.boardname}, 
            {classname : batchChapterReq.classname}, {subjectname: batchChapterReq.subjectname},
             {chaptername: batchChapterReq.chaptername}]})
             .exec( function (err, chapter) {
            if (err) {
                logger.error('batchChapter_save - Error - ' + err);
                return callback(err);
            }
            if(chapter == null){
                logger.warn('batchChapter_save - ownerId:' + ownerId
                            + ', boardname:' + batchChapterReq.boardname
                            + ', classname:' + batchChapterReq.classname
                            + ', subjectname:' + batchChapterReq.subjectname
                            + ', chaptername:' + batchChapterReq.chaptername
                            + ', No chapter found');
                return callback(null, ResponseCode.ChapterNotFound); 
            }
            BatchChapter.findOne({$and: [{ownerId: ownerId},
                {batch :batch._id}, {chapter :chapter._id}]})
                .exec( function (err, batchChapter) {
                if (err) {
                    logger.error('batchChapter_save - BatchChapter.findOne - Error - ' + err);
                    return callback(err);
                }
                if(batchChapter == null) {
                    batchChapter = new BatchChapter({
                            ownerId: ownerId,
                            batch: batch._id,
                            chapter: chapter._id,
                            startDate: batchChapterReq.startDate,
                            endDate: batchChapterReq.endDate,
                            status: 1,
                            updatedon: new Date(),
                            updatedby: userId
                    });  
                    batchChapter.save(function (err) {
                        if (err) {
                            logger.error('batchChapter_save - batchChapter.save - Error - ' + err);
                            return callback(err);
                        }
                        logger.info('batchChapter_save - ownerId:' + ownerId
                        + ', batchfriendlyname:' + batchChapterReq.batchfriendlyname
                        + ', boardname:' + batchChapterReq.boardname + ', classname:' + batchChapterReq.classname
                        + ', subjectname:' + batchChapterReq.subjectname + ', chaptername:' + batchChapterReq.chaptername
                        + ', BatchChapter saved successfully');
                        callback(null, ResponseCode.Success);
                    })                  
                }
                else{
                    // batchChapter.startDate = batchChapterReq.startDate;
                    // batchChapter.endDate = batchChapterReq.endDate;
                    // batchChapter.updatedon = new Date();
                    // batchChapter.updatedby = userId;
                    logger.warn('batchChapter_save - ownerId:' + ownerId
                        + ', batchfriendlyname:' + batchChapterReq.batchfriendlyname
                        + ', boardname:' + batchChapterReq.boardname + ', classname:' + batchChapterReq.classname
                        + ', subjectname:' + batchChapterReq.subjectname + ', chaptername:' + batchChapterReq.chaptername
                        + ', BatchChapter already published');
                        callback(null, ResponseCode.ChapterAlreadyPublished);
                }                
            })
        })
    });    
}

exports.student_chapters = (entityId, callback)=> {
    Student.findOne({_id : entityId},(err, student)=>{
        if(err){
            logger.error('student_contents - Student.findOne - Error - ' + err);
            callback(err);
        }
        if(student != null){
            BatchChapter.find({batch: {$in: student.batches}}).populate({
                path: 'chapter',
                // Get friends of friends - populate the 'friends' array for every friend
                populate: { path: 'contents' }
              })
                .exec((err, batchChapters)=>{
                if(err){
                    logger.error('studentContent_create - Student.find - Error - ' + err);
                    return callback(err);
                }
                logger.info('student_contents - entityId:' + entityId + ' Notes retrieved successfully');
                callback(null, ResponseCode.Success, batchChapters);
            })
        }
        else{
            logger.warn('student_contents - entityId:' + entityId + ' Student not found');
            callback(null, ResponseCode.StudentNotFound);
        }
    });
};