const Organization = require("../models/organization.model");
const Tutor = require("../models/tutor.model");
const Student = require("../models/student.model");
const Parent = require("../models/parent.model");
const Assessment = require("../models/assessment.model");
const AttemptedAssessment = require("../models/attemptedassessment.model");
const StudentAttendance = require("../models/studentAttendance.model");
const StudentContent = require("../models/studentContent.model");
const Batch = require("../models/batch.model");
const BatchChapter = require("../models/batchChapter.model");
const Schedule = require("../models/schedule.model");
const Session = require("../models/session.model");
const Section = require("../models/section.model");
const StudentPaymentAccount = require("../models/studentPaymentAccount.model");
const OrganizationPaymentAccount = require("../models/orgPaymentAccount.model");
const PurchasedEdumacPackage = require("../models/purchasedEdumacPackage.model");
const StudentPayable = require("../models/studentPayble.model");
const MasterPackage = require("../models/masterPackage.model");
const logger = require("../utils/logger.utils");
const { ResponseCode } = require("../utils/response.utils");
const Role = require("../models/role.model");
const User = require("../models/user.model");
const crypto = require("crypto");
const dateFns = require("date-fns");
const shortid = require("shortid");
const { getDownloadUrlForVideoKyc } = require("../utils/aws.utils");
const { encryptWithAES } = require("../utils/referralGenerator.utils");
const EmailFactory = require("../utils/emailFactory.utils");
const { updateJWTOnboarding } = require('../utils/passport.utils')
const mongoose = require('mongoose');
const EmailProvider = EmailFactory.create(
  process.env.EMAILPROVIDER || "SendInBlue"
);

exports.organization_details = (ownerId, callback) => {
  Organization.findOne({ ownerId: ownerId })
    .populate("package")
    .exec((err, organization) => {
      if (err) {
        logger.error(
          "organization_details - Organization.findOne - Error - " + err
        );
        return callback(err);
      }
      if (organization != null) {
        logger.info(
          "organization_details - ownerId:" +
            ownerId +
            " Organization retrieved successfully"
        );
        callback(null, ResponseCode.Success, organization);
      } else {
        logger.warn(
          "organization_details - ownerId:" +
            ownerId +
            " Organization not found"
        );
        callback(null, ResponseCode.OrganizationNotFound);
      }
    });
};

exports.org_courses = (ownerId, callback) => {
  Organization.findOne({ ownerId: ownerId })
    .populate("courseDetails")
    .exec((err, organization) => {
      if (err) {
        logger.error("org_courses - Organization.findOne - Error - " + err);
        callback(err);
      }
      if (organization != null) {
        logger.info(
          "org_courses - ownerId:" +
            ownerId +
            " Organization Courses retrieved successfully"
        );
        return callback(null, ResponseCode.Success, organization.courseDetails);
      } else {
        logger.warn(
          "org_courses - ownerId:" + ownerId + " Organization not found"
        );
        return callback(null, ResponseCode.OrganizationNotFound);
      }
    });
};

exports.organization_create = (
  ownerId,
  userId,
  entityId,
  organization,
  callback
) => {
  Organization.findOne({ ownerId: ownerId }, (err, organizationDB) => {
    if (err) {
      logger.error(
        "organization_create - Organization.findOne - Error - " + err
      );
      callback(err);
    }
    if (organizationDB == null) {
      crypto.randomBytes(32, (err, buffer) => {
        if (err) {
          logger.error("tutor_create - crypto.randomBytes - Error - " + err);
        }
        const emailVerificationToken = buffer.toString("hex");
        const expireEmailVerificationToken =
          Date.now() + parseInt(process.env.EMAIL_VERIFICATION_BUFFER_TIME);
        const canUseReferralTill = dateFns.addDays(new Date(), 5);
        organizationDB = new Organization({
          ownerId: ownerId,
          mobileNo: organization.mobileNo,
          organizationName: organization.organizationName,
          emailId: organization.emailId,
          dob: organization.dob,
          gstin: organization.gstin,
          businessType: organization.businessType,
          businessPAN: organization.businessPAN,
          businessName: organization.businessName,
          ownerPAN: organization.ownerPAN,
          ownerName: organization.ownerName,
          image: organization.image,
          aadhaar: organization.aadhaar,
          bankIfsc: organization.bankIfsc,
          bankAccount: organization.bankAccount,
          address: organization.address,
          city: organization.city,
          pinCode: organization.pinCode,
          stateName: organization.stateName,
          courseDetails: organization.courseDetails,
          kycDetails: organization.kycDetails,
          package: organization.package,
          contentSize: 0,
          customChapter: 0,
          quizCount: 0,
          emailVerificationToken: emailVerificationToken,
          expireEmailVerificationToken: expireEmailVerificationToken,
          canUseReferralTill: canUseReferralTill,
        });
        organizationDB.userReferralCode = encryptWithAES(
          `Organization_${organizationDB._id}`
        );
        organizationDB.save(function (err, data) {
          if (err) {
            logger.error(
              "organization_create - organizationDB.save - Error - " + err
            );
            return callback(err);
          }
          Role.findOne({ name: "ROLE_ORGANIZATION" }, async (err, role) => {
            User.findOneAndUpdate(
              { $and: [{ _id: userId }, { "owner.ownerId": ownerId }] },
              {
                $pull: { "owner.$.roles": { role: role._id } },
              },
              (err, update) => {
                if (!err) {
                  const newRole = {
                    role: role._id,
                    entityId: data._id,
                  };
                  User.findOneAndUpdate(
                    { $and: [{ _id: userId }, { "owner.ownerId": ownerId }] },
                    {
                      $addToSet: { "owner.$.roles": newRole },
                    },
                    (err, update) => {
                      if (err) {
                        logger.error(
                          "organization_create - User.findOneAndUpdate - Error - " +
                            err
                        );
                        return callback(err);
                      }
                      if (
                        process.env.GLOBALNOTIFICATIONSWITCH === "true" &&
                        process.env.SENDEMAIL === "true"
                      ) {
                        const mail = [
                          {
                            name: data.organizationName,
                            email: data.emailId,
                          },
                        ];
                        const argsSignUpConfirmation = {
                          type: "Sign_Up",
                          tutor_name: data.organizationName,
                          login_page: process.env.PLATFORM_LOGIN_PAGE,
                        };
                        EmailProvider.sendMail(
                          mail,
                          argsSignUpConfirmation,
                          (err, code, data) => {
                            if (err) {
                              logger.error(
                                "organization_create - SignUpConfirmation sendMail - Error - " +
                                  err
                              );
                            }
                            if (code === ResponseCode.Success) {
                              logger.info(
                                "organization_create - SignUpConfirmation emailId:" +
                                  data.emailId +
                                  ", EMAIL sent successfully"
                              );
                            } else {
                              logger.warn(
                                "organization_create - SignUpConfirmation emailId:" +
                                  data.emailId +
                                  ", EMAIL sending failed"
                              );
                            }
                          }
                        );
                        const tokenLink = `${process.env.CLIENT_URL}/organization/${data._id}/${data.emailVerificationToken}`;
                        const argsConfirmEmail = {
                          type: "Confirm_Email",
                          confirm_link: tokenLink,
                        };
                        EmailProvider.sendMail(
                          mail,
                          argsConfirmEmail,
                          (err, code, data) => {
                            if (err) {
                              logger.error(
                                "organization_create - ConfirmEmail sendMail - Error - " +
                                  err
                              );
                            }
                            if (code === ResponseCode.Success) {
                              logger.info(
                                "organization_create - ConfirmEmail emailId:" +
                                  data.emailId +
                                  ", EMAIL sent successfully"
                              );
                            } else {
                              logger.warn(
                                "organization_create - ConfirmEmail emailId:" +
                                  data.emailId +
                                  ", EMAIL sending failed"
                              );
                            }
                          }
                        );
                      }
                      // Creating copy of current Edumatica purchased package
                      // Find masterpackage
                      MasterPackage.findById(data.package).exec(
                        (err, masterpackage) => {
                          if (err) {
                            logger.error(
                              "tutor_create - MasterPackage.findById - Error - " +
                                err
                            );
                            return callback(err);
                          }
                          if (!masterpackage) {
                            logger.error(
                              "tutor_create - MasterPackage.findById - Error No masterpackage to purchase"
                            );
                            return;
                          }
                          if (masterpackage) {
                            const currentEdumacPackage = new PurchasedEdumacPackage(
                              {
                                ownerId: ownerId,
                                accountType: "Organization", // Whether a independent teacher or Organization
                                entityId: data._id,
                                name: masterpackage.name,
                                planId: masterpackage.planId, // can be trial | Edumatica_Basic | Edumatica_Plus
                                graceperiod: masterpackage.graceperiod,
                                paymentplan: "PREPAID",
                                paymentcycle: "MONTHLY", //discount not included , Discount will be kept seperate.
                                cost: masterpackage.cost,
                                perstudentcost: masterpackage.perstudentcost,
                                recordingQuota: masterpackage.recordingQuota, //In hours
                                courseCount: masterpackage.courseCount,
                                studentCount: masterpackage.studentCount,
                                tutorCount: masterpackage.tutorCount,
                                batchCount: masterpackage.batchCount,
                                sessionCount: masterpackage.sessionCount,
                                contentSize: masterpackage.contentSize,
                                customChapter: masterpackage.customChapter,
                              }
                            );
                            currentEdumacPackage
                              .save()
                              .then((packageDb) => {
                                // Creating transactional account for the tutor tracks all payments to Edumatica and maintains balance with the platform
                                // renewal date forwarded by grace for the first time account activation
                                const currentRenewalDate = dateFns.add(
                                  new Date(),
                                  { days: packageDb.grace_period }
                                );
                                const newTutorPaymentAcc = new OrganizationPaymentAccount(
                                  {
                                    ownerId: ownerId,
                                    accountType: "Organization", // Whether a independent teacher or Organization
                                    entityId: data._id, //If an independent tutor , then tutor Id, otherwise OrganizationId
                                    purchasedEdumacPackage: packageDb._id,
                                    retainerShipFeeBalance: packageDb.cost,
                                    totaldebitBalance: packageDb.cost,
                                    currentgraceperiod: packageDb.graceperiod,
                                    renewalDate: currentRenewalDate, // next renewal will be accordingly or should it be new Date()
                                  }
                                );
                                newTutorPaymentAcc
                                  .save()
                                  .then((payAccDb) => {
                                    logger.info(
                                      "organization_create - entityId:" +
                                        entityId +
                                        " Organization Created successfully"
                                    );
                                    callback(null, ResponseCode.Success);
                                  })
                                  .catch((err) => {
                                    logger.error(
                                      "organization_create - OrganizationPaymentAccount.Create - Error - " +
                                        err
                                    );
                                    return callback(err);
                                  });
                              })
                              .catch((err) => {
                                logger.error(
                                  "organization_create - PurchasedEdumacPackage.Create - Error - " +
                                    err
                                );
                                return callback(err);
                              });
                          }
                        }
                      );
                    }
                  );
                }
              }
            );
          });
        });
      });
    } else {
      Organization.findOneAndUpdate(
        { ownerId: ownerId },
        { $set: organization },
        {new:true},
        function (err, data) {
          if (err) {
            logger.error(
              "organization_create - Organization.findOneAndUpdate - Error - " +
                err
            );
            return callback(err);
          }
          logger.info(
            "organization_create - ownerId:" +
              ownerId +
              " Organization updated successfully"
          );
          callback(null, ResponseCode.Success, data);
        }
      );
    }
  });
};

exports.organization_onboard = (
  ownerId,
  userId,
  entityId,
  organization,
  callback
) => {
  Organization.findOne({ ownerId: ownerId }, (err, organizationDB) => {
    if (err) {
      logger.error(
        "organization_onboard - Organization.findOne - Error - " + err
      );
      callback(err);
    }
    if (organizationDB == null) {
      crypto.randomBytes(32, (err, buffer) => {
        if (err) {
          logger.error("tutor_create - crypto.randomBytes - Error - " + err);
        }
        const emailVerificationToken = buffer.toString("hex");
        const expireEmailVerificationToken =
          Date.now() + parseInt(process.env.EMAIL_VERIFICATION_BUFFER_TIME);
        const canUseReferralTill = dateFns.addDays(new Date(), 5);
        organizationDB = new Organization({
          ownerId: ownerId,
          mobileNo: organization.mobileNo,
          organizationName: organization.organizationName,
          emailId: organization.emailId,
          dob: organization.dob,
          gstin: organization.gstin,
          businessType: organization.businessType,
          businessPAN: organization.businessPAN,
          businessName: organization.businessName,
          ownerPAN: organization.ownerPAN,
          ownerName: organization.ownerName,
          image: organization.image,
          aadhaar: organization.aadhaar,
          bankIfsc: organization.bankIfsc,
          bankAccount: organization.bankAccount,
          address: organization.address,
          city: organization.city,
          pinCode: organization.pinCode,
          stateName: organization.stateName,
          courseDetails: organization.courseDetails,
          kycDetails: organization.kycDetails,
          package: organization.package,
          contentSize: 0,
          customChapter: 0,
          quizCount: 0,
          emailVerificationToken: emailVerificationToken,
          expireEmailVerificationToken: expireEmailVerificationToken,
          canUseReferralTill: canUseReferralTill,
          roleStatus: organization.roleStatus ? organization.roleStatus : 'FRESHER'
        });
        organizationDB.userReferralCode = encryptWithAES(
          `Organization_${organizationDB._id}`
        );
        organizationDB.save(function (err, data) {
          if (err) {
            logger.error(
              "organization_onboard - organizationDB.save - Error - " + err
            );
            return callback(err);
          }
          
          Role.findOne({ name: "ROLE_ORGANIZATION" }, async (err, role) => {
            User.findOne({_id: userId}, (err, usr) => {
              const allOwner = [...usr.owner];
              const ownOwner = allOwner.find(own => String(own.ownerId) === String(ownerId))
              const allRoles = [...ownOwner.roles]
              const ownRoleindex = allRoles.findIndex(rol => String(rol.role) === String(role._id))
              allRoles[ownRoleindex].entityId = data._id

              User.findOneAndUpdate({ $and: [{_id: userId}, {"owner.ownerId": ownerId}]}, {
                $set: { "owner.$.roles": allRoles } }, (err, update) => {
                if(err) {
                  logger.error('organization_onboard - User.findOneAndUpdate - Error - ' + err);
                  return callback(err);
                }

                if (
                  process.env.GLOBALNOTIFICATIONSWITCH === "true" &&
                  process.env.SENDEMAIL === "true"
                ) {
                  const mail = [
                    {
                      name: data.organizationName,
                      email: data.emailId,
                    },
                  ];
                  const argsSignUpConfirmation = {
                    type: "Sign_Up",
                    tutor_name: data.organizationName,
                    login_page: process.env.PLATFORM_LOGIN_PAGE,
                  };
                  EmailProvider.sendMail(
                    mail,
                    argsSignUpConfirmation,
                    (err, code, data) => {
                      if (err) {
                        logger.error(
                          "organization_onboard - SignUpConfirmation sendMail - Error - " +
                            err
                        );
                      }
                      if (code === ResponseCode.Success) {
                        logger.info(
                          "organization_onboard - SignUpConfirmation emailId:" +
                            data.emailId +
                            ", EMAIL sent successfully"
                        );
                      } else {
                        logger.warn(
                          "organization_onboard - SignUpConfirmation emailId:" +
                            data.emailId +
                            ", EMAIL sending failed"
                        );
                      }
                    }
                  );
                  const tokenLink = `${process.env.CLIENT_URL}/organization/${data._id}/${data.emailVerificationToken}`;
                  const argsConfirmEmail = {
                    type: "Confirm_Email",
                    confirm_link: tokenLink,
                  };
                  EmailProvider.sendMail(
                    mail,
                    argsConfirmEmail,
                    (err, code, data) => {
                      if (err) {
                        logger.error(
                          "organization_onboard - ConfirmEmail sendMail - Error - " +
                            err
                        );
                      }
                      if (code === ResponseCode.Success) {
                        logger.info(
                          "organization_onboard - ConfirmEmail emailId:" +
                            data.emailId +
                            ", EMAIL sent successfully"
                        );
                      } else {
                        logger.warn(
                          "organization_onboard - ConfirmEmail emailId:" +
                            data.emailId +
                            ", EMAIL sending failed"
                        );
                      }
                    }
                  );
                }
                // Creating copy of current Edumatica purchased package
                // Find masterpackage
                MasterPackage.findById(data.package).exec(
                  (err, masterpackage) => {
                    if (err) {
                      logger.error(
                        "tutor_create - MasterPackage.findById - Error - " +
                          err
                      );
                      return callback(err);
                    }
                    if (!masterpackage) {
                      logger.error(
                        "tutor_create - MasterPackage.findById - Error No masterpackage to purchase"
                      );
                      return;
                    }
                    if (masterpackage) {
                      const currentEdumacPackage = new PurchasedEdumacPackage(
                        {
                          ownerId: ownerId,
                          accountType: "Organization", // Whether a independent teacher or Organization
                          entityId: data._id,
                          name: masterpackage.name,
                          planId: masterpackage.planId, // can be trial | Edumatica_Basic | Edumatica_Plus
                          graceperiod: masterpackage.graceperiod,
                          paymentplan: "PREPAID",
                          paymentcycle: "MONTHLY", //discount not included , Discount will be kept seperate.
                          cost: masterpackage.cost,
                          perstudentcost: masterpackage.perstudentcost,
                          recordingQuota: masterpackage.recordingQuota, //In hours
                          courseCount: masterpackage.courseCount,
                          studentCount: masterpackage.studentCount,
                          tutorCount: masterpackage.tutorCount,
                          batchCount: masterpackage.batchCount,
                          sessionCount: masterpackage.sessionCount,
                          contentSize: masterpackage.contentSize,
                          customChapter: masterpackage.customChapter,
                        }
                      );
                      currentEdumacPackage
                        .save()
                        .then((packageDb) => {
                          // Creating transactional account for the tutor tracks all payments to Edumatica and maintains balance with the platform
                          // renewal date forwarded by grace for the first time account activation
                          const currentRenewalDate = dateFns.add(
                            new Date(),
                            { days: packageDb.grace_period }
                          );
                          const newTutorPaymentAcc = new OrganizationPaymentAccount(
                            {
                              ownerId: ownerId,
                              accountType: "Organization", // Whether a independent teacher or Organization
                              entityId: data._id, //If an independent tutor , then tutor Id, otherwise OrganizationId
                              purchasedEdumacPackage: packageDb._id,
                              retainerShipFeeBalance: packageDb.cost,
                              totaldebitBalance: packageDb.cost,
                              currentgraceperiod: packageDb.graceperiod,
                              renewalDate: currentRenewalDate, // next renewal will be accordingly or should it be new Date()
                            }
                          );
                          newTutorPaymentAcc
                            .save()
                            .then((payAccDb) => {
                              logger.info(
                                "organization_onboard - entityId:" +
                                  entityId +
                                  " Organization Created successfully"
                              );

                              User.aggregate([
                                { $match : { $and: [{_id: mongoose.Types.ObjectId(userId)}] } },
                                { $unwind: "$owner" },
                                { $unwind: "$owner.roles" },
                                { 
                                    $lookup: {
                                        "from": "roles",
                                        "localField": "owner.roles.role",
                                        "foreignField": "_id",
                                        "as": "owner.roles.role"
                                    }
                                },
                                { $unwind: "$owner.roles.role" },
                                { 
                                    $lookup: {
                                        "from": "abilityStores",
                                        "localField": "owner.roles.entityId",
                                        "foreignField": "consumerId",
                                        "as": "owner.roles.permissions.accesspermissions"
                                    }
                                },
                                { 
                                    $lookup: {
                                        "from": "abilityStores",
                                        "localField": "_id",
                                        "foreignField": "userId",
                                        "as": "owner.roles.permissions.controlpermissions"
                                    }
                                },
                                {
                                    $group: {
                                        _id: { _id : "$_id" , password: "$password", mobile: "$mobile",
                                            status: "$status", failedAttemptCount: "$failedAttemptCount", 
                                            failedOn: "$failedOn", __v: "$__v", updatedon: "$updatedon",
                                            updatedby: "$updatedby", owner_id: "$owner._id",
                                            owner_ownerId: "$owner.ownerId"
                                        },
                                        roles: { "$push": "$owner.roles" }
                                    }
                                },
                                {
                                    $group: {
                                        _id: { _id : "$_id._id" , password: "$_id.password", mobile: "$_id.mobile",
                                            status: "$_id.status", failedAttemptCount: "$_id.failedAttemptCount", 
                                            failedOn: "$_id.failedOn", __v: "$_id.__v", updatedon: "$_id.updatedon",
                                            updatedby: "$_id.updatedby"
                                        },
                                        owner: { "$push": {_id: "$_id.owner_id", ownerId: "$_id.owner_ownerId", roles: "$roles" } }
                                    }
                                },
                                {
                                    $project: { 
                                        _id: "$_id._id", owner: 1, password: "$_id.password", mobile: "$_id.mobile",
                                        status: "$_id.status", updatedon: "$_id.updatedon", updatedby: "$_id.updatedby",
                                        failedAttemptCount: "$_id.failedAttemptCount", failedOn: "$_id.failedOn", __v: "$_id.__v"
                                    }
                                },
                                { $sort: { _id: 1 } }
                              ], async (err, data) => {
                                  data = data[0]
                                  const {accessToken, refreshToken} = await updateJWTOnboarding(data)
                                  callback(null, ResponseCode.Success, accessToken, refreshToken, data);  
                              })

                            })
                            .catch((err) => {
                              logger.error(
                                "organization_onboard - OrganizationPaymentAccount.Create - Error - " +
                                  err
                              );
                              return callback(err);
                            });
                        })
                        .catch((err) => {
                          logger.error(
                            "organization_onboard - PurchasedEdumacPackage.Create - Error - " +
                              err
                          );
                          return callback(err);
                        });
                    }
                  }
                );
              })
            })
          });
        });
      });
    } else {
      Organization.findOneAndUpdate(
        { ownerId: ownerId },
        { $set: organization },
        function (err, data) {
          if (err) {
            logger.error(
              "organization_onboard - Organization.findOneAndUpdate - Error - " +
                err
            );
            return callback(err);
          }
          logger.info(
            "organization_onboard - ownerId:" +
              ownerId +
              " Organization updated successfully"
          );
          callback(null, ResponseCode.Success);
        }
      );
    }
  });
};

exports.addTutorsForOrganization = (ownerId, userId, tutors, callback) => {
  Organization.findOne({ ownerId: ownerId })
    .populate("package")
    .exec((err, organization) => {
      if (err) {
        logger.error(
          "addTutorsForOrganization - Organization.findOne - Error - " + err
        );
        callback(err);
      }
      if (organization != null) {
        if (
          tutors.length + organization.tutorList.length <=
          organization.package.tutorCount
        ) {
          var tutorMobileNos = [];
          tutors.forEach((tutor) => {
            tutorMobileNos.push(tutor.mobileNo);
          });
          Organization.find({$and: [{ownerId:ownerId}, {mobileNo: {$in: tutorMobileNos}}]}).exec((err, orgDb) => {
            if (err) {
              logger.error(
                "addTutorsForOrganization - Organization.find - Error - " + err
              );
              return callback(err);
            }
            if(orgDb.length > 0){
              return callback(null, ResponseCode.UserRegistered);
            } else {
              Tutor.find({
                $and: [
                  { ownerId: organization.ownerId },
                  { mobileNo: { $in: tutorMobileNos } },
                ],
              }).exec((err, tutorsDB) => {
                if (err) {
                  logger.error(
                    "addTutorsForOrganization - Tutor.find - Error - " + err
                  );
                  return callback(err);
                }
                User.find({
                  $and: [
                    { "owner.ownerId": ownerId },
                    { mobile: { $in: tutorMobileNos } },
                  ],
                }).exec((err, usersData) => {
                  if (err) {
                    logger.error("org_addTutor - User.find - Error - " + err);
                    return callback(err);
                  }
                  if (usersData.length > 0) {
                    return callback(null, ResponseCode.UserRegistered);
                  } else {
                    var tutorsToSave = [];
                    var tutorIdsToSave = [];
                    tutors.forEach((tutor) => {
                      // New Tutor -- Create and add to Organization
                      var tutorDB = tutorsDB.filter(function (el) {
                        return el.mobileNo == tutor.mobileNo;
                      })[0];
                      if (!tutorDB) {
                        let newTutor = new Tutor({
                          ownerId: organization.ownerId,
                          mobileNo: tutor.mobileNo,
                          tutorName: tutor.tutorName,
                          emailId: tutor.emailId,
                          qualifications: tutor.qualifications,
                          schoolName: tutor.schoolName,
                          location: tutor.location,
                          pinCode: tutor.pinCode,
                          stateName: tutor.stateName,
                          cityName: tutor.cityName,
                          courseDetails: tutor.courseDetails,
                          status: 1,
                          updatedon: new Date(),
                          updatedby: userId,
                          roleStatus: "FRESHER",
                        });
                        tutorsToSave.push(newTutor);
                        tutorIdsToSave.push(newTutor._id);
                      } //// Existing tutor -- Add to Organization if not already added
                      else {
                        if (
                          !organization.tutorList.includes(tutorDB._id.toString())
                        ) {
                          tutorIdsToSave.push(tutorDB._id);
                        }
                      }
                    });
                    if (tutorsToSave.length > 0) {
                      Tutor.insertMany(tutorsToSave, function (err) {
                        if (err) {
                          logger.error(
                            "addTutorsForOrganization - Tutor.insertMany - Error - " +
                              err
                          );
                          return callback(err);
                        }
                      });
                    }
                    if (tutorIdsToSave.length > 0) {
                      Organization.findOneAndUpdate(
                        { _id: organization._id },
                        { $push: { tutorList: { $each: tutorIdsToSave } } },
                        (err, data) => {
                          if (err) {
                            logger.error(
                              "addTutorsForOrganization - Organization.findOneAndUpdate - Error - " +
                                err
                            );
                            return callback(err);
                          }
                        }
                      );
                      logger.info(
                        "addTutorsForOrganization - ownerId:" +
                          ownerId +
                          " Tutor(s) added successfully"
                      );
                      callback(null, ResponseCode.Success,tutorIdsToSave);
                    } else {
                      logger.warn(
                        "addTutorsForOrganization - ownerId:" +
                          ownerId +
                          " Organization Tutor already exist"
                      );
                      callback(null, ResponseCode.OrganizationTutorAlreadyExist);
                    }
                  }
                });
              });
            }
          })
        } else {
          logger.warn(
            "addTutorsForOrganization - ownerId:" +
              ownerId +
              " Organization tutor limit exceeded"
          );
          callback(null, ResponseCode.TutorLimitExceeded);
        }
      } else {
        logger.warn(
          "addTutorsForOrganization - ownerId:" +
            ownerId +
            " Organization not found"
        );
        callback(null, ResponseCode.OrganizationNotFound);
      }
    });
};

exports.getTutors = (ownerId, callback) => {
  Organization.findOne({ ownerId: ownerId })
    .populate("tutorList")
    .exec((err, organization) => {
      if (err) {
        logger.error("getTutors - Organization.findOne - Error - " + err);
        callback(err);
      }
      if (organization != null) {
        logger.info(
          "getTutors - ownerId:" +
            ownerId +
            " Organization Tutors retrieved successfully"
        );
        console.log(organization.tutorList.length)
        return callback(null, ResponseCode.Success, organization.tutorList);
      } else {
        logger.warn(
          "getTutors - ownerId:" + ownerId + " Organization Not found"
        );
        return callback(null, ResponseCode.OrganizationNotFound);
      }
    });
};

exports.removeTutorsForOrganization = (ownerId, tutors, callback) => {
  Tutor.find({ $and: [{ ownerId: ownerId }, { mobileNo: { $in: tutors } }] })
    .select({ _id: 1 })
    .exec((err, data) => {
      if (err) {
        logger.error(
          "removeTutorsForOrganization - Tutor.find - Error - " + err
        );
        return callback(err);
      }
      var tutorIds = data.map((item) => {
        return item["_id"].toString();
      });
      if (tutorIds.length > 0) {
        Organization.findOne({ ownerId: ownerId }).exec((err, organization) => {
          if (err) {
            logger.error(
              "removeTutorsForOrganization - Organization.findOne - Error - " +
                err
            );
            callback(err);
          }
          if (
            organization != null &&
            organization.tutorList != null &&
            organization.tutorList.length > 0
          ) {
            organization.tutorList = organization.tutorList.filter(
              (el) => !tutorIds.includes(el.toString())
            );
            organization.save(function (err) {
              if (err) {
                logger.error(
                  "removeTutorsForOrganization - organization.save -  Error - " +
                    err
                );
                return callback(err);
              }

              Tutor.updateMany(
                { _id: { $in: tutorIds } },
                { $set: { roleStatus: "DEACTIVATE" } },
                (err, data) => {}
              );

              Assessment.find(
                { userId: { $in: tutorIds } },
                (err, assessment) => {
                  const assessmentIds = assessment.map((item) => {
                    return item["_id"].toString();
                  });
                  let sectionIds = assessment.flatMap((item) => item["_id"]);
                  sectionIds = sectionIds.map((item) => item.toString());
                  Assessment.deleteMany(
                    { _id: { $in: assessmentIds } },
                    (err, data) => {}
                  );
                  Section.deleteMany(
                    { _id: { $in: sectionIds } },
                    (err, data) => {}
                  );
                }
              );

              Batch.find({ tutorId: { $in: tutorIds } }, (err, batchTbd) => {
                const batchIds = batchTbd.map((item) => {
                  return item["_id"].toString();
                });
                Batch.deleteMany({ _id: { $in: batchIds } }, (err, data) => {});
                BatchChapter.deleteMany(
                  { batch: { $in: batchIds } },
                  (err, data) => {}
                );

                for (let index = 0; index < batchIds.length; index++) {
                  const element = batchIds[index];
                  Student.updateMany(
                    { batches: element },
                    { $pull: { batches: element } },
                    (err, data) => {}
                  );
                }
              });

              Schedule.deleteMany(
                { tutorId: { $in: tutorIds } },
                (err, data) => {}
              );
              Session.deleteMany(
                { tutorId: { $in: tutorIds } },
                (err, data) => {}
              );

              logger.info(
                "removeTutorsForOrganization - ownerId:" +
                  ownerId +
                  " Tutors removed for organization successfully"
              );
              return callback(null, ResponseCode.Success);
            });
          } else {
            logger.warn(
              "removeTutorsForOrganization - ownerId:" +
                ownerId +
                " Tutor(s) not found in Tutor"
            );
            callback(null, ResponseCode.TutorNotFound);
          }
        });
      } else {
        logger.warn(
          "removeTutorsForOrganization - ownerId:" +
            ownerId +
            " Tutor(s) not found"
        );
        callback(null, ResponseCode.TutorNotFound);
      }
    });
};

exports.org_updateTutor = (ownerId, tutor, callback) => {
  Organization.find({ ownerId: ownerId }, (err, data) => {
    if (err) {
      logger.error("org_updateTutors - Organization.find - Error - " + err);
      return callback(err);
    }
    Tutor.findByIdAndUpdate(tutor._id, { $set: tutor }, (err) => {
      if (err) {
        logger.error(
          "org_updateTutors - Tutor.findOneAndUpdate - Error - " + err
        );
        return callback(err);
      }
      logger.info(
        "org_updateTutor - ownerId: " + ownerId + "Tutor updated successfully"
      );
      callback(null, ResponseCode.Success);
    });
  });
};

exports.org_addStudents = (ownerId, userId, students, callback) => {
  Organization.findOne({ ownerId: ownerId })
    .populate("package")
    .exec((err, organization) => {
      if (err) {
        logger.error("org_addStudents - Organization.findOne - Error - " + err);
        callback(err);
      }
      if (organization != null) {
        if (
          students.length + organization.studentList.length <=
          organization.package.studentCount
        ) {
          var studentEmails = [];
          var studentMobileNos = [];
          var parentMobileNos = [];
          students.forEach((student) => {
            studentMobileNos.push(student.mobileNo);
            if(studentEmails.includes(student.emailId)){
              return callback(null, ResponseCode.EmailDuplicate)
            } else {
              studentEmails.push(student.emailId)
            }
            if (student.parentMobileNo.length > 0) {
              parentMobileNos.push(student.parentMobileNo);
            }
          });
          Tutor.find({$and: [{ownerId:ownerId}, {mobileNo: { $in: studentMobileNos}}]}).exec((err, existingTutor) => {
            if(err) {
              logger.error("org_addStudents - Tutor.find - Error - " + err);
              return callback(err);
            }
            if (existingTutor.length > 0) {
              return callback(null, ResponseCode.UserRegistered);
            } else{
              Student.find({
                $and: [
                  { ownerId: ownerId },
                  { mobileNo: { $in: studentMobileNos } },
                ],
              }).exec((err, studentsDB) => {
                if (err) {
                  logger.error("org_addStudents - Student.find - Error - " + err);
                  return callback(err);
                }
                User.find({
                  $and: [
                    { ownerId: ownerId },
                    { mobileNo: { $in: studentMobileNos } },
                  ],
                }).exec((err, usersData) => {
                  if (err) {
                    logger.error("org_addStudents - Student.find - Error - " + err);
                    return callback(err);
                  }
                  if (usersData.length > 0) {
                    return callback(null, ResponseCode.UserRegistered);
                  }
                });
                Parent.find({
                  $and: [
                    { ownerId: ownerId },
                    { mobileNo: { $in: parentMobileNos } },
                  ],
                }).exec((err, parentsDB) => {
                  if (err) {
                    logger.error("org_addStudents - Parent.find - Error - " + err);
                    return callback(err);
                  }
                  var studentsToSave = [];
                  var studentIdsToSave = [];
                  var parentsToSave = [];
                  students.forEach((student) => {
                    // New Student -- Create and add to Organization
                    var studentDB = studentsDB.filter(function (el) {
                      return el.mobileNo == student.mobileNo;
                    })[0];
                    logger.debug(studentDB);
                    if (!studentDB) {
                      let newStudent = new Student({
                        ownerId: ownerId,
                        mobileNo: student.mobileNo,
                        studentName: student.studentName,
                        emailId: student.emailId,
                        location: student.location,
                        pinCode: student.pinCode,
                        stateName: student.stateName,
                        cityName: student.cityName,
                        schoolName: student.schoolName,
                        boardName: student.boardName,
                        className: student.className,
                        status: 1,
                        updatedon: new Date(),
                        updatedby: userId,
                        roleStatus: "FRESHER",
                      });
                      if (student.parentMobileNo.length > 0) {
                        var parentDB = parentsDB.filter(function (el) {
                          return el.mobileNo == student.parentMobileNo;
                        })[0];
                        if (!parentDB) {
                          let newParent = new Parent({
                            ownerId: ownerId,
                            mobileNo: student.parentMobileNo,
                            status: 1,
                            updatedon: new Date(),
                            updatedby: userId,
                            roleStatus: "FRESHER",
                          });
                          parentsToSave.push(newParent);
                          newStudent.parent = newParent._id;
                        } else {
                          newStudent.parent = parentDB._id;
                        }
                      }
                      studentsToSave.push(newStudent);
                      studentIdsToSave.push(newStudent._id);
                    } //// Existing student -- Add to Organization if not already added
                    else {
                      logger.debug("student ID:" + studentDB._id);
                      Student.findByIdAndUpdate(studentDB._id, {
                        ownerId: ownerId,
                        mobileNo: student.mobileNo,
                        studentName: student.studentName,
                        location: student.location,
                        pinCode: student.pinCode,
                        stateName: student.stateName,
                        cityName: student.cityName,
                        schoolName: student.schoolName,
                        boardName: student.boardName,
                        className: student.className,
                        status: 1,
                        updatedon: new Date(),
                        updatedby: userId,
                      })
                        .then(() => {
                          if (
                            !organization.studentList.includes(
                              studentDB._id.toString()
                            )
                          ) {
                            studentIdsToSave.push(studentDB._id);
                          }
                        })
                        .catch((err) => {
                          logger.error(
                            "org_addStudents - Student.update - Error - " + err
                          );
                        });
                    }
                  });
                  if (parentsToSave.length > 0) {
                    Parent.insertMany(parentsToSave, function (err) {
                      if (err) {
                        logger.error(
                          "org_addStudents - Parent.insertMany - Error - " + err
                        );
                        return callback(err);
                      }
                    });
                  }
                  if (studentsToSave.length > 0) {
                    Student.insertMany(studentsToSave, function (err) {
                      if (err) {
                        logger.error(
                          "org_addStudents - Student.insertMany - Error - " + err
                        );
                        return callback(err);
                      }
                    });
                  }
                  if (studentIdsToSave.length > 0) {
                    Organization.findOneAndUpdate(
                      { _id: organization._id },
                      { $push: { studentList: { $each: studentIdsToSave } } },
                      (err, data) => {
                        if (err) {
                          logger.error(
                            "org_addStudents - Organization.findOneAndUpdate - Error - " +
                              err
                          );
                          return callback(err);
                        }
                      }
                    );
                    var studentPaymentAccountsToSave = [];
                    studentIdsToSave.forEach((ele) => {
                      const newStudentPaymentAcc = new StudentPaymentAccount({
                        ownerId: ownerId,
                        entityId: ele,
                        student: ele,
                        status: 1,
                        updatedon: new Date(),
                        updatedby: userId,
                      });
                      studentPaymentAccountsToSave.push(newStudentPaymentAcc);
                    });
                    if (studentPaymentAccountsToSave.length > 0) {
                      StudentPaymentAccount.insertMany(studentPaymentAccountsToSave)
                        .then((result) => {
                          logger.info(
                            "org_addStudents - ownerId:" +
                              ownerId +
                              " Student(s) added successfully"
                          );
                          callback(null, ResponseCode.Success,studentIdsToSave);
                        })
                        .catch((err) => {
                          logger.error(
                            "tutor_addStudents - StudentPaymentAccount.insertMany - Error - " +
                              err
                          );
                          return callback(err);
                        });
                    }
                  } else {
                    logger.warn(
                      "org_addStudents - ownerId:" +
                        ownerId +
                        " Organization Student Already Exist And Updated"
                    );
                    callback(null, ResponseCode.OrganizationStudentAlreadyExist);
                  }
                });
              });
            }
          })
        } else {
          logger.warn(
            "org_addStudents - ownerId:" +
              ownerId +
              " Organization Student limit exceeded"
          );
          callback(null, ResponseCode.StudentLimitExceeded);
        }
      } else {
        logger.warn(
          "org_addStudents - ownerId:" + ownerId + " Organization not found"
        );
        callback(null, ResponseCode.OrganizationNotFound);
      }
    });
};

exports.student_update = (ownerId, userId, studentReq, callback) => {
  Student.findOne(
    { ownerId: ownerId, mobileNo: studentReq.mobileNo },
    (err, student) => {
      if (err) {
        logger.error("student_update - Student.findOne - Error - " + err);
        callback(err);
      }
      if (student == null) {
        logger.warn(
          "student_update - ownerId:" +
            ownerId +
            " mobileNo:" +
            studentReq.mobileNo +
            " Student not found"
        );
        callback(null, ResponseCode.StudentNotFound);
      } else {
        if (
          studentReq.parentMobileNo != "" &&
          studentReq.parentMobileNo != undefined
        ) {
          Parent.findOne({
            ownerId: ownerId,
            mobileNo: studentReq.parentMobileNo,
          })
            .exec()
            .then((parent) => {
              if (parent == null) {
                if (!studentReq.parent) {
                  const newParent = new Parent({
                    ownerId: ownerId,
                    mobileNo: studentReq.parentMobileNo,
                    parentName: "",
                    emailId: "",
                    status: 1,
                    updatedon: new Date(),
                    updatedby: ownerId,
                    roleStatus: "FRESHER",
                  });


                  newParent.save().then((parent) => {
                    
                    student.parent = parent._id;
                    student.emailId = studentReq.emailId,
                    student.studentName = studentReq.studentName;
                    student.enrollmentId = studentReq.enrollmentId;
                    student.boardName = studentReq.boardName;
                    student.className = studentReq.className;
                    student.location = studentReq.location;
                    student.pinCode = studentReq.pinCode;
                    student.mobileNo =
                      student.roleStatus != "FRESHER"
                        ? studentReq.mobileNo
                        : student.mobileNo;
                    student.image = studentReq.image
                    student.stateName = studentReq.stateName
                    student.schoolName = studentReq.schoolName
                    student.roleStatus = studentReq.roleStatus
                      ? studentReq.roleStatus
                      : student.roleStatus
                      ? student.roleStatus
                      : "FRESHER"
                      student.dob = studentReq.dob
                        ? studentReq.dob
                        : student.dob
                        ? student.dob
                        : ""
                      student.gender = studentReq.gender
                        ? studentReq.gender
                        : student.gender
                        ? student.gender
                        : ""
                    student.country = studentReq.country ? studentReq.country : (student.country ? student.country : '');
                    logger.debug(student);
                    student
                      .save()
                      .then((student) => {
                        logger.info(
                          "student_update - ownerId:" +
                            ownerId +
                            " mobileNo:" +
                            studentReq.mobileNo +
                            " Student updated successfully"
                        );
                        return callback(null, ResponseCode.Success);
                      })
                      .catch((err) => {
                        logger.info(
                          "student_update - ownerId:" +
                            ownerId +
                            " mobileNo:" +
                            studentReq.mobileNo +
                            " Student save failed"
                        );
                        return callback(err);
                      });
                  });
                } }
                else if (parent.roleStatus == "FRESHER") {
                  parent.mobileNo = studentReq.parentMobileNo;
                  parent
                    .save()
                    .then(() => {
                      logger.info(
                        "Parent Mobile Number updated for ownerId:" +
                          ownerId +
                          " mobileNo:" +
                          studentReq.mobileNo
                      );
                      (student.emailId = studentReq.emailId),
                        (student.studentName = studentReq.studentName);
                      student.enrollmentId = studentReq.enrollmentId;
                      student.boardName = studentReq.boardName;
                      student.className = studentReq.className;
                      student.location = studentReq.location;
                      student.pinCode = studentReq.pinCode;
                      student.mobileNo =
                        student.roleStatus != "FRESHER"
                          ? studentReq.mobileNo
                          : student.mobileNo;
                      (student.image = studentReq.image),
                        (student.stateName = studentReq.stateName),
                        (student.schoolName = studentReq.schoolName);
                      (student.roleStatus = studentReq.roleStatus
                        ? studentReq.roleStatus
                        : student.roleStatus
                        ? student.roleStatus
                        : "FRESHER"),
                        (student.dob = studentReq.dob
                          ? studentReq.dob
                          : student.dob
                          ? student.dob
                          : ""),
                        (student.gender = studentReq.gender
                          ? studentReq.gender
                          : student.gender
                          ? student.gender
                          : "");
                        student.country = studentReq.country ? studentReq.country : (student.country ? student.country : '');
                      logger.debug(student);
                      student
                        .save()
                        .then(() => {
                          logger.info(
                            "student_update - ownerId:" +
                              ownerId +
                              " mobileNo:" +
                              studentReq.mobileNo +
                              " Student updated successfully"
                          );
                          return callback(null, ResponseCode.Success);
                        })
                        .catch((err) => {
                          logger.info(
                            "student_update - ownerId:" +
                              ownerId +
                              " mobileNo:" +
                              studentReq.mobileNo +
                              " Student save failed"
                          );
                          return callback(err);
                        });
                    })
                    .catch((err) => {
                      logger.error("Parent.save() error: " + err);
                    });
                } else if(parent.roleStatus == "ACTIVE") {
                    (student.emailId = studentReq.emailId);
                    (student.studentName = studentReq.studentName);
                  student.enrollmentId = studentReq.enrollmentId;
                  student.boardName = studentReq.boardName;
                  student.className = studentReq.className;
                  student.location = studentReq.location;
                  student.pinCode = studentReq.pinCode;
                  student.mobileNo =
                    student.roleStatus != "FRESHER"
                      ? studentReq.mobileNo
                      : student.mobileNo;
                  (student.image = studentReq.image),
                    (student.stateName = studentReq.stateName),
                    (student.schoolName = studentReq.schoolName);
                  (student.roleStatus = studentReq.roleStatus
                    ? studentReq.roleStatus
                    : student.roleStatus
                    ? student.roleStatus
                    : "FRESHER"),
                    (student.dob = studentReq.dob
                      ? studentReq.dob
                      : student.dob
                      ? student.dob
                      : ""),
                    (student.gender = studentReq.gender
                      ? studentReq.gender
                      : student.gender
                      ? student.gender
                      : "");
                      student.country = studentReq.country ? studentReq.country : (student.country ? student.country : '');
                      logger.debug(student);
                      student
                        .save()
                        .then(() => {
                          logger.info(
                            "student_update - ownerId:" +
                              ownerId +
                              " mobileNo:" +
                              studentReq.mobileNo +
                              " Student updated successfully"
                          );
                          return callback(null, ResponseCode.Success);
                        })
                        .catch((err) => {
                          logger.info(
                            "student_update - ownerId:" +
                              ownerId +
                              " mobileNo:" +
                              studentReq.mobileNo +
                              " Student save failed"
                          );
                          return callback(err);
                        });
                }

            })
            .catch((err) => {
              logger.error("Parent.findOne error: " + err);
            });
        }
        else{
            (student.emailId = studentReq.emailId);
                    (student.studentName = studentReq.studentName);
                  student.enrollmentId = studentReq.enrollmentId;
                  student.boardName = studentReq.boardName;
                  student.className = studentReq.className;
                  student.location = studentReq.location;
                  student.pinCode = studentReq.pinCode;
                  student.mobileNo =
                    student.roleStatus != "FRESHER"
                      ? studentReq.mobileNo
                      : student.mobileNo;
                  (student.image = studentReq.image),
                    (student.stateName = studentReq.stateName),
                    (student.schoolName = studentReq.schoolName);
                  (student.roleStatus = studentReq.roleStatus
                    ? studentReq.roleStatus
                    : student.roleStatus
                    ? student.roleStatus
                    : "FRESHER"),
                    (student.dob = studentReq.dob
                      ? studentReq.dob
                      : student.dob
                      ? student.dob
                      : ""),
                    (student.gender = studentReq.gender
                      ? studentReq.gender
                      : student.gender
                      ? student.gender
                      : "");
                    student
                        .save()
                        .then(() => {
                          logger.info(
                            "student_update - ownerId:" +
                              ownerId +
                              " mobileNo:" +
                              studentReq.mobileNo +
                              " Student updated successfully"
                          );
                          return callback(null, ResponseCode.Success);
                        })
                        .catch((err) => {
                          logger.info(
                            "student_update - ownerId:" +
                              ownerId +
                              " mobileNo:" +
                              studentReq.mobileNo +
                              " Student save failed"
                          );
                          return callback(err);
                        });
            
        }
      }
    }
  );
};

exports.org_getClassStudents = (ownerId, boardName, className, callback) => {
  Organization.findOne(
    { ownerId }.populate({
      path: "studentList",
      match: { className, boardName },
    })
  )
    .lean()
    .exec()
    .then((organization) => {
      if (organization != null) {
        logger.info(
          "org_getStudents - ownerId:" +
            ownerId +
            " Organization Students retrieved successfully"
        );
        const studentListDb = organization.studentList;
        const parentIds = studentListDb.map((student) => student.parent);
        Parent.find({ _id: { $in: parentIds } }).then((parents, err) => {
          if (err) {
            logger.error("tutor_getStudents - Parent.findOne - Error - " + err);
            callback(err);
          }
          const studentList = studentListDb.map((student) => {
            if (!student.parent) {
              return student;
            } else {
              let toReturn;
              parents.every((parent) => {
                if (parent._id.toString() === student.parent.toString()) {
                  student = {
                    ...student,
                    parentMobileNo: parent.mobileNo,
                  };
                  toReturn = student;
                  return false;
                } else return true;
              });
              return toReturn;
            }
          });

          // studentListDb.forEach((student) => {
          //     if(!student.parent) {
          //         studentList.push(student);
          //     } else {
          //         parents.every((parent) => {
          //             if(parent._id.toString() === student.parent.toString()){
          //                 student.parentMobileNo = parent.mobileNo;
          //                 studentList.push(student);
          //                 return false;
          //             } else return true;
          //         });
          //     }
          // })

          return callback(null, ResponseCode.Success, studentList);
        });
      } else {
        logger.warn(
          "org_getStudents - ownerId:" + ownerId + " Organization Not found"
        );
        return callback(null, ResponseCode.OrganizationNotFound);
      }
    })
    .catch((err) => {
      logger.error("org_getStudents - Organization.findOne - Error - " + err);
      callback(err);
    });
};
exports.org_getStudents = (ownerId, callback) => {
  Organization.findOne({ ownerId: ownerId })
    .populate("studentList")
    .exec((err, organization) => {
      if (err) {
        logger.error("org_getStudents - Organization.findOne - Error - " + err);
        callback(err);
      }
      if (organization != null) {
        logger.info(
          "org_getStudents - ownerId:" +
            ownerId +
            " Organization Students retrieved successfully"
        );
        const studentListDb = organization.studentList;
        const parentIds = studentListDb.map((student) => student.parent);
        Parent.find({ _id: { $in: parentIds } }).then((parents, err) => {
          if (err) {
            logger.error("tutor_getStudents - Parent.findOne - Error - " + err);
            callback(err);
          }
          const studentList = studentListDb.map((student) => {
            if (!student.parent) {
              return student;
            } else {
              let toReturn;
              parents.every((parent) => {
                if (parent._id.toString() === student.parent.toString()) {
                  student = {
                    ...student._doc,
                    ...{ parentMobileNo: parent.mobileNo },
                  };
                  student.parentMobileNo = parent.mobileNo;
                  toReturn = student;
                  return false;
                } else return true;
              });
              return toReturn;
            }
          });

          // studentListDb.forEach((student) => {
          //     if(!student.parent) {
          //         studentList.push(student);
          //     } else {
          //         parents.every((parent) => {
          //             if(parent._id.toString() === student.parent.toString()){
          //                 student.parentMobileNo = parent.mobileNo;
          //                 studentList.push(student);
          //                 return false;
          //             } else return true;
          //         });
          //     }
          // })

          return callback(null, ResponseCode.Success, studentList);
        });
      } else {
        logger.warn(
          "org_getStudents - ownerId:" + ownerId + " Organization Not found"
        );
        return callback(null, ResponseCode.OrganizationNotFound);
      }
    });
};

// Delete studnet payable and purchased course package
exports.org_removeStudents = (ownerId, students, callback) => {
  Student.find({
    $and: [{ ownerId: ownerId }, { mobileNo: { $in: students } }],
  })
    .select({ _id: 1 })
    .exec((err, data) => {
      if (err) {
        logger.error("org_removeStudents - Student.find - Error - " + err);
        return callback(err);
      }
      var studentsIds = data.map((item) => {
        return item["_id"].toString();
      });
      if (studentsIds.length > 0) {
        Organization.findOne({ ownerId: ownerId }).exec((err, organization) => {
          if (err) {
            logger.error(
              "org_removeStudents - Organization.findOne - Error - " + err
            );
            callback(err);
          }
          if (
            organization != null &&
            organization.studentList != null &&
            organization.studentList.length > 0
          ) {
            organization.studentList = organization.studentList.filter(
              (el) => !studentsIds.includes(el.toString())
            );
            organization.save(function (err) {
              if (err) {
                logger.error(
                  "org_removeStudents - organization.save -  Error - " + err
                );
                return callback(err);
              }

              Student.updateMany(
                { _id: { $in: studentsIds } },
                { $set: { roleStatus: "DEACTIVATE" } },
                (err, data) => {}
              );
              StudentPayable.updateMany(
                { student: { $in: studentsIds } },
                { $set: { currentStatus: "DEACTIVATE" } },
                (err, data) => {}
              );
              OrganizationPaymentAccount.findOneAndUpdate(
                {
                  $and: [{ ownerId: ownerId }, { accountType: "Organization" }],
                },
                { $pull: { studentPayable: { $in: studentsIds } } },
                (err, data) => {}
              );

              for (let index = 0; index < studentsIds.length; index++) {
                const element = studentsIds[index];
                Batch.updateMany(
                  { students: element },
                  { $pull: { students: element } },
                  (err, data) => {}
                );
              }

              AttemptedAssessment.deleteMany(
                { studentId: { $in: studentsIds } },
                (err, data) => {}
              );
              StudentAttendance.deleteMany(
                { student: { $in: studentsIds } },
                (err, data) => {}
              );
              StudentContent.deleteMany(
                { student: { $in: studentsIds } },
                (err, data) => {}
              );

              logger.info(
                "org_removeStudents - ownerId:" +
                  ownerId +
                  " Students removed for organization successfully"
              );
              return callback(null, ResponseCode.Success, studentsIds);
            });
          } else {
            logger.warn(
              "org_removeStudents - ownerId:" +
                ownerId +
                " Student(s) not found in Organization"
            );
            callback(null, ResponseCode.StudentNotFound);
          }
        });
      } else {
        logger.warn(
          "org_removeStudents - ownerId:" + ownerId + " Student(s) not found"
        );
        callback(null, ResponseCode.StudentNotFound);
      }
    });
};

exports.getVideoKycToken = (ownerId, entityId, callback) => {
  Organization.findOne({
    $and: [{ ownerId: ownerId }, { _id: entityId }],
  }).exec((err, orgDb) => {
    if (err) {
      logger.error("getVideoKycToken - Organization.findOne - Error - " + err);
      return callback(err);
    }
    if (!orgDb) {
      logger.warn(
        "getVideoKycToken - ownerId:" + ownerId + " Organization Not found"
      );
      return callback(null, ResponseCode.OrganizationNotFound);
    }
    if (orgDb) {
      if (!orgDb.videoKycAttempts || orgDb.videoKycAttempts < 5) {
        if (!orgDb.isVideoKycDone && orgDb.isVideoKycDone === false) {
          const newToken = shortid.generate();
          orgDb.videoKycToken = newToken;
          orgDb.videoKycAttempts = orgDb.videoKycAttempts + 1;
          orgDb
            .save()
            .then((response) => {
              return callback(null, ResponseCode.Success, newToken);
            })
            .catch((err) => {
              logger.error(
                "getVideoKycToken - Organization.save - Error - " + err
              );
              return callback(err);
            });
        }
      } else {
        logger.warn(
          "getVideoKycToken - ownerId:" +
            ownerId +
            " Too many attempts try again tomorrow"
        );
        return callback(null, ResponseCode.VideoKycAttemptsExceeded);
      }
    }
  });
};

exports.updateVideoKycSuccessUuid = (
  ownerId,
  entityId,
  uuid,
  fileName,
  callback
) => {
  Organization.findOne({
    $and: [{ ownerId: ownerId }, { _id: entityId }],
  }).exec((err, orgDb) => {
    if (err) {
      logger.error(
        "updateVideoKycSuccessUuid - Organization.findOne - Error - " + err
      );
      return callback(err);
    }
    if (!orgDb) {
      logger.warn(
        "updateVideoKycSuccessUuid - ownerId:" +
          ownerId +
          " Organization Not found"
      );
      return callback(null, ResponseCode.OrganizationNotFound);
    }
    if (orgDb) {
      getDownloadUrlForVideoKyc(ownerId, uuid, (err, code, signedUrl) => {
        if (err) {
          logger.error(
            "updateVideoKycSuccessUuid - getDownloadUrlForVideoKyc. AWS utils - Error - " +
              err
          );
          return callback(err);
        }
        if (code === ResponseCode.Success) {
          orgDb.videoKycUuid = uuid;
          orgDb.isVideoKycDone = true;
          orgDb.videoKycFileName = fileName;
          orgDb
            .save()
            .then((result) => {
              return callback(null, ResponseCode.Success, signedUrl);
            })
            .catch((err) => {
              logger.error(
                "updateVideoKycSuccessUuid - Organization.save - Error - " + err
              );
              return callback(err);
            });
        }
      });
    }
  });
};

exports.redoVideoKyc = (ownerId, entityId, callback) => {
    Organization.findOne({$and: [{ownerId:ownerId}, {_id:entityId}]}).exec((err, user) => {
        if(err) {
            logger.error('redoVideoKyc - Organization.findOne - Error - ' + err);
            return callback(err);
        }
        if(!user) {
            logger.warn('redoVideoKyc - ownerId:' + ownerId + ' Organization Not found');
            return callback(null, ResponseCode.OrganizationNotFound);
        }
        if(user) {
            user. videoKycToken = '';
            user.videoKycUuid = '';
            user.videoKycFileName= '';
            user.isVideoKycDone= false;
            user.videoKycVerified= false;
            user.save().then(result => {
                return callback(null, ResponseCode.Success)
            }).catch(err => {
                logger.error('redoVideoKyc - Organization.save - Error - ' + err);
                return callback(err);
            })
        }
    })
}


exports.fetchKycDetails = ( organizationId, callback) => {
    Organization.findOne({$and:[{_id:organizationId}]}).exec((err, userDb) => {
        if(err) {
            logger.error('fetchKycDetails - Tutor.findOne - Error - ' + err);
            return callback(err)
        }
        if(!userDb) {
            return callback(null, ResponseCode.TutorNotFound)
        }
        if(userDb) {
            const kycArrray = userDb.kycDetails;
            const videoKycDetails = {
                location: userDb.location,
                pinCode: userDb.pinCode,
                stateName: userDb.stateName,
                cityName: userDb.cityName,
                businessType: userDb.businessType,
                businessName: userDb.businessName,
                ownerPAN: userDb.ownerPAN,    // from here
                businessPAN: userDb.businessPAN,
                aadhaar : userDb.aadhaar,
                bankIfsc : userDb.bankIfsc,
                bankAccount : userDb.bankAccount,
                aadhaarKycZip : userDb.aadhaarKycZip,
                aadhaarKycShareCode : userDb.aadhaarKycShareCode,
                aadhaarKycStatus : userDb.aadhaarKycStatus,
                aadhaarMobile : userDb.aadhaarMobile,
                videoKycToken: userDb.videoKycToken,
                videoKycUuid: userDb.videoKycUuid,
                gstin: userDb.gstin,
                videoKycFileName: userDb.videoKycFileName,
                isVideoKycDone: userDb.isVideoKycDone,
                videoKycVerified: userDb.videoKycVerified,
                videoKycAttempts: userDb.videoKycAttempts,
            }
            return callback(null, ResponseCode.Success, kycArrray, videoKycDetails)
        }
    })
}