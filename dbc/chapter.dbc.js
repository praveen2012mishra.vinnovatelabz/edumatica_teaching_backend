const Chapter = require('../models/chapter.model');
const logger = require('../utils/logger.utils');
const { ResponseCode } = require('../utils/response.utils');
const Content = require('../models/content.model');

exports.chapter_create = (ownerId, userId, chapterReq, callback)=> {
    Chapter.findOne({$and: [{ownerId : ownerId}, {batchId: chapterReq.batchId}, {boardname : chapterReq.boardname}, 
        {classname : chapterReq.classname}, {subjectname: chapterReq.subjectname},
         {chaptername: chapterReq.chaptername}]})
         .exec( function (err, chapter) {
        if (err) {
            logger.error('chapter_create - Error - ' + err);
            return callback(err);
        }
        if(chapter == null) {
            let chapter = new Chapter(
                {
                    ownerId: ownerId,
                    batchId: chapterReq.batchId,
                    batchfriendlyname: chapterReq.batchfriendlyname,
                    boardname: chapterReq.boardname,
                    classname: chapterReq.classname,
                    subjectname: chapterReq.subjectname,           
                    chaptername: chapterReq.chaptername,
                    status: 1,
                    updatedon: new Date(),
                    updatedby: userId
                }
            );
            chapter.save(function (err) {
                if (err) {
                    logger.error('chapter_create - Error - ' + err);
                    return callback(err);
                }
                logger.info('chapter_create - ownerId:' + ownerId + ', batchId:' + chapter.batchId
                + ', boardname:' + chapterReq.boardname + ', classname:' + chapterReq.classname
                + ', subjectname:' + chapterReq.subjectname + ', chaptername:' + chapterReq.chaptername
                + ', Chapter Created successfully');
                return callback(null, ResponseCode.Success)
            })
        }
        else{
            logger.warn('chapter_create - ownerId:' + ownerId + ', batchId:' + chapter.batchId
                + ', boardname:' + chapterReq.boardname + ', classname:' + chapterReq.classname
                + ', subjectname:' + chapterReq.subjectname + ', chaptername:' + chapterReq.chaptername
                + ', Chapter already exists');
            return callback(null, ResponseCode.ChapterAlreadyExists)
        }
    })
}

exports.chapter_addMasterChapters = (ownerId, userId, chapterReq, callback) => {
    const batchId = chapterReq.batchId;
    const batchfriendlyname = chapterReq.batchfriendlyname;
    const boardname = chapterReq.boardname;
    const classname = chapterReq.classname;
    const subjectname = chapterReq.subjectname;
    const chapternameArr = chapterReq.chaptername;
    Chapter.find({$and: [{ownerId : ownerId}, {batchId: chapterReq.batchId}, {boardname : chapterReq.boardname}, 
        {classname : chapterReq.classname}, {subjectname: chapterReq.subjectname},
         {chaptername: {$in: chapternameArr}}]})
             .exec( function (err, chapter) {
                 console.log(chapter, err)
            if (err) {
                logger.error('chapter_create - Chapter.find Error - ' + err);
                return callback(err);
            }
            if(chapter.length === 0) {
                function createData(
                    ownerId,
                    batchId,
                    batchfriendlyname,
                    boardname,
                    classname,
                    subjectname,
                    chaptername,
                    status,
                    updatedon,
                    updatedby
                  ) {
                    return {ownerId,
                        batchId,
                        batchfriendlyname,
                        boardname,
                        classname,
                        subjectname,
                        chaptername,
                        status,
                        updatedon,
                        updatedby };
                  }
                  var chapters = []
                  chapternameArr.map(chapterName => {
                      chapters.push(createData(ownerId,batchId,batchfriendlyname,boardname,classname,subjectname,chapterName,1,new Date(),userId))
                  })
                  Chapter.insertMany(chapters).then(response => {
                        logger.info('chapter_create - ownerId:' + ownerId + ', batchId:' + chapter.batchId
                        + ', boardname:' + chapterReq.boardname + ', classname:' + chapterReq.classname
                        + ', subjectname:' + chapterReq.subjectname + ', chaptername:' + chapterReq.chaptername
                        + ', Chapter Created successfully');
                        return callback(null, ResponseCode.Success)
                  }).catch(err => {
                        logger.error('chapter_create - Error - ' + err);
                        return callback(err);
                  })
            }
            else{
                logger.warn('chapter_create - ownerId:' + ownerId + ', batchId:' + chapter.batchId
                    + ', boardname:' + chapterReq.boardname + ', classname:' + chapterReq.classname
                    + ', subjectname:' + chapterReq.subjectname + ', chaptername:' + chapterReq.chaptername
                    + ', Chapter already exists');
                return callback(null, ResponseCode.ChapterAlreadyExists)
            }
        })
}

exports.chapter_chapters = (ownerId, batchId, boardname, classname, subjectname, callback)=> {
    Chapter.find({$and: [{ownerId : ownerId}, {batchId:batchId}, {boardname : boardname}, 
                        {classname : classname}, {subjectname: subjectname}] })
                        .select({"__v":0, "contents":0})
                        .exec( function (err, chapters) {
        if (err) {
            logger.error('chapter_chapters - Error - ' + err);
            return callback(err);
        }
        logger.info('chapter_chapters - ownerId:' + ownerId + ', batchId:' + batchId
                + ', boardname:' + boardname + ', classname:' + classname
                + ', subjectname:' + subjectname
                + ', Chapter(s) retrieved successfully');
        callback(null, ResponseCode.Success, chapters);
    })
}

exports.chapter_details = (batchId, ownerId, boardname, classname, subjectname, chaptername, callback)=> {
    Chapter.findOne({$and: [{ownerId : ownerId}, {batchId:batchId}, {boardname : boardname}, 
        {classname : classname}, {subjectname: subjectname},
         {chaptername: chaptername}]}).populate('contents')
         .select({"__v":0})
         .exec( function (err, chapter) {
        if (err) {
            logger.error('chapter_details - Error - ' + err);
            return callback(err);
        }
        logger.info('chapter_details - ownerId:' + ownerId
                + ', boardname:' + boardname + ', classname:' + classname
                + ', subjectname:' + subjectname + ', chaptername:' + chaptername
                + ', Chapter retrieved successfully');
        return callback(null, ResponseCode.Success, chapter);
    })
}

exports.chapter_update = (ownerId, userId, chapter, callback)=> {
    let chapterToUpdate = {};
    chapterToUpdate.updatedon = new Date();
    chapterToUpdate.updatedby = userId; 
    chapterToUpdate.chaptername = chapter.newchaptername;
    Chapter.findOneAndUpdate({$and: [{ownerId : ownerId}, {batchId: chapter.batchId}, {boardname : chapter.boardname}, 
        {classname : chapter.classname}, {subjectname: chapter.subjectname},
         {chaptername: chapter.chaptername}]} , {$set: chapterToUpdate}, function (err, data) {
        if (err) {
            logger.error('chapter_update - Error - ' + err);
            return callback(err);
        }
        
        logger.info('chapter_update - ownerId:' + ownerId + ', batchId:' + chapter.batchId
                + ', boardname:' + chapter.boardname + ', classname:' + chapter.classname
                + ', subjectname:' + chapter.subjectname + ', chaptername:' + chapter.chaptername
                + ', Chapter updated successfully');
        callback(null, ResponseCode.Success);
    })
}

exports.chapter_delete = (ownerId, chapter, callback)=> {
    Chapter.findOneAndDelete({$and: [{ownerId : ownerId}, {batchId: chapter.batchId}, {boardname : chapter.boardname}, 
        {classname : chapter.classname}, {subjectname: chapter.subjectname},
         {chaptername: chapter.chaptername}] }, function (err, data) {
        if (err) {
            logger.error('chapter_delete - Error - ' + err);
            return callback(err);
        }
        logger.info('chapter_delete - ownerId:' + ownerId + ', batchId:' + chapter.batchId
                + ', boardname:' + chapter.boardname + ', classname:' + chapter.classname
                + ', subjectname:' + chapter.subjectname + ', chaptername:' + chapter.chaptername
                + ', Chapter deleted successfully');
        callback(null, ResponseCode.Success);
    })
}