const uuid = require('uuid')
const bcrypt = require('bcryptjs')
const Role = require('../models/role.model')
const BackOfficeAdmin = require('../models/backOfficeAdmin.model')
const BackOfficeAdminUser = require('../models/backOfficeAdminUser.model')
const { ResponseCode } = require('../utils/response.utils')
const logger = require('../utils/logger.utils')
const { call } = require('file-loader')
const blockDuration = 60 * 60 * 1000 // 1 hour
const maxAllowedFailedAttemptCount = 3

// const registerUser = (username, password, adminType, callback) => {
//     BackOfficeAdminUser.findOne({username}).exec((err, user) => {
//         if(err) return callback(err)
//         if(user == null) {
//             const salt = bcrypt.genSaltSync(10)
//             const passwordHash = bcrypt.hashSync(password, salt)
            
//             const newAdminUser = new BackOfficeAdminUser({
//                 username,
//                 password: passwordHash,
//                 adminType,
//                 status: 1,
//             })
//             newAdminUser.save((err) => {
//                 if(err) {
//                     console.log('error')
//                     return callback(err)
//                 }
//                 logger.info('admin registered successfully')
//                 return callback(null, ResponseCode.Success)
//             })
//         } else {
//             return callback(null, ResponseCode.UserRegistered)
//         }
//     })
// }

// exports.register = (mobileNo, emailId, password, adminType, callback) => {
//     const query = {$and: [mobileNo ? {mobileNo} : {emailId}, {adminType}]}

//     BackOfficeAdmin.findOne(query).exec((err, data) => {
//         if(err) {
//             return callback(err)
//         }
//         if(data == null) return callback(null, ResponseCode.UserNotExist)

//         if(data) {
//             const username = mobileNo ? mobileNo : emailId
//             registerUser(username, password, adminType, callback)
//         }
//     })
// }

exports.authenticate = (username, password, callback) => {
    BackOfficeAdminUser.findOne({username})
    .populate({
        path : 'adminRole',
        populate : {
            path : 'permissions'
        }
    })
    .exec((err, user) => {
        if(err) return callback(err)

        if(user) {
            const maxAllowedFailedAttemptCount = 3

            const currentTime = new Date();
            let failedAttemptCount = user.failedAttemptCount;
            let failedOn = user.failedOn;
            if (!failedOn || currentTime.getTime() - failedOn.getTime() > blockDuration) {
                failedAttemptCount = 0;
            }

            if (failedAttemptCount < maxAllowedFailedAttemptCount) {
                bcrypt.compare(password, user.password, (err, success) => {
                    if(err) return callback(err)
                    if(success) {
                        failedAttemptCount = 0;
                        failedOn = null;
                        BackOfficeAdminUser.updateOne({username}, {failedAttemptCount, failedOn}).exec((err) => {
                            if (err) {
                                logger.error('authenticate - BackOfficeAdminUser.save -  Error - ' + err);
                                return callback(err);
                            }                            
                            logger.info('authenticate - BackOfficeAdminUser:' + username + ' Admin logged in successfully');
                            return callback(null, ResponseCode.Success, user)
                        })
                    } else {
                        failedAttemptCount = failedAttemptCount + 1;
                        failedOn = new Date();
                        BackOfficeAdminUser.updateOne({username}, {failedAttemptCount, failedOn}).exec((err) => {
                            if (err) {
                                logger.error('authenticate - user.save -  Error - ' + err);
                                return callback(err);
                            }
                            logger.warn('authenticate - user:' + username + ' Incorrect user or password.');
                            return callback(null, ResponseCode.IncorrectUserOrPassword);
                        })
                    }
                })
            } else {
                logger.warn('authenticate - user:' + username + ' Max attempt count exceeded');
                return callback(null, ResponseCode.MaxLoginAttemptCountExceeded, null);
            }   
        } else return callback(null, ResponseCode.UserNotExist, user)
    })
}

exports.getOtp = (username, callback) => {
    BackOfficeAdminUser.findOne({username}).exec((err, doc) => {
        if(err) return callback(err)
        if(!doc) return callback(null, ResponseCode.UserNotExist)
        
        const otp = Math.floor(Math.random() * 899999 + 100000)
        doc.otp = otp
        doc.otpGeneratedOn = new Date()
        doc.isOtpVerified = false
        doc.save(err => {
            if(err) return callback(err)
            return callback(null, ResponseCode.Success, otp)
        })
    })
}

exports.verifyOtp = (username, otp, callback) => {
    BackOfficeAdminUser.findOne({username}).exec((err, doc) => {
        if(err) return callback(err)
        if(!doc) return callback(null, ResponseCode.UserNotExist)
        // adding 5 mins to the otp generated time
        const expiryTime = doc.otpGeneratedOn.setMinutes(doc.otpGeneratedOn.getMinutes() + 5)
        if(new Date() > expiryTime) return callback(null, ResponseCode.OtpTimeExpired)
        if(otp !== doc.otp) return callback(null, ResponseCode.IncorrectOtp)

        doc.isOtpVerified = true
        doc.save(err => {
            if(err) return callback(err)
            return callback(null, ResponseCode.Success)
        })
    })
}

exports.setPassword = (username, newPassword, callback) => {
    BackOfficeAdminUser.findOne({username}).exec((err, doc) => {
        if(err) return callback(err)
        if(!doc) return callback(null, ResponseCode.UserNotExist)
        if(!doc.isOtpVerified) return callback(null, ResponseCode.OtpNotVerified)

        const salt = bcrypt.genSaltSync(10)
        const passwordHash = bcrypt.hashSync(newPassword, salt)
        const toUpdate = {
            password: passwordHash,
            isOtpVerified: false,
            updatedby: username,
            updatedon: new Date()
        }
        Object.assign(doc, toUpdate)
        doc.save(err => {
            if(err) return callback(err)
            return callback(null, ResponseCode.Success)
        })
    })
}