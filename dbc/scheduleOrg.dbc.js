const Schedule = require('../models/schedule.model');
const Batch = require('../models/batch.model');
const Tutor = require('../models/tutor.model');
const logger = require('../utils/logger.utils');
const uuid = require('uuid');
const { ResponseCode } = require('../utils/response.utils');
const Organization = require('../models/organization.model');

const mongoose = require('mongoose');

function timeToDecimal(t) {
    var arr = t.split(':');
    var dec = parseInt((arr[1]/6)*10, 10);

    return parseFloat(parseInt(arr[0], 10) + '.' + (dec<10?'0':'') + dec);
}

exports.saveSchedulesInBatch = (ownerId, userId, batchReq, callback)=>{
    Organization.findOne({ownerId : ownerId}).populate('package').exec((err, organization)=>{
        if(err){
            logger.error('saveSchedulesInBatch - Organization.findOne - Error - ' + err);
            return next(err);
        }
        if(batchReq.schedules.length + organization.scheduleList.length <= organization.package.sessionCount){
            Batch.findOne({$and: [{ownerId : ownerId}, 
                {batchfriendlyname: batchReq.batchfriendlyname}] })
                .exec( function (err, batch) {
                if (err) {
                    logger.error('saveSchedulesInBatch - Batch.findOne - Error - ' + err);
                    return callback(err);
                }
                if(batch == null)  {
                    logger.warn('saveSchedulesInBatch - ownerId:' + ownerId
                                            + ' No batch found');
                    return callback(null, ResponseCode.BatchNotFound); 
                }
                else {
                    Schedule.find({tutorId : batch.tutorId})
                    .exec( function (err, dbSchedules) {
                        if (err) {
                            logger.error('saveSchedulesInBatch - Schedule.find - Error - ' + err);
                            return callback(err);
                        }
                        let scheduleOverlaps = false;
                        let schedulesArray = batchReq.schedules;
                        let scheduleIds = [];
                        let schedules = [];
                        let msg = '';
                        for (var i = 0; i < schedulesArray.length; i++){
                                let a = timeToDecimal(schedulesArray[i].fromhour);
                                let b = timeToDecimal(schedulesArray[i].tohour);
                                for (var j = 0; j < dbSchedules.length; j++){
                                let x = timeToDecimal(dbSchedules[j].fromhour);
                                let y = timeToDecimal(dbSchedules[j].tohour);
                                if (schedulesArray[i].dayname == dbSchedules[j].dayname) {
                                    if (!((a  < x && b <= x) || (y <= a && y < b))){
                                            scheduleOverlaps = true;
                                            msg = 'Invalid Schedule- dayname:' 
                                            + schedulesArray[i].dayname + ' fromhour:' + schedulesArray[i].fromhour 
                                            + ' tohour:' + schedulesArray[i].tohour 
                                            + ' overlaps with existing schedule dayname:' 
                                            + dbSchedules[j].dayname + ' fromhour:' + dbSchedules[j].fromhour 
                                            + ' tohour:' + dbSchedules[j].tohour;
                                            break;
                                    }
                                }
                            }
                        }
                        
                        if(scheduleOverlaps == false){
                            schedulesArray.forEach(schedule => {
                                let dbSchedule = new Schedule(
                                {
                                    ownerId : ownerId,
                                    tutorId: batch.tutorId,
                                    batch: batch._id, 
                                    dayname: schedule.dayname,
                                    fromhour: schedule.fromhour,
                                    tohour: schedule.tohour,
                                    status: 1,
                                    updatedon: new Date(),
                                    updatedby: userId
                                });
                                schedules.push(dbSchedule);
                                scheduleIds.push(dbSchedule._id); 
                            })
                            Schedule.insertMany(schedules,function (err,data) {
                                if (err) {
                                    logger.error('saveSchedulesInBatch - Schedule.insertMany - Error - ' + err);
                                    return callback(err);
                                }
                                organization.scheduleList = organization.scheduleList.concat(scheduleIds);
                                organization.save((err)=>{
                                    if(err){
                                        logger.error('saveSchedulesInBatch - organization.save - Error - ' + err);
                                        return callback(err);
                                    }
                                    logger.info('saveSchedulesInBatch - ownerId:' + ownerId
                                    + ' Schedule(s) Created successfully');
                                    return callback(null, ResponseCode.Success);
                                });
                            })   
                        }
                        else{
                            logger.warn('saveSchedulesInBatch - ownerId:' + ownerId
                                            + ' ' + msg);
                            return callback(null, ResponseCode.SchedulesOverlap, msg); 
                        }
                    });
                } 
            });
        }
        else{
            logger.warn('saveSchedulesInBatch - ownerId:' + ownerId
                            + ' Organization Schedule limit exceeded');
            return callback(null, ResponseCode.ScheduleLimitExceeded); 
        }
    });
}

exports.deleteSchedule = (ownerId, tutorId, dayname, fromhour, callback)=>{
    Schedule.findOneAndDelete({$and:[{ownerId : ownerId}, {tutorId: tutorId}, 
            {dayname: dayname},{fromhour: fromhour}]},
        function (err, schedule) {
        if (err) {
            logger.error('deleteSchedule - Schedule.findOneAndRemove - Error - ' + err);
            return callback(err);
        }
        Organization.findOne({ownerId : ownerId}, function (err, organization) {
            if (err) {
                logger.error('deleteSchedule - Organization.findOne - Error - ' + err);
                return callback(err);
            }
            organization.scheduleList = organization.scheduleList.filter(function(item) {
                return item != schedule._id.toString()
            })
            organization.save(function (err) {
                if (err) {
                    logger.error('deleteSchedule - organization.save - Error - ' + err);
                    return callback(err);
                }
                const scheduleList = organization.scheduleList.filter(function(item) {
                    return item != schedule._id.toString()
                })
                organization.update({scheduleList: scheduleList}, function (err) {
                    if (err) {
                        logger.error('deleteSchedule - organization.update - Error - ' + err);
                        return callback(err);
                    }
                    logger.info('deleteSchedule - ownerId:' + ownerId
                    + ', dayname:' + dayname + ' , fromhour:' + fromhour
                    + ' Deleted Schedule successfully');
                    callback(null, ResponseCode.Success);
                });
            });
        });
    });
}

exports.schedule_update = (ownerId, userId, scheduleReq, callback) => {
    Schedule.findOne({$and: [ {ownerId : ownerId},
        {_id: mongoose.Types.ObjectId(scheduleReq.scheduleId)}] }, (err, sched) => {
        if(err){
            logger.error('findSchedule - Schedule.findOne - Error - ' + err);
            return next(err);
        }
        if(sched) {
            let schedule = {};

            schedule.dayname = scheduleReq.dayname;
            schedule.tutorId = scheduleReq.tutorId ? mongoose.Types.ObjectId(scheduleReq.tutorId) : sched.tutorId;
            schedule.batch = scheduleReq.batch ? mongoose.Types.ObjectId(scheduleReq.batch) : sched.batch;
            schedule.fromhour = scheduleReq.fromhour;
            schedule.tohour = scheduleReq.tohour;
            schedule.updatedon = new Date();
            schedule.updatedby = userId;

            Schedule.findOneAndUpdate({$and: [ {ownerId : ownerId},
                {_id: mongoose.Types.ObjectId(scheduleReq.scheduleId)}] },
                {$set: schedule}, (err, data) => {
                if (err) {
                    logger.error('schedule_update - Schedule.findOneAndUpdate - Error - ' + err);
                    return callback(err);
                }
                logger.info('schedule_update - ownerId:' + ownerId + ' Schedule updated successfully');
                callback(null, ResponseCode.Success);
            });

            if(scheduleReq.tutorId) {
                Tutor.findOneAndUpdate({_id: sched.tutorId}, { $pull: { scheduleList: mongoose.Types.ObjectId(scheduleReq.scheduleId) } }, (err, data) => {})
                Tutor.findOneAndUpdate({_id: mongoose.Types.ObjectId(scheduleReq.tutorId)}, { $addToSet: { scheduleList: mongoose.Types.ObjectId(scheduleReq.scheduleId) } }, (err, data) => {})                
            }

        } else {
            logger.warn('findSchedule - ownerId:' + ownerId + ' Schedule not found');      
            callback(null, ResponseCode.ScheduleNotFound);  
        }
    })
}