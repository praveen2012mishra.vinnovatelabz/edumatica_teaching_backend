const logger = require("../utils/logger.utils");
const { ResponseCode } = require("../utils/response.utils");
const Meeting = require("../models/bbbMeetings.model");
const Event = require("../models/bbbEvents.model");
const mongoose = require("mongoose");

exports.bbbWebHook_hookeventlistener_meetingCreated = (
  meetingID,
  internalMeetingID,
  scheduleID,
  meetingName,
  createDate,
  createTime
) => {
  const meeting = new Meeting({
    _id: new mongoose.Types.ObjectId(),
    meetingID,
    internalMeetingID,
    scheduleID,
    meetingName,
    createDate,
    createTime,
  });

  meeting
    .save()
    .then((result) => {
      //console.log(result);
      logger.debug(
        "Database action bbbWebHook_hookeventlistener - save meeting details - result:" +
          result
      );
    })
    .catch((err) => {
      //console.log(err);
      logger.error(
        "Error database bbbWebHook_hookeventlistener - save meeting details - error:" +
          err
      );
    });
};

exports.bbbWebHook_hookeventlistener_userEvent = (
  newEventID,
  type,
  userID,
  name,
  role,
  eventTriggerTime
) => {
  const newevent = new Event({
    _id: newEventID,
    type,
    userID,
    name,
    role,
    eventTriggerTime,
  });

  newevent
    .save()
    .then((result) => {
      //console.log(result);
      logger.debug(
        "Database action bbbWebHook_hookeventlistener - save event details - result:" +
          result
      );
    })
    .catch((err) => {
      //console.log(err);
      logger.error(
        "Error database bbbWebHook_hookeventlistener - save event details - error:" +
          err
      );
    });
};

exports.bbbWebHook_hookeventlistener_tutorJoinedEvent = (
  filter,
  userID,
  name
) => {
  Meeting.findOneAndUpdate(
    filter,
    { tutorID: userID, tutorName: name },
    { new: true }
  ).then(function () {
    Meeting.findOne(filter)
      .populate("events", "_id type userID name role eventTriggerTime")
      .then((result) => {
        //console.log("", result, "\n", "Meeting Schema Updated with Tutor")
        logger.debug(
          "Database action bbbWebHook_hookeventlistener - save tutor details to meeting model - result:" +
            result
        );
      }).catch(err => {
        logger.error(
          "Error database bbbWebHook_hookeventlistener - save tutor details to meeting model - error:" +
            err
        );
      })
  });
};

exports.bbbWebHook_hookeventlistener_eventsToMeeting = (filter, newEventID) => {
  Meeting.findOneAndUpdate(
    filter,
    { $push: { events: newEventID } },
    { new: true }
  ).then(function () {
    Meeting.findOne(filter)
      .populate("events", "_id type userID name role eventTriggerTime")
      .then((result) => {
        //console.log(result)
        logger.debug(
          "Database action bbbWebHook_hookeventlistener - save eventID to meeting model - result:" +
            result
        );
      }).catch(err => {
        logger.error(
          "Error database bbbWebHook_hookeventlistener - save eventID to meeting model - error:" +
            err
        );
      })
  });
};

exports.bbbWebHook_hookeventlistener_endMeetingEvent = (
  newEventID,
  type,
  eventTriggerTime
) => {
  const newevent = new Event({
    _id: newEventID,
    type,
    eventTriggerTime,
  });

  newevent
    .save()
    .then((result) => {
      //console.log(result);
      logger.debug(
        "Database action bbbWebHook_hookeventlistener - end meeting event save - result:" +
          result
      );
    })
    .catch((err) => {
      //console.log(err);
      logger.error(
        "Error database bbbWebHook_hookeventlistener - end meeting event save - error:" +
          err
      );
    });
};

exports.bbbWebHook_hookeventlistener_endMeetingEventidEntry = (
  filter,
  newEventID,
  internalMeetingID
) => {
  Meeting.findOneAndUpdate(
    filter,
    { $push: { events: newEventID } },
    { new: true }
  ).then(function () {
    Meeting.findOne({
      internalMeetingID: internalMeetingID,
    })
      .populate("events", "_id type userID name role eventTriggerTime")
      .then((result) => {
        //console.log(result)
        logger.debug(
          "Database action bbbWebHook_hookeventlistener - end meeting event id save to meeting model - result:" +
            result
        );
      }).catch(err => {
        logger.error(
          "Error database bbbWebHook_hookeventlistener - end meeting event id save to meeting model - error:" +
            err
        );
      })
  });
};


exports.bbbWebHook_hookeventlistener_recordingEvent = (
  filter, isRecorded, recordingURL
) => {
  Meeting.findOneAndUpdate(
    filter,
    { isRecorded: isRecorded, recordingURL: recordingURL },
    { new: true }
  ).then(function () {
    Meeting.findOne(filter)
      .populate("events", "_id type userID name role eventTriggerTime")
      .then((result) => {
        logger.debug(
          "Database action bbbWebHook_hookeventlistener_recordingEvent - recording info save to meeting model - result:" +
            result
        );
      }).catch(err => {
        logger.error(
          "Error database bbbWebHook_hookeventlistener_recordingEvent - recording info save to meeting model - error:" +
            err
        );
      })
  });
}