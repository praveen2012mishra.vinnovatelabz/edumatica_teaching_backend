const mongoose = require("mongoose");
const MasterNotificationTemplate = require("../models/masterNotificationTemplate.model");
const logger = require("../utils/logger.utils");
const { ResponseCode } = require("../utils/response.utils");

exports.notificationMsgTemplate = (args, msg) => {
    if(!args) return
    MasterNotificationTemplate.findOne({messageType: args.type}).then(template => {
        //console.log(template)
        let message = template.msgIdentifierText
        const keys = Object.keys(args)
        keys.forEach(key => {
            if(key !== "type") {
                if(message.includes(`#${key}#`)) {
                    message=message.replace(`#${key}#`,args[key])
                }
            }
        });
        if(message.includes("#")) {
            logger.error(
                "notificationMsgTemplate - extra # - Error arguments received are not sufficient."
              );
            //   return
        }
        return msg(message)
    }).catch(err => {
        logger.error(
            "notificationMsgTemplate - MasterNotificationTemplate.findOne - Error - " +
              err
          );
    })
}

exports.notificationPushTemplate = (args, msg) => {
    if(!args) return
    MasterNotificationTemplate.findOne({messageType: args.type}).then(template => {
        //console.log(template)
        let message = template.pushIdentifierText
        const keys = Object.keys(args)
        keys.forEach(key => {
            if(key !== "type") {
                if(message.includes(`#${key}#`)) {
                    message=message.replace(`#${key}#`,args[key])
                }
            }
        });
        if(message.includes("#")) {
            logger.error(
                "notificationPushTemplate - extra # - Error arguments received are not sufficient."
              );
            //   return
        }
        return msg(message)
    }).catch(err => {
        logger.error(
            "notificationPushTemplate - MasterNotificationTemplate.findOne - Error - " +
              err
          );
    })
}

exports.notificationHtmlTemplate = (args, msg) => {
    if(!args) return
    MasterNotificationTemplate.findOne({messageType: args.type}).then(template => {
        //console.log(template)
        let subject = template.messageType;
        let message = template.htmlIdentifierText;
        const keys = Object.keys(args)
        keys.forEach(key => {
            if(key !== "type") {
                if(message.includes(`#${key}#`)) {
                    message=message.replace(`#${key}#`,args[key])
                }
            }
        });
        // if(message.includes("#")) {
        //     logger.error(
        //         "notificationHtmlTemplate - extra # - Error arguments received are not sufficient."
        //       );
        //     //   return
        // }
        return msg(subject, message)
    }).catch(err => {
        logger.error(
            "notificationHtmlTemplate - MasterNotificationTemplate.findOne - Error - " +
              err
          );
    })
}

exports.save_notificationMsgTemplate = (type, template, icon, callback) => {
    MasterNotificationTemplate.findOne({messageType: type}).exec((err, templateDb) => {
        if(err) {
            logger.error(
                "save_notificationMsgTemplate - MasterNotificationTemplate.findOne - Error - " +
                  err
              );
              callback(err);
              return
        }
        if(!templateDb) {
            const newTemplate = new MasterNotificationTemplate({
                _id: new mongoose.Types.ObjectId(),
                messageType: type,
                msgIdentifierText: template,
                icon: icon
            })
            newTemplate.save().then(response => {
                logger.info(
                    "save_notificationMsgTemplate - new MasterNotificationTemplate.save - Response - " +
                    response
                  );
                callback(null, ResponseCode.Success, response)
            }).catch(err => {
                logger.error(
                    "save_notificationMsgTemplate - new MasterNotificationTemplate.save - Error - " +
                      err
                  );
                callback(err)
            })
        }
        if(templateDb) {
            MasterNotificationTemplate.findOneAndUpdate(
                { messageType: type },
                {"$set":{"msgIdentifierText": template, "icon": icon}},
                { new: true }
            ).exec((err, response) => {
                if(err) {
                    logger.error(
                        "save_notificationMsgTemplate - MasterNotificationTemplate.findByIdAndUpdate - Error - " +
                          err
                      );
                    callback(err)
                    return
                }
                logger.info(
                    "save_notificationMsgTemplate - MasterNotificationTemplate.findByIdAndUpdate - Response - " +
                    response
                  );
                callback(null, ResponseCode.Success, response)
            })
        }
    })
}


exports.save_notificationPushTemplate = (type, template, icon, callback) => {
    MasterNotificationTemplate.findOne({messageType: type}).exec((err, templateDb) => {
        if(err) {
            logger.error(
                "save_notificationMsgTemplate - MasterNotificationTemplate.findOne - Error - " +
                  err
              );
              callback(err);
              return
        }
        if(!templateDb) {
            const newTemplate = new MasterNotificationTemplate({
                _id: new mongoose.Types.ObjectId(),
                messageType: type,
                pushIdentifierText: template,
                icon: icon
            })
            newTemplate.save().then(response => {
                logger.info(
                    "save_notificationMsgTemplate - new MasterNotificationTemplate.save - Response - " +
                    response
                  );
                callback(null, ResponseCode.Success, response)
            }).catch(err => {
                logger.error(
                    "save_notificationMsgTemplate - new MasterNotificationTemplate.save - Error - " +
                      err
                  );
                callback(err)
            })
        }
        if(templateDb) {
            MasterNotificationTemplate.findOneAndUpdate(
                { messageType: type },
                {"$set":{"pushIdentifierText": template, "icon": icon}},
                { new: true }
            ).exec((err, response) => {
                if(err) {
                    logger.error(
                        "save_notificationMsgTemplate - MasterNotificationTemplate.findOneAndUpdate - Error - " +
                          err
                      );
                    callback(err)
                    return
                }
                logger.info(
                    "save_notificationMsgTemplate - MasterNotificationTemplate.findOneAndUpdate - Response - " +
                    response
                  );
                callback(null, ResponseCode.Success, response)
            })
        }
    })
}


exports.switch_notificationMsgTemplate = (type, state, callback) => {
    MasterNotificationTemplate.findOne({messageType: type}).exec((err, templateDb) => {
        if(err) {
            logger.error(
                "switch_notificationMsgTemplate - MasterNotificationTemplate.findOne - Error - " +
                  err
              );
              callback(err);
              return
        }
        if(!templateDb) {
            callback(null, ResponseCode.NotificationTemplateNotAvailable)
        }
        if(templateDb) {
            MasterNotificationTemplate.findOneAndUpdate({messageType: type},{msgSwitch:state}).exec((err, response) => {
                if(err) {
                    logger.error(
                        "switch_notificationMsgTemplate - MasterNotificationTemplate.findOneAndUpdate - Error - " +
                          err
                      );
                    callback(err)
                    return
                }
                logger.info(
                    "switch_notificationMsgTemplate - MasterNotificationTemplate.findOneAndUpdate - Response - " +
                    response
                  );
                callback(null, ResponseCode.Success)
            })
        }
    })
}

exports.switch_notificationPushTemplate = (type, state, callback) => {
    MasterNotificationTemplate.findOne({messageType: type}).exec((err, templateDb) => {
        if(err) {
            logger.error(
                "save_notificationMsgTemplate - MasterNotificationTemplate.findOne - Error - " +
                  err
              );
              callback(err);
              return
        }
        if(!templateDb) {
            callback(null, ResponseCode.NotificationTemplateNotAvailable)
        }
        if(templateDb) {
            MasterNotificationTemplate.findOneAndUpdate({messageType: type},{pushSwitch:state}).exec((err, response) => {
                if(err) {
                    logger.error(
                        "switch_notificationPushTemplate - MasterNotificationTemplate.findOneAndUpdate - Error - " +
                          err
                      );
                    callback(err)
                    return
                }
                logger.info(
                    "switch_notificationPushTemplate - MasterNotificationTemplate.findOneAndUpdate - Response - " +
                    response
                  );
                callback(null, ResponseCode.Success)
            })
        }
    })
}