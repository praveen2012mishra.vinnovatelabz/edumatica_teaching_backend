const MasterQuestion = require("../models/masterQuestion.model");
const logger = require("../utils/logger.utils");
const { ResponseCode } = require("../utils/response.utils");
const checksum = require("checksum");

const createQuestionBody = (row, indexObject, dataArr) => {
  
  return {
    srno: row[indexObject.srno],
    boardname:
      indexObject.boardname === -1 || row[indexObject.boardname] === null
        ? "CBSE"
        : row[indexObject.boardname],
    classname: row[indexObject.classname],
    subjectname: row[indexObject.subjectname],
    chaptername: row[indexObject.chaptername].toLowerCase(),
    subTopic:
      indexObject.subTopic === -1 || row[indexObject.subTopic] === null
        ? "misc"
        : row[indexObject.subTopic].trim().toLowerCase(),
    questionDescription: row[indexObject.questionDescription]
      .toString()
      .replace(/(\r\n|\n|\r|_x000D_)/gm, '\n'),
    commonQuestionPart:
      row[indexObject.commonQuestionPart] == undefined
        ? ""
        : row[indexObject.commonQuestionPart].length < 11
        ? ""
        : row[indexObject.commonQuestionPart],
    option1: (row[indexObject.option1] === null ? "" : row[indexObject.option1])
      .toString()
      .replace(/(\r\n|\n|\r|_x000D_)/gm, " "),
    option2: (row[indexObject.option2] === null ? "" : row[indexObject.option2])
      .toString()
      .replace(/(\r\n|\n|\r|_x000D_)/gm, " "),
    option3: (row[indexObject.option3] === null ? "" : row[indexObject.option3])
      .toString()
      .replace(/(\r\n|\n|\r|_x000D_)/gm, " "),
    option4: (row[indexObject.option4] === null ? "" : row[indexObject.option4])
      .toString()
      .replace(/(\r\n|\n|\r|_x000D_)/gm, " "),
    answer: row[indexObject.answer]
      .toString()
      .split(",")
      .map((el) => {
        if (el === "1" || el === "A") {
          return "A";
        } else if (el === "2" || el === "B") {
          return "B";
        } else if (el === "3" || el === "C") {
          return "C";
        } else if (el === "4" || el === "D") {
          return "D";
        }
      }),
    answerDescription: (indexObject.answerDescription === -1 ||
    row[indexObject.answerDescription] === null
      ? ""
      : row[indexObject.answerDescription]
    )
      .toString()
      .replace(/(\r\n|\n|\r|_x000D_)/gm, " "),
    nestedQuestions: [],
    complexity:
      indexObject.complexity === -1 || row[indexObject.complexity] === null
        ? "medium"
        : row[indexObject.complexity].trim().toLowerCase(),
    imageLinks:
      indexObject.imageLinks === -1 || row[indexObject.imageLinks] === null
        ? []
        : row[indexObject.imageLinks]
            .split(",")
            .map((img) =>
              dataArr.find((file) => file.filename === img.toLowerCase())
            ),
    status: 1,
    marks: 1,
    source: "MasterQuestion",
    type:
      indexObject.type === -1 || row[indexObject.type] === null
        ? "single"
        : row[indexObject.type],
    percentageError: 5,
    negativeMarking: 0,
  };
};

exports.uploadData_dbc = (
  ownerId,
  userId,
  excelFileName,
  headers,
  rows,
  filesData,
  callback
) => {
  const commonQuestionTextArr = [];
  var questionIndex = 0
  if (
    !headers.includes("Class") ||
    !headers.includes("S_No") ||
    !headers.includes("Subject") ||
    !headers.includes("Topic") ||
    !headers.includes("Option1") ||
    !headers.includes("Option2") ||
    !headers.includes("Option3") ||
    !headers.includes("Option4") ||
    !headers.includes("Question_Description") ||
    !headers.includes("Answer_option_number") ||
    !headers.includes("Answer_Description")
  ) {
    callback(null, ResponseCode.MissingHeadersInFile);
  } else {
    MasterQuestion.distinct("filename")
      .exec()
      .then((result) => {
        if (result.includes(excelFileName)) {
          callback(null, ResponseCode.DuplicateExcelFileName);
        } else {
          const questionDetails = [];
          
          const excelData = [];
          const indexObject = [
            ["S_No", "srno"],
            ["Board", "boardname"],
            ["Class", "classname"],
            ["Subject", "subjectname"],
            ["Topic", "chaptername"],
            ["Sub_Topic", "subTopic"],
            ["Common_Question_Part", "commonQuestionPart"],
            ["Question_Description", "questionDescription"],
            ["Option1", "option1"],
            ["Option2", "option2"],
            ["Option3", "option3"],
            ["Option4", "option4"],
            ["Answer_option_number", "answer"],
            ["Answer_Description", "answerDescription"],
            ["Complexity", "complexity"],
            ["Image_Links", "imageLinks"],
            ["Type", "type"],
          ].reduce((acc, el) => {
            
            return {
              ...acc,
              [el[1]]: headers.findIndex((element) => element === el[0]),
            };
          }, {});
          

          rows.forEach((row, index) => {
            if (
              row[indexObject.classname] === null ||
              row[indexObject.classname] === undefined
            ) {
              excelData.push({ row: index + 2, error: "Classname" });
              return;
            }
            if (
              row[indexObject.srno] === null ||
              row[indexObject.srno] === undefined
            ) {
              excelData.push({ row: index + 2, error: "Sr No" });
              return;
            }
            if (
              row[indexObject.subjectname] === null ||
              row[indexObject.subjectname] === undefined
            ) {
              excelData.push({ row: index + 2, error: "Subjectname" });
              return;
            }
            if (
              row[indexObject.chaptername] === null ||
              row[indexObject.chaptername] === undefined
            ) {
              excelData.push({ row: index + 2, error: "Chaptername" });
              return;
            }
            if (
              row[indexObject.questionDescription] === null ||
              row[indexObject.questionDescription] === undefined
            ) {
              excelData.push({ row: index + 2, error: "Question Desc" });
              return;
            }
            if (
              row[indexObject.answer] === null ||
              row[indexObject.answer] === undefined
            ) {
              excelData.push({ row: index + 2, error: "answer" });
              return;
            }
            if (
              indexObject.imageLinks !== -1 &&
              row[indexObject.imageLinks] !== null
            ) {
              if (
                row[indexObject.imageLinks]
                  .split(",")
                  .some(
                    (img) =>
                      filesData.find(
                        (file) => file.filename === img.toLowerCase()
                      ) === undefined
                  )
              ) {
                excelData.push({
                  row: index + 2,
                  error: "Image:" + row[indexObject.imageLinks],
                });
                return;
              }
            }

            if (
              row[indexObject.commonQuestionPart] != undefined &&
              row[indexObject.commonQuestionPart].length > 10
            ) {
              
              if (
                commonQuestionTextArr.findIndex((el) => {
                  return el.text == checksum(row[indexObject.commonQuestionPart].replace(/(\r\n|\n|\r|_x000D_)/gm, " "));
                }) == -1
              ) {
                logger.debug("Common Question inserted at :"+questionDetails.length+" Generated Checksum:" + checksum(row[indexObject.commonQuestionPart].replace(/(\r\n|\n|\r|_x000D_)/gm, " ")))
                commonQuestionTextArr.push({
                  text: checksum(row[indexObject.commonQuestionPart].replace(/(\r\n|\n|\r|_x000D_)/gm, " ")),
                  questionIndex: questionDetails.length,
                });
                var commonQuestionData = {
                  srno: row[indexObject.srno],
                  boardname:
                    indexObject.boardname === -1 ||
                    row[indexObject.boardname] === null
                      ? "CBSE"
                      : row[indexObject.boardname],
                  classname: row[indexObject.classname],
                  subjectname: row[indexObject.subjectname],
                  chaptername: row[indexObject.chaptername].toLowerCase(),
                  subTopic : indexObject.subTopic === -1 || row[indexObject.subTopic] === null
                  ? "misc"
                  : row[indexObject.subTopic].trim().toLowerCase(),
                  complexity:"comprehensions",
                  commonQuestionPart: row[indexObject.commonQuestionPart],
                  source: "MasterQuestion",
                  imageLinks:[]
                };

                

                

                

                
                
                console.log(row[indexObject.commonQuestionPart])
                let questionData = createQuestionBody(
                  row,
                  indexObject,
                  filesData,
                );

                
                let newQuestion = new MasterQuestion({
                  ...questionData,
                  filename: excelFileName,
                  checksum: checksum(JSON.stringify(questionData)),
                  updatedon: new Date(),
                  updatedby: userId,
                });

                

                var commonQuestion = new MasterQuestion({
                  ...commonQuestionData,
                  checksum: checksum(JSON.stringify(commonQuestionData)),
                  filename: excelFileName,
                  nestedQuestions:[newQuestion._id],
                  updatedon: new Date(),
                  updatedby: userId,
                });



                questionDetails.push(commonQuestion);
                questionIndex++
                questionDetails.push(newQuestion);
                questionIndex++
              } else {
                let questionData0 = createQuestionBody(
                  row,
                  indexObject,
                  filesData
                );
                let newQuestion0 = new MasterQuestion({
                  ...questionData0,
                  filename: excelFileName,
                  checksum: checksum(JSON.stringify(questionData0)),
                  updatedon: new Date(),
                  updatedby: userId,
                });
                questionDetails.push(newQuestion0);
                logger.debug("Pushing to common question Index :"+commonQuestionTextArr.find(
                  (el) => el.text == checksum(row[indexObject.commonQuestionPart].replace(/(\r\n|\n|\r|_x000D_)/gm, " "))
                ).questionIndex+" Generated Checksum:" + checksum(row[indexObject.commonQuestionPart].replace(/(\r\n|\n|\r|_x000D_)/gm, " ")))
                logger.debug(
                  questionDetails[
                    commonQuestionTextArr.find(
                      (el) => el.text == checksum(row[indexObject.commonQuestionPart].replace(/(\r\n|\n|\r|_x000D_)/gm, " "))
                    ).questionIndex
                  ].srno)
                questionDetails[
                  commonQuestionTextArr.find(
                    (el) => el.text == checksum(row[indexObject.commonQuestionPart].replace(/(\r\n|\n|\r|_x000D_)/gm, " "))
                  ).questionIndex
                ].nestedQuestions.push(newQuestion0._id);
              }
            } else {
              
              var questionData1 = createQuestionBody(row, indexObject, filesData );
              
              const cs = checksum(JSON.stringify(questionData1));

              var newQuestion1 = new MasterQuestion({
                ...questionData1,
                filename: excelFileName,
                checksum: cs,
                updatedon: new Date(),
                updatedby: userId,
              });

              questionDetails.push(newQuestion1);
              questionIndex++
            }

            
          });
          MasterQuestion.insertMany(questionDetails, { ordered: false })
              .then(() => {
                callback(null, ResponseCode.Success, excelData);
              })
              .catch((err) => {
                if (err.code === 11000) {
                  err.writeErrors.forEach((writeError) => {
                    excelData.push({
                      row: writeError.err.op.questionDescription,
                      error: "Duplicate",
                    });
                  });

                  callback(null, ResponseCode.Success, excelData);
                } else {
                  callback(err);
                }
              });
        }
      })
      .catch((err) => {
        callback(err);
      });
  }
};

exports.get_distinct_filename_dbc = (ownerId, callback) => {
  MasterQuestion.distinct("filename")
    .exec()
    .then((filenames) => {
      if (filenames == null) {
        callback(ResponseCode.QuestionNotFound);
      } else {
        callback(null, ResponseCode.Success, filenames);
      }
    })
    .catch((err) => {
      callback(err);
    });
};

exports.delete_by_filename_dbc = (filename, callback) => {
  MasterQuestion.deleteMany({ filename: filename })
    .then(() => {
      callback(null, ResponseCode.Success);
    })
    .catch((err) => {
      callback(err);
    });
};
