const Tutor = require('../models/tutor.model');
const Student = require('../models/student.model');
const Parent = require('../models/parent.model');
const Organization = require('../models/organization.model')
const AttemptedAssessment = require('../models/attemptedassessment.model');
const StudentAttendance = require('../models/studentAttendance.model');
const StudentContent = require('../models/studentContent.model');
const StudentPaymentAccount = require('../models/studentPaymentAccount.model');
const OrganizationPaymentAccount = require('../models/orgPaymentAccount.model');
const PurchasedEdumacPackage = require('../models/purchasedEdumacPackage.model');
const MasterPackage = require('../models/masterPackage.model')
const StudentPayable = require('../models/studentPayble.model')
const Batch = require('../models/batch.model');
const Role = require('../models/role.model');
const User = require('../models/user.model');
const logger = require('../utils/logger.utils');
const { ResponseCode } = require('../utils/response.utils');
var bcrypt = require('bcryptjs');
const crypto = require('crypto');
const dateFns = require('date-fns');
const shortid = require('shortid');
const { getDownloadUrlForVideoKyc } = require('../utils/aws.utils');
const { encryptWithAES } = require('../utils/referralGenerator.utils');
const EmailFactory = require('../utils/emailFactory.utils');
const EmailProvider = EmailFactory.create(process.env.EMAILPROVIDER || 'SendInBlue');
const { updateJWTOnboarding } = require('../utils/passport.utils')
const mongoose = require('mongoose');

exports.tutor_details = (entityId, callback)=>{
    Tutor.findOne({ _id : entityId}).populate('package').exec((err,tutor)=>{
        if(err){
            logger.error('tutor_details - Tutor.findOne - Error - ' + err);
            return callback(err);
        }
        if(tutor != null){
            logger.info('tutor_details - entityId:' + entityId + ' Tutor retrieved successfully');
            callback(null, ResponseCode.Success, tutor);
        }
        else{
            logger.warn('tutor_details - entityId:' + entityId + ' Tutor not found');
            callback(null, ResponseCode.TutorNotFound);
        }
    });
}

exports.tutor_courses = (ownerId, callback)=>{
    Tutor.findOne({ ownerId : ownerId}).populate('courseDetails').exec((err,tutor)=>{
        if(err){
            logger.error('tutor_courses - Tutor.findOne - Error - ' + err);
            callback(err);
        }
        if(tutor != null){
            logger.info('tutor_courses - ownerId:' + ownerId + ' Tutor Courses retrieved successfully');
            return callback(null, ResponseCode.Success, tutor.courseDetails);
        }
        else{
            logger.warn('tutor_courses - ownerId:' + ownerId + ' Tutor not found');
            return callback(null, ResponseCode.TutorNotFound);
        }        
    });
}

exports.tutor_create = (ownerId, userId, entityId, tutor, callback)=>{
    Tutor.findOne({ _id : entityId},(err,tutorDB)=>{
        if(err){
            logger.error('tutor_create - Tutor.findOne - Error - ' + err);
            callback(err);
        }
        if(tutorDB == null){
            crypto.randomBytes(32, (err,buffer) => {
                if(err) {
                    logger.error('tutor_create - crypto.randomBytes - Error - ' + err);
                }
                const emailVerificationToken = buffer.toString("hex");
                const expireEmailVerificationToken = Date.now() + parseInt(process.env.EMAIL_VERIFICATION_BUFFER_TIME)
                const canUseReferralTill = dateFns.addDays(new Date(), 5)
            tutorDB = new Tutor(
                {
                    ownerId: ownerId,
                    mobileNo: tutor.mobileNo,
                    tutorName: tutor.tutorName,
                    enrollmentId: tutor.enrollmentId,
                    emailId: tutor.emailId,
                    qualifications: tutor.qualifications,
                    address: tutor.address,
                    otherQualifications: tutor.otherQualifications,
                    almaMater: tutor.almaMater,
                    schoolName: tutor.schoolName,
                    location: tutor.location,
                    pinCode: tutor.pinCode,
                    stateName: tutor.stateName,
                    cityName: tutor.cityName,
                    image: tutor.image,
                    pan: tutor.pan,
                    aadhaar : tutor.aadhaar,
                    gstin: tutor.gstin,
                    bankIfsc : tutor.bankIfsc,
                    bankAccount : tutor.bankAccount,
                    dob: tutor.dob,
                    package: tutor.package,
                    contentSize: 0,
                    customChapter: 0,
                    quizCount: 0,
                    courseDetails: tutor.courseDetails,
                    kycDetails: tutor.kycDetails,
                    emailVerificationToken: emailVerificationToken,
                    expireEmailVerificationToken: expireEmailVerificationToken,
                    canUseReferralTill: canUseReferralTill,
                }
            );
            tutorDB.userReferralCode = encryptWithAES(`Tutor_${tutorDB._id}`)
            tutorDB.save(function (err, data) {
                if (err) {
                    logger.error('tutor_create - tutor.save - Error - ' + err);
                    return callback(err);
                }

                Role.findOne({name : 'ROLE_TUTOR'}, async (err,role)=>{
                    User.findOneAndUpdate({ $and: [{_id: userId}, {"owner.ownerId": ownerId}]}, {
                        $pull: { "owner.$.roles": { role: role._id} } }, (err, update) => {
                        if(!err) {
                            const newRole = {
                                role: role._id,
                                entityId: data._id
                            }
                            User.findOneAndUpdate({ $and: [{_id: userId}, {"owner.ownerId": ownerId}]}, {
                                $addToSet: { "owner.$.roles": newRole } }, (err, update) => {
                                    if(err) {
                                        logger.error('tutor_create - User.findOneAndUpdate - Error - ' + err);
                                        return callback(err);
                                    }
                                    if(process.env.GLOBALNOTIFICATIONSWITCH === "true" && process.env.SENDEMAIL === "true") {
                                        // mail receiver detail should be array can accept more than one receiver
                                        const mail = [{
                                            name: data.tutorName,
                                            email:data.emailId,
                                        }]
                                        const argsSignUpConfirmation = {type: "Sign_Up" ,tutor_name: data.tutorName, login_page: process.env.PLATFORM_LOGIN_PAGE}
                                        EmailProvider.sendMail(mail, argsSignUpConfirmation, (err, code, data) => {
                                            if(err){
                                                logger.error('tutor_create - SignUpConfirmation sendMail - Error - ' + err);
                                            }
                                            if(code === ResponseCode.Success) {
                                                logger.info('tutor_create - SignUpConfirmation emailId:' + data.emailId
                                                    + ', EMAIL sent successfully');
                                            }
                                            else{
                                                logger.warn('tutor_create - SignUpConfirmation emailId:' + data.emailId
                                                + ', EMAIL sending failed');
                                            }
                                        })
                                        const tokenLink = `${process.env.CLIENT_URL}/tutor/${data._id}/${data.emailVerificationToken}`
                                        const argsConfirmEmail = {type: "Confirm_Email", confirm_link: tokenLink}
                                        EmailProvider.sendMail(mail, argsConfirmEmail, (err, code, data) => {
                                            if(err){
                                                logger.error('tutor_create - ConfirmEmail sendMail - Error - ' + err);
                                            }
                                            if(code === ResponseCode.Success) {
                                                logger.info('tutor_create - ConfirmEmail emailId:' + data.emailId
                                                    + ', EMAIL sent successfully');
                                            }
                                            else{
                                                logger.warn('tutor_create - ConfirmEmail emailId:' + data.emailId
                                                + ', EMAIL sending failed');
                                            }
                                        })
                                    }
                                    // Creating copy of current Edumatica purchased package
                                    // Find master
                                    MasterPackage.findById(data.package).exec((err, masterpackage) => {
                                        if(err) {
                                            logger.error('tutor_create - MasterPackage.findById - Error - ' + err);
                                            return callback(err);
                                        }
                                        if (!masterpackage) {
                                            logger.error("tutor_create - MasterPackage.findById - Error No masterpackage to purchase")
                                            return
                                        }
                                        if (masterpackage) {
                                            const currentEdumacPackage = new PurchasedEdumacPackage({
                                                ownerId : ownerId,
                                                accountType : 'Tutor',                                          // Whether a independent teacher or Organization
                                                entityId: data._id,
                                                name : masterpackage.name,
                                                planId : masterpackage.planId,               // can be trial | Edumatica_Basic | Edumatica_Plus
                                                graceperiod : masterpackage.graceperiod,         
                                                paymentplan: 'PREPAID',
                                                paymentcycle: 'MONTHLY',    //discount not included , Discount will be kept seperate. 
                                                cost: masterpackage.cost,
                                                perstudentcost : masterpackage.perstudentcost,
                                                recordingQuota : masterpackage.recordingQuota,   //In hours 
                                                courseCount: masterpackage.courseCount,
                                                studentCount: masterpackage.studentCount,
                                                // tutorCount: purchasedPackage.tutorCount,     // not applicable for Tutor
                                                batchCount: masterpackage.batchCount,
                                                sessionCount: masterpackage.sessionCount,
                                                contentSize: masterpackage.contentSize,
                                                customChapter: masterpackage.customChapter,
                                            })
                                            currentEdumacPackage.save().then(packageDb => {
                                                // Creating transactional account for the tutor tracks all payments to Edumatica and maintains balance with the platform
                                                // renewal date forwarded by grace for the first time account activation
                                                const currentRenewalDate = dateFns.add(new Date(),{days: packageDb.grace_period})
                                                const newTutorPaymentAcc = new OrganizationPaymentAccount({
                                                    ownerId: ownerId,
                                                    accountType: "Tutor",                                          // Whether a independent teacher or Organization
                                                    entityId: data._id,                                               //If an independent tutor , then tutor Id, otherwise OrganizationId
                                                    purchasedEdumacPackage: packageDb._id,
                                                    retainerShipFeeBalance: packageDb.cost,
                                                    totaldebitBalance: packageDb.cost,
                                                    currentgraceperiod: packageDb.graceperiod,
                                                    renewalDate: currentRenewalDate,    // next renewal will be accordingly or should it be new Date()
                                                })
                                                newTutorPaymentAcc.save().then(payAccDb => {

                                                    logger.info('tutor_create - entityId:' + entityId + ' Tutor Created successfully');
                                                    callback(null, ResponseCode.Success);
                                                }).catch(err => {
                                                    logger.error('tutor_create - OrganizationPaymentAccount.Create - Error - ' + err);
                                                    return callback(err)
                                                })
                                            }).catch(err => {
                                                logger.error('tutor_create - PurchasedEdumacPackage.Create - Error - ' + err);
                                                return callback(err)
                                            })
                                        }
                                    })
                            })
                        }     
                    })
                })
            });
        })
        }
        else{
            Tutor.findOneAndUpdate({_id: entityId},
            {$set: tutor},
            function (err, data) {
                if (err) {
                    logger.error('tutor_create - Tutor.findOneAndUpdate - Error - ' + err);
                    return callback(err);
                }
                logger.info('tutor_create - entityId:' + entityId + ' Tutor updated successfully');
                callback(null, ResponseCode.Success);
            });
        }
    });
}

exports.tutor_onboard = (ownerId, userId, entityId, tutor, callback)=>{
    Tutor.findOne({ _id : entityId},(err,tutorDB)=>{
        if(err){
            logger.error('tutor_onboard - Tutor.findOne - Error - ' + err);
            callback(err);
        }
        if(tutorDB == null){
            crypto.randomBytes(32, (err,buffer) => {
                if(err) {
                    logger.error('tutor_onboard - crypto.randomBytes - Error - ' + err);
                }
                const emailVerificationToken = buffer.toString("hex");
                const expireEmailVerificationToken = Date.now() + parseInt(process.env.EMAIL_VERIFICATION_BUFFER_TIME)
                const canUseReferralTill = dateFns.addDays(new Date(), 5)
            tutorDB = new Tutor(
                {
                    ownerId: ownerId,
                    mobileNo: tutor.mobileNo,
                    tutorName: tutor.tutorName,
                    enrollmentId: tutor.enrollmentId,
                    emailId: tutor.emailId,
                    qualifications: tutor.qualifications,
                    address: tutor.address,
                    otherQualifications: tutor.otherQualifications,
                    almaMater: tutor.almaMater,
                    schoolName: tutor.schoolName,
                    location: tutor.location,
                    pinCode: tutor.pinCode,
                    stateName: tutor.stateName,
                    cityName: tutor.cityName,
                    image: tutor.image,
                    pan: tutor.pan,
                    aadhaar : tutor.aadhaar,
                    gstin: tutor.gstin,
                    bankIfsc : tutor.bankIfsc,
                    bankAccount : tutor.bankAccount,
                    dob: tutor.dob,
                    package: tutor.package,
                    contentSize: 0,
                    customChapter: 0,
                    quizCount: 0,
                    courseDetails: tutor.courseDetails,
                    kycDetails: tutor.kycDetails,
                    emailVerificationToken: emailVerificationToken,
                    expireEmailVerificationToken: expireEmailVerificationToken,
                    canUseReferralTill: canUseReferralTill,
                    roleStatus: tutor.roleStatus ? tutor.roleStatus : 'ACTIVE'
                }
            );
            tutorDB.userReferralCode = encryptWithAES(`Tutor_${tutorDB._id}`)
            tutorDB.save(function (err, data) {
                if (err) {
                    logger.error('tutor_onboard - tutor.save - Error - ' + err);
                    return callback(err);
                }

                Role.findOne({name : 'ROLE_TUTOR'}, async (err,role)=>{
                    User.findOne({_id: userId}, (err, usr) => {
                        const allOwner = [...usr.owner];
                        const ownOwner = allOwner.find(own => String(own.ownerId) === String(ownerId))
                        const allRoles = [...ownOwner.roles]
                        const ownRoleindex = allRoles.findIndex(rol => String(rol.role) === String(role._id))
                        allRoles[ownRoleindex].entityId = data._id

                        User.findOneAndUpdate({ $and: [{_id: userId}, {"owner.ownerId": ownerId}]}, {
                            $set: { "owner.$.roles": allRoles } }, (err, update) => {
                            if(err) {
                                logger.error('tutor_onboard - User.findOneAndUpdate - Error - ' + err);
                                return callback(err);
                            }

                            if(process.env.GLOBALNOTIFICATIONSWITCH === "true" && process.env.SENDEMAIL === "true") {
                                // mail receiver detail should be array can accept more than one receiver
                                const mail = [{
                                    name: data.tutorName,
                                    email:data.emailId,
                                }]
                                const argsSignUpConfirmation = {type: "Sign_Up" ,tutor_name: data.tutorName, login_page: process.env.PLATFORM_LOGIN_PAGE}
                                EmailProvider.sendMail(mail, argsSignUpConfirmation, (err, code, data) => {
                                    if(err){
                                        logger.error('tutor_onboard - SignUpConfirmation sendMail - Error - ' + err);
                                    }
                                    if(code === ResponseCode.Success) {
                                        logger.info('tutor_onboard - SignUpConfirmation emailId:' + data.emailId
                                            + ', EMAIL sent successfully');
                                    }
                                    else{
                                        logger.warn('tutor_onboard - SignUpConfirmation emailId:' + data.emailId
                                        + ', EMAIL sending failed');
                                    }
                                })
                                const tokenLink = `${process.env.CLIENT_URL}/tutor/${data._id}/${data.emailVerificationToken}`
                                const argsConfirmEmail = {type: "Confirm_Email", confirm_link: tokenLink}
                                EmailProvider.sendMail(mail, argsConfirmEmail, (err, code, data) => {
                                    if(err){
                                        logger.error('tutor_onboard - ConfirmEmail sendMail - Error - ' + err);
                                    }
                                    if(code === ResponseCode.Success) {
                                        logger.info('tutor_onboard - ConfirmEmail emailId:' + data.emailId
                                            + ', EMAIL sent successfully');
                                    }
                                    else{
                                        logger.warn('tutor_onboard - ConfirmEmail emailId:' + data.emailId
                                        + ', EMAIL sending failed');
                                    }
                                })
                            }
                            // Creating copy of current Edumatica purchased package
                            // Find master
                            MasterPackage.findById(data.package).exec((err, masterpackage) => {
                                if(err) {
                                    logger.error('tutor_onboard - MasterPackage.findById - Error - ' + err);
                                    return callback(err);
                                }
                                if (!masterpackage) {
                                    logger.error("tutor_onboard - MasterPackage.findById - Error No masterpackage to purchase")
                                    return
                                }
                                if (masterpackage) {
                                    const currentEdumacPackage = new PurchasedEdumacPackage({
                                        ownerId : ownerId,
                                        accountType : 'Tutor',                                          // Whether a independent teacher or Organization
                                        entityId: data._id,
                                        name : masterpackage.name,
                                        planId : masterpackage.planId,               // can be trial | Edumatica_Basic | Edumatica_Plus
                                        graceperiod : masterpackage.graceperiod,         
                                        paymentplan: 'PREPAID',
                                        paymentcycle: 'MONTHLY',    //discount not included , Discount will be kept seperate. 
                                        cost: masterpackage.cost,
                                        perstudentcost : masterpackage.perstudentcost,
                                        recordingQuota : masterpackage.recordingQuota,   //In hours 
                                        courseCount: masterpackage.courseCount,
                                        studentCount: masterpackage.studentCount,
                                        // tutorCount: purchasedPackage.tutorCount,     // not applicable for Tutor
                                        batchCount: masterpackage.batchCount,
                                        sessionCount: masterpackage.sessionCount,
                                        contentSize: masterpackage.contentSize,
                                        customChapter: masterpackage.customChapter,
                                    })
                                    currentEdumacPackage.save().then(packageDb => {
                                        // Creating transactional account for the tutor tracks all payments to Edumatica and maintains balance with the platform
                                        // renewal date forwarded by grace for the first time account activation
                                        const currentRenewalDate = dateFns.add(new Date(),{days: packageDb.grace_period})
                                        const newTutorPaymentAcc = new OrganizationPaymentAccount({
                                            ownerId: ownerId,
                                            accountType: "Tutor",                                          // Whether a independent teacher or Organization
                                            entityId: data._id,                                               //If an independent tutor , then tutor Id, otherwise OrganizationId
                                            purchasedEdumacPackage: packageDb._id,
                                            retainerShipFeeBalance: packageDb.cost,
                                            totaldebitBalance: packageDb.cost,
                                            currentgraceperiod: packageDb.graceperiod,
                                            renewalDate: currentRenewalDate,    // next renewal will be accordingly or should it be new Date()
                                        })
                                        newTutorPaymentAcc.save().then(payAccDb => {

                                            logger.info('tutor_onboard - entityId:' + entityId + ' Tutor Created successfully');
                                            User.aggregate([
                                                { $match : { $and: [{_id: mongoose.Types.ObjectId(userId)}] } },
                                                { $unwind: "$owner" },
                                                { $unwind: "$owner.roles" },
                                                { 
                                                    $lookup: {
                                                        "from": "roles",
                                                        "localField": "owner.roles.role",
                                                        "foreignField": "_id",
                                                        "as": "owner.roles.role"
                                                    }
                                                },
                                                { $unwind: "$owner.roles.role" },
                                                { 
                                                    $lookup: {
                                                        "from": "abilityStores",
                                                        "localField": "owner.roles.entityId",
                                                        "foreignField": "consumerId",
                                                        "as": "owner.roles.permissions.accesspermissions"
                                                    }
                                                },
                                                { 
                                                    $lookup: {
                                                        "from": "abilityStores",
                                                        "localField": "_id",
                                                        "foreignField": "userId",
                                                        "as": "owner.roles.permissions.controlpermissions"
                                                    }
                                                },
                                                {
                                                    $group: {
                                                        _id: { _id : "$_id" , password: "$password", mobile: "$mobile",
                                                            status: "$status", failedAttemptCount: "$failedAttemptCount", 
                                                            failedOn: "$failedOn", __v: "$__v", updatedon: "$updatedon",
                                                            updatedby: "$updatedby", owner_id: "$owner._id",
                                                            owner_ownerId: "$owner.ownerId"
                                                        },
                                                        roles: { "$push": "$owner.roles" }
                                                    }
                                                },
                                                {
                                                    $group: {
                                                        _id: { _id : "$_id._id" , password: "$_id.password", mobile: "$_id.mobile",
                                                            status: "$_id.status", failedAttemptCount: "$_id.failedAttemptCount", 
                                                            failedOn: "$_id.failedOn", __v: "$_id.__v", updatedon: "$_id.updatedon",
                                                            updatedby: "$_id.updatedby"
                                                        },
                                                        owner: { "$push": {_id: "$_id.owner_id", ownerId: "$_id.owner_ownerId", roles: "$roles" } }
                                                    }
                                                },
                                                {
                                                    $project: { 
                                                        _id: "$_id._id", owner: 1, password: "$_id.password", mobile: "$_id.mobile",
                                                        status: "$_id.status", updatedon: "$_id.updatedon", updatedby: "$_id.updatedby",
                                                        failedAttemptCount: "$_id.failedAttemptCount", failedOn: "$_id.failedOn", __v: "$_id.__v"
                                                    }
                                                },
                                                { $sort: { _id: 1 } }
                                            ], async (err, data) => {
                                                data = data[0]
                                                const {accessToken, refreshToken} = await updateJWTOnboarding(data)
                                                callback(null, ResponseCode.Success, accessToken, refreshToken, data);  
                                            })
                                        }).catch(err => {
                                            logger.error('tutor_onboard - OrganizationPaymentAccount.Create - Error - ' + err);
                                            return callback(err)
                                        })
                                    }).catch(err => {
                                        logger.error('tutor_onboard - PurchasedEdumacPackage.Create - Error - ' + err);
                                        return callback(err)
                                    })
                                }
                            })
                        })
                    })
                })
            });
        })
        }
        else{
            Tutor.findOneAndUpdate({_id: entityId},
            {$set: tutor},
            function (err, data) {
                if (err) {
                    logger.error('tutor_onboard - Tutor.findOneAndUpdate - Error - ' + err);
                    return callback(err);
                }
                logger.info('tutor_onboard - entityId:' + entityId + ' Tutor updated successfully');
                callback(null, ResponseCode.Success);
            });
        }
    });
}

exports.tutor_addStudents = (ownerId, userId, students, callback)=>{
    Tutor.findOne({ownerId : ownerId}).populate('package').exec((err,tutor)=>{
        if(err){
            logger.error('tutor_addStudents - Tutor.findOne - Error - ' + err);
            callback(err);
        }
        if(tutor != null){
            if(students.length + tutor.studentList.length <= tutor.package.studentCount){
                var studentMobileNos = [];
                var parentMobileNos = [];
                students.forEach(student => {
                    studentMobileNos.push(student.mobileNo);
                    if(student.parentMobileNo.length > 0){
                        parentMobileNos.push(student.parentMobileNo);
                    }
                });
                Tutor.findOne({ownerId : ownerId}).exec((err, tutorDb) => {
                    if(err) {
                        logger.error('tutor_addStudents - Tutor.findOne Find Tutor as Student - Error - ' + err);
                        callback(err);
                    }
                    if(!tutorDb) {
                        logger.warn('tutor_addStudents - ownerId:' + ownerId + ' Tutor not found');    
                        callback(null, ResponseCode.TutorNotFound);
                    }
                    if(tutorDb) {
                         // Check if Tutor is being added a Student
                        if (studentMobileNos.includes(tutorDb.mobileNo)) {
                            logger.warn("tutor_addStudents - Tutor.findOne Find Tutor as Student - Tutor cannot be a student");
                            callback(null, ResponseCode.TutorAsStudentError)
                            return
                        } else {
                            Student.find({$and: [{ownerId: ownerId},{mobileNo: {$in: studentMobileNos}}]}).exec((err, studentsDB)=>{
                                if(err){
                                    logger.error('tutor_addStudents - Student.find - Error - ' + err);
                                    return callback(err);
                                }
                                Parent.find({$and: [{ownerId: ownerId},{mobileNo: {$in: parentMobileNos}}]}).exec((err, parentsDB)=>{
                                    if(err){
                                        logger.error('tutor_addStudents - Parent.find - Error - ' + err);
                                        return callback(err);
                                    }
                                    var studentsToSave = [];
                                    var studentIdsToSave = [];
                                    var parentsToSave = [];
                                    students.forEach(student => {
                                        // New Student -- Create and add to Tutor
                                        var studentDB = studentsDB.filter(function (el) {
                                            return el.mobileNo == student.mobileNo;
                                        })[0];
                                        if(!studentDB){                                
                                            let newStudent = new Student(
                                                {
                                                    ownerId: ownerId,
                                                    mobileNo: student.mobileNo,
                                                    enrollmentId: student.enrollmentId,
                                                    emailId: student.emailId,
                                                    studentName: student.studentName,
                                                    location: student.location,
                                                    pinCode: student.pinCode,
                                                    stateName: student.stateName,
                                                    cityName: student.cityName,
                                                    schoolName: student.schoolName,
                                                    boardName: student.boardName,
                                                    className: student.className,
                                                    status: 1,
                                                    updatedon: new Date(),
                                                    updatedby: userId,
                                                    roleStatus: "FRESHER"
                                                }
                                            );
                                            if(student.parentMobileNo.length > 0){
                                                var parentDB = parentsDB.filter(function (el) {
                                                    return el.mobileNo == student.parentMobileNo;
                                                })[0];
                                                if(!parentDB){
                                                    let newParent = new Parent(
                                                        {
                                                            ownerId: ownerId,
                                                            mobileNo: student.parentMobileNo,
                                                            status: 1,
                                                            updatedon: new Date(),
                                                            updatedby: userId,
                                                            roleStatus: "FRESHER"
                                                        }
                                                    );
                                                    parentsToSave.push(newParent);
                                                    newStudent.parent = newParent._id;
                                                }
                                                else{
                                                    newStudent.parent = parentDB._id;
                                                }
                                            }
                                            studentsToSave.push(newStudent);
                                            studentIdsToSave.push(newStudent._id);
                                        }  //// Existing student -- Add to Tutor if not already added   
                                        else{
                                            if(!tutor.studentList.includes(studentDB._id.toString())){
                                                studentIdsToSave.push(studentDB._id);
                                            }
                                        }
                                    });
                                    if(parentsToSave.length > 0){
                                        Parent.insertMany(parentsToSave, function (err) {
                                            if (err) {
                                                logger.error('tutor_addStudents - Parent.insertMany - Error - ' + err);
                                                return callback(err);
                                            }
                                        });
                                    }
                                    if(studentsToSave.length > 0){
                                        Student.insertMany(studentsToSave, function (err) {
                                            if (err) {
                                                logger.error('tutor_addStudents - Student.insertMany - Error - ' + err);
                                                return callback(err);
                                            }
                                        });
                                    }
                                    if(studentIdsToSave.length > 0) {
                                        Tutor.findOneAndUpdate({"_id":tutor._id},{$push: {studentList: {$each:studentIdsToSave}}},(err,data)=>{
                                            if(err){
                                                logger.error('tutor_addStudents - Tutor.findOneAndUpdate - Error - ' + err);
                                                return callback(err);
                                            }
                                        }) 
                                        var studentPaymentAccountsToSave = [];
                                        studentIdsToSave.forEach(ele => {
                                            const newStudentPaymentAcc = new StudentPaymentAccount({
                                                ownerId: ownerId,
                                                entityId: ele,
                                                student:  ele,
                                                status: 1,
                                                updatedon: new Date(),
                                                updatedby: userId,
                                            })
                                            studentPaymentAccountsToSave.push(newStudentPaymentAcc)
                                        })
                                        if(studentPaymentAccountsToSave.length > 0) {
                                            StudentPaymentAccount.insertMany(studentPaymentAccountsToSave).then(result => {
                                                logger.info('tutor_addStudents - ownerId:' + ownerId + ' Student(s) added successfully');
                                                callback(null, ResponseCode.Success);
                                            }).catch(err => {
                                                logger.error('tutor_addStudents - StudentPaymentAccount.insertMany - Error - ' + err);
                                                return callback(err);
                                            })
                                        }
                                    }
                                    else{
                                        logger.warn('tutor_addStudents - ownerId:' + ownerId + ' Tutor student already exist');    
                                        callback(null, ResponseCode.TutorStudentAlreadyExist);
                                    }
                                });
                            });
                        }
                    }
                })
            } 
            else {
                logger.warn('tutor_addStudents - ownerId:' + ownerId + ' Tutor student limit exceeded');    
                callback(null, ResponseCode.StudentLimitExceeded);
            }
        }
        else{       
            logger.warn('tutor_addStudents - ownerId:' + ownerId + ' Tutor not found');    
            callback(null, ResponseCode.TutorNotFound);
        }
    });
}

exports.tutor_getStudents = (ownerId, callback)=>{
    Tutor.findOne({ownerId : ownerId}).populate('studentList').lean().exec((err,tutor)=>{
        if(err){
            logger.error('tutor_getStudents - Tutor.findOne - Error - ' + err);
            callback(err);
        }
        if(tutor != null){
            logger.info('tutor_getStudents - ownerId:' + ownerId + ' Tutor Students retrieved successfully');
            const studentListDb = tutor.studentList;
            const parentIds = studentListDb.map((student) => student.parent);
            Parent.find({_id: {$in: parentIds}}).then((parents, err) => {
                if(err){
                    logger.error('tutor_getStudents - Parent.findOne - Error - ' + err);
                    callback(err);
                }
                logger.debug(parents)
                const studentList = studentListDb.map((student) => {
                    if(!student.parent) {
                        return student
                    } else {
                        let toReturn;
                        parents.every((parent) => {
                            if(parent._id.toString() === student.parent.toString()){
                              logger.debug(parent.mobileNo)
                                student = {
                                    ...student,
                                    parentMobileNo: parent.mobileNo
                                }
                                logger.debug(student)

                                toReturn = student
                                return false;
                            } else return true;
                        });
                        return toReturn
                    }
                })

                return callback(null, ResponseCode.Success, studentList);
            })
        }
        else{
            logger.warn('tutor_getStudents - ownerId:' + ownerId + ' Tutor Not found');
            return callback(null, ResponseCode.TutorNotFound);
        }
    });
}

exports.tutor_getStudents_id = (ownerId, tutorid, callback)=>{
    Tutor.findOne({ownerId : ownerId, _id: tutorid}).populate('studentList').lean().exec((err,tutor)=>{
        if(err){
            logger.error('tutor_getStudents - Tutor.findOne - Error - ' + err);
            callback(err);
        }
        if(tutor != null){
            logger.info('tutor_getStudents - ownerId:' + ownerId + ' Tutor Students retrieved successfully');
            const studentListDb = tutor.studentList;
            const parentIds = studentListDb.map((student) => student.parent);
            Parent.find({_id: {$in: parentIds}}).then((parents, err) => {
                if(err){
                    logger.error('tutor_getStudents - Parent.findOne - Error - ' + err);
                    callback(err);
                }
                logger.debug(parents)
                const studentList = studentListDb.map((student) => {
                    if(!student.parent) {
                        return student
                    } else {
                        let toReturn;
                        parents.every((parent) => {
                            if(parent._id.toString() === student.parent.toString()){
                              logger.debug(parent.mobileNo)
                                student = {
                                    ...student,
                                    parentMobileNo: parent.mobileNo
                                }
                                logger.debug(student)

                                toReturn = student
                                return false;
                            } else return true;
                        });
                        return toReturn
                    }
                })

                return callback(null, ResponseCode.Success, studentList);
            })
        }
        else{
            logger.warn('tutor_getStudents - ownerId:' + ownerId + ' Tutor Not found');
            return callback(null, ResponseCode.TutorNotFound);
        }
    });
}

exports.tutor_getStudentsbyParent = (entityId, callback)=> {
    Student.find({parent : entityId}).exec((err,students)=>{
        if(err){
            logger.error('tutor_getStudentsbyParent - Student.find - Error - ' + err);
            return callback(err);
        }
        return callback(null, ResponseCode.Success, students);
    })
}

// Delete studnet payable and purchased course package
exports.tutor_removeStudentsForTutor = (ownerId, students, callback)=>{
    Student.find({$and: [{ownerId: ownerId},{mobileNo: {$in: students}}]}).select({"_id" : 1}).exec((err,data)=>{
        if(err){
            logger.error('tutor_removeStudentForTutor - Student.find - Error - ' + err);
            return callback(err);
        }
        var studentsIds = data.map((item) =>  {return item['_id'].toString()});
        if(studentsIds.length > 0){
            Tutor.findOne({ownerId : ownerId}).exec((err,tutor)=>{
                if(err){
                    logger.error('tutor_removeStudentForTutor - Tutor.findOne - Error - ' + err);
                    callback(err);
                }
                if(tutor != null && tutor.studentList != null && tutor.studentList.length > 0){
                    tutor.studentList = tutor.studentList.filter( ( el ) => !studentsIds.includes( el.toString() ) );
                    tutor.save(function (err) {
                        if (err) {
                            logger.error('tutor_removeStudentForTutor - tutor.save -  Error - ' + err);
                            return callback(err);
                        }

                        Student.updateMany({_id: {$in: studentsIds}}, { $set: { "roleStatus":  "DEACTIVATE" } }, (err, data) => {})
                        StudentPayable.updateMany({student: {$in: studentsIds}},{ $set: { "currentStatus":  "DEACTIVATE" } }, (err, data) => {})
                        OrganizationPaymentAccount.findOneAndUpdate({$and: [{ownerId:ownerId},{accountType:"Tutor"}]},{$pull: { studentPayable: {$in : studentsIds} }}, (err, data) => {})

                        for (let index = 0; index < studentsIds.length; index++) {
                            const element = studentsIds[index];
                            Batch.updateMany({students : element}, { $pull: { students: element } }, (err, data) => {})
                        }
                        
                        AttemptedAssessment.deleteMany({ studentId: {$in: studentsIds}}, (err, data) => {})
                        StudentAttendance.deleteMany({ student: {$in: studentsIds}}, (err, data) => {})
                        StudentContent.deleteMany({ student: {$in: studentsIds}}, (err, data) => {})
                        
                        logger.info('tutor_removeStudentForTutor - ownerId:' + ownerId + ' Students removed for tutor successfully');
                        return callback(null, ResponseCode.Success, studentsIds);
                    })
                }
                else{
                    logger.warn('tutor_removeStudentForTutor - ownerId:' + ownerId + ' Student(s) not found in Tutor');      
                    callback(null, ResponseCode.StudentNotFound);  
                }
            })
        }
        else{
            logger.warn('tutor_removeStudentForTutor - ownerId:' + ownerId + ' Student(s) not found');      
            callback(null, ResponseCode.StudentNotFound);  
        }
    })
}

exports.tutor_updateKYCDocs = (ownerId, kycDocs, callback)=>{
    Tutor.findOne({ownerId : ownerId},(err,tutor)=>{
        if(err){
            logger.error('tutor_updateKYCDocs - Tutor.findOne - Error - ' + err);
            callback(err);
        }
        if(tutor != null){
            tutor.kycDetails = kycDocs;
            Tutor.findOneAndUpdate({ownerId : ownerId},
                {$set: tutor},
                function (err,data){
                if(err){
                    logger.error('tutor_updateKYCDocs - Tutor.findOneAndUpdate - Error - ' + err);
                    return callback(err);
                }
                logger.info('tutor_updateKYCDocs - ownerId:' + ownerId + ' Doc(s) added successfully');
                callback(null, ResponseCode.Success);
            })
        }
        else{     
            logger.warn('tutor_updateKYCDocs - ownerId:' + ownerId + ' Tutor not found');      
            callback(null, ResponseCode.TutorNotFound);
        }
    });
}

exports.student_details = (entityId, callback)=>{
    Student.findOne({_id : entityId}).populate('parent').lean().exec((err, student) => {
        if(err){
            logger.error('student_details - Student.findOne - Error - ' + err);
            callback(err);
        }
        if(student != null){
            if(student.parent!=undefined){
                student = {...student, ...{
                    parentMobileNo: student.parent.mobileNo,
                    parent: student.parent._id
                }}
            }
            
            logger.info('student_details - entityId:' + entityId + ' Student retrieved successfully');
            callback(null, ResponseCode.Success, student);
        }
        else{
            logger.warn('student_details - entityId:' + entityId + ' Student not found');
            callback(null, ResponseCode.StudentNotFound);
        }
    })
}

exports.student_update = (ownerId, userId, studentReq, callback) => {
  Student.findOne(
    { ownerId: ownerId, mobileNo: studentReq.mobileNo },
    (err, student) => {
      if (err) {
        logger.error("student_update - Student.findOne - Error - " + err);
        callback(err);
      }
      if (student == null) {
        logger.warn(
          "student_update - ownerId:" +
            ownerId +
            " mobileNo:" +
            studentReq.mobileNo +
            " Student not found"
        );
        callback(null, ResponseCode.StudentNotFound);
      } else {
        if (
          studentReq.parentMobileNo != "" &&
          studentReq.parentMobileNo != undefined
        ) {
          Parent.findOne({
            ownerId: ownerId,
            mobileNo: studentReq.parentMobileNo,
          })
            .exec()
            .then((parent) => {
              if (parent == null) {
                if (!studentReq.parent) {
                  const newParent = new Parent({
                    ownerId: ownerId,
                    mobileNo: studentReq.parentMobileNo,
                    parentName: "",
                    emailId: "",
                    status: 1,
                    updatedon: new Date(),
                    updatedby: ownerId,
                    roleStatus: "FRESHER",
                  });


                  newParent.save().then((parent) => {
                    
                    student.parent = parent._id;
                    student.emailId = studentReq.emailId,
                    student.studentName = studentReq.studentName;
                    student.enrollmentId = studentReq.enrollmentId;
                    student.boardName = studentReq.boardName;
                    student.className = studentReq.className;
                    student.location = studentReq.location;
                    student.pinCode = studentReq.pinCode;
                    student.mobileNo =
                      student.roleStatus != "FRESHER"
                        ? studentReq.mobileNo
                        : student.mobileNo;
                    student.image = studentReq.image
                    student.stateName = studentReq.stateName
                    student.schoolName = studentReq.schoolName
                    student.roleStatus = studentReq.roleStatus
                      ? studentReq.roleStatus
                      : student.roleStatus
                      ? student.roleStatus
                      : "FRESHER"
                      student.dob = studentReq.dob
                        ? studentReq.dob
                        : student.dob
                        ? student.dob
                        : ""
                      student.gender = studentReq.gender
                        ? studentReq.gender
                        : student.gender
                        ? student.gender
                        : ""
                    student.country = studentReq.country ? studentReq.country : (student.country ? student.country : '');
                    logger.debug(student);
                    student
                      .save()
                      .then((student) => {
                        logger.info(
                          "student_update - ownerId:" +
                            ownerId +
                            " mobileNo:" +
                            studentReq.mobileNo +
                            " Student updated successfully"
                        );
                        return callback(null, ResponseCode.Success);
                      })
                      .catch((err) => {
                        logger.info(
                          "student_update - ownerId:" +
                            ownerId +
                            " mobileNo:" +
                            studentReq.mobileNo +
                            " Student save failed"
                        );
                        return callback(err);
                      });
                  });
                } }
                else if (parent.roleStatus == "FRESHER") {
                  parent.mobileNo = studentReq.parentMobileNo;
                  parent
                    .save()
                    .then(() => {
                      logger.info(
                        "Parent Mobile Number updated for ownerId:" +
                          ownerId +
                          " mobileNo:" +
                          studentReq.mobileNo
                      );
                      (student.emailId = studentReq.emailId),
                        (student.studentName = studentReq.studentName);
                      student.enrollmentId = studentReq.enrollmentId;
                      student.boardName = studentReq.boardName;
                      student.className = studentReq.className;
                      student.location = studentReq.location;
                      student.pinCode = studentReq.pinCode;
                      student.mobileNo =
                        student.roleStatus != "FRESHER"
                          ? studentReq.mobileNo
                          : student.mobileNo;
                      (student.image = studentReq.image),
                        (student.stateName = studentReq.stateName),
                        (student.schoolName = studentReq.schoolName);
                      (student.roleStatus = studentReq.roleStatus
                        ? studentReq.roleStatus
                        : student.roleStatus
                        ? student.roleStatus
                        : "FRESHER"),
                        (student.dob = studentReq.dob
                          ? studentReq.dob
                          : student.dob
                          ? student.dob
                          : ""),
                        (student.gender = studentReq.gender
                          ? studentReq.gender
                          : student.gender
                          ? student.gender
                          : "");
                        student.country = studentReq.country ? studentReq.country : (student.country ? student.country : '');
                      logger.debug(student);
                      student
                        .save()
                        .then(() => {
                          logger.info(
                            "student_update - ownerId:" +
                              ownerId +
                              " mobileNo:" +
                              studentReq.mobileNo +
                              " Student updated successfully"
                          );
                          return callback(null, ResponseCode.Success);
                        })
                        .catch((err) => {
                          logger.info(
                            "student_update - ownerId:" +
                              ownerId +
                              " mobileNo:" +
                              studentReq.mobileNo +
                              " Student save failed"
                          );
                          return callback(err);
                        });
                    })
                    .catch((err) => {
                      logger.error("Parent.save() error: " + err);
                    });
                } else if(parent.roleStatus == "ACTIVE") {
                    (student.emailId = studentReq.emailId);
                    (student.studentName = studentReq.studentName);
                  student.enrollmentId = studentReq.enrollmentId;
                  student.boardName = studentReq.boardName;
                  student.className = studentReq.className;
                  student.location = studentReq.location;
                  student.pinCode = studentReq.pinCode;
                  student.mobileNo =
                    student.roleStatus != "FRESHER"
                      ? studentReq.mobileNo
                      : student.mobileNo;
                  (student.image = studentReq.image),
                    (student.stateName = studentReq.stateName),
                    (student.schoolName = studentReq.schoolName);
                  (student.roleStatus = studentReq.roleStatus
                    ? studentReq.roleStatus
                    : student.roleStatus
                    ? student.roleStatus
                    : "FRESHER"),
                    (student.dob = studentReq.dob
                      ? studentReq.dob
                      : student.dob
                      ? student.dob
                      : ""),
                    (student.gender = studentReq.gender
                      ? studentReq.gender
                      : student.gender
                      ? student.gender
                      : "");
                      student.country = studentReq.country ? studentReq.country : (student.country ? student.country : '');
                      logger.debug(student);
                      student
                        .save()
                        .then(() => {
                          logger.info(
                            "student_update - ownerId:" +
                              ownerId +
                              " mobileNo:" +
                              studentReq.mobileNo +
                              " Student updated successfully"
                          );
                          return callback(null, ResponseCode.Success);
                        })
                        .catch((err) => {
                          logger.info(
                            "student_update - ownerId:" +
                              ownerId +
                              " mobileNo:" +
                              studentReq.mobileNo +
                              " Student save failed"
                          );
                          return callback(err);
                        });
                }

            })
            .catch((err) => {
              logger.error("Parent.findOne error: " + err);
            });
        }
        else{
            (student.emailId = studentReq.emailId);
                    (student.studentName = studentReq.studentName);
                  student.enrollmentId = studentReq.enrollmentId;
                  student.boardName = studentReq.boardName;
                  student.className = studentReq.className;
                  student.location = studentReq.location;
                  student.pinCode = studentReq.pinCode;
                  student.mobileNo =
                    student.roleStatus != "FRESHER"
                      ? studentReq.mobileNo
                      : student.mobileNo;
                  (student.image = studentReq.image),
                    (student.stateName = studentReq.stateName),
                    (student.schoolName = studentReq.schoolName);
                  (student.roleStatus = studentReq.roleStatus
                    ? studentReq.roleStatus
                    : student.roleStatus
                    ? student.roleStatus
                    : "FRESHER"),
                    (student.dob = studentReq.dob
                      ? studentReq.dob
                      : student.dob
                      ? student.dob
                      : ""),
                    (student.gender = studentReq.gender
                      ? studentReq.gender
                      : student.gender
                      ? student.gender
                      : "");
                    student
                        .save()
                        .then(() => {
                          logger.info(
                            "student_update - ownerId:" +
                              ownerId +
                              " mobileNo:" +
                              studentReq.mobileNo +
                              " Student updated successfully"
                          );
                          return callback(null, ResponseCode.Success);
                        })
                        .catch((err) => {
                          logger.info(
                            "student_update - ownerId:" +
                              ownerId +
                              " mobileNo:" +
                              studentReq.mobileNo +
                              " Student save failed"
                          );
                          return callback(err);
                        });
            
        }
      }
    }
  );
};

exports.student_onboard = (ownerId, userId, studentReq, callback) => {
  Student.findOne(
    { ownerId: ownerId, mobileNo: studentReq.mobileNo },
    (err, student) => {
      if (err) {
        logger.error("student_onboard - Student.findOne - Error - " + err);
        callback(err);
      }
      if (student == null) {
        logger.warn(
          "student_onboard - ownerId:" +
            ownerId +
            " mobileNo:" +
            studentReq.mobileNo +
            " Student not found"
        );
        callback(null, ResponseCode.StudentNotFound);
      } else {
        if (
          studentReq.parentMobileNo != "" &&
          studentReq.parentMobileNo != undefined
        ) {
          Parent.findOne({
            ownerId: ownerId,
            mobileNo: studentReq.parentMobileNo,
          })
            .exec()
            .then((parent) => {
              if (parent == null) {
                if (!studentReq.parent) {
                  const newParent = new Parent({
                    ownerId: ownerId,
                    mobileNo: studentReq.parentMobileNo,
                    parentName: "",
                    emailId: "",
                    status: 1,
                    updatedon: new Date(),
                    updatedby: ownerId,
                    roleStatus: "FRESHER",
                  });


                  newParent.save().then((parent) => {
                    
                    student.parent = parent._id;
                    student.emailId = studentReq.emailId,
                    student.studentName = studentReq.studentName;
                    student.enrollmentId = studentReq.enrollmentId;
                    student.boardName = studentReq.boardName;
                    student.className = studentReq.className;
                    student.location = studentReq.location;
                    student.pinCode = studentReq.pinCode;
                    student.mobileNo =
                      student.roleStatus != "FRESHER"
                        ? studentReq.mobileNo
                        : student.mobileNo;
                    student.image = studentReq.image
                    student.stateName = studentReq.stateName
                    student.schoolName = studentReq.schoolName
                    student.roleStatus = studentReq.roleStatus
                      ? studentReq.roleStatus
                      : student.roleStatus
                      ? student.roleStatus
                      : "FRESHER"
                      student.dob = studentReq.dob
                        ? studentReq.dob
                        : student.dob
                        ? student.dob
                        : ""
                      student.gender = studentReq.gender
                        ? studentReq.gender
                        : student.gender
                        ? student.gender
                        : ""
                    student.country = studentReq.country ? studentReq.country : (student.country ? student.country : '');
                    logger.debug(student);
                    student
                      .save()
                      .then((student) => {
                        logger.info(
                          "student_onboard - ownerId:" +
                            ownerId +
                            " mobileNo:" +
                            studentReq.mobileNo +
                            " Student updated successfully"
                        );
                        return callback(null, ResponseCode.Success);
                      })
                      .catch((err) => {
                        logger.info(
                          "student_onboard - ownerId:" +
                            ownerId +
                            " mobileNo:" +
                            studentReq.mobileNo +
                            " Student save failed"
                        );
                        return callback(err);
                      });
                  });
                } }
                else if (parent.roleStatus == "FRESHER") {
                  parent.mobileNo = studentReq.parentMobileNo;
                  parent
                    .save()
                    .then(() => {
                      logger.info(
                        "Parent Mobile Number updated for ownerId:" +
                          ownerId +
                          " mobileNo:" +
                          studentReq.mobileNo
                      );
                      (student.emailId = studentReq.emailId),
                        (student.studentName = studentReq.studentName);
                      student.enrollmentId = studentReq.enrollmentId;
                      student.boardName = studentReq.boardName;
                      student.className = studentReq.className;
                      student.location = studentReq.location;
                      student.pinCode = studentReq.pinCode;
                      student.mobileNo =
                        student.roleStatus != "FRESHER"
                          ? studentReq.mobileNo
                          : student.mobileNo;
                      (student.image = studentReq.image),
                        (student.stateName = studentReq.stateName),
                        (student.schoolName = studentReq.schoolName);
                      (student.roleStatus = studentReq.roleStatus
                        ? studentReq.roleStatus
                        : student.roleStatus
                        ? student.roleStatus
                        : "FRESHER"),
                        (student.dob = studentReq.dob
                          ? studentReq.dob
                          : student.dob
                          ? student.dob
                          : ""),
                        (student.gender = studentReq.gender
                          ? studentReq.gender
                          : student.gender
                          ? student.gender
                          : "");
                        student.country = studentReq.country ? studentReq.country : (student.country ? student.country : '');
                      logger.debug(student);
                      student
                        .save()
                        .then(() => {
                          logger.info(
                            "student_onboard - ownerId:" +
                              ownerId +
                              " mobileNo:" +
                              studentReq.mobileNo +
                              " Student updated successfully"
                          );
                          return callback(null, ResponseCode.Success);
                        })
                        .catch((err) => {
                          logger.info(
                            "student_onboard - ownerId:" +
                              ownerId +
                              " mobileNo:" +
                              studentReq.mobileNo +
                              " Student save failed"
                          );
                          return callback(err);
                        });
                    })
                    .catch((err) => {
                      logger.error("Parent.save() error: " + err);
                    });
                } else if(parent.roleStatus == "ACTIVE") {
                    (student.emailId = studentReq.emailId);
                    (student.studentName = studentReq.studentName);
                  student.enrollmentId = studentReq.enrollmentId;
                  student.boardName = studentReq.boardName;
                  student.className = studentReq.className;
                  student.location = studentReq.location;
                  student.pinCode = studentReq.pinCode;
                  student.mobileNo =
                    student.roleStatus != "FRESHER"
                      ? studentReq.mobileNo
                      : student.mobileNo;
                  (student.image = studentReq.image),
                    (student.stateName = studentReq.stateName),
                    (student.schoolName = studentReq.schoolName);
                  (student.roleStatus = studentReq.roleStatus
                    ? studentReq.roleStatus
                    : student.roleStatus
                    ? student.roleStatus
                    : "FRESHER"),
                    (student.dob = studentReq.dob
                      ? studentReq.dob
                      : student.dob
                      ? student.dob
                      : ""),
                    (student.gender = studentReq.gender
                      ? studentReq.gender
                      : student.gender
                      ? student.gender
                      : "");
                      student.country = studentReq.country ? studentReq.country : (student.country ? student.country : '');
                      logger.debug(student);
                      student
                        .save()
                        .then(() => {
                          logger.info(
                            "student_onboard - ownerId:" +
                              ownerId +
                              " mobileNo:" +
                              studentReq.mobileNo +
                              " Student updated successfully"
                          );
                          return callback(null, ResponseCode.Success);
                        })
                        .catch((err) => {
                          logger.info(
                            "student_onboard - ownerId:" +
                              ownerId +
                              " mobileNo:" +
                              studentReq.mobileNo +
                              " Student save failed"
                          );
                          return callback(err);
                        });
                }

            })
            .catch((err) => {
              logger.error("Parent.findOne error: " + err);
            });
        }
        else{
            (student.emailId = studentReq.emailId);
                    (student.studentName = studentReq.studentName);
                  student.enrollmentId = studentReq.enrollmentId;
                  student.boardName = studentReq.boardName;
                  student.className = studentReq.className;
                  student.location = studentReq.location;
                  student.pinCode = studentReq.pinCode;
                  student.mobileNo =
                    student.roleStatus != "FRESHER"
                      ? studentReq.mobileNo
                      : student.mobileNo;
                  (student.image = studentReq.image),
                    (student.stateName = studentReq.stateName),
                    (student.schoolName = studentReq.schoolName);
                  (student.roleStatus = studentReq.roleStatus
                    ? studentReq.roleStatus
                    : student.roleStatus
                    ? student.roleStatus
                    : "FRESHER"),
                    (student.dob = studentReq.dob
                      ? studentReq.dob
                      : student.dob
                      ? student.dob
                      : ""),
                    (student.gender = studentReq.gender
                      ? studentReq.gender
                      : student.gender
                      ? student.gender
                      : "");
                    student
                        .save()
                        .then(() => {
                          logger.info(
                            "student_onboard - ownerId:" +
                              ownerId +
                              " mobileNo:" +
                              studentReq.mobileNo +
                              " Student updated successfully"
                          );
                          return callback(null, ResponseCode.Success);
                        })
                        .catch((err) => {
                          logger.info(
                            "student_onboard - ownerId:" +
                              ownerId +
                              " mobileNo:" +
                              studentReq.mobileNo +
                              " Student save failed"
                          );
                          return callback(err);
                        });
            
        }
      }
    }
  );
};

exports.parent_details = (entityId, callback)=>{
    Parent.findOne({ _id : entityId},(err, parent)=>{
        if(err){
            logger.error('parent_details - Parent.findOne - Error - ' + err);
            callback(err);
        }
        if(parent != null){
            logger.info('parent_details - entityId:' + entityId + ' Parent retrieved successfully');
            callback(null, ResponseCode.Success, parent);
        }
        else{
            logger.warn('parent_details - entityId:' + entityId + ' Parent not found');
            callback(null, ResponseCode.ParentNotFound);
        }
    });
}

exports.parentDetailsForTutor = (ownerId, mobileNo,callback) =>{
    Parent.findOne({ownerId:ownerId, mobileNo:mobileNo},(err,parent)=>{
        if(err){
            logger.error('parent-details_for_tutor - Parent.findOne - Error '+err)
        }
        else{
            if(parent==null){
                logger.warn('parent_details_for_tutor ownerId:'+ownerId+' mobileNo:'+mobileNo+' not found')
                callback(null, ResponseCode.ParentNotFound)
            }
            else{
                logger.info('parent_details_for_tutor ownerId:'+ownerId+' mobileNo:'+mobileNo+' rettrieved successfully')
                callback(null, ResponseCode.Success,parent)
            }
        }
    })
}

exports.parent_update = (entityId, parentReq, callback)=>{
    Parent.findOne({_id : entityId},(err, parent)=>{
        if(err){
            logger.error('parent_update - Parent.findOne - Error - ' + err);
            callback(err);
        }
        if(parent != null){
            parent.parentName = parentReq.parentName;
            parent.emailId = parentReq.emailId;
            parent.roleStatus = parentReq.roleStatus ? parentReq.roleStatus : (parent.roleStatus ? parent.roleStatus : 'FRESHER');
            parent.dob = parentReq.dob ? parentReq.dob : (parent.dob ? parent.dob : '');
            parent.gender = parentReq.gender ? parentReq.gender : (parent.gender ? parent.gender : '');        
            parent.country = parentReq.country ? parentReq.country : (parent.country ? parent.country : '');

            parent.save(function (err) {
                if (err) {
                    logger.error('parent_update - parent.save - Error - ' + err);
                    return callback(err);
                }
                
                logger.info('parent_update - entityId:' + entityId + ' Parent updated successfully');
                return callback(null, ResponseCode.Success);
            });
        }
        else{
            logger.warn('parent_update - entityId:' + entityId + ' Parent not found');
            callback(null, ResponseCode.ParentNotFound);
        }
    });
}

exports.parent_onboard = (entityId, parentReq, callback)=>{
    Parent.findOne({_id : entityId},(err, parent)=>{
        if(err){
            logger.error('parent_onboard - Parent.findOne - Error - ' + err);
            callback(err);
        }
        if(parent != null){
            parent.parentName = parentReq.parentName;
            parent.emailId = parentReq.emailId;
            parent.roleStatus = parentReq.roleStatus ? parentReq.roleStatus : (parent.roleStatus ? parent.roleStatus : 'FRESHER');
            parent.dob = parentReq.dob ? parentReq.dob : (parent.dob ? parent.dob : '');
            parent.gender = parentReq.gender ? parentReq.gender : (parent.gender ? parent.gender : '');        
            parent.country = parentReq.country ? parentReq.country : (parent.country ? parent.country : '');

            parent.save(function (err) {
                if (err) {
                    logger.error('parent_onboard - parent.save - Error - ' + err);
                    return callback(err);
                }
                
                logger.info('parent_onboard - entityId:' + entityId + ' Parent updated successfully');
                return callback(null, ResponseCode.Success);
            });
        }
        else{
            logger.warn('parent_onboard - entityId:' + entityId + ' Parent not found');
            callback(null, ResponseCode.ParentNotFound);
        }
    });
}

exports.changePassword = (userId, currPassword, newPassword, callback)=>{
    User.findOne({_id:userId},(err,user)=>{
        if(err){
            logger.error('changePassword - User.findOne - Error - ' + err);
            callback(err);
        }
        console.log(user)
        if(user != null){  
            console.log(bcrypt.compareSync(currPassword, user.password))
            if (bcrypt.compareSync(currPassword, user.password)) { 
                var salt = bcrypt.genSaltSync(10);
                var passwordHash = bcrypt.hashSync(newPassword, salt);
                user.password = passwordHash;
                user.save(function (err) {


                        if (err) {
                            logger.error('changePassword - user.save - Error - ' + err);
                            return callback(err);
                        }
                  logger.info('changePassword - userId:' + userId
                                + ' Password changed successfully');
                        callback(null, ResponseCode.Success);
                }); 
            }
            else{
                logger.warn('changePassword - userId:' + userId
                            + ' Incorrect password');
                return callback(null, ResponseCode.IncorrectPassword);
            }           
        }
        else{
            logger.warn('changePassword - userId:' + userId
                            + ' User doesn\'t exist');
            return callback(null, ResponseCode.UserNotExist);
        }
    })

}


exports.getVideoKycToken = (ownerId, entityId, callback) => {
    Tutor.findOne({$and: [{ownerId:ownerId}, {_id:entityId}]}).exec((err, tutorDoc) => {
        if(err) {
            logger.error('getVideoKycToken - Tutor.findOne - Error - ' + err);
            return callback(err);
        }
        if(!tutorDoc) {
            logger.warn('getVideoKycToken - ownerId:' + ownerId + ' Tutor Not found');
            return callback(null, ResponseCode.TutorNotFound);
        }
        if(tutorDoc) {
            if (!tutorDoc.videoKycAttempts || tutorDoc.videoKycAttempts < 5) {
                if (!tutorDoc.isVideoKycDone && tutorDoc.isVideoKycDone === false) {
                    const newToken = shortid.generate();
                    tutorDoc.videoKycToken = newToken;
                    tutorDoc.videoKycAttempts = tutorDoc.videoKycAttempts + 1;
                    tutorDoc.save().then(response => {
                        return callback(null, ResponseCode.Success, newToken)
                    }).catch(err => {
                        logger.error('getVideoKycToken - Tutor.save - Error - ' + err);
                        return callback(err);
                    })
                }
            } else {
                logger.warn('getVideoKycToken - ownerId:' + ownerId + ' Too many attempts try again tomorrow');
                return callback(null, ResponseCode.VideoKycAttemptsExceeded);
            }
            
        }
    })
}

exports.updateVideoKycSuccessUuid = (ownerId, entityId, uuid, fileName, callback) => {
    Tutor.findOne({$and: [{ownerId: ownerId}, {_id: entityId}]}).exec((err, tutorDb) => {
        if(err) {
            logger.error('updateVideoKycSuccessUuid - Tutor.findOne - Error - ' + err);
            return callback(err);
        }
        if(!tutorDb) {
            logger.warn('updateVideoKycSuccessUuid - ownerId:' + ownerId + ' Tutor Not found');
            return callback(null, ResponseCode.TutorNotFound);
        }
        if(tutorDb) {
            getDownloadUrlForVideoKyc(ownerId, uuid, (err, code, signedUrl) => {
                if(err) {
                    logger.error('updateVideoKycSuccessUuid - getDownloadUrlForVideoKyc. AWS utils - Error - ' + err);
                    return callback(err);
                }
                if(code === ResponseCode.Success) {
                    tutorDb.videoKycUuid = uuid;
                    tutorDb.isVideoKycDone = true;
                    tutorDb.videoKycFileName = fileName;
                    tutorDb.save().then(result => {
                        return callback(null, ResponseCode.Success, signedUrl)
                    }).catch(err => {
                        logger.error('updateVideoKycSuccessUuid - Tutor.save - Error - ' + err);
                        return callback(err);
                    })
                }
            })
        }
    })
}


exports.redoVideoKyc = (ownerId, entityId, callback) => {
    Tutor.findOne({$and: [{ownerId:ownerId}, {_id:entityId}]}).exec((err, user) => {
        if(err) {
            logger.error('redoVideoKyc - Tutor.findOne - Error - ' + err);
            return callback(err);
        }
        if(!user) {
            logger.warn('redoVideoKyc - ownerId:' + ownerId + ' Tutor Not found');
            return callback(null, ResponseCode.TutorNotFound);
        }
        if(user) {
            user. videoKycToken = '';
            user.videoKycUuid = '';
            user.videoKycFileName= '';
            user.isVideoKycDone= false;
            user.videoKycVerified= false;
            user.save().then(result => {
                return callback(null, ResponseCode.Success)
            }).catch(err => {
                logger.error('redoVideoKyc - Tutor.save - Error - ' + err);
                return callback(err);
            })
        }
    })
}


exports.fetchKycDetails = (tutorId, callback) => {
    Tutor.findOne({$and:[{_id:tutorId}]}).exec((err, userDb) => {
        if(err) {
            logger.error('fetchKycDetails - Tutor.findOne - Error - ' + err);
            return callback(err)
        }
        if(!userDb) {
            return callback(null, ResponseCode.TutorNotFound)
        }
        if(userDb) {
            const kycArrray = userDb.kycDetails;
            const videoKycDetails = {
                location: userDb.location,
                pinCode: userDb.pinCode,
                stateName: userDb.stateName,
                cityName: userDb.cityName,
                pan: userDb.pan,    // from here
                aadhaar : userDb.aadhaar,
                bankIfsc : userDb.bankIfsc,
                bankAccount : userDb.bankAccount,
                aadhaarKycZip : userDb.aadhaarKycZip,
                aadhaarKycShareCode : userDb.aadhaarKycShareCode,
                aadhaarKycStatus : userDb.aadhaarKycStatus,
                aadhaarMobile : userDb.aadhaarMobile,
                videoKycToken: userDb.videoKycToken,
                videoKycUuid: userDb.videoKycUuid,
                gstin: userDb.gstin,
                videoKycFileName: userDb.videoKycFileName,
                isVideoKycDone: userDb.isVideoKycDone,
                videoKycVerified: userDb.videoKycVerified,
                videoKycAttempts: userDb.videoKycAttempts,
            }
            return callback(null, ResponseCode.Success, kycArrray, videoKycDetails)
        }
    })
}

const orgTutorPoints = {
    // personal details
    tutorName: 5,
    emailId: 5,
    qualifications: 5,
    pinCode: 5,
    cityName: 2.5,
    stateName: 2.5,
    schoolName: 5,
    dob: 2.5,
    image: 5,
    gender: 2.5,

    // course details
    courseDetails: 10
}

const tutorPoints = Object.assign({}, orgTutorPoints, {
    // others
    pan: 5,
    aadhaar: 5,
    bankAccount: 2.5,
    bankIfsc: 2.5,

    // docs
    kycDetails: 15, //aadhaar = 5, pan = 5, bank = 5

    // kyc
    allKycStatus: 10,
    videoKycVerified: 5,
    isVideoKycDone: 5
})

const orgPoints = {
    // basic details
    organizationName: 5,
    emailId: 5,
    pinCode: 5,
    city: 2.5,
    stateName: 2.5,
    dob: 2.5,
    image: 5,

    // course details
    courseDetails: 10,

    // others
    ownerPAN: 5,
    businessPAN: 5,
    aadhaar: 5,
    bankAccount: 2.5,
    bankIfsc: 2.5,

    // docs
    kycDetails: 20, //aadhaar = 5, pan = 5 + 5, bank = 5

    // kyc
    allKycStatus: 10,
    videoKycVerified: 5,
    isVideoKycDone: 5
}

exports.getProfilePercentage = (entityId, role, callback) => {
    let profilePoints, Model, notFoundResponse
    if(role === 'ROLE_TUTOR') {
        profilePoints = tutorPoints
        Model = Tutor
        notFoundResponse = ResponseCode.TutorNotFound
    }
    if(role === 'ROLE_ORGANIZATION_TUTOR') {
        profilePoints = orgTutorPoints
        Model = Tutor
        notFoundResponse = ResponseCode.TutorNotFound
    }
    if(role === 'ROLE_ORGANIZATION') {
        profilePoints = orgPoints
        Model = Organization
        notFoundResponse = ResponseCode.OrganizationNotFound
    }

    Model.findById(entityId).exec((err, doc) => {        
        if(doc && profilePoints) {
            let totalPoints = 0
            let points = 0
            const missingDatas = []

            for (let [key, value] of Object.entries(profilePoints)) {
                totalPoints += value

                if(key === 'kycDetails') {
                    const length = doc.kycDetails.length
                    const noOfDocs = value/5
                    if(length > 0) points += length * (value/noOfDocs)
                    if(length < noOfDocs) missingDatas.push({pendingKycDoc: noOfDocs - length})
                    continue;
                }

                if(key === 'courseDetails') {
                    if(doc.courseDetails.length > 0) {
                        points += value
                    } else missingDatas.push(key)
                    continue;
                }

                if(key === 'allKycStatus') {
                    if(doc.allKycStatus === 'verified') {
                        points += value
                    } else if (doc.allKycStatus === 'checked') {
                        points += value/2
                    } else missingDatas.push({kycStatus: doc.allKycStatus})
                    continue;
                }

                if(doc[key]) {
                    points += value
                } else missingDatas.push(key)
            }

            const percentageCompleted = Math.round(points/totalPoints * 1000) / 1000

            return callback(null, ResponseCode.Success, percentageCompleted, points, totalPoints, missingDatas)
        }

        return callback(null, notFoundResponse)
    })
}