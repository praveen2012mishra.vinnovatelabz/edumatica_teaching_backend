const Assessment = require('../models/assessment.model');
const AssessmentQuestion = require('../models/assessmentQuestion.model');
const Student = require('../models/student.model');
const AttemptedAssessment = require('../models/attemptedassessment.model')
const logger = require('../utils/logger.utils');
const { ResponseCode } = require('../utils/response.utils');
const Section = require('../models/section.model');



exports.assessment_createAssessment = (ownerId, userId, assessmentReq, callback)=> {
    Assessment.findOne({$and: [{ownerId : ownerId , userId: userId}, {assessmentname : assessmentReq.assessmentname}, {status: 1}]})
        .exec( function (err, assessment) {
        if (err){
            logger.error('assessment_createAssessment - Assessment.findOne - Error - ' + err);
            return callback(err);
        }
        if(assessment == null)  {
            let assessmentDetailIds = [];
            let assessmentDetails = [];
            let sections = assessmentReq.sections
            sections.forEach((section)=>{
                let newSection = new Section({
                    title:section.title,
                    questions: section.questions
                })
                assessmentDetailIds.push(newSection._id)
                assessmentDetails.push(newSection)
            })
            

            Section.insertMany(assessmentDetails,(err)=>{
                if(err){
                    logger.error('assessment_createAssessment - Section.insertMany - Error - ' + err);
                    return callback(err);
                }
        
                let assessment = new Assessment({
                    ownerId: ownerId,
                    userId:userId,
                    boardname:  assessmentReq.boardname,
                    classname:  assessmentReq.classname,
                    subjectname:  assessmentReq.subjectname,
                    assessmentname:  assessmentReq.assessmentname,
                    instructions: assessmentReq.instructions,
                    published: false,
                    duration:  assessmentReq.duration,
                    totalMarks: assessmentReq.totalMarks,
                    sections: assessmentDetailIds,
                    status: 1,
                    updatedon: new Date(),
                    updatedby: userId
                });
                assessment.save()
                .then((assessmentData)=>{
                    logger.info('assessment_createAssessment - ownerId:' + ownerId
                    + ', assessmentname:' + assessmentReq.assessmentname
                    + ', Assessment saved successfully');
                    return callback(null, ResponseCode.Success,assessmentData);
                })
                .catch((err)=>{
                    logger.error('assessment_createAssessment - assessment.save - Error - ' + err);
                    callback(err);
                })
            });
        }
        else{
            logger.warn('assessment_createAssessment - ownerId:' + ownerId
            + ', assessmentname:' + assessmentReq.assessmentname
            + ', Assessment already exists');
            return callback(null, ResponseCode.AssessmentAlreadyExists);
        }
    });
}

exports.assessment_deleteAssessment = (ownerId , userId, assessmentId, callback) =>{
    Assessment.findOne({_id:assessmentId,ownerId:ownerId,userId:userId,status:1}).exec()
    .then((assessment)=>{
        if(assessment===null){
            logger.warn('assessment_deleteAssessment - ownerId:' + ownerId
            + ', assessmentId:' + assessmentReq.assessmentId
            + ', Assessment not found');
            return callback(null, ResponseCode.AssessmentNotFound);
        }
        else{
            Assessment.findByIdAndUpdate(assessmentId, {$set:{status:0}}).exec()
            .then(()=>{
                logger.info('assessment_deleteAssessment - ownerId:' + ownerId
                    + ', assessmentId:' + assessmentId
                    + ', Assessment deleted successfully');
                    return callback(null, ResponseCode.Success);
            })
            .catch((err)=>{
                logger.error('assessment_deleteAssessment - assessment.findByIdAndDelete - Error - ' + err);
                return callback(err)
            })
        }
    })
    .catch((err)=>{
        logger.error('assessment_deleteAssessment - assessment.findById - Error - ' + err);
        return callback(err)
    })
}

exports.assessment_deleteAssessments = (ownerId , userId, assessments, callback) =>{
    Assessment.updateMany({ _id: {$in: assessments}}, {$set:{status:0}}, (err, data) => {
        if(err) {
            logger.error('assessment_deleteAssessments - assessment.updateMany - Error - ' + err);
            return callback(err)
        }
        logger.info('assessment_deleteAssessments - ownerId:' + ownerId + ', Assessment deleted successfully');
        return callback(null, ResponseCode.Success);
    })
}

exports.assessment_patchAssessment = (ownerId, userId, assessmentId,assessmentReq,updateBoolean, callback)=> {
    Assessment.findOne({_id:assessmentId,ownerId:ownerId,userId:userId,status:1}).populate({path:'sections',select:'questions'})
        .exec( function (err, assessment) {
        if (err){
            logger.error('assessment_patchAssessment - Assessment.findById - Error - ' + err);
            return callback(err);
        }
        if(assessment !== null)  {
                if(updateBoolean!=="false"){
                    // assessment.sections.forEach((section)=>{
                    //     if(!assessmentReq.section.includes(section._id)){
                    //         Section.findById(section._id,'questions').exec()
                    //         .then((sectionDoc)=>{
                    //             AssessmentQuestion.deleteMany({_id:{$in:sectionDoc.questions}}).exec()
                    //             .then(()=>{
                    //                 Section.deleteOne({_id:section._id}).exec()
                    //                 .then(()=>{
                    //                     Assessment.updateOne({_id:assessment._id},{$pull:{sections:section._id}}).exec()
                    //                     .then(()=>{
                    //                         callback(null,ResponseCode.Success)
                    //                     })
                    //                     .catch((err)=>{
                    //                         logger.error('Error in removing section from assessment. Error:'+err)
                    //                     })
                    //                 })
                    //                 .catch((err)=>{
                    //                     logger.error('Error in deleting Section. Error:'+err)
                    //                     callback(err)
                    //                 })
                    //             })
                    //             .catch((err)=>{
                    //                 logger.error('Error in deleting questions.error:'+err)
                    //                 callback(err)
                    //             })
                    //         })
                    //     }
                    // })
                    Promise.all(assessment.sections.map((section=>{
                        return AssessmentQuestion.deleteMany({_id:{$in:section.questions}})
                    })))
                    .then(()=>{
                        Promise.all(assessment.sections.map((section)=>{
                            return Section.deleteOne({_id:section._id})
                        }))
                        .then(()=>{
                            Assessment.findByIdAndUpdate(assessment._id,{sections:[]}).then(()=>{
                            let sectionIds = []
                            let sectionDetails =[]

                            Promise.all(assessmentReq.sections.map((section,sectionIndex)=>{
                                if(section.questions.length>0 || sectionIndex===0){
                                    let questionIds = [] 
                                let questionDetails = []

                                section.questions.forEach((question)=>{
                                    logger.debug(!question.question)
                                    logger.debug(question)
                                    if(!question.question){
                                        question={questionSource:"MasterQuestion" , question:question._id, marks:question.marks,percentageError:question.percentageError,negativeMarking:question.negativeMarking,updatedby:userId,updatedon:new Date()}
                                    }
                                    logger.debug(question)
                                    const newQuestion = new AssessmentQuestion(question)
                                    logger.debug(newQuestion)
                                    questionIds.push(newQuestion._id)
                                    questionDetails.push(newQuestion)
                                })
                                logger.debug(questionIds)
                                newSection = new Section({
                                    title:section.title,
                                    totalMarks: section.totalMarks,
                                    duration: section.duration,
                                    instructions: section.instructions,
                                    questions:questionIds,
                                })

                                sectionIds.push(newSection._id)
                                sectionDetails.push(newSection)
                                return AssessmentQuestion.insertMany(questionDetails)
                                }
                                
                                
                            }))
                            .then(()=>{
                                Section.insertMany(sectionDetails,{overwrite:true})
                                .then(()=>{
                                    logger.debug(sectionIds)
                                    Assessment.findByIdAndUpdate(assessment._id,{sections:sectionIds})
                                    .then(()=>{
                                       return callback(null,ResponseCode.Success)
                                    })
                                    .catch((err)=>{
                                       return callback(err)
                                    })
                                })
                                .catch((err)=>{
                                   return callback(err)
                                })
                            })
                            .catch((err)=>{
                                return callback(err)
                            })
                            })
                            .catch((err)=>{
                               return  callback(err)
                            })
                        })
                        .catch((err)=>{
                           return  callback(err)
                        })
                    })
                    .catch((err)=>{
                        return callback(err)
                    })
                    
                    // assessmentReq.sections.forEach((section)=>{
                        
                    //     let questionIds= []
                    //     let questionDetails=[]
                        
                    //     section.questions.forEach((question)=>{
                    //         delete question._id
                    //         question={...question,source:"modified"}
                    //         const newQuestion = new AssessmentQuestion(question)
                    //         questionIds.push(newQuestion._id)
                    //         questionDetails.push(newQuestion)
                    //     })



                        
                    //     Section.findById(section._id,"questions").exec()
                    //     .then((sectionDoc)=>{
                            
                    //         AssessmentQuestion.deleteMany({_id:{$in:sectionDoc.questions}})
                    //         .then(()=>{
                    //             Section.findByIdAndUpdate(section._id,{questions:[]},{overwrite:true})
                    //             .then(()=>{
                    //                 AssessmentQuestion.insertMany(questionDetails,{overwrite:true},(err)=>{
                    //                     if(err){
                    //                         logger.error('Error inserting questions'+err)
                    //                         return callback(null,ResponseCode.QuestionNotFound)
                    //                     }
                    //                     const sectionBody = {
                    //                         title:section.title,
                    //                         totalMarks: section.totalMarks,
                    //                         duration: section.duration,
                    //                         instructions: section.instructions,
                    //                         questions:questionIds,
                    //                     }
                                        
                    //                     Section.findByIdAndUpdate(section._id,sectionBody).exec().then((sectionDoc)=>{
                    //                         logger.info('Section with sectionId:'+section._id+' updated successfully')
                    //                         return callback(null,ResponseCode.Success)
                    //                     })
                    //                     .catch((err)=>{
                                            
                    //                         logger.warn('Section findById failed for sectionId:'+section._id+' Error:'+err)
                    //                         return callback(null,ResponseCode.SectionNotFound)
                    //                     })
                    //                 })
                    //             })
                    //             .catch((err)=>{
                    //                 logger.error('Error clearing section Id array')
                    //                 return callback(null,ResponseCode.QuestionNotFound)
                    //             })
                    //         })
                    //         .catch((err)=>{
                    //             logger.error('Error deleting previous questions error:'+err)
                    //             return callback(null,ResponseCode.QuestionNotFound)
                    //         })
                    //     })
                    //     .catch((error)=>{
                    //         logger.error('Section not found sectionId:'+section._id)
                    //         AssessmentQuestion.insertMany(questionDetails,{overwrite:true},(err)=>{
                    //         if(err){
                    //                         logger.error('Error inserting questions'+err)
                    //                         return callback(null,ResponseCode.QuestionNotFound)
                    //                     }
                    //                     const sectionBody = new Section({
                    //                         title:section.title,
                    //                         totalMarks: section.totalMarks,
                    //                         duration: section.duration,
                    //                         instructions: section.instructions,
                    //                         questions:questionIds,
                    //                     })
                                        
                    //                     sectionBody.save().then((sectionDoc)=>{
                    //                         logger.info('Section with sectionId:'+section._id+' created successfully')
                    //                         logger.debug(sectionDoc)
                    //                         Assessment.findOneAndUpdate({_id:assessmentId},{$push:{sections:sectionDoc._id}})
                    //                         .then(()=>{
                    //                             logger.debug("Section created and pushed")
                    //                             return callback(null,ResponseCode.Success)
                    //                         })
                    //                         .catch((err)=>{
                    //                             logger.error("Error in pushing newly created section. Error :"+err)
                    //                         })
                                            
                    //                     })
                    //                     .catch((err)=>{
                    //                         logger.error("Error in saving new Section. error:"+err)
                    //                     })
                    //         })
                    //     })
                    // })
                }
                else{
                        let patchAssessmentObject = {
                        ownerId: ownerId,
                        boardname:  assessmentReq.boardname,
                        classname:  assessmentReq.classname,
                        subjectname:  assessmentReq.subjectname,
                        assessmentname:  assessmentReq.assessmentname,
                        instructions: assessmentReq.instructions,
                        duration:  assessmentReq.duration,
                        totalMarks: assessmentReq.totalMarks,
                        published: assessmentReq.published == undefined ? assessment.published : assessmentReq.published ,
                        sections: assessmentReq.sections,
                        status: 1,
                        updatedon: new Date(),
                        updatedby: userId
                    }
                    
                    Assessment.findByIdAndUpdate(assessmentId,patchAssessmentObject).exec()
                     .then(()=>{
                    logger.info('assessment_patchAssessment - ownerId:' + ownerId
                    + ', assessmentId:' + assessmentId
                    + ', Assessment updated successfully');
                    return callback(null, ResponseCode.Success);
                     })
                    .catch((err)=>{
                    logger.error('assessment_patchAssessment - assessment.update - Error - ' + err);
                    return callback(err)
                })
                }

                
        }
        else{
            logger.warn('assessment_patchAssessment - ownerId:' + ownerId
            + ', assessmentId:' + assessmentReq.assessmentId
            + ', Assessment not found');
            return callback(null, ResponseCode.AssessmentNotFound);
        }
    });
}


exports.assessment_getAssessments = (ownerId, userId,privateBool, callback)=> {
    privateBool?Assessment.find({ownerId : ownerId , userId:userId, status:1}).select({"__v":0}).populate({path:'sections',select:'questions',populate:{path:'questions',select:'marks'}})
    .exec( function (err, assessments) {
        if (err) {
            logger.error('assessment_getAssessments - Assessment.find - Error - ' + err);
            return callback(err);
        }
        logger.info('assessment_getAssessments - ownerId:' + ownerId
                    + ', Assessment(s) retrieved successfully');
        callback(null, ResponseCode.Success, assessments);
    }):
    Assessment.find({$and:[{ownerId : ownerId},{status:1}]}).select({"__v":0}).populate({path:'sections',select:'questions'})
    .exec( function (err, assessments) {
        if (err) {
            logger.error('assessment_getAssessments - Assessment.find - Error - ' + err);
            return callback(err);
        }
        logger.info('assessment_getAssessments - ownerId:' + ownerId
                    + ', Assessment(s) retrieved successfully');
        callback(null, ResponseCode.Success, assessments);
    });
}

exports.assessment_details = (ownerId,userId,  assessmentId, callback)=> {
    Assessment.findOne({ownerId:ownerId,userId:userId, _id : assessmentId, status:1})
        .populate({path:'sections',options:{lean:true},populate:{path:'questions', options: { lean: true }, populate:{path:'question', options: { lean: true }}}})
        .lean().exec( function (err, assessment) {
        if (err) {
            logger.error('assessment_details - Assessment.findOne - Error - ' + err);
            return callback(err);
        }
        if(assessment===null){
            logger.info('assessment_details - ownerId:' + ownerId
                    + ', assessmentId:' + assessmentId
                    +', userId:'+userId
                    + ', Assessment not found');
            callback(null,ResponseCode.AssessmentNotFound)
        }
        else{
            logger.debug(assessment)
            var tempQues 
            const assessmentReorder = {
                ...assessment,
                sections:assessment.sections.map((section)=>{
                
                return {
                    ...section,
                    questions: section.questions.map((ques)=>{
                        logger.debug(ques)
                    tempQues=ques.question
                    const extras = {
                        marks: ques.marks,
                        negativeMarking: ques.negativeMarking
                    }
                    delete ques.question
                    delete ques._id
                    return {
                        ...ques,
                        ...tempQues,
                        ...extras
                    }
                    })
                }
            })
        }
            logger.info('assessment_details - ownerId:' + ownerId
                    + ', assessmentId:' + assessmentId
                    + ', Assessment retrieved successfully');
            callback(null, ResponseCode.Success, assessmentReorder);
        }
        
    });
}




exports.assessment_assignassessment_dbc = (ownerId, userId , assignAssessmentData , studentArray, callback)=>{
    Assessment.findOne({ownerId:ownerId, userId:userId , _id : assignAssessmentData.assessment, status:1}).exec()
    .then((assessment)=>{
        if(assessment===null){
            logger.warn('Assessment not found . AssessmentId : '+assignAssessmentData.assessment)
            callback(null, ResponseCode.AssessmentNotFound)
        }

        else{
            const attemptAssessmentDetails = []
            studentArray.forEach((student)=>{
                const newAttemptAssessment = new AttemptedAssessment({
                    ownerId:ownerId,
                    startDate:assignAssessmentData.startDate,
                    studentId:student.student,
                    batchId:student.batch,
                    endDate:assignAssessmentData.endDate,
                    assessment:assignAssessmentData.assessment,
                    solutionTime:assignAssessmentData.solutionTime,
                    isSubmitted:false,
                    answers:[],
                    attemptedQuestions:[],
                    currentQuestion:0,
                    isStarted:false,
                    updatedby:userId,
                    updatedon:new Date()
                })
                
                attemptAssessmentDetails.push(newAttemptAssessment)
            })
            logger.info(attemptAssessmentDetails)
            AttemptedAssessment.insertMany(attemptAssessmentDetails)
            .then(()=>{
                callback(null,ResponseCode.Success)
            })
            .catch((err)=>{
                logger.info('Error inserting attemptassessment Error :'+err)
                callback(null,ResponseCode.AttemptedAssessmentSaveFailed)
            })
        }
    })
    .catch((err)=>{
        callback(err)
    })
}

exports.assessment_fetchAssignAssessment = (ownerId, query, callback) => {
    const studentId = query.studentId;
    const assessmentId = query.assessmentId;
    let mongoQuery;

    if(assessmentId && studentId) {
        mongoQuery = {$and:[{ownerId: ownerId}, {assessment: assessmentId}, {students:{$elemMatch:{studentId: studentId}}}]}
    } else if(assessmentId) {
        mongoQuery = {$and:[{ownerId: ownerId}, {assessment: assessmentId}]}
    } else {
        mongoQuery = {$and:[{ownerId: ownerId}]}
    }

    AttemptedAssessment.find(mongoQuery).populate({path:"assessment", model:"Assessment"}).exec((err, data) => {
        if(err) {
            logger.error('assessment_fetchAssignAssessment - AttemptedAssessment.find - Error - ' + err);
            return callback(err); 
        } else {
            data = data.filter(list => list.assessment);
            logger.error('assessment_fetchAssignAssessment - AttemptedAssessment.find - Success');
            return callback(null, ResponseCode.Success, data);
        }
    })
}

exports.assessment_deleteAssignAssessment = (ownerId, query, callback) => {
    const studentId = query.studentId;
    const assessmentId = query.assessmentId;
    let mongoQuery;

    if(assessmentId && studentId) {
        mongoQuery = {$and:[{ownerId: ownerId}, {assessment: assessmentId}, {students:{$elemMatch:{studentId: studentId}}}]}
    } else {
        mongoQuery = {$and:[{ownerId: ownerId}, {assessment: assessmentId}]}
    }

    AttemptedAssessment.deleteMany(mongoQuery, (err, data)  => {
        if(err) {
            logger.error('assessment_deleteAssignAssessment - AttemptedAssessment.find - Error - ' + err);
            return callback(err); 
        } else {
            logger.error('assessment_deleteAssignAssessment - AttemptedAssessment.find - Success');
            return callback(null, ResponseCode.Success);
        }
    })
}


exports.assessment_copyassessment_dbc = (ownerId, assessmentData, callback)=>{
    Assessment.findOne({$and:[{_id:assessmentData._id},{status:1}]}).populate({path:'sections',populate:{path:'questions'}}).exec()
    .then((originalAssessment)=>{
        if(originalAssessment===null){
            callback(null,ResponseCode.AssessmentNotFound)
        }
        else{
            
            const sectionIds =[]
            const sectionDetails =[]
            originalAssessment.sections.forEach((section)=>{
                const questionIds =[]
                const questionDetails = []
                section.questions.forEach((question)=>{
                    
                    const newQuestion = new AssessmentQuestion({
                      question: question.question,
                      questionSource: question.questionSource,
                      percentageError: question.percentageError,
                      negativeMarking: question.negativeMarking,
                      marks: question.marks,
                      updatedon: new Date(),
                      updatedby: question.updatedby,
                    });
                    questionDetails.push(newQuestion)
                    questionIds.push(newQuestion._id)
                })
                AssessmentQuestion.insertMany(questionDetails)
                .catch((err)=>{
                    callback(err)
                })
                const newSection = new Section({
                  title: section.title,
                  questions:questionIds,
                  totalMarks: section.totalMarks,
                  duration: section.duration,
                  instructions: section.instructions,
                });
                sectionIds.push(newSection._id);
                sectionDetails.push(newSection);
            })
            
            Section.insertMany(sectionDetails)
            .catch((err)=>{
                callback(err)
            })
            
            newAssessment = new Assessment({
              ownerId: ownerId, // tutor profile service
              userId: originalAssessment.userId, // for specific tutor service
              boardname: originalAssessment.boardname, // master service
              classname: originalAssessment.classname, // master service
              subjectname: originalAssessment.subjectname, // master service
              assessmentname: originalAssessment.assessmentname + " -Copy", // string with length 5 - 50
              totalMarks: originalAssessment.totalMarks, // string with length 5 - 50
              duration: originalAssessment.duration, // in minutes
              instructions: originalAssessment.instructions,
              sections: sectionIds,
              status: originalAssessment.status,
              updatedon: new Date(),
              updatedby: originalAssessment.updatedby,
            });
            newAssessment.save()
            .then((assessmentDoc)=>{
                callback(null,ResponseCode.Success,assessmentDoc)
            })
            .catch((err)=>{
                callback(err)
            })
        }
    })
    .catch((err)=>{
        callback(err)
    })
}

