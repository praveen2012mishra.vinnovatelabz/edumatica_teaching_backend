const logger = require('../utils/logger.utils');
const { ResponseCode } = require('../utils/response.utils');
const AttemptedAssessment = require('../models/attemptedassessment.model')
const Assessment = require('../models/assessment.model')
const { checkAnswer } = require('../utils/helpers.utils')

exports.attemptassessment_patchassessment = (ownerId, userId,entityId, attemptAssessmentId , attemptedassessmentData , callback)=> {
    AttemptedAssessment.findOne({ownerId:ownerId,studentId:entityId,_id:attemptAssessmentId})
    .then((attemptedAssessmentDoc)=>{
        if(attemptedAssessmentDoc===null){
            logger.warn('attemptassessment_patchassessment - Error. attemptassessment not found attemptedAssessmentId:'+attemptAssessmentId)
            return callback(null,ResponseCode.AttemptedAssessmentNotFound)
        }
        else if(attemptedAssessmentDoc.isSubmitted){
            logger.warn('attemptassessment_patchassessment - Error. attemptassessment not found attemptedAssessmentId:'+attemptAssessmentId)
            return callback(null,ResponseCode.AttemptedAssessmentAlreadySubmitted)
        }
        else{
            logger.info('attemptassessment_patchassessment - Patching attemptedassessment')
            const updateData = {
                updatedby:userId,
                updatedon:new Date(),
                ...attemptedassessmentData
            }
            AttemptedAssessment.findByIdAndUpdate(attemptAssessmentId,updateData)
            .then(()=>{
                logger.info('attemptassessment_patchassessment - attemptassessment patched. attemptassessmentId:'+attemptAssessmentId)
                return callback(null,ResponseCode.Success)
            })
            .catch((err)=>{
                logger.error('patch attemptedassessment failed. AttemptedassessmentId:' +attemptAssessmentId + 'Error:'+err)
                return callback(null,ResponseCode.AttemptedAssessmentSaveFailed)
            })
        }
        
    })
    .catch((err)=>{
        logger.error('attemptassessment_saveassessment - Error : '+err)
        return callback(null.ResponseCode.AttemptedAssessmentSaveFailed)
    })
}

exports.attemptassessment_getassessment = (ownerId , entityId  , attemptAssessmentId , callback) =>{
    AttemptedAssessment.findOne({ownerId:ownerId,studentId:entityId,_id:attemptAssessmentId})
    .populate({path:'assessment',options:{lean:true},populate:{path:"sections",options:{lean:true},populate:{path:"questions",options:{lean:true},populate:{path:'question',options:{lean:true},select:"-answerDescription -answer"}}}})
    .lean().exec()
    .then((attemptAssessmentDoc)=>{
        if(attemptAssessmentDoc===null){
            logger.info('attemptassessment_getassessment_dbc - Assessment not found. assessmentId:'+attemptAssessmentId +'. OwnerId:'+ownerId+'. StudentId:'+entityId)
            return callback(null,ResponseCode.AttemptedAssessmentNotFound)
        }
        else{
            if(attemptAssessmentDoc.isSubmitted){
                logger.info('attemptassessment_getassessment_dbc - Assessment already submitted. assessmentId:'+attemptAssessmentId +'. OwnerId:'+ownerId+'. StudentId:'+entityId)
            }
            else if(attemptAssessmentDoc.isStarted){
                logger.info('attemptassessment_getassessment_dbc - Assessment already started. assessmentId:'+attemptAssessmentId +'. OwnerId:'+ownerId+'. StudentId:'+entityId)
            }
            else{
                logger.info('attemptassessment_getassessment_dbc - Assessment found successfully. assessmentId:'+attemptAssessmentId +'. OwnerId:'+ownerId+'. StudentId:'+entityId)
            }
            var tempQues 
            const reorderAttemptAssessment = {
                ...attemptAssessmentDoc,
                assessment:{
                    ...attemptAssessmentDoc.assessment,
                    sections: attemptAssessmentDoc.assessment.sections.map((section,sectionIndex)=>{
                        return {
                            ...section,
                            questions: section.questions.map((ques,quesIndex)=>{
                                tempQues=ques.question
                                delete ques.question
                                delete ques._id
                                return {
                                ...ques,
                                ...tempQues
                                 }
                            })
                        }
                    })
                }
            }
            return callback(null,ResponseCode.Success,reorderAttemptAssessment)
        }
    })
    .catch((err)=>{
        logger.info('attemptassessment_getassessmentdbc error - '+err)
        return callback(err)
    })
}


exports.attemptassessment_getanswersdata = (ownerId,entityId,attemptAssessmentId,callback) =>{
    AttemptedAssessment.findOne({_id:attemptAssessmentId,studentId:entityId,ownerId:ownerId}).exec()
    .then((attemptedAssessmentDoc)=>{
        if(attemptedAssessmentDoc===null){
            logger.info('Attempt Assessment not found. attemptassessmentId:'+attemptAssessmentId)
            callback(null,ResponseCode.AttemptedAssessmentNotFound)
        }
        else{
            if(!attemptedAssessmentDoc.isSubmitted){
                logger.info('Attempt Assessment not submitted attemptassessmentId:'+attemptAssessmentId)
                callback(null,ResponseCode.AttemptedAssessmentNotSubmitted)
            }
            else if(new Date()<new Date(attemptedAssessmentDoc.solutionTime)){
                callback(null,ResponseCode.SolutionsNotAccesible)
            }
            else{
                AttemptedAssessment.findOne({_id:attemptAssessmentId,studentId:entityId,ownerId:ownerId}).populate({path:'assessment',options:{lean:true}, select:'sections answers',populate:{path:'sections',options:{lean:true}, populate:{path:'questions',options:{lean:true}, populate:{path:'question',select:'answer answerDescription type' , option:{lean:true}}}}}).lean().exec()
                .then((attemptedAssessmentDoc)=>{
                    var tempQues
                    logger.debug(JSON.stringify(attemptedAssessmentDoc))
                    const reorderAttemptedAssessmentDoc =  {
                        ...attemptedAssessmentDoc,
                        assessment:{
                            ...attemptedAssessmentDoc.assessment,
                            sections: attemptedAssessmentDoc.assessment.sections.map((section)=>{
                                return {
                                    ...section,
                                    questions: section.questions.map(ques=>{
                                        tempQues=ques.question
                                        delete ques.question
                                        delete ques._id
                                        return {
                                        ...ques,
                                        ...tempQues
                                         }
                                    })
                                }
                            })
                        }
                    }
                    logger.debug(JSON.stringify(reorderAttemptedAssessmentDoc))
                    const answersData = reorderAttemptedAssessmentDoc.assessment.sections.map((section,index)=>{
                        return section.questions.map((question,ind)=>{
                            return {index:ind+1,correct:question.answer.join(","),answerDescription:question.answerDescription,marks:checkAnswer(question.answer,attemptedAssessmentDoc.answers[index][ind],question.type,question.percentageError,question.negativeMarking,question.marks).marks,result:checkAnswer(question.answer,attemptedAssessmentDoc.answers[index][ind],question.type,question.percentageError,question.negativeMarking,question.marks).result,your:(attemptedAssessmentDoc.answers[index][ind].length===0 || attemptedAssessmentDoc.answers[index][ind][0]==="" ?"Unanswered":attemptedAssessmentDoc.answers[index][ind].join(","))}
                        })
                    })
                    logger.debug(answersData)
                    callback(null,ResponseCode.Success,answersData)
                })
                .catch((err)=>{
                    logger.info('Unable to find answers for assessmentId:' + attemptAssessmentId)
                    callback(err)
                })
            }
        }
        
    })
    .catch((err)=>{
        callback(err)
    })
}

exports.attemptassessment_getassessments = (ownerId,entityId,callback) =>{
    AttemptedAssessment.find({ownerId:ownerId,studentId:entityId}).populate("assessment").exec()
    .then((attemptAssessmentDocs)=>{
        if(attemptAssessmentDocs===null){
            logger.info("No assessment found entityId:"+entityId)
            callback(null,ResponseCode.AssessmentNotFound)
        }
        else{
            callback(null,ResponseCode.Success,attemptAssessmentDocs)
        }
    })
    .catch((err)=>{
        callback(err)
    })
}

exports.attemptassessment_getAllChildAssessments = (ownerId,entityId,studentId,callback) =>{
    AttemptedAssessment.find({ownerId:ownerId,studentId:studentId}).populate("assessment").exec()
    .then((attemptAssessmentDocs)=>{
        if(attemptAssessmentDocs===null){
            logger.info("No assessment found entityId:"+studentId)
            callback(null,ResponseCode.AssessmentNotFound)
        }
        else{
            callback(null,ResponseCode.Success,attemptAssessmentDocs)
        }
    })
    .catch((err)=>{
        callback(err)
    })
}

exports.attemptassessment_getassessmentstudentdata_dbc = (ownerId, assessmentId, callback) =>{
    AttemptedAssessment.aggregate([
      {
        $match: {
          ownerId: ownerId
        },
      },
      {
        $group: {
            _id: "$assessment",
            studentArr: {
              $push: "$studentId"
            }
          },
      }
    ])
    .then((aggDocs)=>{
        if(aggDocs===null){
            logger.debug("No assessment found for this query")
            callback(null,ResponseCode.AttemptedAssessmentNotFound)
        }
        else{
            callback(null,ResponseCode.Success,aggDocs)
        }
    })
    .catch((err)=>{
        logger.debug('Error in operation'+err)
        callback(err)
    })
}

exports.attemptassessment_getassignedassessments_dbc = (ownerId,callback) =>{
    AttemptedAssessment.distinct('assessment').exec()
    .then((assessmentArr)=>{
        if(assessmentArr===null){
            callback(null,ResponseCode.AssessmentNotFound)
        }
        else{
            callback(null,ResponseCode.Success,assessmentArr)
        }
    })
    .catch((err)=>{
        callback(err)
    })
}

exports.attemptassessment_getallassignedassessments_dbc = (userId,callback) =>{
    Assessment.find({userId:userId}).exec()
    .then((assessmentArr)=>{
        if(assessmentArr===null){
            callback(null,ResponseCode.AssessmentNotFound)
        }
        else{
            callback(null,ResponseCode.Success,assessmentArr)
        }
    })
    .catch((err)=>{
        callback(err)
    })
}

exports.attemptassessment_getAttemptAssessmentbyAssessmentId = (ownerId, assessmentId, callback) =>{
    AttemptedAssessment.find({ownerId:ownerId,assessment:assessmentId}).populate("studentId", "studentName mobileNo emailId").exec()
    .then((attemptAssessmentArr)=>{
        if(attemptAssessmentArr===null){
            callback(null,ResponseCode.AssessmentNotFound)
        }
        else{
            callback(null,ResponseCode.Success,attemptAssessmentArr)
        }
    })
    .catch((err)=>{
        callback(err)
    })
}