const MasterChapter = require('../models/masterChapter.model');
const MasterSchool = require('../models/masterSchool.model');
const MasterPackage = require('../models/masterPackage.model');
const MasterCity = require('../models/masterCity.model');
const cache = require('memory-cache');
const logger = require('../utils/logger.utils');
const { ResponseCode } = require('../utils/response.utils');
const duration = 24 * 60 * 60 * 1000  // 24 hours

exports.masterData_chapters = (callback)=> {
    var masterChapters = cache.get('masterChapters');
    if(masterChapters == null){
        MasterChapter.find().exec( function (err, chapters) {
            if (err) {
                logger.error('masterData_chapters - MasterChapter.find - Error - ' + err);
                return callback(err);
            }
            if(!chapters && chapters.length > 0){
                cache.put('masterChapters', chapters,  duration);
            }
            logger.info('masterData_chapters retrieved from db successfully');
            callback(null, ResponseCode.Success, chapters);
        })
    }
    else{
        logger.info('masterData_chapters Data retrieved from Cache');
        callback(null, ResponseCode.Success, masterChapters);
    }
}

exports.masterData_schools = (callback)=> {
    var masterSchools = cache.get('masterSchools');
    if(masterSchools == null){
        MasterSchool.find().exec( function (err, schools) {
            if (err) {
                logger.error('masterData_schools - MasterSchool.find - Error - ' + err);
                return callback(err);
            }
            if(!schools && schools.length > 0){
                cache.put('masterSchools', schools,  duration);
            }
            logger.info('masterData_schools retrieved from db successfully');
            callback(null, ResponseCode.Success, schools);
        })
    }
    else{
        logger.info('masterData_schools Data retrieved from Cache');
        callback(null, ResponseCode.Success, masterSchools);
    }
}

exports.masterData_packages = (callback)=> {
    var masterPackages = cache.get('masterPackages');
    if(masterPackages == null){
        MasterPackage.find().exec( function (err, packages) {
            if (err) {
                logger.error('masterData_packages - MasterPackage.find - Error - ' + err);
                return callback(err);
            }
            if(packages && packages.length > 0){
                cache.put('masterPackages', packages,  duration);
                logger.info('masterData_packages retrieved from db successfully');
                callback(null, ResponseCode.Success, packages);
            }
            else{
                logger.error('masterData_packages not found');
                callback(null, ResponseCode.MasterPackagesNotFound, packages);
            }
        })
    }
    else{
        logger.info('masterData_packages Data retrieved from Cache');
        callback(null, ResponseCode.Success, masterPackages);
    }
}

exports.masterData_cities = (callback)=> {
    var masterCities = cache.get('masterCities');
    if(masterCities == null){
        MasterCity.find().exec( function (err, cities) {
            if (err) {
                logger.error('masterData_cities - MasterCity.find - Error - ' + err);
                return callback(err);
            }
            if(cities && cities.length > 0){
                cache.put('masterCities', cities,  duration);
                logger.info('masterData_cities retrieved from db successfully');
                callback(null, ResponseCode.Success, cities);
            }
            else{
                logger.error('masterData_cities not found');
                callback(null, ResponseCode.MasterCitiesNotFound, cities);
            }
        })
    }
    else{
        logger.info('masterData_cities Data retrieved from Cache');
        callback(null, ResponseCode.Success, masterCities);
    }
}