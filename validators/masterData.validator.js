const { query } = require('express-validator');
const { PIN_PATTERN } = require('../utils/constant.utils');

const getClassList_ValidationRules = () => {
    return [ 
        query('boardname', 'boardname doesn\'t exists').exists(),
       ]
}

const getSubjectList_ValidationRules = () => {
    return [ 
        query('boardname', 'boardname doesn\'t exists').exists(),
        query('classname', 'classname doesn\'t exists').exists(),
       ]
}

const getChapterList_ValidationRules = () => {
    return [ 
        query('boardname', 'boardname doesn\'t exists').exists(),
        query('classname', 'classname doesn\'t exists').exists(),
        query('subjectname', 'subjectname doesn\'t exists').exists(),
       ]
}

const getCitiesByPinCode_ValidationRules = () => {
    return [ 
        query('pinCode', 'pinCode doesn\'t exists').matches(PIN_PATTERN),
       ]
}

const getSchoolsByCity_ValidationRules = () => {
    return [ 
        query('cityName', 'cityName doesn\'t exists').exists(),
       ]
}

module.exports = {
    getClassList_ValidationRules,
    getSubjectList_ValidationRules,
    getChapterList_ValidationRules,
    getSchoolsByCity_ValidationRules,
    getCitiesByPinCode_ValidationRules
}
