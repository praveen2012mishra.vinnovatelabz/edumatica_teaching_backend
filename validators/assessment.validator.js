const { body, query } = require('express-validator')



const assessmentDetails_ValidationRules = () => {
    return [ 
        query('assessmentId', 'assessmentId doesn\'t exists').exists(),
       ]
}

const studentAssessmentDetails_ValidationRules = () => {
    return [ 
        query('assessmentId', 'assessmentId doesn\'t exists').exists(),
        query('tutorId', 'tutorId doesn\'t exists').exists(),
       ] 
}

const assessmentCreate_ValidationRules = () => {
    return [ 
        body('boardname', 'boardname doesn\'t exists').exists(),
        body('classname', 'classname doesn\'t exists').exists(),
        body('subjectname', 'subjectname doesn\'t exists').exists(),
        body('assessmentname', 'assessmentname doesn\'t exists').exists(),
        body('duration', 'duration doesn\'t exists').exists(),
        body('totalMarks', 'totalMarks doesn\'t exists').exists(),
        body('duration', 'duration doesn\'t follows norms').isNumeric({gt:9}),
        body('totalMarks', 'totalMarks doesn\'t follows norms').isInt({gt:9}),
       ]
}

const assessmentPatch_ValidationRules = () => {
    return [ 
        query('update','Update Boolean doesn\'t exists').exists(),
        query('assessmentId','assessment Id doesn\'t exists').exists(),
        body('boardname', 'boardname doesn\'t exists').exists(),
        body('classname', 'classname doesn\'t exists').exists(),
        body('subjectname', 'subjectname doesn\'t exists').exists(),
        body('assessmentname', 'assessmentname doesn\'t exists').exists(),
        body('duration', 'duration doesn\'t exists').exists(),
        body('totalMarks', 'totalMarks doesn\'t exists').exists(),
        body('duration', 'duration doesn\'t follows norms').isNumeric({gt:9}),
        body('totalMarks', 'totalMarks doesn\'t follows norms').isInt({gt:9}),
       ]
}

const assessmentsDetails_ValidationRules = () =>{
    return [
        query('private','Private Boolean reference doesn\'t exists').exists()
    ]
}

const assessmentDelete_ValidationRules = () => {
    return [ 
        query('assessmentId','assessment Id doesn\'t exists').exists(),
       ]
}

const assessmentsDelete_ValidationRules = () => {
    return [ 
        body('assessmentList', 'studentList not present').exists(),
       ]
}

const assessmentAssign_ValidationRules = () =>{
    return [
        body('studentArray','student array is empty').not().isEmpty(),
        body('assessmentData','assessment Data doesn\'t exists').exists()
    ]
}

const assessmentCopy_ValidationRules = () =>{
    return [
        body('assessmentData','assessment data object doesn\'t exists').exists()
    ]
}

const deleteAssessmentAssign_ValidationRules = () => {
    return [
        query('assessmentId', 'assessmentId is not Present').exists()
    ]
}



module.exports = {
    assessmentDetails_ValidationRules, deleteAssessmentAssign_ValidationRules,
    studentAssessmentDetails_ValidationRules, assessmentCopy_ValidationRules,
    assessmentCreate_ValidationRules, assessmentPatch_ValidationRules, assessmentDelete_ValidationRules,
    assessmentAssign_ValidationRules,assessmentsDetails_ValidationRules, assessmentsDelete_ValidationRules
}