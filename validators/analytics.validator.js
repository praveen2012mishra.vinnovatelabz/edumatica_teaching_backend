const { body, query } = require('express-validator')

const analytics_studentAssessment = () => {
    return [ 
        query('studentId', 'userId doesn\'t exists').exists(),
       ]
}

const analytics_studentAssessmentGetAnswer = () => {
    return [ 
        query('attemptAssessmentId', 'attemptAssessmentId doesn\'t exists').exists(),
        query('studentId', 'userId doesn\'t exists').exists(),
       ]
}

module.exports = {
    analytics_studentAssessment,
    analytics_studentAssessmentGetAnswer
}