const { body, query } = require('express-validator')



const patchassessment_ValidationRules = () => {
    return [
        body('isSubmitted','Submit Boolean doesn\'t exists').exists(),
        query('attemptassessmentId','Attempt Assessment Id doesn\'t exists').exists(),
        body('startTime','Start Time shouldn\'t exists').not().exists(),
        body('answers','Answers array doesn\'t exists').exists(),
        body('attemptedQuestions','Attempted array doesn\'t exists').exists(),
        body('assessment','Assessment reference doesn\'t exists').exists(),
        body('assessmentData','Assessment Data doesn\'t exists').exists(),
        body('endTime','End Time shouldn\'t exists').not().exists(),
    ]
}

const firstPatchassessment_ValidationRules = () => {
    return [
        query('attemptassessmentId','Attempt Assessment Id doesn\'t exists').exists(),
        body('startTime','Start Time shouldn\'t exists').not().exists(),
        body('answers','Answers array doesn\'t exists').exists(),
        body('attemptedQuestions','Attempted array doesn\'t exists').exists(),
        body('assessment','Assessment reference doesn\'t exists').exists(),
        body('endTime','End Time shouldn\'t exists').exists(),
    ]
}




const getAttemptAssessment_ValidationRules = () =>{
    return [
        query('attemptassessmentId','Assessment ID doesn\'t exists').exists()
    ]
}

const getAttemptAssessmentAnswers_ValidationRules = () =>{
    return [
        query('attemptassessmentId','Attempt Assessment ID doesn\'t exists').exists()
    ]
}

const getAttemptAssessmentbyAssessmentId_ValidationRules = () =>{
    return [
        query('assessmentId','Assessment ID doesn\'t exists').exists()
    ]
}


module.exports = {patchassessment_ValidationRules,getAttemptAssessment_ValidationRules,firstPatchassessment_ValidationRules,getAttemptAssessmentAnswers_ValidationRules, getAttemptAssessmentbyAssessmentId_ValidationRules}