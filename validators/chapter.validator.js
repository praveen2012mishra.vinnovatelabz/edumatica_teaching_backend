const { body, query } = require('express-validator')

const chapterCreate_ValidationRules = () => {
    return [ 
        body('batchId', 'batchId doesn\'t exists').exists(),
        body('batchfriendlyname', 'batchfriendlyname doesn\'t exists').exists(),
        body('boardname', 'boardname doesn\'t exists').exists(),
        body('classname', 'classname doesn\'t exists').exists(),
        body('subjectname', 'subjectname doesn\'t exists').exists(),
	    body('chaptername', 'chaptername doesn\'t exists').exists()
       ]
}

const addMasterChapters_ValidationRules = () => {
    return [ 
        body('batchId', 'batchId doesn\'t exists').exists(),
        body('batchfriendlyname', 'batchfriendlyname doesn\'t exists').exists(),
        body('boardname', 'boardname doesn\'t exists').exists(),
        body('classname', 'classname doesn\'t exists').exists(),
        body('subjectname', 'subjectname doesn\'t exists').exists(),
	    body('chaptername', 'chaptername doesn\'t exists').exists()
       ]
}

const chapterGetChapters_ValidationRules = () => {
    return [ 
        query('batchId', 'batchId doesn\'t exists').exists(),
        query('boardname', 'boardname doesn\'t exists').exists(),
        query('classname', 'classname doesn\'t exists').exists(),
        query('subjectname', 'subjectname doesn\'t exists').exists()
       ]
}

const chapterDetails_ValidationRules = () => {
    return [ 
        query('batchId', 'batchId doesn\'t exists').exists(),
        query('boardname', 'boardname doesn\'t exists').exists(),
        query('classname', 'classname doesn\'t exists').exists(),
        query('subjectname', 'subjectname doesn\'t exists').exists(),
	    query('chaptername', 'chaptername doesn\'t exists').exists()
       ]
}

const chapterUpdate_ValidationRules = () => {
    return [ 
        body('batchId', 'batchId doesn\'t exists').exists(),
        body('batchfriendlyname', 'batchfriendlyname doesn\'t exists').exists(),
        body('boardname', 'boardname doesn\'t exists').exists(),
        body('classname', 'classname doesn\'t exists').exists(),
        body('subjectname', 'subjectname doesn\'t exists').exists(),
        body('chaptername', 'chaptername doesn\'t exists').exists(),
        body('newchaptername', 'newchaptername doesn\'t exists').exists(),
       ]
}

const chapterDelete_ValidationRules = () => {
    return [ 
        body('batchId', 'batchId doesn\'t exists').exists(),
        body('batchfriendlyname', 'batchfriendlyname doesn\'t exists').exists(),
        body('boardname', 'boardname doesn\'t exists').exists(),
        body('classname', 'classname doesn\'t exists').exists(),
        body('subjectname', 'subjectname doesn\'t exists').exists(),
        body('chaptername', 'chaptername doesn\'t exists').exists(),
       ]
}

module.exports = {
    chapterCreate_ValidationRules,
    chapterDetails_ValidationRules, chapterGetChapters_ValidationRules,
    chapterUpdate_ValidationRules, chapterDelete_ValidationRules, addMasterChapters_ValidationRules
}
