const { body, query } = require('express-validator');
const { PHONE_PATTERN, EMAIL_PATTERN, PASSWORD_PATTERN } = require('../utils/constant.utils');

const tutorCreate_ValidationRules =     () => {
    return [ 
        body('mobileNo', 'mobile number is invalid').matches(PHONE_PATTERN),
        body('tutorName', 'tutor name is invalid').exists().isLength({ min: 3 },{ min: 50 }),
        body('emailId', 'emailId is invalid').matches(EMAIL_PATTERN),
        body('qualifications', 'qualifications is invalid').exists(),
        body('schoolName', 'schoolName is invalid').exists(),
       ]
}

const tutorCheckCourseCounte_ValidationRules =     () => {
    return [
        body('courseDetails', 'courseDetails is invalid').exists(),
       ]
}

const addStudentsByTutor_ValidationRules = () => {
    return [ 
        body('studentList', 'studentList not present').exists(),
       ]
}

const getParentForTutor_ValidationRules = () => {
    return [ 
        query('mobileNo', 'Mobile No of parent not present').exists(),
       ]
}

const removeStudentsByTutor_ValidationRules = () => {
    return [ 
        body('studentList', 'studentList not present').exists(),
       ]
}

const changePassword_ValidationRules = () => {
    return [ 
        body('currPassword', 'current password is invalid').matches(PASSWORD_PATTERN),
        body('newPassword', 'new password is invalid').matches(PASSWORD_PATTERN),
        ]
}

const updateKYCDocsByTutor_ValidationRules = () => {
    return [ 
        body('kycDocType', 'kycDocType not present').exists(),
        body('kycDocFormat', 'kycDocFormat not present').exists(),
        body('kycDocLocation', 'kycDocLocation not present').exists(),
       ]
}

const getUploadUrlForKYCDoc_ValidationRules = () => {
    return [ 
        query('fileName', 'fileName not present').exists(),
       ]
}

const getDownloadUrlForKYCDoc_ValidationRules = () => {
    return [ 
        query('uuid', 'uuid not present').exists(),
       ]
}

const delUploadedKYCDoc_ValidationRules = () => {
    return [ 
        query('uuid', 'uuid not present').exists(),
       ]
}

const uploadZip_ValidationRules = () => {
    return [
        body().exists(),
    ]
}

const updateStudent_ValidationRules = () => {
    return [ 
        body('student', 'student not present').exists(),
       ]
}

const updateParent_ValidationRules = () => {
    return [ 
        body('emailId', 'emailId not present').matches(EMAIL_PATTERN),
        body('parentName', 'parentName not present').exists(),
        ]
}

const generateVideoKycAwsLink_ValidationRules = () => {
    return [ 
        body('fileType', 'fileType not present').exists(),
        ]
}

const updateVideoKycSuccessUuid_ValidationRules = () => {
    return [ 
        body('uuid', 'uuid not present').exists(),
        body('fileName', 'fileName not present').exists(),
        ]
}

const getVideoKycDownloadLink_ValidationRules = () => {
    return [
        query('uuid', 'uuid not present').exists(),
    ]
}

const fetchTutorKycDetails_ValidationRules = () => {
    return [
        query('tutorId', 'tutorId not present').exists(),
    ]
}

module.exports = {
    tutorCreate_ValidationRules,
    addStudentsByTutor_ValidationRules,
    removeStudentsByTutor_ValidationRules,
    updateKYCDocsByTutor_ValidationRules,
    getUploadUrlForKYCDoc_ValidationRules,
    getDownloadUrlForKYCDoc_ValidationRules,
    uploadZip_ValidationRules,
    updateStudent_ValidationRules,
    updateParent_ValidationRules,
    changePassword_ValidationRules,
    generateVideoKycAwsLink_ValidationRules,
    updateVideoKycSuccessUuid_ValidationRules,
    getVideoKycDownloadLink_ValidationRules,
    getParentForTutor_ValidationRules,
    fetchTutorKycDetails_ValidationRules,
    tutorCheckCourseCounte_ValidationRules,
    tutorCheckCourseCounte_ValidationRules,
    delUploadedKYCDoc_ValidationRules
}