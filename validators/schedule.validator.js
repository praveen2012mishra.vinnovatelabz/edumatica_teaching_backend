const { body, query } = require('express-validator')

const scheduleCreate_ValidationRules = () => {
    return [         
        body('batchfriendlyname', 'batchfriendlyname doesn\'t exist').exists(),
        body('schedules', 'schedules not present').exists()
       ]
}

const scheduleDetails_ValidationRules = () => {
    return [ 
        query('dayname', 'day doesn\'t exist').exists(),
        query('fromhour', 'fromhour doesn\'t exist').exists()
       ]
}

const scheduleGetTutorRoom_ValidationRules = () => {
    return [ 
        query('dayname', 'day doesn\'t exist').exists(),
        query('fromhour', 'fromhour doesn\'t exist').exists()
       ]
}

const scheduleUpdate_ValidationRules = () => {
    return [ 
        body('dayname', 'day doesn\'t exist').exists(),
        body('fromhour', 'fromhour doesn\'t exist').exists()
       ]
}

const scheduleDelete_ValidationRules = () => {
    return [ 
        body('dayname', 'day doesn\'t exist').exists(),
        body('fromhour', 'fromhour doesn\'t exist').exists()
       ]
}

const scheduleGetStudentRoom_ValidationRules = () => {
    return [ 
        query('dayname', 'day doesn\'t exist').exists(),
        query('fromhour', 'fromhour doesn\'t exist').exists(),
        query('tutorId', 'tutorId doesn\'t exist').exists()
       ]
}

const getParentSchedules_ValidationRules = () => {
    return [ 
        query('studentId', 'studentId doesn\'t exist').exists()
       ]
}


module.exports = {
    scheduleCreate_ValidationRules, scheduleDetails_ValidationRules,
    scheduleDelete_ValidationRules, scheduleGetTutorRoom_ValidationRules,
    scheduleGetStudentRoom_ValidationRules, scheduleUpdate_ValidationRules,
    getParentSchedules_ValidationRules
}