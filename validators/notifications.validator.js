const { body, query, param } = require("express-validator");

const pushNotificationsSaveDeviceTokens_ValidationRules = () => {
    return [
      body("userId", "userId doesn't exists").exists(),
      body("mobileNo", "mobileNo doesn't exists").exists(),
      body("deviceToken", "deviceToken doesn't exists").exists(),
    ];
  };

const saveMsgTemplate_ValidationRules = () => {
    return [
        body("type", "type doesn't exists").exists(),
        body("template", "template doesn't exists").exists(),
        body("icon", "icon doesn't exists").exists(),
    ];
};

const savePushTemplate_ValidationRules = () => {
  return [
      body("type", "type doesn't exists").exists(),
      body("template", "template doesn't exists").exists(),
      body("icon", "icon doesn't exists").exists(),
  ];
};

const sendCustomPush_ValidationRules = () => {
  return [ 
      body("recipients", "recipients doesn't exists").exists(),
      body("batchArr", "batchArr doesn't exists").exists(),
      body("title", "title doesn't exists").exists(),
      body("brief", "brief doesn't exists").exists(),
      body("uuid", "uuid doesn't exists").exists(),
  ]
}

const sendMail_ValidationRules = () => {
  return [
    query("mailID", "mailID doesn't exists").exists(),
  ]
}

const getAllNotifications_ValidationRules = () => {
  return [
    query("userId", "userId doesn't exists").exists(),
  ]
}

const updateNotifications_ValidationRules = () => {
  return [
    body("userId", "userId doesn't exists").exists(),
    body("notificationIdArr", "notificationIdArr doesn't exists").exists(),
  ]
}

const switchMsgTemplate_ValidationRules = () => {
  return [
    body("type", "type doesn't exists").exists(),
    body("state", "state doesn't exists").exists(),
  ]
}

const switchPushTemplate_ValidationRules = () => {
  return [
    body("type", "type doesn't exists").exists(),
    body("state", "state doesn't exists").exists(),
  ]
}

const announcementFileUpload_ValidationRules = () => {
  return [
    body("fileType", "fileType doesn't exists").exists(),
  ]
}

const announcementFileDownload_ValidationRules = () => {
  return [
    query("uuid", "uuid doesn't exists").exists(),
  ]
}

const fetchAnnouncement_ValidationRules = () => {
  return [
    
  ]
}

const fetchAnnouncementById_ValidationRules = () => {
  return [
    param("announcementId", "announcementId doesn\'t exists").exists(),
  ];
};

  module.exports = {
      pushNotificationsSaveDeviceTokens_ValidationRules,
      saveMsgTemplate_ValidationRules,
      savePushTemplate_ValidationRules,
      sendCustomPush_ValidationRules,
      sendMail_ValidationRules,
      getAllNotifications_ValidationRules,
      updateNotifications_ValidationRules,
      switchMsgTemplate_ValidationRules,
      switchPushTemplate_ValidationRules,
      announcementFileUpload_ValidationRules,
      announcementFileDownload_ValidationRules,
      fetchAnnouncement_ValidationRules,
      fetchAnnouncementById_ValidationRules
  };