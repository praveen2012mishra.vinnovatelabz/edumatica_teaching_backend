const { body } = require('express-validator');

const batchChapterSave_ValidationRules = () => {
    return [ 
        body('batchfriendlyname', 'batchfriendlyname don\'t exists').exists(),
        body('boardname', 'boardname doesn\'t exists').exists(),
        body('classname', 'classname don\'t exists').exists(),
        body('subjectname', 'subjectname doesn\'t exists').exists(),
        body('chaptername', 'batchfriendlyname don\'t exists').exists()
       ]
}

module.exports = {
    batchChapterSave_ValidationRules
}