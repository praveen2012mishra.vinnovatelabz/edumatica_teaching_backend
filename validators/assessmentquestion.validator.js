const { body, query } = require('express-validator')


const getTopics_validationRules = () =>{
    return [
        query('boardname','Boardname doesn\'t exists').exists(),
        query('classname','Boardname doesn\'t exists').exists(),
        query('subjectname','Boardname doesn\'t exists').exists(),
    ]
}

const getQuestions_validationRules = () =>{
    return [
        body('questionIds','Question Ids doesn\'t exists').exists(),
    ]
}


module.exports={getTopics_validationRules,getQuestions_validationRules}