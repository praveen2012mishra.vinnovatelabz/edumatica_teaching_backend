const { body, query } = require('express-validator')
const { BACKOFFICE_ADMIN_TYPES, EMAIL_PATTERN, OTP_PATTERN, PASSWORD_PATTERN } = require('../utils/constant.utils')

// const register_ValidationRules = () => {
//     return [
//         body('password', 'invalid password').exists().matches(PASSWORD_PATTERN),

//         body('adminType', 'invalid adminType').exists().custom((val) => BACKOFFICE_ADMIN_TYPES.includes(val))
//     ]
// }

const adminAuthenticate_ValidationRules = () => {
    return [
        body('userName', 'invalid username').exists().matches(EMAIL_PATTERN),

        body('password', 'invalid password').exists().matches(PASSWORD_PATTERN)
    ]
}

const getOtp_ValidationRules = () => {
    return [
        body('username', 'invalid username').exists().matches(EMAIL_PATTERN)
    ]
}

const verifyOtp_ValidationRules = () => {
    return [
        body('username', 'invalid username').exists().matches(EMAIL_PATTERN),
        body('otp', 'invalid otp').exists().matches(OTP_PATTERN)
    ]
}

const setPassword_ValidationRules = () => {
    return [
        body('username', 'invalid username').exists().matches(EMAIL_PATTERN),
        body('newPassword', 'invalid password').exists().matches(PASSWORD_PATTERN)
    ]
}

module.exports = {
    // register_ValidationRules,
    adminAuthenticate_ValidationRules,
    getOtp_ValidationRules,
    verifyOtp_ValidationRules,
    setPassword_ValidationRules
}