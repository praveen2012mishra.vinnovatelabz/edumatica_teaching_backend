const { body, query } = require('express-validator');
const { PHONE_PATTERN, EMAIL_PATTERN, ORG_NAME_PATTERN } = require('../utils/constant.utils');

const organizationCreate_ValidationRules = () => {
    return [ 
        body('mobileNo', 'mobile number is invalid').matches(PHONE_PATTERN),
        body('organizationName', 'organization name is invalid').matches(ORG_NAME_PATTERN),
        body('emailId', 'emailId is invalid').matches(EMAIL_PATTERN),
       ]
}

const orgCheckCourseCounte_ValidationRules =     () => {
    return [
        body('courseDetails', 'courseDetails is invalid').exists(),
       ]
}

const addTutorsForOrganization_ValidationRules = () => {
    return [ 
        body('tutorList', 'tutorList not present').exists(),
       ]
}

const removeTutors_ValidationRules = () => {
    return [ 
        body('tutorList', 'tutorList not present').exists(),
       ]
}

const addStudents_ValidationRules = () => {
    return [ 
        body('studentList', 'studentList not present').exists(),
       ]
}

const removeStudents_ValidationRules = () => {
    return [ 
        body('studentList', 'studentList not present').exists(),
       ]
}

const generateVideoKycAwsLink_ValidationRules = () => {
    return [ 
        body('fileType', 'fileType not present').exists(),
        ]
}

const updateVideoKycSuccessUuid_ValidationRules = () => {
    return [ 
        body('uuid', 'uuid not present').exists(),
        body('fileName', 'fileName not present').exists(),
        ]
}

const getVideoKycDownloadLink_ValidationRules = () => {
    return [
        query('uuid', 'uuid not present').exists(),
    ]
}

const getStudentsForClass_ValidationRules = () => {
    return [ 
        query('className', 'Classname not present').exists(),
        query('boardName', 'BoardName not present').exists(),
       ]
}

const fetchOrgKycDetails_ValidationRules = () => {
    return [
        query('organizationId', 'organizationId not present').exists(),
    ]
}

const updateStudent_ValidationRules = () => {
    return [ 
        body('student', 'student not present').exists(),
       ]
}

module.exports = {
    organizationCreate_ValidationRules,
    addTutorsForOrganization_ValidationRules,
    removeTutors_ValidationRules,
    addStudents_ValidationRules,
    removeStudents_ValidationRules,
    generateVideoKycAwsLink_ValidationRules,
    updateVideoKycSuccessUuid_ValidationRules,
    getVideoKycDownloadLink_ValidationRules,
    getStudentsForClass_ValidationRules,
    fetchOrgKycDetails_ValidationRules,
    orgCheckCourseCounte_ValidationRules,
    updateStudent_ValidationRules
}