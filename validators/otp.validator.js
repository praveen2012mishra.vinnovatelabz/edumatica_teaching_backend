const { body } = require('express-validator');
const { PHONE_PATTERN, OTP_PATTERN } = require('../utils/constant.utils');

const generateOTP_ValidationRules = () => {
    return [ 
        body('mobileNo', 'mobile number is invalid').matches(PHONE_PATTERN)
       ]
}

const validateOTP_ValidationRules = () => {
    return [ 
        body('mobileNo', 'mobile number is invalid').matches(PHONE_PATTERN),
        body('otp', 'otp is invalid').matches(OTP_PATTERN),
        body('userType', 'userType is invalid').exists(),
       ]
}

const validateForForgetPwd_ValidationRules = () => {
    return [ 
        body('mobileNo', 'mobile number is invalid').matches(PHONE_PATTERN),
        body('otp', 'otp is invalid').matches(OTP_PATTERN),
       ]
}

module.exports = {
    generateOTP_ValidationRules, validateOTP_ValidationRules, validateForForgetPwd_ValidationRules
}