const { body, query } = require("express-validator");

const bbbWebHookCreateHook_ValidationRules = () => {
  return [body("meetingID", "meetingID doesn't exists").exists()];
};

const bbbWebHookDestroyHook_ValidationRules = () => {
  return [body("hookID", "hookID doesn't exists").exists()];
};

const bbbWebHookGethookslist_ValidationRules = () => {
  return [body("meetingID", "meetingID doesn't exists").exists()];
};

module.exports = {
  bbbWebHookCreateHook_ValidationRules,
  bbbWebHookDestroyHook_ValidationRules,
  bbbWebHookGethookslist_ValidationRules,
};
