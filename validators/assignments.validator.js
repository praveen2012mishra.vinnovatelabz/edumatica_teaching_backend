const { body, query } = require('express-validator');

const createAssignment_ValidationRules = () => {
    return [
        body('boardname', 'boardname not present').exists(),
        body('subjectname', 'subjectname not present').exists(),
        body('assignmentname', 'assignmentname not present').exists(),
        body('marks', 'marks not present').exists(),
        body('instructions', 'instructions not present').exists(),
        body('brief', 'brief not present').exists(),
        body('startDate', 'startDate not present').exists(),
        body('endDate', 'endDate not present').exists(),
        body('batches', 'batches not present').exists(),
        body('students', 'batches not present').exists(),
        body('taskDocs', 'taskDocs not present').exists()
    ]
}

const createAssignmentDraft_ValidationRules = () => {
    return [
        body('boardname', 'boardname not present').exists(),
        body('subjectname', 'subjectname not present').exists(),
        body('assignmentname', 'assignmentname not present').exists(),
        body('marks', 'marks not present').exists(),
        body('instructions', 'instructions not present').exists(),
        body('brief', 'brief not present').exists(),
        body('startDate', 'startDate not present').exists(),
        body('endDate', 'endDate not present').exists(),
        body('batches', 'batches not present').exists(),
        body('students', 'batches not present').exists(),
        body('taskDocs', 'taskDocs not present').exists()
    ]
}

const fetchStudentAssignment_ValidationRules = () => {
    return [
        query('studentId', 'studentId not present').exists()
    ]
}

const fetchAssignedAssignment_ValidationRules = () => {
    return [
        query('assignmentId', 'assignmentId not present').exists()
    ]
}

const getDownloadUrlForAssignmentDoc_ValidationRules = () => {
    return [
        query('uuid', 'uuid not present').exists(),
    ]
}

const getUploadUrlForAssignmentDoc_ValidationRules = () => {
    return [ 
        query('fileName', 'fileName not present').exists(),
    ]
}

const submitAssignmentDoc_ValidationRules = () => {
    return [
        body('studentId', 'studentId not present').exists(),
        body('assignmentId', 'assignmentId not present').exists(),
        body('answerDocs', 'answerDocs not present').exists()
    ]
}

const evaluateAssignmentDoc_ValidationRules = () => {
    return [
        body('studentId', 'studentId not present').exists(),
        body('assignmentId', 'assignmentId not present').exists(),
        body('tutorfeedback', 'tutorfeedback not present').exists()
    ]
}

module.exports = {
    createAssignment_ValidationRules, fetchStudentAssignment_ValidationRules,
    fetchAssignedAssignment_ValidationRules, getDownloadUrlForAssignmentDoc_ValidationRules,
    submitAssignmentDoc_ValidationRules, createAssignmentDraft_ValidationRules,
    getUploadUrlForAssignmentDoc_ValidationRules, evaluateAssignmentDoc_ValidationRules
}