const { body, query } = require('express-validator')

const batchCreate_ValidationRules = () => {
    return [ 
        body('boardname', 'boardname doesn\'t exists').exists().isLength({ min: 1 },{ min: 50 }),
        body('classname', 'classname doesn\'t exists').exists().isLength({ min: 1 },{ min: 50 }),
        body('subjectname', 'subjectname doesn\'t exists').exists().isLength({ min: 1 },{ min: 50 }),
        body('batchfriendlyname', 'batchfriendlyname length must be minimum 5 and max 50').exists().isLength({ min: 5 },{ min: 50 }),
        body('batchstartdate', 'batchstartdate needs to be a valid date').exists().custom(isValidDate),
        body('batchenddate', 'batchendate needs to be a valid date').exists().custom(isValidDate).custom(isAfterDate),
        body('students', 'students doesn\'t exists').exists()
       ]
}

function isValidDate(value) {
    if (!value.match(/^\d{4}-\d{2}-\d{2}$/)) return false;
    const date = new Date(value);
    return date.toISOString().slice(0, 10) === value;
  }

  function isAfterDate(value, {req}) {
    const endDate = new Date(value);
    const startDate = new Date(req.body.batchstartdate);
    return endDate > startDate;
  }

const batchUpdate_ValidationRules = () => {
    return [ 
        body('boardname', 'boardname doesn\'t exists').exists(),
        body('classname', 'classname doesn\'t exists').exists(),
        body('subjectname', 'subjectname doesn\'t exists').exists(),
        body('batchfriendlyname', 'batchfriendlyname length must be minimum 5 and max 50').exists().isLength({ min: 5 },{ min: 50 }),
        body('batchstartdate', 'batchstartdate needs to be a valid date').exists().custom(isValidDate),
        body('batchenddate', 'batchendate needs to be a valid date').exists().custom(isValidDate),
        body('batchenddate','batchenddate cannot be less than batchstartdate').exists().custom(isAfterDate),
        body('students', 'students doesn\'t exists').exists()
       ]
}

const batchDetails_ValidationRules = () => {
    return [ 
        query('batchfriendlyname', 'batchfriendlyname length must be minimum 5 and max 50').exists()
       ]
}

const batchDetails_id_ValidationRules = () => {
    return [ 
        query('id', 'id length must be minimum 5 and max 50').exists()
       ]
}


const batchDelete_ValidationRules = () => {
    return [ 
        body('batchfriendlyname', 'batchfriendlyname length must be minimum 5 and max 50').exists()
       ]
}

const batchesSearch_ValidationRules = () => {
    return [ 
        query('boardname', 'boardname doesn\'t exists').exists(),
        query('classname', 'classname doesn\'t exists').exists(),
        query('subjectname', 'subjectname doesn\'t exists').exists()
       ]
}

const getUploadUrlForSyllabus_ValidationRules = () => {
    return [ 
        query('batch', 'batch not present').exists(),
        query('contentType', 'contentType not present').exists(),
    ]
}

const getDownloadUrlForSyllabus_ValidationRules = () => {
    return [
        query('uuid', 'uuid not present').exists(),
        query('batch', 'batch not present').exists(),
    ]
}

const updateSyllabus_ValidationRules = () => {
    return [
        body('uuid', 'uuid not present').exists(),
        body('batch', 'batch not present').exists(),
    ]
}



module.exports = {
    batchCreate_ValidationRules, batchUpdate_ValidationRules,
    batchDetails_ValidationRules,
    batchDelete_ValidationRules,
    batchesSearch_ValidationRules,
    getUploadUrlForSyllabus_ValidationRules,
    getDownloadUrlForSyllabus_ValidationRules,
    updateSyllabus_ValidationRules,
    batchDetails_id_ValidationRules
}