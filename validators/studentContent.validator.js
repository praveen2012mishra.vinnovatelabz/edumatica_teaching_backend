const { body, query } = require('express-validator');

const studentContentCreate_ValidationRules = () => {
    return [ 
        body('batchfriendlyname', 'batchfriendlyname don\'t exists').exists(),
        body('contentname', 'contentname doesn\'t exists').exists(),
       ]
}

const studentContentAllstudentContents_ValidationRules = () => {
    return [ 
        query('tutorId', 'tutorId doesn\'t exists').exists()
       ]
}

const studentContentGetContent_ValidationRules = () => {
    return [ 
        query('tutorId', 'tutorId doesn\'t exists').exists(),
        query('contentname', 'contentname doesn\'t exists').exists(),
       ]
}

const studentContentSaveAnswers_ValidationRules = () => {
    return [ 
        body('tutorId', 'tutorId doesn\'t exists').exists(),
        body('contentname', 'contentname doesn\'t exists').exists(),
        body('answers', 'answers doesn\'t exists').exists()
       ]
}

const studentContentViewResults_ValidationRules = () => {
    return [ 
        body('batchfriendlyname', 'batchfriendlyname don\'t exists').exists(),
        body('contentname', 'contentname doesn\'t exists').exists(),
       ]
}

module.exports = {
    studentContentCreate_ValidationRules,
    studentContentGetContent_ValidationRules,
    studentContentAllstudentContents_ValidationRules,
    studentContentSaveAnswers_ValidationRules,
    studentContentViewResults_ValidationRules
}