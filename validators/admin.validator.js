const { body, query } = require('express-validator');
const { isArray } = require('lodash');
const { ROLE_STATUSES, KYC_STATUSES, EMAIL_PATTERN, PHONE_PATTERN, PASSWORD_PATTERN } = require('../utils/constant.utils')

const createAdmin_ValidationRules = () => {
    return [
        body('adminReq', 'invalid adminReq').exists()
        .custom(val => Object.keys(val).length <= 4),
        body('adminReq.firstName', 'invalid firstName').exists().isLength({ min: 3 },{ min: 50 }),
        body('adminReq.lastName', 'invalid lastName').exists().isLength({ min: 1 },{ min: 50 }), // min 1 char considering initials
        body('adminReq.emailId', 'invalid emailId').matches(EMAIL_PATTERN),
        body('adminReq.mobileNo', 'invalid mobileNo').matches(PHONE_PATTERN),

        body('adminUserReq', 'invalid adminUserReq').exists()
        .custom(val => val.role && /*val.role !== 'super' &&*/ Object.keys(val).length === 3),
        body('adminUserReq.username', 'invalid username').matches(EMAIL_PATTERN),
        body('adminUserReq.password', 'invalid password').matches(PASSWORD_PATTERN),
    ]
}

const editAdmin_ValidationRules = () => {
    return [
        body('oldEmailId', 'oldEmailId can not be empty').matches(EMAIL_PATTERN)
    ]
}

const deleteAdmin_ValidationRules = () => {
    return [
        query('emailId', 'emailId can not be empty').matches(EMAIL_PATTERN)
    ]
}

const createPermissions_ValidationRules = () => {
    return [
        body('permissions', 'invalid permissions').custom(val => {
            if(!isArray(val)) return false
            if(val.every(cur => cur.name.length > 3 && cur.group.length > 3)) return true
        })
    ]
}

const createRole_ValidationRules = () => {
    return [
        body('name', 'invalid name').exists().isLength({ min: 3 },{ min: 50 }),
        body('permissions', 'invalid permissions').custom(val => {
            if(!isArray(val)) return false
            if(val.every(cur => cur.length > 3)) return true
        })
    ]
}

const editRole_ValidationRules = () => {
    return [
        body('name', 'invalid name').exists().isLength({ min: 3 },{ min: 50 }),
        body('permissions', 'invalid permissions').custom(val => {
            if(!isArray(val)) return false
            if(val.every(cur => cur.length > 3)) return true
        })
    ]
}

const changePassword_ValidationRules = () => {
    return [
        body('oldPassword', 'oldPassword can not be empty').matches(PASSWORD_PATTERN),
        body('newPassword', 'newPassword can not be empty').matches(PASSWORD_PATTERN)
    ]
}

const resetPassword_ValidationRules = () => {
    return [
        body('usernameToReset', 'invalid username').exists().matches(EMAIL_PATTERN),
        body('newPassword', 'newPassword can not be empty').matches(PASSWORD_PATTERN)
    ]
}

const sendNewPasswordMail_ValidaionRules = () => {
    return [
        body('name', 'invalid name').exists().isLength({ min: 3 },{ min: 50 }),
        body('email', 'invalid email').matches(EMAIL_PATTERN),
        body('newPassword', 'invalid newPassword').matches(PASSWORD_PATTERN)
    ]
}

const getUsers_ValidationRules = () => {
    const userTypes = ['org', 'tutor', 'student', 'parent']
    return [
        body('userType', 'invalid userType').custom(val => userTypes.includes(val))
    ]
}

const fetchKycStatus_ValidationRules = () => {
    const userTypes = ['org', 'tutor', 'bothTutorOrg']
    return [
        body('userType', 'invalid userType').custom(val => userTypes.includes(val))
    ]
}

const fetchKycData_ValidationRules = () => {
    const userTypes = ['org', 'tutor']
    return [
        body('userType', 'invalid userType').custom(val => userTypes.includes(val)),
        body('userId', 'invalid userId').exists().isLength({ min: 3 },{ min: 50 })
    ]
}

const updateUserStatus_ValidationRules = () => {
    return [
        body('userType', 'invalid userType').exists()
        .custom((value) => value === 'tutor' || value === 'org'),

        body('userId', 'invalid userId').exists().isLength({min: 3}, {min: 50}),

        body('roleStatus', 'invalid roleStatus').exists()
        .custom((value) => ROLE_STATUSES.includes(value))
    ]
}

// const generateRegisterLink_ValidationRules = () => {
//     return [
//         query('adminType', 'invalid adminType').exists()
//         .custom((val) => BACKOFFICE_ADMIN_TYPES.includes(val))
//     ]
// }

const verifyUser_ValidationRules = () => {
    return [
        body('userType', 'invalid userType').exists()
        .custom((value) => value === 'tutor' || value === 'org'),

        body('userId', 'invalid userId').exists().isLength({min: 3}, {min: 50}),
        
        body('allKycStatus', 'invalid allKycStatus').exists()
        .custom((value) => KYC_STATUSES.includes(value))
    ]
}

module.exports = {
    updateUserStatus_ValidationRules,
    verifyUser_ValidationRules,
    // generateRegisterLink_ValidationRules,
    createAdmin_ValidationRules,
    editAdmin_ValidationRules,
    deleteAdmin_ValidationRules,
    createPermissions_ValidationRules,
    createRole_ValidationRules,
    editRole_ValidationRules,
    changePassword_ValidationRules,
    resetPassword_ValidationRules,
    sendNewPasswordMail_ValidaionRules,
    getUsers_ValidationRules,
    fetchKycStatus_ValidationRules,
    fetchKycData_ValidationRules
}