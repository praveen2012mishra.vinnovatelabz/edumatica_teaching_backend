const { body, query } = require("express-validator");

const bbbRecordingsGetRecordingInfoById_ValidationRules = () => {
  return [body("meetingID", "meetingID doesn't exists").exists()];
};

const bbbRecordingsDeleteRecordingById_ValidationRules = () => {
  return [body("recordID", "recordID doesn't exists").exists()];
};

const bbbRecordingsDeleteRecordingData_ValidationRules = () => {
  return [query("internalMeetingID", "internalMeetingID doesn't exists").exists()];
}

module.exports = {
  bbbRecordingsGetRecordingInfoById_ValidationRules,
  bbbRecordingsDeleteRecordingById_ValidationRules,
  bbbRecordingsDeleteRecordingData_ValidationRules
};
