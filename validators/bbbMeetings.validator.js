const { body, query } = require("express-validator");

const bbbMeetingsCreateMeeting_ValidationRules = () => {
  return [
    body("meetingID", "meetingID doesn't exists").exists(),
    body("meetingName", "meetingName doesn't exists").exists(),
  ];
};

const bbbMeetingsJoinMeeting_ValidationRules = () => {
  return [
    body("meetingID", "meetingID doesn't exists").exists(),
    body("joineeName", "joineeName doesn't exists").exists(),
    body("joineerole", "joineerole doesn't exists").exists(),
    body("userID", "userID doesn't exists").exists(),
  ];
};

const bbbMeetingsIsMeetingRunning_ValidationRules = () => {
  return [body("meetingID", "meetingID doesn't exists").exists()];
};

const bbbMeetingsGetMeetingInfo_ValidationRules = () => {
  return [body("meetingID", "meetingID doesn't exists").exists()];
};

const bbbMeetingsGetPostMeetingEventsInfo_ValidationRules = () => {
  return [query("meetingID", "meetingID doesn't exists").exists()];
};

const bbbMeetingsGetPostIndividualMeetingEventsInfo_ValidationRules = () => {
  return [query("internalMeetingID", "internalMeetingID doesn't exists").exists()];
}

const bbbMeetingsFeedback_ValidationRules = () => {
  return [
    body("batchId", "batchId does not exists").exists(),
    body("rating", "Rating does not exits").exists(),
  ]
}

module.exports = {
  bbbMeetingsCreateMeeting_ValidationRules,
  bbbMeetingsJoinMeeting_ValidationRules,
  bbbMeetingsGetMeetingInfo_ValidationRules,
  bbbMeetingsIsMeetingRunning_ValidationRules,
  bbbMeetingsGetPostMeetingEventsInfo_ValidationRules,
  bbbMeetingsGetPostIndividualMeetingEventsInfo_ValidationRules,
  bbbMeetingsFeedback_ValidationRules
};
