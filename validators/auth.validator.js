const { body } = require('express-validator');
const { PHONE_PATTERN, OTP_PATTERN, PASSWORD_PATTERN, ORG_CODE_PATTERN } = require('../utils/constant.utils');

const register_ValidationRules = () => {
    return [ 
        body('mobileNo', 'mobile number is invalid').matches(PHONE_PATTERN),
        body('otp', 'otp is invalid').matches(OTP_PATTERN),
        body('password', 'password is invalid').matches(PASSWORD_PATTERN),
        body('userType', 'userType is invalid').exists(),
       ]
}

const checkOrgCode_ValidationRules = () => {
    return [ 
        body('orgCode', 'Code Should be Alphabet starting Alphanumeric with _( underscore) allowed and alteast 5 characters').matches(ORG_CODE_PATTERN),
       ]
}

const setPassword_ValidationRules = () => {
    return [ 
        body('mobileNo', 'mobile number is invalid').matches(PHONE_PATTERN),
        body('otp', 'otp is invalid').matches(OTP_PATTERN),
        body('password', 'password is invalid').matches(PASSWORD_PATTERN),
       ]
}

const authenticate_ValidationRules = () => {
    return [ 
        body('password', 'password is invalid').matches(PASSWORD_PATTERN),
        ]
}

const refreshtoken_ValidationRules = () => {
    return [ 
        body('token', 'token do not exist').exists(),
        ]
}

const verifyCaptcha_ValidationRules = () => {
    return [ 
        body('captchaValue', 'captcha Value is invalid').exists(),
        ]
}

const verifyEmail_ValidationRules = () => {
    return [
        body('role', 'role does not exist').exists(),
        body('entityId', 'entityId does not exist').exists(),
        body('verificationtoken', 'verificationtoken does not exist').exists(),
    ]
}

module.exports = {
    checkOrgCode_ValidationRules, register_ValidationRules, setPassword_ValidationRules, 
    authenticate_ValidationRules, verifyCaptcha_ValidationRules, refreshtoken_ValidationRules, verifyEmail_ValidationRules
}