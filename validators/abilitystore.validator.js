const { body } = require('express-validator')

const abilitystore_createability_ValidationRules = () => {
    return [
        body('pageId','pageId doesn\'t exists').exists(),
        body('consumerId','consumerId doesn\'t exists').exists(),
        body('abilityJSON' , 'abilityJSON ddoesn\'t exists').exists(),
        body('consumerId' , 'abilityJSON ddoesn\'t exists').isArray(),
        body('abilityJSON' , 'abilityJSON ddoesn\'t exists').isArray(),
    ]
}

const abilitystore_patchability_ValidationRules = () => {
    return [
        body('abilityId','abilityId doesn\'t exists').exists(),
        body('consumerId','consumerId doesn\'t exists').exists(),
        body('abilityJSON' , 'abilityJSON ddoesn\'t exists').exists()
    ]
}


module.exports = {
    abilitystore_createability_ValidationRules,
    abilitystore_patchability_ValidationRules
}