const { body, query } = require('express-validator')

const contentCreate_ValidationRules = () => {
    return [ 
        body('boardname', 'boardname doesn\'t exists').exists(),
        body('classname', 'classname doesn\'t exists').exists(),
        body('subjectname', 'subjectname doesn\'t exists').exists(),
	    body('chaptername', 'chaptername doesn\'t exists').exists(),
        body('contenttype', 'contenttype doesn\'t exists').exists(),
        body('contentname', 'contentname doesn\'t exists').exists(),
       ]
}

const getSignedUrlForUpload_ValidationRules = () => {
    return [ 
        query('boardname', 'boardname doesn\'t exists').exists(),
        query('classname', 'classname doesn\'t exists').exists(),
        query('subjectname', 'subjectname doesn\'t exists').exists(),
	    query('chaptername', 'chaptername doesn\'t exists').exists(),
       ]
}

const getSignedUrlForDownload_ValidationRules = () => {
    return [ 
        query('boardname', 'boardname doesn\'t exists').exists(),
        query('classname', 'classname doesn\'t exists').exists(),
        query('subjectname', 'subjectname doesn\'t exists').exists(),
        query('chaptername', 'chaptername doesn\'t exists').exists(),
        query('uuid', 'uuid doesn\'t exists').exists(),
       ]
}

const getStudentSignedUrlForDownload_ValidationRules = () => {
    return [ 
        query('tutorId', 'tutorId doesn\'t exists').exists(),
        query('boardname', 'boardname doesn\'t exists').exists(),
        query('classname', 'classname doesn\'t exists').exists(),
        query('subjectname', 'subjectname doesn\'t exists').exists(),
        query('chaptername', 'chaptername doesn\'t exists').exists(),
        query('uuid', 'uuid doesn\'t exists').exists(),
       ]
}


const contentGetContents_ValidationRules = () => {
    return [ 
        query('boardname', 'boardname is invalid').exists().isLength({ min: 3 },{ min: 50 }),
        query('classname', 'classname is invalid').exists().isLength({ min: 1 },{ min: 50 }),
        query('subjectname', 'subjectname is invalid').exists().isLength({ min: 3 },{ min: 50 }),
	    query('chaptername', 'chaptername is invalid').exists().isLength({ min: 3 },{ min: 50 }),
        query('contenttype', 'chaptername is invalid').exists().isLength({ min: 3 },{ min: 50 })
       ]
}

const contentDetails_ValidationRules = () => {
    return [ 
        query('contentname', 'contentname doesn\'t exists').exists(),
       ]
}

const contentDelete_ValidationRules = () => {
    return [ 
        body('contentname', 'contentname doesn\'t exists').exists(),
       ]
}

const contentUpdate_ValidationRules = () => {
    return [ 
        body('contentname', 'contentname doesn\'t exists').exists(),
        body('questions', 'questions doesn\'t exists').exists(),
       ]
}

module.exports = {
    contentCreate_ValidationRules,
    getSignedUrlForUpload_ValidationRules,
    getSignedUrlForDownload_ValidationRules,
    getStudentSignedUrlForDownload_ValidationRules,
    contentUpdate_ValidationRules,
    contentDetails_ValidationRules,
    contentGetContents_ValidationRules,
    contentDelete_ValidationRules
}
