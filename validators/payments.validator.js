const { body, query, param } = require("express-validator");

const createCourseBundle_ValidationRules = () => {
  return [
    body("batchId", "batchId doesn\'t exists").exists(),
    body("packageArr", "packageArr doesn\'t exists").exists(),
  ];
};

const fetchCourseBundleById_ValidationRules = () => {
  return [
    param("bundleId", "bundleId doesn\'t exists").exists(),
  ];
};

const fetchCourseBundle_ValidationRules = () => {
  return [

  ];
};

const updateCourseBundle_ValidationRules = () => {
  return [
    body("bundleId", "bundleId doesn\'t exists").exists(),
    body("paymentPackageId", "paymentPackageId doesn\'t exists").exists(),
    body("newfees", "newfees doesn\'t exists").exists(),
    body("newdiscount", "newdiscount doesn\'t exists").exists(),
  ];
};

const deleteCourseBundle_ValidationRules = () => {
  return [
    query("bundleId", "bundleId doesn\'t exists").exists(),
  ]
}

const fetchStudentAcc_ValidationRules = () => {
  return [
    body("studentList", "studentList doesn\'t exists").exists(),
  ]
}

const fetchOrgAcc_ValidationRules = () => {
  return [
    // query("roleBasedUserId", "studentId doesn\'t exists").exists(), //  tutorId or organizationId (entityId)
  ]
}

const generatePayOrder_ValidationRules = () => {
  return [
    body("purchasedPackageId", "purchasedPackageId doesn\'t exists").exists(),
    body("batchId", "batchId doesn\'t exists").exists(),
    body("amount", "amount doesn\'t exists").exists(),
    body("paymentcycle", "paymentcycle doesn\'t exists").exists(),
  ]
}

const generateOrgPayOrder_ValidationRules = () => {
  return [
    body("accountType", "accountType doesn\'t exists").exists(),
    body("purchasedEdumacPackageId", "purchasedEdumacPackageId doesn\'t exists").exists(),
    body("amount", "amount doesn\'t exists").exists(),
    body("paymentcycle", "paymentcycle doesn\'t exists").exists(),
  ]
}

const generateEdumacPackagepayOrder_ValidationRules = () => {
  return [
    body("accountType", "accountType doesn\'t exists").exists(),
    body("purchasedEdumacPackageId", "purchasedEdumacPackageId doesn\'t exists").exists(),
    body("amount", "amount doesn\'t exists").exists(),
    body("paymentcycle", "paymentcycle doesn\'t exists").exists(),
    body("newPackagePlanId", "newPackagePlanId doesn\'t exists").exists(),
  ]
}

const validateReferralCode_ValidationRules = () => {
  return [
    query("accountType", "accountType doesn\'t exists").exists(),
    query("referralCode", "referralCode doesn\'t exists").exists(),
  ]
}

const validateOfflinePayment_ValidationRules = () => {
  return [
    body("amount", "amount doesn\'t exists").exists(),
    body("purchasedCoursePackageId", "purchasedCoursePackageId doesn\'t exists").exists(),
    body("paymentcycle", "paymentcycle doesn\'t exists").exists(),
  ]
}

// const authenticatePaymentSuccess_ValidationRules = () => {
//   return [
//     // body("orderCreationId", "orderCreationId doesn\'t exists").exists(),
//     body("razorpayPaymentId", "razorpayPaymentId doesn\'t exists").exists(),
//     body("razorpayOrderId", "razorpayOrderId doesn\'t exists").exists(),
//     body("razorpaySignature", "razorpaySignature doesn\'t exists").exists(),
//   ]
// }

const updatePayOrders_ValidationRules = () => {
  return [
    body("orderArr", "orderArr doesn\'t exists").exists(),
  ]
}

const paymentStatus_ValidationRules = () => {
  return [
    
  ]
}

const fetchStatus_ValidationRules = () => {
  return [
    query("orderId", "orderId doesn\'t exists").exists(),
  ]
}

module.exports = {
    createCourseBundle_ValidationRules,
    fetchCourseBundle_ValidationRules,
    updateCourseBundle_ValidationRules,
    fetchCourseBundleById_ValidationRules,
    deleteCourseBundle_ValidationRules,
    fetchStudentAcc_ValidationRules,
    generatePayOrder_ValidationRules,
    generateOrgPayOrder_ValidationRules,
    fetchOrgAcc_ValidationRules,
    validateReferralCode_ValidationRules,
    validateOfflinePayment_ValidationRules,
    generateEdumacPackagepayOrder_ValidationRules,
    updatePayOrders_ValidationRules,
    paymentStatus_ValidationRules,
    fetchStatus_ValidationRules
};
