const { 
    payments_createCourseBundle, 
    payments_fetchCourseBundle, 
    payments_updateCourseBundle, 
    payments_fetchCourseBundleById, 
    payments_deleteCourseBundle, 
    payments_fetchStudentAcc, 
    payments_generatePayOrder, 
    payments_authenticatePaymentSuccess, 
    payments_handleRouting, 
    payments_generateOrgPayOrder, 
    payments_fetchOrgAcc,
    validateReferralCode,
    getMasterPackages,
    payments_validateOfflinePayment,
    payments_generateEdumacPackagepayOrder,
    payments_updatePayOrders,
    payments_checkStatus
} = require('../dbc/payments.dbc');
const logger = require('../utils/logger.utils');
const { ResponseCode, getResponseMsg } = require('../utils/response.utils');
const Razorpay = require("razorpay")
const shortid = require('shortid')

exports.payments_createCourseBundle =  (req, res, next) => {
    const ownerId = req.user.ownerId;
    const userId = req.user._id;
    const entityId = req.user.entityId;
    const batchId = req.body.batchId
    const packageArr = req.body.packageArr;

    logger.debug('Enter payments_createCourseBundle - ownerId:' + ownerId);
    payments_createCourseBundle(ownerId, userId, entityId, batchId, packageArr, (err, code) => {
        if(err){
            logger.error('Exit payments_createCourseBundle - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit payments_createCourseBundle - ownerId:' + ownerId);
    });
};

exports.payments_fetchCourseBundleById = (req, res, next) => {
    const ownerId = req.user.ownerId;
    const userId = req.user._id;
    const bundleId = req.params.bundleId
    logger.debug('Enter payments_fetchCourseBundleById - ownerId:' + ownerId + ' bundleId:' + bundleId);
    payments_fetchCourseBundleById(ownerId, userId, bundleId, (err, code, courseBundle) => {
        if(err){
            logger.error('Exit payments_fetchCourseBundleById - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), courseBundle:courseBundle});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit payments_fetchCourseBundleById - ownerId:' + ownerId + ' bundleId:' + bundleId);
    });
}

exports.payments_fetchCourseBundle = (req, res, next) => {
    const ownerId = req.user.ownerId;
    const userId = req.user._id;
    logger.debug('Enter payments_fetchCourseBundle - ownerId:' + ownerId);
    payments_fetchCourseBundle(ownerId, userId, (err, code, courseBundleArr) => {
        if(err){
            logger.error('Exit payments_fetchCourseBundle - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), courseBundleArr:courseBundleArr});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit payments_fetchCourseBundle - ownerId:' + ownerId);
    });
};

exports.payments_updateCourseBundle = (req, res, next) => {
    const ownerId = req.user.ownerId;
    const userId = req.user._id;
    const bundleId = req.body.bundleId;
    const paymentPackageId = req.body.paymentPackageId;
    const newfees = req.body.newfees;
    const newdiscount = req.body.newdiscount;
    
    logger.debug('Enter payments_updateCourseBundle - ownerId:' + ownerId);
    payments_updateCourseBundle(ownerId, userId, bundleId, paymentPackageId, newfees, newdiscount, (err, code) => {
        if(err){
            logger.error('Exit payments_updateCourseBundle - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit payments_updateCourseBundle - ownerId:' + ownerId);
    });
};

exports.payments_deleteCourseBundle = (req, res, next) => {
    const ownerId = req.user.ownerId;
    const userId = req.user._id;
    const bundleId = req.query.bundleId
    logger.debug('Enter payments_deleteCourseBundle - ownerId:' + ownerId + ' bundleId:' + bundleId);
    payments_deleteCourseBundle(ownerId, userId, bundleId, (err, code) => {
        if(err){
            logger.error('Exit payments_deleteCourseBundle - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit payments_deleteCourseBundle - ownerId:' + ownerId + ' bundleId:' + bundleId);
    });
}

exports.payments_fetchStudentAcc = (req, res, next) => {
    const ownerId = req.user.ownerId;
    const userId = req.user._id;
    const studentList = req.body.studentList
    logger.debug('Enter payments_fetchStudentAcc - ownerId:' + ownerId);
    payments_fetchStudentAcc(ownerId, userId, studentList, (err, code, studentAcc) => {
        if(err){
            logger.error('Exit payments_fetchStudentAcc - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), studentAcc:studentAcc});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit payments_fetchStudentAcc - ownerId:' + ownerId);
    });
}

exports.payments_fetchOrgAcc = (req, res, next) => {
    const ownerId = req.user.ownerId;
    const userId = req.user._id;
    const entityId = req.user.entityId;
    logger.debug('Enter payments_fetchOrgAcc - ownerId:' + ownerId + ' entityId:' + entityId);
    payments_fetchOrgAcc(ownerId, userId, entityId, (err, code, paymentAcc) => {
        if(err){
            logger.error('Exit payments_fetchOrgAcc - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), paymentAcc:paymentAcc});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit payments_fetchOrgAcc - ownerId:' + ownerId + ' entityId:' + entityId);
    });
}

exports.payments_generatePayOrder = async(req, res, next) => {
    const ownerId = req.user.ownerId;
    const userId = req.user._id;
    const entityId = req.user.entityId;
    const amount = req.body.amount;
    const purchasedPackageId = req.body.purchasedPackageId;
    const batchId = req.body.batchId;
    const paymentcycle = req.body.paymentcycle;

    logger.debug('Enter payments_generatePayOrder - ownerId:' + ownerId + ' entityId:' + entityId);

	const currency = 'INR'

	const payOrder = {
		amount: amount,
		currency,
		orderId: `${req.user.entityId}${shortid.generate()}`,
        accountType: "Student",
        created_at: new Date(),
        packageUpdate: false,
	}
    try {
        payments_generatePayOrder(ownerId, userId, entityId, purchasedPackageId, batchId, paymentcycle, payOrder, (err, code, order) => {
            if(err){
                logger.error('Exit payments_generatePayOrder - Error - ' + err);
                return next(err);
            }
            if(code === ResponseCode.Success){
                res.status(200).json({code : code, message: getResponseMsg(code), payOrder:order});
            }
            else{
                res.status(422).json({code : code, message: getResponseMsg(code)});
            }
            logger.debug('Exit payments_generatePayOrder - ownerId:' + ownerId + ' entityId:' + entityId);
        });
	} catch (err) {
	    logger.error("payments_generatePayOrder - Error - " + err);
        if(err) {
            res.status(422).json({
            code: 1001,
            message: "Failed to generate pay order from payment partner",
            error: err,
            });
        }
	}
}


exports.payments_generateOrgPayOrder = async(req, res, next) => {
    const ownerId = req.user.ownerId;
    const userId = req.user._id;
    const entityId = req.user.entityId;
    const accountType = req.body.accountType;
    const amount = req.body.amount;
    const purchasedEdumacPackageId = req.body.purchasedEdumacPackageId;
    const paymentcycle = req.body.paymentcycle;

    logger.debug('Enter payments_generateOrgPayOrder - ownerId:' + ownerId + ' entityId:' + entityId);

	const currency = 'INR'

	const payOrder = {
		amount: amount,
		currency,
		orderId: `${req.user.entityId}${shortid.generate()}`,
        accountType:accountType,
        created_at: new Date(),
        packageUpdate: false,
	}
    try {

        payments_generateOrgPayOrder(ownerId, userId, entityId, purchasedEdumacPackageId, paymentcycle, payOrder, (err, code, order) => {
            if(err){
                logger.error('Exit payments_generateOrgPayOrder - Error - ' + err);
                return next(err);
            }
            if(code === ResponseCode.Success){
                res.status(200).json({code : code, message: getResponseMsg(code), payOrder:order});
            }
            else{
                res.status(422).json({code : code, message: getResponseMsg(code)});
            }
            logger.debug('Exit payments_generateOrgPayOrder - ownerId:' + ownerId + ' entityId:' + entityId);
        });
	} catch (err) {
	    logger.error("payments_generateOrgPayOrder - Error - " + err);
        if(err) {
            res.status(422).json({
            code: 1001,
            message: "Failed to generate pay order from payment partner",
            error: err,
            });
        }
	}
}

exports.payments_generateEdumacPackagepayOrder = (req,res,next) => {
    const ownerId = req.user.ownerId;
    const userId = req.user._id;
    const entityId = req.user.entityId;
    const accountType = req.body.accountType;
    const amount = req.body.amount;
    const purchasedEdumacPackageId = req.body.purchasedEdumacPackageId;
    const paymentcycle = req.body.paymentcycle;
    const newPackagePlanId = req.body.newPackagePlanId
    console.log(req.body)

    logger.debug('Enter payments_generateEdumacPackagepayOrder - ownerId:' + ownerId + ' entityId:' + entityId);

	const currency = 'INR'

	const payOrder = {
		amount: amount,
		currency,
		orderId: `${req.user.entityId}${shortid.generate()}`,
        accountType:accountType,
        created_at: new Date(),
        packageUpdate: true,
        newPackagePlanId: newPackagePlanId,
	}
    try {

        payments_generateEdumacPackagepayOrder(ownerId, userId, entityId, purchasedEdumacPackageId, paymentcycle, payOrder, (err, code, order) => {
            if(err){
                logger.error('Exit payments_generateEdumacPackagepayOrder - Error - ' + err);
                return next(err);
            }
            if(code === ResponseCode.Success){
                res.status(200).json({code : code, message: getResponseMsg(code), payOrder:order});
            }
            else{
                res.status(422).json({code : code, message: getResponseMsg(code)});
            }
            logger.debug('Exit payments_generateEdumacPackagepayOrder - ownerId:' + ownerId + ' entityId:' + entityId);
        });
	} catch (err) {
	    logger.error("payments_generateEdumacPackagepayOrder - Error - " + err);
        if(err) {
            res.status(422).json({
            code: 1001,
            message: "Failed to generate pay order from payment partner",
            error: err,
            });
        }
	}
}

// paymnetOrderAuthenticationModel for both admin tutor and organization
exports.payments_authenticatePaymentSuccess = (req, res, next) => {
    console.log(req.body)

    logger.debug('Enter payments_authenticatePaymentSuccess - req.body:' + req.body);
	const crypto = require('crypto')
    // console.log(Cashfree.Payouts.VerifySignature(req.body))
      const secretKey = process.env.CASHFREE_SECRET;
      const sortedkeys = Object.keys(req.body);
      var signatureData = "";
  
      for (var i = 0; i < sortedkeys.length; i++) {
        var k = sortedkeys[i];
        if(k === "signature") continue
        signatureData +=req.body[k];
      }
      var signature = crypto.createHmac('sha256',secretKey).update(signatureData).digest('base64');
      console.log(signatureData, signature)

	if (signature === req.body.signature) {
		logger.debug('payments_authenticatePaymentSuccess - Authentication: Success');
		// to DBC
        payments_authenticatePaymentSuccess(req.body)
        // to route to different linked-accounts if req.body.payload.payment.entity.status = captured

        if(req.body.txStatus === "SUCCESS") {
            payments_handleRouting(req.body, (err, linkedAccount, amountToTutor, amountToEdumac) => {
                if(err) {
                    // handle error send notification to back office
                }
                // if(amountToTutor === 0 && amountToEdumac > 0) {
                //     console.log(linkedAccount, amountToTutor, amountToEdumac)
                //     Payouts.Transfers.RequestTransfer(
                //         {
                //         "beneId": "edumaticaLinkedAcc",
                //         "transferId": shortid.generate(),
                //         "amount": amountToEdumac.toString(),
                //         }   
                //     );
                // }
                // if(linkedAccount && amountToTutor !== 0) {
                //     // https://github.com/cashfree/cashfree-payout-node/blob/master/app.js
                //     console.log(linkedAccount, amountToTutor, amountToEdumac)
                //     // should create webhook to catch success of transfer
                //     const Cashfree = require("cashfree-sdk");
 
                //     //Initialize Cashfree Payout
                //     let Payouts = Cashfree.Payouts;
                //     Payouts.Init({
                //         "ENV": "TEST", 
                //         "ClientID": process.env.PAYOUT_ID,
                //         "ClientSecret": process.env.PAYOUT_SECRET
                //     });

                //     Payouts.Transfers.RequestTransfer(
                //         {
                //         "beneId": linkedAccount,
                //         "transferId": shortid.generate(),
                //         "amount": amountToTutor.toString(),
                //         }   
                //     );

                //     Payouts.Transfers.RequestTransfer(
                //         {
                //         "beneId": "edumaticaLinkedAcc",
                //         "transferId": shortid.generate(),
                //         "amount": amountToEdumac.toString(),
                //         }   
                //     );

                // }
            })
        }
	} else {
        console.log('Unsafe Payment!!! Check for fraud')
		// to check fraud
	}
    res.status(200).end();
}


exports.validateReferralCode = (req,res,next) => {
    const ownerId = req.user.ownerId;
    const entityId = req.user.entityId;
    const accountType = req.query.accountType
    const referralCode = req.query.referralCode;
    // console.log(accountType, referralCode)
    logger.debug('Enter validateReferralCode - ownerId:' + ownerId + ' entityId:' + entityId);
    validateReferralCode(ownerId, entityId, accountType, referralCode, (err, code) => {
        if(err){
            logger.error('Exit validateReferralCode - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit validateReferralCode - ownerId:' + ownerId + ' entityId:' + entityId);
    });
}


exports.getMasterPackages = (req, res, next) => {
    const ownerId = req.user.ownerId;
    const entityId = req.user.entityId;
    logger.debug('Enter getMasterPackages - ownerId:' + ownerId + ' entityId:' + entityId);
    getMasterPackages(ownerId, (err, code, masterPackages) => {
        if(err){
            logger.error('Exit getMasterPackages - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), masterPackages:masterPackages});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit getMasterPackages - ownerId:' + ownerId + ' entityId:' + entityId);
    });
}

exports.payments_validateOfflinePayment = (req, res, next) => {
    const ownerId = req.user.ownerId;
    const entityId = req.user.entityId;
    const accountType = req.user.role === "ROLE_TUTOR" ? "Tutor" : "Organization"
    const amount = req.body.amount;
    const purchasedCoursePackageId = req.body.purchasedCoursePackageId;
    const paymentcycle = req.body.paymentcycle;
    logger.debug('Enter payments_validateOfflinePayment - ownerId:' + ownerId + ' entityId:' + entityId);
    payments_validateOfflinePayment(ownerId, entityId, accountType, amount, purchasedCoursePackageId, paymentcycle, (err, code) => {
        if(err){
            logger.error('Exit payments_validateOfflinePayment - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit payments_validateOfflinePayment - ownerId:' + ownerId + ' entityId:' + entityId);
    });
}


exports.payments_updatePayOrders = (req, res, next) => {
    const orderArr = req.body.orderArr;
    logger.debug('Enter payments_updatePayOrders');
    payments_updatePayOrders(orderArr, (err, code) => {
        if(err){
            logger.error('Exit payments_updatePayOrders - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit payments_updatePayOrders');
    });
}


exports.payments_paymentStatus = (req, res, next) => {
    const ownerId = req.user.ownerId;
    const entityId = req.user.entityId;
    const accountType = req.user.role === "ROLE_TUTOR" ? "Tutor" : req.user.role === "ROLE_STUDENT" ? "Student" : "Organization"
    logger.debug('Enter payments_paymentStatus - ownerId:' + ownerId + ' entityId:' + entityId);
    payments_paymentStatus(ownerId, entityId, accountType, (err, code) => {
        if(err){
            logger.error('Exit payments_paymentStatus - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit payments_paymentStatus - ownerId:' + ownerId + ' entityId:' + entityId);
    });
}

exports.payments_checkStatus = (req, res, next) => {
    const ownerId = req.user.ownerId;
    const entityId = req.user.entityId;
    const orderId = req.query.orderId;
    logger.debug('Enter payments_checkStatus - ownerId:' + ownerId + ' entityId:' + entityId);
    payments_checkStatus(ownerId, entityId, orderId, (err, code, status) => {
        if(err){
            logger.error('Exit payments_checkStatus - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), orderStatus:status});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit payments_checkStatus - ownerId:' + ownerId + ' entityId:' + entityId);
    });
}