const logger = require('../utils/logger.utils');
const {batchChapter_save, student_chapters} = require('../dbc/batchChapter.dbc');
const { ResponseCode, getResponseMsg } = require('../utils/response.utils');

exports.batchChapter_save = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const userId = req.user._id;
    const batchChapter = req.body;
    logger.debug('Enter batchChapter_save - ownerId:' + ownerId
    + ', batchfriendlyname:' + batchChapter.batchfriendlyname
    + ', boardname:' + batchChapter.boardname
    + ', classname:' + batchChapter.classname
    + ', subjectname:' + batchChapter.subjectname
    + ', chaptername:' + batchChapter.chaptername);
    batchChapter_save(ownerId, userId, batchChapter, (err, code)=>{
        if(err){
            logger.error('batchChapter_save - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit batchChapter_save - ownerId:' + ownerId
        + ', batchfriendlyname:' + batchChapter.batchfriendlyname
        + ', boardname:' + batchChapter.boardname
        + ', classname:' + batchChapter.classname
        + ', subjectname:' + batchChapter.subjectname
        + ', chaptername:' + batchChapter.chaptername);
    });
};

exports.student_chapters = function (req, res, next) {
    const entityId = req.user.entityId;
    logger.debug('Enter student_chapters - entityId:' + entityId);
    student_chapters(entityId, (err, code, batchChapters)=>{
        if(err){
            logger.error('student_chapters - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), batchChapters: batchChapters});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit student_chapters - entityId:' + entityId);
    });
};