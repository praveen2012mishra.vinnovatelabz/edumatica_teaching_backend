const logger = require('../utils/logger.utils');
const {content_create, content_delete
    } = require('../dbc/contentOrg.dbc');
const { getSignedURLForUpload, getSignedURLForDownload, sizeOf } = require('../utils/aws.utils');
const { organization_details } = require('../dbc/organization.dbc');
const {student_chapters} = require('../dbc/batchChapter.dbc');
const { ResponseCode, getResponseMsg } = require('../utils/response.utils');

exports.content_create = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const userId = req.user._id;
    const contentReq = req.body;
    logger.debug('Enter content_create - ownerId:' + ownerId
    + ', boardname:' + contentReq.boardname + ', classname:' + contentReq.classname
    + ', subjectname:' + contentReq.subjectname + ', chaptername:' + contentReq.chaptername
    + ', contentname:' + contentReq.contentname);
    if(contentReq.contenttype == 'pdf'
    || contentReq.contenttype == 'video'
    || contentReq.contenttype == 'image'
    || contentReq.contenttype == 'document'){
        var folder = contentReq.boardname + '-' 
                                + contentReq.classname + '-' 
                                + contentReq.subjectname + '-'
                                + contentReq.chaptername;
        var fileName = ownerId + '/' + folder + '/' + contentReq.uuid;
        sizeOf(fileName, (err, code, size)=>{
            if(err){
                logger.error('content_create - Error - ' + err);
                return next(err);
            }
            contentReq.contentlength = size;
            content_create(ownerId, userId, contentReq, (err, code)=>{
                if(err){
                    logger.error('content_create - Error - ' + err);
                    return next(err);
                }                
                if(code === ResponseCode.Success){
                    res.status(200).json({code : code, message: getResponseMsg(code)});
                }
                else{
                    res.status(422).json({code : code, message: getResponseMsg(code)});
                }
                logger.debug('Exit content_create - ownerId:' + ownerId
                + ', boardname:' + contentReq.boardname + ', classname:' + contentReq.classname
                + ', subjectname:' + contentReq.subjectname + ', chaptername:' + contentReq.chaptername
                + ', contentname:' + contentReq.contentname);
            });
        })
    }
    else{
        content_create(ownerId, userId, contentReq, (err, code)=>{
            if(err){
                logger.error('content_create - Error - ' + err);
                return next(err);
            }
            if(code === ResponseCode.Success){
                res.status(200).json({code : code, message: getResponseMsg(code)});
            }
            else{
                res.status(422).json({code : code, message: getResponseMsg(code)});
            }
            logger.debug('Exit content_create - ownerId:' + ownerId
            + ', boardname:' + contentReq.boardname + ', classname:' + contentReq.classname
            + ', subjectname:' + contentReq.subjectname + ', chaptername:' + contentReq.chaptername
            + ', contentname:' + contentReq.contentname);
        });
    }
}

exports.content_delete = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const contentname = req.body.contentname;
    logger.debug('Enter content_delete - ownerId:' + ownerId
    + ', contentname:' + contentname);

    content_delete(ownerId, contentname, (err, code)=>{
        if(err){
            logger.error('content_delete - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit content_delete - ownerId:' + ownerId
        + ', contentname:' + contentname);
    });
};

exports.content_getSignedURLForUpload = (req,res,next)=>{
    const ownerId = req.user.ownerId;
    const content = {
        boardname: req.query.boardname,
        classname: req.query.classname,
        subjectname: req.query.subjectname,
        chaptername: req.query.chaptername,
        contenttype: req.query.contenttype,
        contentlength: req.query.contentlength
    };
    logger.debug('Enter content_getSignedURLForUpload - ownerId:' + ownerId
     + ', boardname:' + content.boardname + ', classname:' + content.classname
     + ', subjectname:' + content.subjectname + ', chaptername:' + content.chaptername);
    if(content.contenttype != 'image/jpg'
    && content.contenttype != 'image/jpeg'
    && content.contenttype != 'image/png'
    && content.contenttype != 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
    && content.contenttype != 'application/pdf'
    && content.contenttype != 'video/mp4'){ 
        logger.warn('content_getSignedURLForUpload - ownerId:' + ownerId
                        + ', contentname:' + content.contentname
                        + ', Content type not allowed');
        res.status(422).json({ code: ResponseCode.ContentTypeNotAllowed,
        message: getResponseMsg(ResponseCode.ContentTypeNotAllowed)});
    }
    else if(((content.contenttype === 'image/jpg' || content.contenttype != 'image/png' || content.contenttype != 'image/jpeg') && content.contentlength > 104857600) // 100 MB image
    || (content.contenttype === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' && content.contentlength > 104857600) // 100 MB doc
    || (content.contenttype === 'application/pdf' && content.contentlength > 104857600) // 100 MB Slide
    || (content.contenttype === 'video/mp4' && content.contentlength > 104857600)){ // 100 MB video
        logger.warn('content_getSignedURLForUpload - ownerId:' + ownerId
                        + ', contentname:' + content.contentname
                        + ', Content limit not allowed');
        res.status(422).json({ code: ResponseCode.ContentSizeNotAllowed,
            message: getResponseMsg(ResponseCode.ContentSizeNotAllowed)});
    }
    else {
        organization_details(ownerId, (err, code, organization)=>{
            if(err){
                logger.error('content_getSignedURLForUpload - Error - ' + err);
                return next(err);
            }
            if(organization.contentSize < organization.package.contentSize){
                getSignedURLForUpload(ownerId, content, content.contenttype, (err, code, url, uuid)=>{
                    if(err){
                        logger.error('content_getSignedURLForUpload - Error - ' + err);
                        return next(err);
                    }
                    if(code === ResponseCode.Success){
                        res.status(200).json({code : code, message: getResponseMsg(code), url: url, uuid: uuid});
                    }
                    else{
                        res.status(422).json({code : code, message: getResponseMsg(code)});
                    }
                });
            }
            else{
                logger.warn('content_getSignedURLForUpload - ownerId:' + ownerId
                        + ', contentname:' + content.contentname
                        + ', Organization content limit exceeded');
                res.status(422).json( {code: ResponseCode.ContentLimitExceeded, 
                message: getResponseMsg(ResponseCode.ContentLimitExceeded)});
            }
        });
        
    }
    logger.debug('Exit content_getSignedURLForUpload - ownerId:' + ownerId
            + ', boardname:' + content.boardname + ', classname:' + content.classname
            + ', subjectname:' + content.subjectname + ', chaptername:' + content.chaptername);
}

exports.content_getSignedURLForDownload = (req,res,next)=>{
    const ownerId = req.user.ownerId;
    const content = {
        boardname: req.query.boardname,
        classname: req.query.classname,
        subjectname: req.query.subjectname,
        chaptername: req.query.chaptername,
        uuid: req.query.uuid,
    };
    logger.debug('Enter content_getSignedURLForDownload - ownerId:' + ownerId
    + ', boardname:' + content.boardname + ', classname:' + content.classname
    + ', subjectname:' + content.subjectname + ', chaptername:' + content.chaptername
    + ', uuid:' + content.uuid);
    getSignedURLForDownload(ownerId, content, (err, code, url)=>{
        if(err){
            logger.error('content_getSignedURLForDownload - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), url: url});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit content_getSignedURLForDownload - ownerId:' + ownerId
        + ', boardname:' + content.boardname + ', classname:' + content.classname
        + ', subjectname:' + content.subjectname + ', chaptername:' + content.chaptername
        + ', uuid:' + content.uuid);
    });
}

exports.content_getStudentSignedURLForDownload = (req,res,next)=>{
    const ownerId = req.user.ownerId;
    const tutorId = req.query.tutorId;
    const content = {
        boardname: req.query.boardname,
        classname: req.query.classname,
        subjectname: req.query.subjectname,
        chaptername: req.query.chaptername,
        uuid: req.query.uuid,
    };
    logger.debug('Enter content_getStudentSignedURLForDownload - ownerId:' + ownerId
    + ', boardname:' + content.boardname + ', classname:' + content.classname
    + ', subjectname:' + content.subjectname + ', chaptername:' + content.chaptername
    + ', uuid:' + content.uuid);

    getSignedURLForDownload(tutorId, content, (err, code, url)=>{
        if(err){
            logger.error('content_getStudentSignedURLForDownload - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), url: url});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit content_getStudentSignedURLForDownload - ownerId:' + ownerId
        + ', boardname:' + content.boardname + ', classname:' + content.classname
        + ', subjectname:' + content.subjectname + ', chaptername:' + content.chaptername
        + ', uuid:' + content.uuid);
    });
}
