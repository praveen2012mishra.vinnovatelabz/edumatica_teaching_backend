const {batch_create, batch_batches, batch_details,
    batch_update, batch_delete ,batches_search_dbc, batch_bbbbatches, syllabusUpdate, batch_details_id} = require('../dbc/batch.dbc');
const logger = require('../utils/logger.utils');
const { ResponseCode, getResponseMsg } = require('../utils/response.utils');
const { getUploadUrlForSyllabus, getDownloadUrlForSyllabus } = require('../utils/aws.utils');

exports.batch_create = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const entityId = req.user.entityId;
    const userId = req.user._id;
    const batch = req.body;
    
    logger.debug('Enter batch_create - ownerId:' + ownerId + ', batchfriendlyname:' + batch.batchfriendlyname);
    if(batch.students == null || batch.students.length == 0){
        logger.warn('ownerId:' + ownerId + ', Student(s) cannot be empty');
        res.status(422).json({code : ResponseCode.InvalidInput, message: getResponseMsg(ResponseCode.InvalidInput)});
        logger.debug('Exit batch_create - ownerId:' + ownerId + ', batchfriendlyname:' + batch.batchfriendlyname);
    }
    else{
        batch_create(ownerId, entityId, userId, batch, (err, code)=>{
            if(err){
                logger.error('batch_create - Error - ' + err);
                return next(err);
            }
            if(code === ResponseCode.Success){
                res.status(200).json({code : code, message: getResponseMsg(code)});
            }
            else{
                res.status(422).json({code : code, message: getResponseMsg(code)});
            }
            logger.debug('Exit batch_create - ownerId:' + ownerId + ', batchfriendlyname:' + batch.batchfriendlyname);      
        });
    }
};

exports.batch_batches = function (req, res, next) {
    const ownerId = req.user.ownerId;
    logger.debug('Enter batch_batches - ownerId:' + ownerId);
    batch_batches(ownerId, (err, code, batches)=>{
        if(err){
            logger.error('batch_batches - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), batchList: batches});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit batch_batches - ownerId:' + ownerId);
    });
};

exports.batch_bbbbatches = function (req, res, next) {
    const ownerId = req.user.ownerId;
    logger.debug('Enter batch_bbbbatches - ownerId:' + ownerId);
    batch_bbbbatches(ownerId, (err, code, batches)=>{
        if(err){
            logger.error('batch_bbbbatches - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), batchList: batches});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit batch_bbbbatches - ownerId:' + ownerId);
    });
};

exports.batch_details = function (req, res, next) {
    const entityId = req.user.entityId;
    const batchfriendlyname = req.query.batchfriendlyname;

    logger.debug('Enter batch_details - entityId:' + entityId + ', batchfriendlyname:' + batchfriendlyname);
    batch_details(entityId, batchfriendlyname, (err, code, batch)=>{
        if(err){
            logger.error('batch_details - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), batch: batch});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit batch_details - entityId:' + entityId + ', batchfriendlyname:' + batchfriendlyname);
    });
};

exports.batch_details_id = function (req, res, next) {
    const entityId = req.user.entityId;
    const id = req.query.id;

    logger.debug('Enter batch_details_id - entityId:' + entityId + ', id:' + id);
    batch_details_id(entityId, id, (err, code, batch)=>{
        if(err){
            logger.error('batch_details_id - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), batch: batch});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit batch_details_id - entityId:' + entityId + ', id:' + id);
    });
};

exports.batch_update = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const entityId = req.user.entityId;
    const userId = req.user._id;
    const batch = req.body;
    logger.debug('Enter batch_update - ownerId:' + ownerId + ', batchfriendlyname:' + batch.batchfriendlyname);
    if(batch.students == null || batch.students.length == 0){
        logger.warn('ownerId:' + ownerId + ', Student(s) cannot be empty');
        res.status(422).json({code : ResponseCode.InvalidInput, message: getResponseMsg(ResponseCode.InvalidInput)});
        logger.debug('Exit batch_update - ownerId:' + ownerId + ', batchfriendlyname:' + batch.batchfriendlyname);
    }
    else{
        batch_update(ownerId, entityId, userId, batch, (err, code)=>{
            if(err){
                logger.error('batch_update - Error - ' + err);
                return next(err);
            }
            if(code === ResponseCode.Success){
                res.status(200).json({code : code, message: getResponseMsg(code)});
            }
            else{
                res.status(422).json({code : code, message: getResponseMsg(code)});
            }
            logger.debug('Exit batch_update - ownerId:' + ownerId + ', batchfriendlyname:' + batch.batchfriendlyname);
        });
    }
};

exports.batch_delete = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const batchfriendlyname = req.body.batchfriendlyname;
    logger.debug('Enter batch_delete - ownerId:' + ownerId + ', batchfriendlyname:' + batchfriendlyname);
    batch_delete(ownerId, batchfriendlyname, (err, code)=>{
        if(err){
            logger.error('batch_delete - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit batch_delete - ownerId:' + ownerId + ', batchfriendlyname:' + batchfriendlyname);
    });
};

exports.batches_search = (req,res,next) =>{
    const ownerId = req.user.ownerId
    const userId = req.user._id
    const boardname=req.query.boardname
    const classname=req.query.classname
    const subjectname=req.query.subjectname

    logger.debug('Enter batches_search - ownerId:' + ownerId + ', boardname:' + boardname+', classname:'+classname+', subjectname:'+subjectname);

    batches_search_dbc(ownerId,userId,boardname,classname,subjectname,(err,code,batchList)=>{
        if(err){
            logger.error('batches_search - Error '+err)
        }
        if(code===ResponseCode.Success){
            res.status(200).json({code:200, message:getResponseMsg(code), batchList:batchList})
        }
        else{
            res.status(422).json({code:code, message: getResponseMsg(code)})
        }

        logger.debug('Exit batches_search - ownerId:' + ownerId + ', boardname:' + boardname+', classname:'+classname+', subjectname:'+subjectname);

    })
    
}

exports.getUploadUrlForSyllabus = (req,res,next)=>{
    const ownerId = req.user.ownerId;
    const { batch, contentType } = req.query;
    logger.debug('Enter getUploadUrlForSyllabus - ownerId:' + ownerId);

    getUploadUrlForSyllabus(ownerId, batch, contentType, (err, code, url, uuid) => {
        if(err) {
            logger.error('getUploadUrlForSyllabus - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success) {
            res.status(200).json({code : code, message: getResponseMsg(code), url: url, uuid: uuid});
        }
        else {
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit getUploadUrlForSyllabus - ownerId:' + ownerId);
    });
}

exports.getDownloadUrlForSyllabus = (req, res, next)=>{
    const ownerId = req.user.ownerId;
    const uuid = req.query.uuid;
    const { batch } = req.query;
    logger.debug('Enter getDownloadUrlForSyllabus - ownerId:' + ownerId);
    getDownloadUrlForSyllabus(ownerId, batch, uuid, (err, code, url)=>{
        if(err){
            logger.error('getDownloadUrlForSyllabus - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), url: url});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit getDownloadUrlForSyllabus - ownerId:' + ownerId);
    });
}

exports.syllabusUpdate = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const { batch, uuid } = req.body;
    logger.debug('Enter batch_update - ownerId:' + ownerId + ', batchId:' + batch);

    syllabusUpdate(batch, uuid, (err, code)=>{
        if(err){
            logger.error('batch_update - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit batch_update - ownerId:' + ownerId + ', batchId:' + batch);
    });

};
