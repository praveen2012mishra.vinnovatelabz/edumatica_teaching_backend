const logger = require('../utils/logger.utils');
const { ResponseCode, getResponseMsg } = require('../utils/response.utils');
const { topics_get_dbc, get_questions_dbc } = require('../dbc/assessmentquestion.dbc');
const { response } = require('express');


exports.topics_get = (req,res,next) =>{
    const ownerId = req.user.ownerId
    const userId = req.user._id
    const boardname = req.query.boardname
    const classname = req.query.classname
    const subjectname = req.query.subjectname
    logger.debug('Enter topics_get - ownerId:' + ownerId);
    topics_get_dbc(ownerId, userId, boardname,classname,subjectname, (err, code, topics)=>{
        if(err){
            logger.error('topics_get - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code) ,topics:topics});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit topics_get - ownerId:' + ownerId);
    });
}


exports.questions_get = (req,res, next) =>{
    const ownerId = req.user.ownerId
    const questionIds = req.body.questionIds
    logger.debug('Enter questions_get - ownerId:' + ownerId);
    get_questions_dbc(questionIds,(err,code,questions)=>{
        if(err){
            logger.debug('Error questions_get - ownerId:' + ownerId+ ' Error:'+err);
            return next(err)
        }
        else{
            if(code=== ResponseCode.Success){
                res.status(200).json({code : code, message: getResponseMsg(code) ,questions:questions});
            }
            else{
                res.status(422).json({code: code, message: getResponseMsg(code)})
            }
        }
        
    })
    logger.debug('Exit question_get - ownerId: '+ownerId)

}
