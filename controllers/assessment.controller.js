const logger = require('../utils/logger.utils');
const {assessment_createAssessment, assessment_patchAssessment , assessment_deleteAssessment, assessment_deleteAssessments,
    assessment_getAssessments, assessment_details,assessment_assignassessment_dbc,assessment_copyassessment_dbc,
    assessment_fetchAssignAssessment, assessment_deleteAssignAssessment
} = require('../dbc/assessment.dbc');
const { ResponseCode, getResponseMsg } = require('../utils/response.utils');


exports.assessment_createAssessment = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const userId = req.user._id;
    const assessmentReq  = req.body;
    logger.debug('Enter assessment_createAssessment - ownerId:' + ownerId
    + ', assessmentname:' + assessmentReq.assessmentname);
    assessment_createAssessment(ownerId, userId, assessmentReq, (err, code , assessmentData)=>{
        if(err){
            logger.error('assessment_createAssessment - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code) , assessment:assessmentData});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        
        logger.debug('Exit assessment_createAssessment - ownerId:' + ownerId
        + ', assessmentname:' + assessmentReq.assessmentname);
    });
};

exports.assessment_patchAssessment = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const userId = req.user._id;
    const assessmentReq  = req.body;
    const assessmentId = req.query.assessmentId
    const updateBoolean = req.query.update
    console.log(assessmentId)
    logger.debug('Enter assessment_patchAssessment - ownerId:' + ownerId
    + ', assessmentId:' + assessmentId);
    assessment_patchAssessment(ownerId, userId, assessmentId, assessmentReq,updateBoolean, (err, code)=>{
        if(err){
            logger.error('assessment_patchAssessment - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit assessment_patchAssessment - ownerId:' + ownerId
        + ', assessmentId:' + assessmentId);
    });
};

exports.assessment_deleteAssessment = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const userId = req.user._id;
    const assessmentId = req.query.assessmentId
    console.log(assessmentId)
    logger.debug('Enter assessment_deleteAssessment - ownerId:' + ownerId
    + ', assessmentId:' + assessmentId);
    assessment_deleteAssessment(ownerId, userId, assessmentId, (err, code)=>{
        if(err){
            logger.error('assessment_deleteAssessment - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit assessment_deleteAssessment - ownerId:' + ownerId
        + ', assessmentId:' + assessmentId);
    });
};

exports.assessment_deleteAssessments = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const userId = req.user._id;
    const assessments = req.body.assessmentList;
    logger.debug('Enter assessment_deleteAssessments - ownerId:' + ownerId);
    assessment_deleteAssessments(ownerId, userId, assessments, (err, code)=>{
        if(err){
            logger.error('assessment_deleteAssessments - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit assessment_deleteAssessments - ownerId:' + ownerId);
    });
};


exports.assessment_getAssessments = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const userId = req.user._id
    const privateBool = req.query.private
    logger.debug('Enter assessment_getAssessments - ownerId:' + ownerId);
    assessment_getAssessments(ownerId, userId,privateBool ,(err, code, assessments)=>{
        if(err){
            logger.error('assessment_getAssessments - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, 
                message: getResponseMsg(code), assessmentList: assessments});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit assessment_getAssessments - ownerId:' + ownerId);
    });
};

exports.assessment_details = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const userId = req.user._id
    const assessmentId = req.query.assessmentId;
    logger.debug('Enter assessment_details - ownerId:' + ownerId
    + ', assessmentId:' + assessmentId);
    assessment_details(ownerId, userId, assessmentId, (err, code, assessment)=>{
        if(err){
            logger.error('assessment_details - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, 
                message: getResponseMsg(code), assessment: assessment});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit assessment_details - ownerId:' + ownerId
        + ', assessmentId:' + assessmentId);
    });
};


exports.assessment_assignAssessment = (req,res,next) =>{
    const ownerId= req.user.ownerId
    const userId = req.user._id
    const assessmentData = req.body.assessmentData
    const studentArray = req.body.studentArray
    logger.debug('Enter assessment_assignAssessment - ownerId:' + ownerId
    + ', assessmentId:' + assessmentData.assessment);

    assessment_assignassessment_dbc(ownerId,userId,assessmentData,studentArray,(err,code)=>{
        if(err){
            logger.error('assessment_assignAssessment - Error:'+err)
        }
        if(code === ResponseCode.Success){
            res.status(200).json({ code: code ,message:getResponseMsg(code)})
        }
        else{
            res.status(422).json({code : code , message: getResponseMsg(code)})
        }
        logger.debug('Exit assessment_assignAssessment - ownerId:' + ownerId
        + ', assessmentId:' + assessmentData.assessment);
    })
}

exports.assessment_fetchAssignAssessment = (req, res, next) => {
    const ownerId = req.user.ownerId;
    const query = req.query;

    logger.debug('Enter assessment_fetchAssignAssessment - ownerId:' + ownerId);
    assessment_fetchAssignAssessment(ownerId, query, (err, code, assessments)=>{
        if(err){
            logger.error('assessment_fetchAssignAssessment - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), assessments: assessments});
        } else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit assessment_fetchAssignAssessment - ownerId:' + ownerId);  
    });
}

exports.assessment_deleteAssignAssessment = (req, res, next) => {
    const ownerId = req.user.ownerId;
    const query = req.query;

    logger.debug('Enter assessment_deleteAssignAssessment - ownerId:' + ownerId);
    assessment_deleteAssignAssessment(ownerId, query, (err, code)=>{
        if(err){
            logger.error('assessment_deleteAssignAssessment - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        } else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit assessment_deleteAssignAssessment - ownerId:' + ownerId);  
    });
}


exports.assessment_copyassessment = (req,res,next) =>{
    const ownerId = req.user.ownerId
    const assessmentData= req.body.assessmentData

    logger.debug('Enter assessment_copyassessment ownerId:'+ownerId)

    assessment_copyassessment_dbc(ownerId,assessmentData,(err,code,assessmentDoc)=>{
        if(err){
            logger.error('assessment_copyassessment - Error:'+err)
        }
        else{
            if(code===ResponseCode.Success){
                res.status(200).json({code:code,message:getResponseMsg(code),assessmentDoc:assessmentDoc})
            }
            else{
                res.status(422).json({code:code,message:getResponseMsg(code)})
            }
        }
    })
    logger.debug('Exit assessment_copyassessment ownerId:'+ownerId)

}