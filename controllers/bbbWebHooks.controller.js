const logger = require("../utils/logger.utils");
const { getBBBResponse } = require('../utils/bbbresponseparser.utils');
const { ResponseCode, getResponseMsg } = require("../utils/response.utils");

const sha1 = require("sha1");

//Create a meeting specific hook
exports.bbbWebHook_createHook = async (req, res) => {
  const params = req.body;
  const meetingID = params.meetingID;

  logger.debug("Enter bbbWebHook_createHook - meetingID:" + meetingID);

  const HOOK_STRING = `hooks/createcallbackURL=${process.env.BBB_HOOKS_CALLBACK_URL}&meetingID=${meetingID}&getRaw=false`;
  const HOOK_STRING_WTH_SECRET = `${HOOK_STRING}${process.env.BBB_CONFIG_SECRET}`;
  const SHA1_STRING = sha1(HOOK_STRING_WTH_SECRET);
  const CREATE_HOOK = `${process.env.BBB_URI_ENDPOINT}hooks/create?callbackURL=${process.env.BBB_HOOKS_CALLBACK_URL}&meetingID=${meetingID}&getRaw=false&checksum=${SHA1_STRING}`;
  
  getBBBResponse(CREATE_HOOK).then(result => {
    try {
      res.status(200).json({
        code: ResponseCode.Success,
        message: getResponseMsg(ResponseCode.Success),
        data: result,
      });
    } catch (err) {
      logger.error("bbbWebHook_createHook - Error - " + err);
  
      res.status(422).json({
        code: ResponseCode.MeetingDataCorrupted,
        message: getResponseMsg(ResponseCode.MeetingDataCorrupted),
        error: err,
      });
    }
  })
  
  logger.debug("Exit bbbWebHook_createHook - meetingID:" + meetingID);
};

//Destroy a hook by hookID
exports.bbbWebHook_destroyHook = async (req, res) => {
  const params = req.body;
  const hookID = params.hookID;

  logger.debug("Enter bbbWebHook_destroyHook - hookID:" + hookID);

  const HOOK_STRING = `hooks/destroyhookID=${hookID}`;
  const HOOK_STRING_WTH_SECRET = `${HOOK_STRING}${process.env.BBB_CONFIG_SECRET}`;
  const SHA1_STRING = sha1(HOOK_STRING_WTH_SECRET);
  const DESTROY_HOOK = `${process.env.BBB_URI_ENDPOINT}hooks/destroy?hookID=${hookID}&checksum=${SHA1_STRING}`;

  getBBBResponse(DESTROY_HOOK).then(result => {
    try {
      res.status(200).json({
        code: ResponseCode.Success,
        message: getResponseMsg(ResponseCode.Success),
        data: result,
      });
    } catch (err) {
      logger.error("bbbWebHook_destroyHook - Error - " + err);
  
      res.status(422).json({
        code: ResponseCode.MeetingDataCorrupted,
        message: getResponseMsg(ResponseCode.MeetingDataCorrupted),
        error: err,
      });
    }
  })
  
  logger.debug("Exit bbbWebHook_destroyHook - hookID:" + hookID);
};

//Get the list of hooks for a specific meeting
exports.bbbWebHook_gethookslist = async (req, res) => {
  const params = req.body;
  const meetingID = params.meetingID;

  logger.debug("Enter bbbWebHook_gethookslist - meetingID:" + meetingID);

  const HOOK_STRING = `hooks/listmeetingID=${meetingID}`;
  const HOOK_STRING_WTH_SECRET = `${HOOK_STRING}${process.env.BBB_CONFIG_SECRET}`;
  const SHA1_STRING = sha1(HOOK_STRING_WTH_SECRET);
  const HOOK_LIST = `${process.env.BBB_URI_ENDPOINT}hooks/list?meetingID=${meetingID}&checksum=${SHA1_STRING}`;

  getBBBResponse(HOOK_LIST).then(result => {
    try {
      res.status(200).json({
        code: ResponseCode.Success,
        message: getResponseMsg(ResponseCode.Success),
        data: result,
      });
    } catch (err) {
      logger.error("bbbWebHook_gethookslist - Error - " + err);
  
      res.status(422).json({
        code: ResponseCode.MeetingDataCorrupted,
        message: getResponseMsg(ResponseCode.MeetingDataCorrupted),
        error: err,
      });
    }
  })
  
  logger.debug("Exit bbbWebHook_gethookslist - meetingID:" + meetingID);
};
