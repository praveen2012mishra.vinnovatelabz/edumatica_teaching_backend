const { analytics_studentAssessment, analytics_studentAssessmentGetAnswer } = require('../dbc/analytics.dbc');
const logger = require('../utils/logger.utils');
const { ResponseCode, getResponseMsg } = require('../utils/response.utils');

exports.analytics_studentAssessment = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const studentId = req.query.studentId;
    logger.debug('Enter analytics_studentAssessment - ownerId:' + ownerId + ' studentId:' + studentId);
    analytics_studentAssessment(ownerId, studentId, (err, code , attemptAssessmentDocs) => {
        if(err){
            logger.error('analytics_studentAssessment - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code) , attemptAssessmentDocs:attemptAssessmentDocs});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit analytics_studentAssessment - ownerId:' + ownerId + ' studentId:' + studentId);
    });
};

exports.analytics_studentAssessmentGetAnswer = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const studentId = req.query.studentId;
    const attemptAssessmentId = req.query.attemptAssessmentId;
    
    logger.debug('Enter analytics_studentAssessmentGetAnswer - studentId:' + studentId + ' attemptAssessmentId' + attemptAssessmentId);
    analytics_studentAssessmentGetAnswer(ownerId, studentId, attemptAssessmentId, (err, code , attemptAssessmentAnswers) => {
        if(err){
            logger.error('analytics_studentAssessment - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code) , attemptAssessmentAnswers:attemptAssessmentAnswers});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit analytics_studentAssessmentGetAnswer - studentId:' + studentId + ' attemptAssessmentId' + attemptAssessmentId);
    });
}