const logger = require('../utils/logger.utils');
const   { saveSchedulesInBatch, getSchedules, getScheduleDetails, getStudentAttendance, schedule_update,
          deleteSchedule, getTutorRoom, getStudentRoom, getStudentSchedules, getParentSchedules
        } = require('../dbc/schedule.dbc');
const { ResponseCode, getResponseMsg } = require('../utils/response.utils');

function timeToDecimal(t) {
    var arr = t.split(':');
    var dec = parseInt((arr[1]/6)*10, 10);

    return parseFloat(parseInt(arr[0], 10) + '.' + (dec<10?'0':'') + dec);
}

exports.schedule_create = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const userId = req.user._id;
    const batch = req.body;
    logger.debug('Enter schedule_create - ownerId:' + ownerId + ', batchfriendlyname:' + batch.batchfriendlyname);
    let scheduleOverlaps = false;
    let schedulesArray = batch.schedules;
    let msg = '';
    for (var i = 0; i < schedulesArray.length; i++){
        let a = timeToDecimal(schedulesArray[i].fromhour);
        let b = timeToDecimal(schedulesArray[i].tohour);
        if(Number.isNaN(a) || Number.isNaN(b) || a < 0 || a>24 || b <0 || b>24 || a >= b){
            scheduleOverlaps = true;
            msg = 'Invalid Schedule- dayname:' + schedulesArray[i].dayname 
            + ' fromhour:' + schedulesArray[i].fromhour 
            + ' tohour:' + schedulesArray[i].tohour;
            break;
        }
    }
    if(schedulesArray.length > 1)
    {        
        for (var i = 0; i < schedulesArray.length - 1; i++){
            let a = timeToDecimal(schedulesArray[i].fromhour);
            let b = timeToDecimal(schedulesArray[i].tohour);
            for (var j = i + 1; j < schedulesArray.length; j++){
                let x = timeToDecimal(schedulesArray[j].fromhour);
                let y = timeToDecimal(schedulesArray[j].tohour);
                if (schedulesArray[i].dayname == schedulesArray[j].dayname) {
                    if (!((a  < x && b <= x) || (y <= a && y < b))){
                            scheduleOverlaps = true;
                            msg = 'Invalid Schedule- dayname:' + schedulesArray[i].dayname + ' fromhour:' + schedulesArray[i].fromhour 
                            + ' tohour:' + schedulesArray[i].tohour 
                            + ' overlaps with dayname:' + schedulesArray[j].dayname + ' fromhour:' + schedulesArray[j].fromhour 
                            + ' tohour:' + schedulesArray[j].tohour;
                            break;
                    }
                }
            }
        }
    }
    if(scheduleOverlaps == true){
        logger.warn('schedule_create - ownerId:' + ownerId
                                    + ' ' + msg);
        res.status(422).json({code: ResponseCode.SchedulesOverlap, message: msg});
    }
    else{
        saveSchedulesInBatch(ownerId, userId, batch, (err, code, msg)=>{
            if(err){
                logger.error('schedule_create - Error - ' + err);
                return next(err);
            }
            if(code === ResponseCode.Success){
                res.status(200).json({code : code, message: getResponseMsg(code)});
            }
            else if(code === ResponseCode.SchedulesOverlap){
                logger.warn('schedule_create - ownerId:' + ownerId
                                            + ' ' + msg);
                res.status(422).json({code: ResponseCode.SchedulesOverlap, message: msg});
            }
            else{
                res.status(422).json({code : code, message: getResponseMsg(code)});
            }
        });
    }
    logger.debug('Exit schedule_create - ownerId:' + ownerId + ', batchfriendlyname:' + batch.batchfriendlyname);
};

exports.schedule_schedules = function (req, res, next) {    
    const ownerId = req.user.ownerId;
    logger.debug('Enter schedule_schedules - ownerId:' + ownerId);
    getSchedules(ownerId, (err, code, schedules)=>{
        if(err){
            logger.error('schedule_schedules - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), scheduleList: schedules});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit schedule_schedules - ownerId:' + ownerId);
    });
};

exports.schedule_getStudentSchedules = function (req, res, next) {  
    const entityId = req.user.entityId;  
    logger.debug('Enter schedule_getStudentSchedules - entityId:' + entityId);
    getStudentSchedules(entityId, (err, code, schedules)=>{
        if(err){
            logger.error('schedule_getStudentSchedules - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), scheduleList: schedules});
        }
        else{
             res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit schedule_getStudentSchedules - entityId:' + entityId);
    });
};

exports.getStudentAttendance = function (req, res, next) {  
    const entityId = req.user.entityId;  
    logger.debug('Enter getStudentAttendance - entityId:' + entityId);
    getStudentAttendance(entityId, (err, code, attendances)=>{
        if(err){
            logger.error('getStudentAttendance - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), attendanceList: attendances});
        }
        else{
             res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit getStudentAttendance - entityId:' + entityId);
    });
};

exports.getParentSchedules = function (req, res, next) {  
    const entityId = req.user.entityId;  
    logger.debug('Enter getParentSchedules - entityId:' + entityId);
    const studentId = req.query.studentId
    getParentSchedules(entityId, studentId, (err, code, schedules)=>{
        if(err){
            logger.error('getParentSchedules - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), scheduleList: schedules});
        }
        else{
             res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit getParentSchedules - entityId:' + entityId);
    });
};

exports.schedule_getTutorRoom = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const entityId = req.user.entityId;
    const userId = req.user._id;
    const dayname = req.query.dayname;
    const fromhour = req.query.fromhour;
    logger.debug('Enter schedule_getTutorRoom - ownerId:' + ownerId 
    + ', dayname:' + dayname + ' , fromhour:' + fromhour);    
    getTutorRoom(ownerId, entityId, userId, dayname, fromhour, (err, code, roomid)=>{
        if(err){
            logger.error('schedule_getTutorRoom - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), roomid: roomid});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit schedule_getTutorRoom - ownerId:' + ownerId
        + ', dayname:' + dayname + ' , fromhour:' + fromhour);
    });
};


exports.schedule_getStudentRoom = function (req, res, next) {    
    const entityId = req.user.entityId;
    const userId = req.user._id;
    const dayname = req.query.dayname;
    const fromhour = req.query.fromhour;
    const tutorId = req.query.tutorId;
    logger.debug('Enter schedule_getStudentRoom - entityId:' + entityId
    + ', dayname:' + dayname + ' , fromhour:' + fromhour);    
    getStudentRoom(entityId, userId, dayname, fromhour, tutorId, (err, code, roomid)=>{
        if(err){
            logger.error('schedule_getStudentRoom - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), roomid: roomid});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit schedule_getStudentRoom - entityId:' + entityId
        + ', dayname:' + dayname + ' , fromhour:' + fromhour);
    });
};

exports.schedule_details = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const dayname = req.query.dayname;
    const fromhour = req.query.fromhour;
    logger.debug('Enter schedule_details - ownerId:' + ownerId
    + ', dayname:' + dayname + ' , fromhour:' + fromhour);
    getScheduleDetails(ownerId, dayname, fromhour, (err, code, schedule)=>{
        if(err){
            logger.error('schedule_details - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), schedule: schedule});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit schedule_details - ownerId:' + ownerId
        + ', dayname:' + dayname + ' , fromhour:' + fromhour);
    });
    
};

exports.schedule_update = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const userId = req.user.userId;
    const scheduleReq = req.body;
    logger.debug('Enter schedule_update - ownerId:' + ownerId);
    schedule_update(ownerId, userId, scheduleReq, (err, code)=>{
        if(err){
            logger.error('schedule_update - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit schedule_update - ownerId:' + ownerId);
    });
    
};

exports.schedule_delete = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const dayname = req.body.dayname;
    const fromhour = req.body.fromhour;
    logger.debug('Enter schedule_delete - ownerId:' + ownerId
    + ', dayname:' + dayname + ' , fromhour:' + fromhour);
    deleteSchedule(ownerId, dayname, fromhour, (err, code)=>{
        if(err){
            logger.error('schedule_delete - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit schedule_delete - ownerId:' + ownerId
        + ', dayname:' + dayname + ' , fromhour:' + fromhour);
    });
    
};