const {batch_create, batch_batches, batch_details,
    batch_update, batch_delete} = require('../dbc/batchOrg.dbc');
const logger = require('../utils/logger.utils');
const { ResponseCode, getResponseMsg } = require('../utils/response.utils');

exports.batch_create = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const entityId = req.user.entityId;
    const userId = req.user._id;
    const batch = req.body;
    logger.debug('Enter batch_create - ownerId:' + ownerId + ', batchfriendlyname:' + batch.batchfriendlyname);
    if(batch.students == null || batch.students.length == 0){
        logger.warn('ownerId:' + ownerId + ', Student(s) cannot be empty');
        res.status(422).json({code : ResponseCode.InvalidInput, message: getResponseMsg(ResponseCode.InvalidInput)});
        logger.debug('Exit batch_create - ownerId:' + ownerId + ', batchfriendlyname:' + batch.batchfriendlyname);
    }
    else{
        batch_create(ownerId, entityId, userId, batch, (err, code)=>{
            if(err){
                logger.error('batch_create - Error - ' + err);
                return next(err);
            }
            if(code === ResponseCode.Success){
                res.status(200).json({code : code, message: getResponseMsg(code)});
            }
            else{
                res.status(422).json({code : code, message: getResponseMsg(code)});
            }
            logger.debug('Exit batch_create - ownerId:' + ownerId + ', batchfriendlyname:' + batch.batchfriendlyname);      
        });
    }
};

exports.batch_batches = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const usertype = req.query.usertype;
    const studentId = req.query.studentId

    const request = {
        usertype, studentId
    }

    logger.debug('Enter batch_batches - ownerId:' + ownerId);
    batch_batches(ownerId, request, (err, code, batches)=>{
        if(err){
            logger.error('batch_batches - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), batchList: batches});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit batch_batches - ownerId:' + ownerId);
    });
};

exports.batch_bbbbatches = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const usertype = req.query.usertype;
    const studentId = req.query.studentId

    const request = {
        usertype, studentId
    }

    logger.debug('Enter batch_bbbbatches - ownerId:' + ownerId);
    batch_bbbbatches(ownerId, request, (err, code, batches)=>{
        if(err){
            logger.error('batch_bbbbatches - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), batchList: batches});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit batch_bbbbatches - ownerId:' + ownerId);
    });
};

exports.batch_details = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const batchfriendlyname = req.query.batchfriendlyname;
    logger.debug('Enter batch_details - ownerId:' + ownerId + ', batchfriendlyname:' + batchfriendlyname);
    batch_details(ownerId, batchfriendlyname, (err, code, batch)=>{
        if(err){
            logger.error('batch_details - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), batch: batch});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit batch_details - ownerId:' + ownerId + ', batchfriendlyname:' + batchfriendlyname);
    });
};

exports.batch_update = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const entityId = req.user.entityId;
    const userId = req.user._id;
    const batch = req.body;
    logger.debug('Enter batch_update - ownerId:' + ownerId + ', batchfriendlyname:' + batch.batchfriendlyname);
    if(batch.students == null || batch.students.length == 0){
        logger.warn('ownerId:' + ownerId + ', Student(s) cannot be empty');
        res.status(422).json({code : ResponseCode.InvalidInput, message: getResponseMsg(ResponseCode.InvalidInput)});
        logger.debug('Exit batch_update - ownerId:' + ownerId + ', batchfriendlyname:' + batch.batchfriendlyname);
    }
    else{
        batch_update(ownerId, entityId, userId, batch, (err, code)=>{
            if(err){
                logger.error('batch_update - Error - ' + err);
                return next(err);
            }
            if(code === ResponseCode.Success){
                res.status(200).json({code : code, message: getResponseMsg(code)});
            }
            else{
                res.status(422).json({code : code, message: getResponseMsg(code)});
            }
            logger.debug('Exit batch_update - ownerId:' + ownerId + ', batchfriendlyname:' + batch.batchfriendlyname);
        });
    }
};

exports.batch_delete = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const batchfriendlyname = req.body.batchfriendlyname;
    logger.debug('Enter batch_delete - ownerId:' + ownerId + ', batchfriendlyname:' + batchfriendlyname);
    batch_delete(ownerId, batchfriendlyname, (err, code)=>{
        if(err){
            logger.error('batch_delete - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit batch_delete - ownerId:' + ownerId + ', batchfriendlyname:' + batchfriendlyname);
    });
};