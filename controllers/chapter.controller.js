const logger = require('../utils/logger.utils');
const {chapter_create, chapter_chapters, chapter_details, chapter_update,
    chapter_delete,
    chapter_addMasterChapters} = require('../dbc/chapter.dbc');
const { ResponseCode, getResponseMsg } = require('../utils/response.utils');

exports.chapter_create = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const userId = req.user._id;
    const chapter = req.body;
    logger.debug('Enter chapter_create - ownerId:' + ownerId + ', batchId:' + chapter.batchId
     + ', boardname:' + chapter.boardname + ', classname:' + chapter.classname
     + ', subjectname:' + chapter.subjectname + ', chaptername:' + chapter.chaptername);
    chapter_create(ownerId, userId, chapter, (err, code)=>{
        if(err){
            logger.error('chapter_create - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit chapter_create - ownerId:' + ownerId + ', batchId:' + chapter.batchId
        + ', boardname:' + chapter.boardname + ', classname:' + chapter.classname
        + ', subjectname:' + chapter.subjectname + ', chaptername:' + chapter.chaptername);
    });
};

exports.chapter_addMasterChapters = function (req,res,next) {
    const ownerId = req.user.ownerId;
    const userId = req.user._id;
    const chapter = req.body;
    logger.debug('Enter chapter_addMasterChapters - ownerId:' + ownerId + ', batchId:' + chapter.batchId
     + ', boardname:' + chapter.boardname + ', classname:' + chapter.classname
     + ', subjectname:' + chapter.subjectname + ', chapternames:' + chapter.chaptername);
     chapter_addMasterChapters(ownerId, userId, chapter, (err, code)=>{
        if(err){
            logger.error('chapter_addMasterChapters - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
            return
        } else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit chapter_addMasterChapters - ownerId:' + ownerId + ', batchId:' + chapter.batchId
        + ', boardname:' + chapter.boardname + ', classname:' + chapter.classname
        + ', subjectname:' + chapter.subjectname + ', chapternames:' + chapter.chapters);
    });
}

exports.chapter_chapters = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const batchId = req.query.batchId;
    const boardname = req.query.boardname;
    const classname = req.query.classname;
    const subjectname = req.query.subjectname;
    logger.debug('Enter chapter_chapters - ownerId:' + ownerId + ', batchId:' + batchId
     + ', boardname:' + boardname + ', classname:' + classname
     + ', subjectname:' + subjectname);
    chapter_chapters(ownerId, batchId, boardname, classname, subjectname, (err, code, chapters)=>{
        if(err){
            logger.error('chapter_chapters - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), chapterList: chapters});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit chapter_chapters - ownerId:' + ownerId + ', batchId:' + batchId
        + ', boardname:' + boardname + ', classname:' + classname
        + ', subjectname:' + subjectname);
    });
};

exports.chapter_details = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const batchId = req.query.batchId;
    const boardname = req.query.boardname;
    const classname = req.query.classname;
    const subjectname = req.query.subjectname;
    const chaptername = req.query.chaptername;
    logger.debug('Enter chapter_details - ownerId:' + ownerId + ', batchId:' + batchId
     + ', boardname:' + boardname + ', classname:' + classname
     + ', subjectname:' + subjectname + ', chaptername:' + chaptername);
    chapter_details(batchId, ownerId, boardname, classname, subjectname, chaptername, (err, code, chapter)=>{
        if(err){
            logger.error('chapter_details - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), chapter: chapter});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit chapter_details - ownerId:' + ownerId + ', batchId:' + batchId
     + ', boardname:' + boardname + ', classname:' + classname
     + ', subjectname:' + subjectname + ', chaptername:' + chaptername);
    });
};

exports.chapter_update = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const userId = req.user.userId;
    const chapter = req.body;
    logger.debug('Enter chapter_update - ownerId:' + ownerId + ', batchId:' + chapter.batchId
     + ', boardname:' + chapter.boardname + ', classname:' + chapter.classname
     + ', subjectname:' + chapter.subjectname + ', chaptername:' + chapter.chaptername);
     chapter_update(ownerId, userId, chapter, (err, code)=>{
        if(err){
            logger.error('chapter_update - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit chapter_update - ownerId:' + ownerId + ', batchId:' + chapter.batchId
        + ', boardname:' + chapter.boardname + ', classname:' + chapter.classname
        + ', subjectname:' + chapter.subjectname + ', chaptername:' + chapter.chaptername);
    });
};

exports.chapter_delete = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const chapter = req.body;
    logger.debug('Enter chapter_delete - ownerId:' + ownerId + ', batchId:' + chapter.batchId
     + ', boardname:' + chapter.boardname + ', classname:' + chapter.classname
     + ', subjectname:' + chapter.subjectname + ', chaptername:' + chapter.chaptername);
    chapter_delete(ownerId, chapter, (err, code)=>{
        if(err){
            logger.error('chapter_delete - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit chapter_delete - ownerId:' + ownerId + ', batchId:' + chapter.batchId
        + ', boardname:' + chapter.boardname + ', classname:' + chapter.classname
        + ', subjectname:' + chapter.subjectname + ', chaptername:' + chapter.chaptername);
    });
};