const { generateOTP, validateOTP, validateForForgetPwd } = require('../dbc/otp.dbc');
const logger = require('../utils/logger.utils');
const { ResponseCode, getResponseMsg } = require('../utils/response.utils');
const SmsFactory = require('../utils/smsFactory.utils');
const SmsProvider = SmsFactory.create(process.env.SMSPROVIDER || 'TextLocal');

exports.generateOTP = function (req, res, next) {
    const mobileNo = req.body.mobileNo;
    logger.debug('Enter generateOTP - mobileNo:' + mobileNo);
    generateOTP(mobileNo, (err, code, otp)=>{
        if(err){
            logger.error('Exit generateOTP - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), otp: otp});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit generateOTP - mobileNo:' + mobileNo);
    });
};

exports.validateOTP = function (req, res, next) {
    const ownerId = req.body.ownerId;
    const mobileNo = req.body.mobileNo;
    const otp = req.body.otp;
    const userType = req.body.userType;
    const forgetPassword = req.body.forgetPassword
    logger.debug('Enter validateOTP - mobileNo:' + mobileNo);
    validateOTP(ownerId, mobileNo, otp, userType,forgetPassword ,(err, code)=>{
        if(err){
            logger.error('Exit validateOTP - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit validateOTP - mobileNo:' + mobileNo);
    });
};

exports.validateForForgetPwd = function (req, res, next) {
    const mobileNo = req.body.mobileNo;
    const otp = req.body.otp;
    logger.debug('Enter validateForForgetPwd - mobileNo:' + mobileNo);
    validateForForgetPwd(mobileNo, otp, (err, code)=>{
        if(err){
            logger.error('Exit validateForForgetPwd - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit validateForForgetPwd - mobileNo:' + mobileNo);
    });
};