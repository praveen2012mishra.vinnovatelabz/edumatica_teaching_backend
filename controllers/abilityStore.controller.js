const logger = require('../utils/logger.utils');
const { create_ability_dbc,patch_ability_dbc,getAbilities_dbc} = require("../dbc/abilityStore.dbc")
const { ResponseCode, getResponseMsg } = require('../utils/response.utils');
const { create } = require('../models/student.model');

exports.abilityStore_createAbility = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const userId = req.user._id
    const abilityJSON= req.body.abilityJSON
    const consumerId = req.body.consumerId
    const pageId = req.body.pageId
    logger.debug('Enter createAbility - ownerId:' + ownerId + 'userId:' +userId);
    
    create_ability_dbc(ownerId,pageId,userId,consumerId,abilityJSON,(err,code,abilityDoc)=>{
        if(err){
            logger.error('Error createAbility. Error: '+err)
        }
        else{
            if(code===ResponseCode.Success){
                res.status(200).json({code:code,message:getResponseMsg(code),abilityDoc:abilityDoc})
            }
            else{
                res.status(422).json({code:code,message:getResponseMsg(code)})
            }
        }
    })
    logger.debug('Exit createAbility - ownerId:' + ownerId + 'userId:' +userId);
};

exports.abilityStore_patchAbility = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const userId = req.user._id
    const abilityId = req.body.abilityId
    const abilityJSON= req.body.abilityJSON
    const consumerId = req.body.consumerId

    logger.debug('Enter patchAbility - ownerId:' + ownerId + 'userId:' +userId);
    
    patch_ability_dbc(ownerId,userId,abilityId,consumerId,abilityJSON,(err,code,abilityDoc)=>{
        if(err){
            logger.error('Error patchAbility. Error: '+err)
        }
        else{
            if(code===ResponseCode.Success){
                res.status(200).json({code:code,message:getResponseMsg(code),abilityDoc:abilityDoc})
            }
            else{
                res.status(422).json({code:code,message:getResponseMsg(code)})
            }
        }
    })
    logger.debug('Exit patchAbility - ownerId:' + ownerId + 'userId:' +userId);
};

exports.abilityStore_getAbilities = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const userId = req.user._id
    const entityId = req.user.entityId

    logger.debug('Enter getAbilities - ownerId:' + ownerId + 'userId:' +userId);
    
    getAbilities_dbc(ownerId,userId,entityId,(err,code,abilityDocs)=>{
        if(err){
            logger.error('Error getAbilities. Error: '+err)
        }
        else{
            if(code===ResponseCode.Success){
                res.status(200).json({code:code,message:getResponseMsg(code),abilityDocs:abilityDocs})
            }
            else{
                res.status(422).json({code:code,message:getResponseMsg(code)})
            }
        }
    })
    logger.debug('Exit getAbilities - ownerId:' + ownerId + 'userId:' +userId);
};