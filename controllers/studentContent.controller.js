const logger = require('../utils/logger.utils');
const {studentContent_create, studentContent_details,studentContent_allContents,
studentContent_saveAnswers, studentContent_viewResults} = require('../dbc/studentContent.dbc');
const { ResponseCode, getResponseMsg } = require('../utils/response.utils');

exports.studentContent_create = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const userId = req.user._id;
    const studentContentReq = req.body;
    logger.debug('Enter studentContent_create - ownerId:' + ownerId
    + ', contentname:' + studentContentReq.contentname);
    studentContent_create(ownerId, userId, studentContentReq, (err, code)=>{
        if(err){
            logger.error('studentContent_create - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit studentContent_create - ownerId:' + ownerId
        + ', contentname:' + studentContentReq.contentname);
    });
};

exports.studentContent_allContents = function (req, res, next) {
    const ownerId = req.user.ownerId;
    logger.debug('Enter studentContent_allContents - ownerId:' + ownerId);
    studentContent_allContents(ownerId, (err, code, contents)=>{
        if(err){
            logger.error('studentContent_allContents - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, 
                message: getResponseMsg(code), contentList: contents});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit studentContent_allContents - ownerId:' + ownerId);
    });
};

exports.studentContent_details = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const contentname = req.query.contentname;
    logger.debug('Enter studentContent_details - ownerId:' + ownerId
    + ', contentname:' + contentname);
    studentContent_details(ownerId, contentname, (err, code, content)=>{
        if(err){
            logger.error('studentContent_details - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, 
                message: getResponseMsg(code), content: content});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit studentContent_details - ownerId:' + ownerId
        + ', contentname:' + contentname);
    });
};

exports.studentContent_saveAnswers = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const userId = req.user._id;
    const studentContentReq = req.body;
    logger.debug('Enter studentContent_saveAnswers - ownerId:' + ownerId
    + ', contentname:' + studentContentReq.contentname);
    studentContent_saveAnswers(ownerId, userId, studentContentReq, (err, code, content)=>{
        if(err){
            logger.error('studentContent_saveAnswers - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, 
                message: getResponseMsg(code), content: content});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit studentContent_saveAnswers - ownerId:' + ownerId
        + ', contentname:' + studentContentReq.contentname);
    });
};

exports.studentContent_viewResults = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const studentContentReq = req.body;
    logger.debug('Enter studentContent_viewResults - ownerId:' + ownerId
    + ', contentname:' + studentContentReq.contentname);
    studentContent_viewResults(ownerId, studentContentReq, (err, code, contents)=>{
        if(err){
            logger.error('studentContent_viewResults - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, 
                message: getResponseMsg(code), contentList: contents});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit studentContent_viewResults - ownerId:' + ownerId
    + ', contentname:' + studentContentReq.contentname);
    });
};
