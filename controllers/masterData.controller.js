const { masterData_chapters, masterData_schools, 
        masterData_cities } = require('../dbc/masterData.dbc');
const logger = require('../utils/logger.utils');
const { ResponseCode, getResponseMsg } = require('../utils/response.utils');

exports.getCityList = function (req, res, next) {
    const ownerId = req.user.ownerId;
    logger.debug('Enter getCityList - ownerId:' + ownerId);  
    masterData_schools((err, code, schools)=>{
        if(err){
            logger.error('getCityList - Error - ' + err);
            return next(err);
        }
        seen = Object.create(null),
        result = schools.filter(o => {
            var key = ['cityName'].map(k => o[k]).join('|');
            if (!seen[key]) {
                seen[key] = true;
                return true;
            }
        });
        res.status(200).json({ code: ResponseCode.Success,
            message: getResponseMsg(ResponseCode.Success), cityList: result});
    });
    logger.debug('Exit getCityList - ownerId:' + ownerId);  
};

exports.getCitiesByPinCode = function (req, res, next) {
    const ownerId = req.user.ownerId;
    logger.debug('Enter getCitiesByPinCode - ownerId:' + ownerId);  
    masterData_cities((err, code, allCities)=>{
        if(err){
            logger.error('getCitiesByPinCode - Error - ' + err);
            return next(err);
        }
        var filterCities = allCities.filter(function (el) {
            return el.pinCode == req.query.pinCode;
          });
        res.status(200).json({ code: ResponseCode.Success,
            message: getResponseMsg(ResponseCode.Success), cityList: filterCities});
        logger.debug('Exit getCitiesByPinCode - ownerId:' + ownerId);  
    });
};

exports.getSchoolsByCity = function (req, res, next) {
    const ownerId = req.user.ownerId;
    logger.debug('Enter getSchoolsByCity - ownerId:' + ownerId);  
    masterData_schools((err, code, allSchools)=>{
        if(err){
            logger.error('getSchoolsByCity - Error - ' + err);
            return next(err);
        }
        var citySchools = allSchools.filter(function (el) {
            return el.cityName == req.query.cityName;
          });
        res.status(200).json({ code: ResponseCode.Success,
            message: getResponseMsg(ResponseCode.Success), schoolList: citySchools});
        logger.debug('Exit getSchoolsByCity - ownerId:' + ownerId);  
    });
};

exports.getChapterList = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const chapter = {
        boardname: req.query.boardname,
        classname: req.query.classname,
        subjectname: req.query.subjectname
    }
    logger.debug('Enter getChapterList - ownerId:' + ownerId
     + ', boardname:' + chapter.boardname + ', classname:' + chapter.classname
     + ', subjectname:' + chapter.subjectname);
    masterData_chapters((err, code, chapters)=>{
        if(err){
            return next(err);
        }
        var filteredChapters = chapters.filter(function (el) {
            return el.boardName == chapter.boardname &&
                   el.className == chapter.classname &&
                   el.subjectName == chapter.subjectname;
          });
        seen = Object.create(null),
        result = filteredChapters.filter(o => {
            var key = ['chapter'].map(k => o[k]).join('|');
            if (!seen[key]) {
                seen[key] = true;
                return true;
            }
        });
        res.status(200).json({ code: ResponseCode.Success,
            message: getResponseMsg(ResponseCode.Success), chapterDetailsList: result});
    });
    logger.debug('Exit getChapterList - ownerId:' + ownerId
    + ', boardname:' + chapter.boardname + ', classname:' + chapter.classname
    + ', subjectname:' + chapter.subjectname);
};

exports.getBoardsList = function (req, res, next) {  
    const ownerId = req.user.ownerId;
    logger.debug('Enter getBoardsList - ownerId:' + ownerId);  
    masterData_chapters((err, code, chapters)=>{
        if(err){
            logger.error('getBoardsList - Error - ' + err);
            return next(err);
        }
        seen = Object.create(null),
        result = chapters.filter(o => {
            var key = ['boardName'].map(k => o[k]).join('|');
            if (!seen[key]) {
                seen[key] = true;
                return true;
            }
        });
        res.status(200).json({ code: ResponseCode.Success,
            message: getResponseMsg(ResponseCode.Success), boardDetailsList: result});
    });
    logger.debug('Exit getBoardsList - ownerId:' + ownerId);  
};

exports.getClassList = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const boardname = req.query.boardname;
    logger.debug('Enter getClassList - ownerId:' + ownerId
    + ', boardname:' + boardname);   
    masterData_chapters((err, code, chapters)=>{
        if(err){
            logger.error('getClassList - Error - ' + err);
            return next(err);
        }
        var filteredClasses = chapters.filter(function (el) {
            return el.boardName == boardname;
          });
        seen = Object.create(null),
        result = filteredClasses.filter(o => {
            var key = ['className'].map(k => o[k]).join('|');
            if (!seen[key]) {
                seen[key] = true;
                return true;
            }
        });
        res.status(200).json({ code: ResponseCode.Success,
            message: getResponseMsg(ResponseCode.Success), classList: result});
    });
    logger.debug('Exit getClassList - ownerId:' + ownerId
    + ', boardname:' + boardname); 
};

exports.getQualificationList = function (req, res, next) {
    const ownerId = req.user.ownerId;
    logger.debug('Enter getQualificationList - ownerId:' + ownerId);  
    res.status(200).json({ code: ResponseCode.Success,
        message: getResponseMsg(ResponseCode.Success), 
        qualificationDetails: [{degree : "Msc", subjectName : "Maths"},
            {degree : "MPhil", subjectName : "English"}
    ]});
    logger.debug('Exit getQualificationList - ownerId:' + ownerId);  
};

exports.getSubjectList = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const boardname = req.query.boardname;
    const classname = req.query.classname;
    logger.debug('Enter getSubjectList - ownerId:' + ownerId
    + ', boardname:' + boardname + ', classname:' + classname);  
    masterData_chapters((err, code, chapters)=>{
        if(err){
            logger.error('getSubjectList - Error - ' + err);
            return next(err);
        }
        var filteredSubjects = chapters.filter(function (el) {
            return (el.boardName == boardname
                    && el.className == classname);
          });
        seen = Object.create(null),
        result = filteredSubjects.filter(o => {
            var key = ['subjectName'].map(k => o[k]).join('|');
            if (!seen[key]) {
                seen[key] = true;
                return true;
            }
        });
        res.status(200).json({ code: ResponseCode.Success,
            message: getResponseMsg(ResponseCode.Success), subjectList: result});
    });
    logger.debug('Exit getSubjectList - ownerId:' + ownerId
    + ', boardname:' + boardname + ', classname:' + classname);  
};