const { notifications_pushNotificationsSaveDeviceTokens, notifications_pushNotificationsFindDeviceTokens, getAllNotifications, updateNotifications, fetchAnnouncement, notification_processAnnouncement, fetchAnnouncementById } = require("../dbc/notificationPush.dbc");
const { save_notificationMsgTemplate, save_notificationPushTemplate, switch_notificationMsgTemplate, switch_notificationPushTemplate } = require("../dbc/notificationTemplate.dbc");
const logger = require("../utils/logger.utils");
const { ResponseCode, getResponseMsg } = require("../utils/response.utils");
const PushNotificationFactory = require('../utils/pushNotificationFactory.utils');
const PushNotificationProvider = PushNotificationFactory.create(process.env.PUSHPROVIDER || 'Fcm');
const EmailFactory = require("../utils/emailFactory.utils");
const { getSignedURLForAnnouncementFileUpload, getDownloadUrlForAnnouncementFileDownload } = require("../utils/aws.utils");
const EmailProvider = EmailFactory.create(process.env.EMAILPROVIDER || 'SendInBlue')

exports.notifications_pushNotificationsSaveDeviceTokens = async (req, res, next) => {
    const ownerId = req.user.ownerId;
    const params = req.body;
    const deviceToken = params.deviceToken
    logger.debug('Enter notifications_pushNotificationsSaveDeviceTokens - ownerId:' + ownerId + ' Device token:' + deviceToken);
    notifications_pushNotificationsSaveDeviceTokens(ownerId, params, (err, code) => {
        if(err){
            logger.error('notifications_pushNotificationsSaveDeviceTokens - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit notifications_pushNotificationsSaveDeviceTokens - ownerId:' + ownerId);
    })
}

exports.save_notificationMsgTemplate = async (req, res, next) => {
    const type = req.body.type;
    const template = req.body.template;
    const icon = req.body.icon;
    logger.debug('Enter save_notificationMsgTemplate - ownerId:' + req.user.ownerId + ' Template: ' + req.body);
    save_notificationMsgTemplate(type, template, icon, (err, code, response) => {
        if(err){
            logger.error('save_notificationMsgTemplate - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit save_notificationMsgTemplate - ownerId:' + req.user.ownerId);
    })
}

exports.save_notificationPushTemplate = async (req, res, next) => {
    const type = req.body.type;
    const template = req.body.template;
    const icon = req.body.icon;
    logger.debug('Enter save_notificationPushTemplate - ownerId:' + req.user.ownerId + ' Template: ' + req.body);
    save_notificationPushTemplate(type, template, icon, (err, code, response) => {
        if(err){
            logger.error('save_notificationPushTemplate - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit save_notificationPushTemplate - ownerId:' + req.user.ownerId);
    })
}

exports.switch_notificationMsgTemplate = async (req, res, next) => {
    const type = req.body.type;
    const state = req.body.state;
    logger.debug('Enter switch_notificationMsgTemplate - ownerId:' + req.user.ownerId + ' Template: ' + req.body);
    switch_notificationMsgTemplate(type, state, (err, code) => {
        if(err){
            logger.error('switch_notificationMsgTemplate - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit switch_notificationMsgTemplate - ownerId:' + req.user.ownerId);
    })
}

exports.switch_notificationPushTemplate = async (req, res, next) => {
    const type = req.body.type;
    const state = req.body.state;
    logger.debug('Enter switch_notificationPushTemplate - ownerId:' + req.user.ownerId + ' Template: ' + req.body);
    switch_notificationPushTemplate(type, state, (err, code) => {
        if(err){
            logger.error('switch_notificationPushTemplate - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit switch_notificationPushTemplate - ownerId:' + req.user.ownerId);
    })
}

exports.generateAnnouncementFileAwsLink = (req, res, next) => {
    const ownerId = req.user.ownerId;
    const entityId = req.user.entityId;
    const fileType = req.body.fileType;
    logger.debug('Enter generateVideoKycAwsLink - entityId:' + entityId);
    getSignedURLForAnnouncementFileUpload(ownerId, fileType, (err, code, signedUrl, newuuid) => {
        if(err){
            logger.error('generateVideoKycAwsLink - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), uploadLink:signedUrl, uuid: newuuid});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit generateVideoKycAwsLink - entityId:' + entityId);
    })
}

exports.getAnnouncementFileDownloadLink = (req, res, next) => {
    const ownerId = req.user.ownerId;
    const uuid = req.query.uuid;
    logger.debug('Enter getVideoKycDownloadLink - ownerId:' + ownerId + 'uuid: ' + uuid);
    getDownloadUrlForAnnouncementFileDownload(ownerId, uuid, (err, code, signedUrl) => {
        if(err) {
            logger.error('getVideoKycDownloadLink - getDownloadUrlForVideoKyc. AWS utils - Error - ' + err);
            return callback(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), downloadLink:signedUrl});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit getVideoKycDownloadLink - ownerId:' + ownerId + 'uuid: ' + uuid);
    })
}

exports.send_customPush = async (req, res, next) => {
    const ownerId = req.user.ownerId;
    const entityId = req.user.entityId;
    const accountType = req.user.role.name === "ROLE_TUTOR" ? "Tutor" : req.user.role.name === "ROLE_STUDENT" ? "Student" : "Organization"
    const recipients = req.body.recipients;
    const batchArr = req.body.batchArr;
    const title = req.body.title;
    const brief = req.body.brief;
    const uuid = req.body.uuid;
    // console.log(req.user.role, accountType, recipients, batchArr, title, brief, uuid)
    logger.debug('Enter send_customPush - ownerId:' + req.user.ownerId);
    // var deviceTokens = []
    notifications_pushNotificationsFindDeviceTokens(recipients, (err, errorcode, tokenArr) => {
        // console.log(errorcode, tokenArr)
        if(err) {
            logger.error('send_customPush - Error in retrieving token - ' + err);
            return next(err);
        }
        if(errorcode === ResponseCode.Success){
            notification_processAnnouncement(ownerId, entityId, accountType, recipients, batchArr, title, brief, uuid, (err, errcode, announcementId) => {
                if(err) {
                    logger.error('send_customPush - Error notification_processAnnouncement Error - ' + err);
                    return next(err);
                }
                if(errcode === ResponseCode.Success && err === null){
                    const announcementData = {
                        announcementId: announcementId.toString(),
                        title: title,
                        brief: brief,
                        fileUuid: uuid,
                        // batchArr: batchArr,
                        // students: recipients
                    }
                    PushNotificationProvider.sendPush(recipients, tokenArr, title, brief, announcementData, (err, code, data) => {
                        if (err) {
                            logger.error('send_customPush - Error PushNotificationProvider.sendPush Error - ' + err);
                            return next(err);
                        }
                        if(code === ResponseCode.Success){
                            logger.info('send_customPush - Success PushNotificationProvider.sendPush - ' + data);
                            res.status(200).json({code : code, message: getResponseMsg(code)});
                        }
                        else{
                            logger.error('send_customPush - Error PushNotificationProvider.sendPush Error');
                            res.status(422).json({code : code, message: getResponseMsg(code)});
                        }
                        logger.debug('Exit send_customPush - ownerId:' + req.user.ownerId);
                    })
                }
            })
        }
    })
}

exports.fetchAnnouncement = async (req,res,next) => {
    const ownerId = req.user.ownerId;
    const entityId = req.user.entityId;
    logger.debug('Enter fetchAnnouncement - ownerId:' + ownerId + ' entityId: ' + entityId);
    fetchAnnouncement(ownerId, entityId, (err, code, announcements) => {
        if(err) {
            logger.error('fetchAnnouncement - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success) {
            res.status(200).json({code : code, message: getResponseMsg(code), announcements: announcements});
        }else {
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit fetchAnnouncement - ownerId:' + ownerId);
    })
}

exports.fetchAnnouncementById = async (req,res,next) => {
    const ownerId = req.user.ownerId;
    const entityId = req.user.entityId;
    const announcementId = req.params.announcementId
    logger.debug('Enter fetchAnnouncementById - ownerId:' + ownerId + ' entityId: ' + entityId);
    fetchAnnouncementById(announcementId, (err, code, announcement) => {
        if(err) {
            logger.error('fetchAnnouncementById - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success) {
            res.status(200).json({code : code, message: getResponseMsg(code), announcement: announcement});
        }else {
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit fetchAnnouncementById - ownerId:' + ownerId);
    })
}

exports.sendMail = async(req,res,next) => {
    const mailID = req.query.mailID
    try {
        EmailProvider.sendMail(mailID)
        res.status(200).json({code : ResponseCode.Success, message: getResponseMsg(ResponseCode.Success)})
    } catch (error) {
        res.status(403)
    }   
}

exports.getAllNotifications = async(req, res, next) => {
    const userId = req.query.userId;
    logger.debug('Enter getAllNotifications - ownerId:' + req.user.ownerId + ' userId: ' + userId);
    getAllNotifications(userId, (err, code, notifications) => {
        if(err) {
            logger.error('getAllNotifications - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success) {
            res.status(200).json({code : code, message: getResponseMsg(code), notifications: notifications});
        }else {
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit getAllNotifications - ownerId:' + req.user.ownerId);
    })
}

exports.updateNotifications = async(req, res, next) => {
    const userId = req.body.userId;
    const notificationIdArr = req.body.notificationIdArr
    // console.log('',userId,'\n',notificationIdArr)
    logger.debug('Enter updateNotifications - ownerId:' + req.user.ownerId + ' userId: ' + userId);
    updateNotifications(userId, notificationIdArr, (err, code, notifications) =>{
        if(err) {
            logger.error('updateNotifications - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success) {
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }else {
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit updateNotifications - ownerId:' + req.user.ownerId);
    })
}