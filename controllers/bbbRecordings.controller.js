const logger = require('../utils/logger.utils');
const sha1 = require("sha1");
const { getBBBResponse } = require('../utils/bbbresponseparser.utils');
const { ResponseCode, getResponseMsg } = require("../utils/response.utils");
const {bbbRecordings_DeleteRecordingData} = require("../dbc/bbbRecordings.dbc")

//GET RECORDING INFO BY MEETINNG ID
exports.bbbRecordings_recordingInfoById = async (req, res) => {
    const params = req.body;
    const meetingID = params.meetingID;

    logger.debug(
      "Enter bbbRecordings_recordingInfoById - meetingID:" +
        meetingID
    );
  
    const API_STRING = `getRecordingsmeetingID=${meetingID}`;
    const API_STRING_WTH_SECRET = `${API_STRING}${process.env.BBB_CONFIG_SECRET}`;
    const SHA1_STRING = sha1(API_STRING_WTH_SECRET);
    const GET_RECORDING_INFO = `${process.env.BBB_URI_ENDPOINT}getRecordings?meetingID=${meetingID}&checksum=${SHA1_STRING}`;
  
    getBBBResponse(GET_RECORDING_INFO).then(result => {
      try {
        res.status(200).json({
          code: ResponseCode.Success,
          message: getResponseMsg(ResponseCode.Success),
          data: result,
        });
      } catch (err) {
        logger.error('bbbRecordings_recordingInfoById - Error - ' + err);
              
        res.status(422).json({
          code: ResponseCode.MeetingDataCorrupted,
          message: getResponseMsg(ResponseCode.MeetingDataCorrupted),
          error: err,
        });
      }
    })
    logger.debug(
      "Exit bbbRecordings_recordingInfoById - meetingID:" +
        meetingID
    );
  };
  
  // DELETE RECORDING BY RECORD ID
  exports.bbbRecordings_deleteRecordingById = async (req, res) => {
    const params = req.body;
    const recordID = params.recordID;

    logger.debug(
      "Enter bbbRecordings_deleteRecordingById - recordID:" +
      recordID
    );
  
    const API_STRING = `deleteRecordingsrecordID=${recordID}`;
    const API_STRING_WTH_SECRET = `${API_STRING}${process.env.BBB_CONFIG_SECRET}`;
    const SHA1_STRING = sha1(API_STRING_WTH_SECRET);
    const DELETE_RECORDING = `${process.env.BBB_URI_ENDPOINT}deleteRecordings?recordID=${recordID}&checksum=${SHA1_STRING}`;
  
    getBBBResponse(DELETE_RECORDING).then(result => {
      try {
        res.status(202).json({
          code: ResponseCode.Success,
          message: getResponseMsg(ResponseCode.Success),
          data: result,
        });
      } catch (err) {
        logger.error('bbbRecordings_deleteRecordingById - Error - ' + err);
         
        res.status(422).json({
          code: ResponseCode.MeetingDataCorrupted,
          message: getResponseMsg(ResponseCode.MeetingDataCorrupted),
          error: err,
        });
      }
    })
    logger.debug(
      "Exit bbbRecordings_deleteRecordingById - recordID:" +
      recordID
    );
  };

  //DELETE RECORDING FROM AWS & DATABASE
  exports.bbbRecordings_DeleteRecordingData = (req, res, next) => {
    const internalMeetingID = req.query.internalMeetingID;
    logger.debug('Enter bbbRecordings_DeleteRecordingData - internalMeetingID:' + internalMeetingID);
    bbbRecordings_DeleteRecordingData(internalMeetingID, (err, code , deleteRecordingData) => {
      if(err){
        logger.error('bbbRecordings_DeleteRecordingData - Error - ' + err);
        return next(err);
      }
      if(code === ResponseCode.Success){
          res.status(200).json({code : code, message: getResponseMsg(code) , data:deleteRecordingData});
      }
      else{
          res.status(422).json({code : code, message: getResponseMsg(code)});
      }
      logger.debug('Exit bbbRecordings_DeleteRecordingData - internalMeetingID:' + internalMeetingID);
    })
  }
  