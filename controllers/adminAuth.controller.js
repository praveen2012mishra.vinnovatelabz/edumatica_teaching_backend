const logger = require('../utils/logger.utils');
const { ResponseCode, getResponseMsg } = require('../utils/response.utils');
require("es6-promise").polyfill()
require("isomorphic-fetch")

const { register, forgotPassword, getOtp, verifyOtp, setPassword } = require('../dbc/adminAuth.dbc')

// exports.register = (req, res, next) => {
//     const mobileNo = req.body.mobileNo
//     const password = req.body.password
//     const adminType = req.body.adminType
//     const emailId = req.body.emailId

//     if((mobileNo && emailId) || (!mobileNo && !emailId)) {
//         return res.status(422).json({code : ResponseCode.InvalidInput, message: getResponseMsg(ResponseCode.InvalidInput)})
//     }

//     logger.debug('Enter register - mobileNo:' + mobileNo)
//     register(mobileNo, emailId, password, adminType, (err, code) => {
//         if(err){
//             logger.error('register - Error - ' + err);
//             return next(err)
//         }
//         if(code === ResponseCode.Success){
//             res.status(200).json({code : code, message: getResponseMsg(code)})
//         }
//         else{
//             res.status(422).json({code : code, message: getResponseMsg(code)})
//         }
//         logger.debug('Exit register - mobileNo:' + mobileNo)
//     })
// }

exports.getOtp = (req, res, next) => {
    const {username} = req.body

    logger.debug('Enter getOtp - success ' + username)
    getOtp(username, (err, code, otp) => {
        if(err){
            logger.error('getOtp - Error - ' + err);
            return next(err)
        }
        if(code === ResponseCode.Success){
            logger.debug('Exit getOtp - success ' + username)
            res.status(200).json({code : code, message: getResponseMsg(code), otp})
        }
        else{
            logger.debug('Exit getOtp - error ' + getResponseMsg(code) + ' ' + username)
            res.status(422).json({code : code, message: getResponseMsg(code)})
        }
    })
}

exports.verifyOtp = (req, res, next) => {
    const {username, otp} = req.body

    logger.debug('Enter verifyOtp - success ' + username)
    verifyOtp(username, otp, (err, code) => {
        if(err){
            logger.error('verifyOtp - Error - ' + err);
            return next(err)
        }
        if(code === ResponseCode.Success){
            logger.debug('Exit verifyOtp - success ' + username)
            res.status(200).json({code : code, message: getResponseMsg(code)})
        }
        else{
            logger.debug('Exit verifyOtp - error ' + getResponseMsg(code) + ' ' + username)
            res.status(422).json({code : code, message: getResponseMsg(code)})
        }
    })
}

exports.setPassword = (req, res, next) => {
    const {username, newPassword} = req.body

    logger.debug('Enter setPassword - success ' + username)
    setPassword(username, newPassword, (err, code) => {
        if(err){
            logger.error('setPassword - Error - ' + err);
            return next(err)
        }
        if(code === ResponseCode.Success){
            logger.debug('Exit setPassword - success ' + username)
            res.status(200).json({code : code, message: getResponseMsg(code)})
        }
        else{
            logger.debug('Exit setPassword - error ' + getResponseMsg(code) + ' ' + username)
            res.status(422).json({code : code, message: getResponseMsg(code)})
        }
    })
}