const logger = require('../utils/logger.utils');
const { admin_details, getUnverifiedTutors, getUnverifiedOrgs, updateUserStatus, getAllOrganizations, getAllTutors, verifyUser, getOrgPaymentOrders, createAdmin, getAdmin, editAdmin, deleteAdmin, createRole, editRole, createPermissions, getPermissions, changePassword, getAdmins, getRoles, resetPassword, sendNewPasswordMail, getUsers, fetchKycStatus, fetchKycData } = require('../dbc/admin.dbc');
const { ResponseCode, getResponseMsg } = require('../utils/response.utils');

// exports.admin_details = function (req, res, next) {
//     const entityId = req.user.entityId;
//     logger.debug('Enter admin_details - Admin');
//     admin_details(entityId, (err, code, admin)=>{
//         if(err){
//             logger.error('admin_details - Error - ' + err);
//             return next(err);
//         }
//         if(code === ResponseCode.Success){
//             res.status(200).json({code : code, 
//                 message: getResponseMsg(code), admin: admin});
//         }
//         else{
//             res.status(422).json({code : code, message: getResponseMsg(code)});
//         }
//         logger.debug('Exit admin_details - Admin');
//     });
// };

exports.permissionsHandler = (req, res, next) => {
    const {adminRole, permissions, username} = req.user

    logger.debug('Enter permissionsHandler - ' + username)
    // req.route.path.split('/').pop()
    if(permissions.includes(req.route.path.split('/').pop()) || adminRole === "super") {
        logger.debug('Exit permissionsHandler - success ' + username)
        return next()
    }
    logger.debug('Exit permissionsHandler - permission not granted ' + username)
    const code = ResponseCode.PermissionNotGranted
    res.status(422).json({code : code, message: getResponseMsg(code)})
}

exports.createAdmin = (req, res, next) => {
    const {adminReq, adminUserReq} = req.body
    const {username} = req.user

    logger.debug('Enter createAdmin - success ' + adminReq.emailId)
    createAdmin(adminReq, adminUserReq, username, (err, code) => {
        if(err){
            logger.error('createAdmin - Error - ' + err);
            return next(err)
        }
        if(code === ResponseCode.Success){
            logger.debug('Exit createAdmin - success ' + adminReq.emailId)
            res.status(200).json({code : code, message: getResponseMsg(code)})
        }
        else{
            logger.debug('Exit createAdmin - error ' + getResponseMsg(code) + ' ' + adminReq.emailId)
            res.status(422).json({code : code, message: getResponseMsg(code)})
        }
    })
}

exports.getAdmin = (req, res, next) => {
    const {emailId} = req.query

    logger.debug('Enter getAdmin - success ' + emailId)
    getAdmin(emailId, (err, code, admin) => {
        if(err){
            logger.error('getAdmin - Error - ' + err);
            return next(err)
        }
        if(code === ResponseCode.Success){
            logger.debug('Exit getAdmin - success ' + emailId)
            res.status(200).json({code : code, message: getResponseMsg(code), admin})
        }
        else{
            logger.debug('Exit getAdmin - error ' + getResponseMsg(code) + ' ' + emailId)
            res.status(422).json({code : code, message: getResponseMsg(code)})
        }
    })
}

exports.editAdmin = (req, res, next) => {
    const username = req.user.username
    const adminReq = req.body

    logger.debug('Enter editAdmin - success ' + adminReq.emailId)
    editAdmin(adminReq, username, (err, code) => {
        if(err){
            logger.error('editAdmin - Error - ' + err);
            return next(err)
        }
        if(code === ResponseCode.Success){
            logger.debug('Exit editAdmin - success ' + adminReq.emailId)
            res.status(200).json({code : code, message: getResponseMsg(code)})
        }
        else{
            logger.debug('Exit editAdmin - error ' + getResponseMsg(code) + ' ' + adminReq.emailId)
            res.status(422).json({code : code, message: getResponseMsg(code)})
        }
    })
}

exports.deleteAdmin = (req, res, next) => {
    const username = req.user.username
    const emailId = req.query.emailId

    logger.debug('Enter deleteAdmin - success ' + emailId)
    deleteAdmin(username, emailId, (err, code) => {
        if(err){
            logger.error('deleteAdmin - Error - ' + err);
            return next(err)
        }
        if(code === ResponseCode.Success){
            logger.debug('Exit deleteAdmin - success ' + emailId)
            res.status(200).json({code : code, message: getResponseMsg(code)})
        }
        else{
            logger.debug('Exit deleteAdmin - error ' + getResponseMsg(code) + ' ' + emailId)
            res.status(422).json({code : code, message: getResponseMsg(code)})
        }
    })
}

exports.createPermissions = (req, res, next) => {
    const {permissions} = req.body
    const {username} = req.user

    logger.debug('Enter createPermissions - success ' + username)
    createPermissions(username, permissions, (err, code) => {
        if(err){
            logger.error('createPermissions - Error - ' + err);
            return next(err)
        }
        if(code === ResponseCode.Success){
            logger.debug('Exit createPermissions - success ' + username)
            res.status(200).json({code : code, message: getResponseMsg(code)})
        }
        else{
            logger.debug('Exit createPermissions - error ' + getResponseMsg(code) + ' ' + username)
            res.status(422).json({code : code, message: getResponseMsg(code)})
        }
    })
}

exports.createRole = (req, res, next) => {
    const {name, permissions} = req.body
    const {username} = req.user

    logger.debug('Enter createRole - success ' + username)
    createRole(name, permissions, username, (err, code) => {
        if(err){
            logger.error('createRole - Error - ' + err);
            return next(err)
        }
        if(code === ResponseCode.Success){
            logger.debug('Exit createRole - success ' + username)
            res.status(200).json({code : code, message: getResponseMsg(code)})
        }
        else{
            logger.debug('Exit createRole - error ' + getResponseMsg(code) + ' ' + username)
            res.status(422).json({code : code, message: getResponseMsg(code)})
        }
    })
}

exports.editRole = (req, res, next) => {
    const {name, permissions} = req.body
    const {username} = req.user

    logger.debug('Enter editRole - success ' + username)
    editRole(name, permissions, username, (err, code) => {
        if(err){
            logger.error('editRole - Error - ' + err);
            return next(err)
        }
        if(code === ResponseCode.Success){
            logger.debug('Exit editRole - success ' + username)
            res.status(200).json({code : code, message: getResponseMsg(code)})
        }
        else{
            logger.debug('Exit editRole - error ' + getResponseMsg(code) + ' ' + username)
            res.status(422).json({code : code, message: getResponseMsg(code)})
        }
    })
}

exports.changePassword = (req, res, next) => {
    const {oldPassword, newPassword, isForced} = req.body
    const {username, entityId} = req.user

    logger.debug('Enter changePassword - success ' + username)
    changePassword(username, entityId, oldPassword, newPassword, isForced, (err, code) => {
        if(err){
            logger.error('changePassword - Error - ' + err);
            return next(err)
        }
        if(code === ResponseCode.Success){
            logger.debug('Exit changePassword - success ' + username)
            res.status(200).json({code : code, message: getResponseMsg(code)})
        }
        else{
            logger.debug('Exit changePassword - error ' + getResponseMsg(code) + ' ' + username)
            res.status(422).json({code : code, message: getResponseMsg(code)})
        }
    })
}

exports.getPermissions = (req, res, next) => {
    const {username} = req.query
    logger.debug('Enter getPermissions - success ' + username)
    getPermissions((err, code, permissions) => {
        if(err){
            logger.error('getPermissions - Error - ' + err);
            return next(err)
        }
        if(code === ResponseCode.Success){
            logger.debug('Exit getPermissions - success ' + username)
            res.status(200).json({code : code, message: getResponseMsg(code), permissions})
        }
        else{
            logger.debug('Exit getPermissions - error ' + getResponseMsg(code) + ' ' + username)
            res.status(422).json({code : code, message: getResponseMsg(code)})
        }
    })
}

exports.getRoles = (req, res, next) => {
    const {username} = req.query
    logger.debug('Enter getRoles - success ' + username)
    getRoles((err, code, roles) => {
        if(err){
            logger.error('getRoles - Error - ' + err);
            return next(err)
        }
        if(code === ResponseCode.Success){
            logger.debug('Exit getRoles - success ' + username)
            res.status(200).json({code : code, message: getResponseMsg(code), roles})
        }
        else{
            logger.debug('Exit getRoles - error ' + getResponseMsg(code) + ' ' + username)
            res.status(422).json({code : code, message: getResponseMsg(code)})
        }
    })
}

exports.getAdmins = (req, res, next) => {
    const {username} = req.user

    logger.debug('Enter getAdmins - success ' + username)
    getAdmins((err, code, admins) => {
        if(err){
            logger.error('getAdmins - Error - ' + err);
            return next(err)
        }
        if(code === ResponseCode.Success){
            logger.debug('Exit getAdmins - success ' + username)
            res.status(200).json({code : code, message: getResponseMsg(code), admins})
        }
        else{
            logger.debug('Exit getAdmins - error ' + getResponseMsg(code) + ' ' + username)
            res.status(422).json({code : code, message: getResponseMsg(code)})
        }
    })
}

exports.resetPassword = (req, res, next) => {
    const {usernameToReset, newPassword} = req.body
    const {username} = req.user

    logger.debug('Enter resetPassword - success ' + username)
    resetPassword(username, usernameToReset, newPassword, (err, code) => {
        if(err){
            logger.error('resetPassword - Error - ' + err);
            return next(err)
        }
        if(code === ResponseCode.Success){
            logger.debug('Exit resetPassword - success ' + username)
            res.status(200).json({code : code, message: getResponseMsg(code)})
        }
        else{
            logger.debug('Exit resetPassword - error ' + getResponseMsg(code) + ' ' + username)
            res.status(422).json({code : code, message: getResponseMsg(code)})
        }
    })
}

exports.sendNewPasswordMail = (req, res, next) => {
    const {name, email, newPassword} = req.body
    const {username} = req.user

    logger.debug('Enter sendNewPasswordMail - success ' + username)
    sendNewPasswordMail(name, email, newPassword, (err, code) => {
        if(err){
            logger.error('sendNewPasswordMail - Error - ' + err);
            return next(err)
        }
        if(code === ResponseCode.Success){
            logger.debug('Exit sendNewPasswordMail - success ' + username)
            res.status(200).json({code : code, message: getResponseMsg(code)})
        }
        else{
            logger.debug('Exit sendNewPasswordMail - error ' + getResponseMsg(code) + ' ' + username)
            res.status(422).json({code : code, message: getResponseMsg(code)})
        }
    })
}

exports.getUsers = (req, res, next) => {
    const {userType, query} = req.body
    const {username} = req.user

    logger.debug('Enter getUsers - success ' + username)
    getUsers(userType, query, (err, code, users) => {
        if(err){
            logger.error('getUsers - Error - ' + err);
            return next(err)
        }
        if(code === ResponseCode.Success){
            logger.debug('Exit getUsers - success ' + username)
            res.status(200).json({code : code, message: getResponseMsg(code), users})
        }
        else{
            logger.debug('Exit getUsers - error ' + getResponseMsg(code) + ' ' + username)
            res.status(422).json({code : code, message: getResponseMsg(code)})
        }
    })
}

exports.updateUserStatus = (req, res, next) => {
    const { userType, userId, roleStatus } = req.body
    const{username} = req.user

    logger.debug('Enter updateUserStatus - ' + username)
    updateUserStatus(username, userType, userId, roleStatus, (err, code) => {
        if(err){
            logger.error('updateUserStatus - Error - ' + err)
            return next(err)
        }
        if(code === ResponseCode.Success){
            logger.debug('Exit updateUserStatus - success' + username)
            res.status(200).json({code : code, message: getResponseMsg(code)})
        }
        else{
            logger.debug('Exit updateUserStatus - error ' + getResponseMsg(code) + ' - ' + username)
            res.status(422).json({code : code, message: getResponseMsg(code)})
        }
    })
}

exports.fetchKycStatus = (req, res, next) => {
    const {userType, query} = req.body
    const {username} = req.user

    logger.debug('Enter fetchKycStatus - success ' + username)
    fetchKycStatus(userType, query, (err, code, kycStatus) => {
        if(err){
            logger.error('fetchKycStatus - Error - ' + err);
            return next(err)
        }
        if(code === ResponseCode.Success){
            logger.debug('Exit fetchKycStatus - success ' + username)
            res.status(200).json({code : code, message: getResponseMsg(code), kycStatus})
        }
        else{
            logger.debug('Exit fetchKycStatus - error ' + getResponseMsg(code) + ' ' + username)
            res.status(422).json({code : code, message: getResponseMsg(code)})
        }
    })
}

exports.fetchKycData = (req, res, next) => {
    const {userType, userId} = req.body
    const {username} = req.user

    logger.debug('Enter fetchKycData - success ' + username)
    fetchKycData(userType, userId, (err, code, kycData) => {
        if(err){
            logger.error('fetchKycData - Error - ' + err);
            return next(err)
        }
        if(code === ResponseCode.Success){
            logger.debug('Exit fetchKycData - success ' + username)
            res.status(200).json({code : code, message: getResponseMsg(code), kycData})
        }
        else{
            logger.debug('Exit fetchKycData - error ' + getResponseMsg(code) + ' ' + username)
            res.status(422).json({code : code, message: getResponseMsg(code)})
        }
    })
}

// exports.generateRegisterLink = (req, res, next) => {
//     const { mobileNo, emailId, adminType } = req.query

//     if((mobileNo && emailId) || (!mobileNo && !emailId)) {
//         return res.status(422).json({code : ResponseCode.InvalidInput, message: getResponseMsg(ResponseCode.InvalidInput)})
//     }

//     logger.debug('Enter generateRegisterLink - ' + adminType)
//     generateRegisterLink(mobileNo, emailId, adminType, (err, code, registerLink) => {
//         if(err){
//             logger.error('generateRegisterLink - Error - ' + err);
//             return next(err)
//         }
//         if(code === ResponseCode.Success){
//             res.status(200).json({code : code, message: getResponseMsg(code), registerLink})
//         }
//         else{
//             res.status(422).json({code : code, message: getResponseMsg(code)})
//         }
//         logger.debug('Exit generateRegisterLink - ' + adminType)
//     })
// }