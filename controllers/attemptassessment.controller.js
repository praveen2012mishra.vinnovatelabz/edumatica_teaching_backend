const logger = require('../utils/logger.utils');
const { ResponseCode, getResponseMsg } = require('../utils/response.utils');
const { attemptassessment_patchassessment ,attemptassessment_getassignedassessments_dbc,attemptassessment_getassessment,attemptassessment_getanswersdata ,attemptassessment_getassessments, attemptassessment_getassessmentstudentdata_dbc, attemptassessment_getallassignedassessments_dbc,attemptassessment_getAttemptAssessmentbyAssessmentId, attemptassessment_getAllChildAssessments } = require("../dbc/attemptassessment.dbc") 




exports.attemptassessment_patchAttemptedAssessment = (req,res,next) =>{
    const attemptAssessmentId= req.query.attemptassessmentId
    const attemptAssessmentData = req.body
    const ownerId = req.user.ownerId
    const entityId = req.user.entityId
    const userId = req.user._id

    logger.debug('Enter attemptassessment_patchAttemptedAssessment attemptassessmentId : ' +attemptAssessmentId)
    attemptassessment_patchassessment(ownerId,userId,entityId,attemptAssessmentId,attemptAssessmentData,(err,code)=>{
        if(err){
            logger.error('attemptassessment_patchAttemptedAssessment - Error - ' + err);
            return next(err);
        }
        else{
            if(code === ResponseCode.Success){
                res.status(200).json({code : code, message: getResponseMsg(code)});
            }
            else{
                res.status(422).json({code : code, message: getResponseMsg(code)});
            }
        }

        logger.debug('Exit attemptassessment_patchAttemptedAssessment - ownerId:' + ownerId
        + ',attemptAssessmentId:'+attemptAssessmentId);
    })

}

exports.attemptassessment_getAttemptAssessment = (req,res,next) =>{
    const assessmentId = req.query.attemptassessmentId
    const ownerId = req.user.ownerId
    const entityId = req.user.entityId

    logger.debug('Enter attemptassessment_getAttemptedAssessment assessmentId:'+assessmentId)
    attemptassessment_getassessment(ownerId,entityId,assessmentId,(err,code,attemptAssessmentDoc)=>{
        if(err){
            logger.error('attemptassessment_getattemptassessment - Error : '+err)
        }
        else{
            if(code===ResponseCode.Success){
                res.status(200).json({code : code , message : getResponseMsg(code), attemptAssessmentDoc: attemptAssessmentDoc})
            }
            else{
                res.status(422).json({code:code,message:getResponseMsg(code)})
            }
        }

        logger.debug('Exit attemptassessment_getAttemptedAssessment assessmentId:'+assessmentId)
    })
}

exports.attemptassessment_getAnswers = (req,res,next) =>{
    const attemptAssessmentId = req.query.attemptassessmentId
    const ownerId = req.user.ownerId
    const entityId = req.user.entityId

    logger.debug('Enter attemptassessment_getAnswers attemptAssessmentId:'+attemptAssessmentId)

    attemptassessment_getanswersdata(ownerId,entityId,attemptAssessmentId,(err,code,answersData)=>{
        if(err){
            logger.error('attemptassessment_getAnswers - Error: '+err)
        }
        else{
            if(code===ResponseCode.Success) {
                res.status(200).json({code:code , message:getResponseMsg(code) , answersData:answersData })
            }
            else {
                res.status(422).json({code:code,message:getResponseMsg(code)})
            }
        }

        logger.debug('Exit attemptassessment_getAnswers attemptAssessmentId:'+attemptAssessmentId)
    })
}

exports.attemptassessment_getAllAssessments = (req,res,next) =>{
    const ownerId=req.user.ownerId
    const entityId=req.user.entityId

    logger.debug('Enter attemptassessment_getassessments ownerId:'+ownerId+". entityId:"+entityId)
    attemptassessment_getassessments(ownerId,entityId,(err,code,attemptassessmentDocs)=>{
        if(err){
            logger.error('attemptassessment_getassessments - Error: '+err)
        }
        else{
            if(code===ResponseCode.Success){
                res.status(200).json({code:code, message:getResponseMsg(code) , attemptAssessmentDocs:attemptassessmentDocs})
            }
            else{
                res.status(422).json({code:code, message:getResponseMsg(code)})
            }
        }
        logger.debug('Exit attemptassessment_getassessments ownerId:'+ownerId+". entityId:"+entityId)
    })
}

exports.attemptassessment_getAllChildAssessments = (req,res,next) =>{
    const ownerId=req.user.ownerId
    const entityId=req.user.entityId
    const studentId = req.query.studentId

    logger.debug('Enter attemptassessment_getAllChildAssessments ownerId:'+ownerId+". entityId:"+entityId)
    attemptassessment_getAllChildAssessments(ownerId,entityId,studentId,(err,code,attemptassessmentDocs)=>{
        if(err){
            logger.error('attemptassessment_getAllChildAssessments - Error: '+err)
        }
        else{
            if(code===ResponseCode.Success){
                res.status(200).json({code:code, message:getResponseMsg(code) , attemptAssessmentDocs:attemptassessmentDocs})
            }
            else{
                res.status(422).json({code:code, message:getResponseMsg(code)})
            }
        }
        logger.debug('Exit attemptassessment_getAllChildAssessments ownerId:'+ownerId+". entityId:"+entityId)
    })
}

exports.attemptassessment_getassessmentstudentdata = (req,res,next) =>{
    const ownerId = req.user.ownerId
    const assessmentId = req.query.assessmentId

    logger.debug('Enter attemptassessment_getassessmentstudentdata ownerId:'+ownerId+". assessmentId:"+assessmentId)

    attemptassessment_getassessmentstudentdata_dbc(ownerId,assessmentId,(err,code, aggDocs)=>{
        if(err){
            logger.error('Error attempt_assessment_getassessmentstudentdata Error:'+err)
        }
        else{
            if(code===ResponseCode.Success){
                res.status(200).json({code:code,message:getResponseMsg(code),studentInfo:aggDocs})
            }
            else{
                res.status(422).json({code:code,message:getResponseMsg(code)})
            }
        }
    })

    logger.debug('Exit attemptassessment_getassessmentstudentdata ownerId:'+ownerId+". assessmentId:"+assessmentId)


}

exports.attemptassessment_getAssignedAssessments = (req,res,next) =>{
    const ownerId = req.user.ownerId
    
    logger.debug('Enter attemptassessment_getassignedassessment owwnerId:'+ownerId)

    attemptassessment_getassignedassessments_dbc(ownerId,(err,code,assessmentArr)=>{
        if(err){
            logger.error('Error attemptassessment_getassignedassessment. Error :'+err)
        }
        else{
            if(code===ResponseCode.Success){
                res.status(200).json({code:code,message:getResponseMsg(code),assessmentArr:assessmentArr})
            }
            else{
                res.status(422).json({code:code,message:getResponseMsg(code)})
            }
        }
    })

    logger.debug('Exit attemptassessment_getassignedassessment owwnerId:'+ownerId)

}

exports.attemptassessment_getAllAssignedAssessments = (req,res,next) =>{
    const ownerId = req.user.ownerId
    const userId = req.user._id
    // console.log(userId)
    
    logger.debug('Enter attemptassessment_getAllAssignedAssessments owwnerId:'+ownerId)

    attemptassessment_getallassignedassessments_dbc(userId,(err,code,assessmentArr)=>{
        if(err){
            logger.error('Error attemptassessment_getAllAssignedAssessments. Error :'+err)
        }
        else{
            if(code===ResponseCode.Success){
                res.status(200).json({code:code,message:getResponseMsg(code),assessmentArr:assessmentArr})
            }
            else{
                res.status(422).json({code:code,message:getResponseMsg(code)})
            }
        }
    })

    logger.debug('Exit attemptassessment_getAllAssignedAssessments owwnerId:'+ownerId)

}


exports.attemptassessment_getAttemptAssessmentbyAssessmentId = (req, res, next) => {
    const ownerId = req.user.ownerId;
    const assessmentId = req.query.assessmentId;

    logger.debug('Enter attemptassessment_getAttemptAssessmentbyAssessmentId owwnerId:'+ownerId)
    attemptassessment_getAttemptAssessmentbyAssessmentId(ownerId, assessmentId,(err,code,attemptAssessmentArr) => {
        if(err){
            logger.error('Error attemptassessment_getAttemptAssessmentbyAssessmentId. Error :'+err)
        }
        else{
            if(code===ResponseCode.Success){
                res.status(200).json({code:code,message:getResponseMsg(code),attemptAssessmentArr:attemptAssessmentArr})
            }
            else{
                res.status(422).json({code:code,message:getResponseMsg(code)})
            }
        }
    })
    logger.debug('Exit attemptassessment_getAttemptAssessmentbyAssessmentId owwnerId:'+ownerId)
}