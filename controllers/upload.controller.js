const logger = require('../utils/logger.utils');
const { ResponseCode, getResponseMsg } = require('../utils/response.utils');
const readXlsxFile = require('read-excel-file/node');
const fs = require('fs')
const unzipper = require('unzipper')
var path = require('path');
const { join } = require('path')
const {uploadData_dbc, get_distinct_filename_dbc, delete_by_filename_dbc} = require("../dbc/upload.dbc");


var deleteFolderRecursive = function(path) {
    if( fs.existsSync(path) ) {
        fs.readdirSync(path).forEach(function(file) {
          var curPath = path + "/" + file;
            if(fs.lstatSync(curPath).isDirectory()) { // recurse
                deleteFolderRecursive(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
      }
  };
  

  exports.get_distinct_filenames_controller = (req,res,next) =>{
      const ownerId=req.user.ownerId

      logger.debug('Enter get_distinct_files. OwnerId:' +ownerId)

      get_distinct_filename_dbc(ownerId,(err,code,filenames)=>{
          if(err){
            logger.error('Error in get_distinct_files. Error :'+err)
            next()
          }
          else{
              if(code===ResponseCode.Success){
                  res.status(200).json({code:code,message:getResponseMsg(code),filenames:filenames})
              }
              else{
                  res.status(422).json({code:code,message:getResponseMsg(code)})
              }
          }
          logger.debug('Exit get_distinct_files. OwnerId:' +ownerId)
      })
  }

  exports.delete_by_filename_controller = (req,res,next) =>{
      const ownerId = req.user.ownerId
      const filename = req.query.filename

      logger.debug('Enter delete_by_filename ownerId:' +ownerId+" filename:"+filename)

      delete_by_filename_dbc(filename,(err,code)=>{
          if(err){
              logger.error('Error in delete_by_filename ownerId:' +ownerId)
              next()
          }
          else{
              if(code===ResponseCode.Success){
                  res.status(200).json({code:code,message:getResponseMsg(code)})
              }
              else{
                  res.status(422).json({code:code, message:getResponseMsg(code)})
              }
          }
      })
  }

  
  


exports.uploadData = (req,res,next) =>{
    const files = req.files
    console.log(files[0].path)
    const ownerId= req.user.ownerId
    const userId = req.user._id
    const imageZip = files.filter((file)=>{
        return path.extname(file.path) ===".zip"
    })[0]
    const excelFile = files.filter((file)=>{
        return path.extname(file.path) ===".xlsx" || path.extname(file.path) ===".xls"
    })[0]


    
    const excelFileName= excelFile.filename
    readXlsxFile(fs.createReadStream(excelFile.path)).then((rows) => {
        if(imageZip!==undefined) {
            fs.createReadStream(imageZip.path)
            .pipe(unzipper.Extract({path:path.normalize(__dirname+'/../uploadData')}))
            .on('close',()=>{
                const isDirectory = source => fs.lstatSync(source).isDirectory()
                const isFile = source => fs.lstatSync(source).isFile()
                const folders= fs.readdirSync(path.normalize(__dirname+'/../uploadData')).map(name => join(__dirname+'/../uploadData', name)).filter(isDirectory)
                console.log(folders)
                const files= fs.readdirSync(path.normalize(folders[0])).map(name => join(folders[0], name)).filter(isFile)
                const filesData = files.map((file)=>{
                    return {
                        filename:path.basename(file).toLowerCase(),
                        encoding:fs.readFileSync(file).toString('base64')
                    }
                })
                const headers = rows[0]
                rows.shift()
                

                logger.debug('Enter uploadData_dbc ownerId:'+ownerId)

                uploadData_dbc(ownerId,userId,excelFileName,headers,rows,filesData,(err,code,excelData)=>{
                    logger.debug(code)
                    if(err){
                        logger.error('Exit uploadData_dbc error -'+err)
                    }
                    else{
                        if(code === ResponseCode.Success){
                                res.status(200).send({code:code,message:getResponseMsg(code),excelData:excelData})
                                const filesDel= fs.readdirSync(path.normalize(__dirname+"/../uploadData")).map(name => join(__dirname+"/../uploadData", name)).filter(isFile)
                                const foldersDel= fs.readdirSync(path.normalize(__dirname+"/../uploadData")).map(name => join(__dirname+"/../uploadData", name)).filter(isDirectory)
                                for (const file of filesDel) {
                                    fs.unlink(file, err => {
                                      if (err) throw err;
                                    });
                                  }

                                for (const folder of foldersDel){
                                    deleteFolderRecursive(folder)
                                }
                        }
                        else{
                            res.status(422).send({code:code,message:getResponseMsg(code)})
                            const filesDel= fs.readdirSync(path.normalize(__dirname+"/../uploadData")).map(name => join(__dirname+"/../uploadData", name)).filter(isFile)
                                const foldersDel= fs.readdirSync(path.normalize(__dirname+"/../uploadData")).map(name => join(__dirname+"/../uploadData", name)).filter(isDirectory)
                                for (const file of filesDel) {
                                    fs.unlink(file, err => {
                                      if (err) throw err;
                                    });
                                  }

                                for (const folder of foldersDel){
                                    deleteFolderRecursive(folder)
                                }
                        }
                    }
                })

                logger.debug('Exit uploadData_dbc ownerId:'+ownerId)
            })
        }
        else{
            const headers = rows[0]
            rows.shift()

            logger.debug('Enter uploadData_dbc without Files ownerId:'+ownerId)

            uploadData_dbc(ownerId,userId,headers,rows,[],(err,code,excelData)=>{
                if(err){
                    logger.error('Exit uploadData_dbc error -'+err)
                }
                else{
                    if(code = ResponseCode.Success){
                            res.status(200).send({code:code,message:getResponseMsg(code),excelData:excelData})
                    }
                    else{
                        res.status(422).send({code:code,message:getResponseMsg(code)})
                    }
                }
            })

            logger.debug('Exit uploadData_dbc without Files ownerId:'+ownerId)
        }
      })

}