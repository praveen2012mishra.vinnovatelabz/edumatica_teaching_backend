const logger = require('../utils/logger.utils');
const { organization_details, org_courses, organization_create, student_update,
    getTutors, removeTutorsForOrganization, addTutorsForOrganization, org_updateTutor,
    org_addStudents, org_getStudents, org_removeStudents, org_getStudentsForClass, getVideoKycToken, updateVideoKycSuccessUuid, redoVideoKyc, fetchKycDetails, organization_onboard } = require('../dbc/organization.dbc');
const { ResponseCode, getResponseMsg } = require('../utils/response.utils');
const { masterData_packages } = require('../dbc/masterData.dbc');
const { PHONE_PATTERN, EMAIL_PATTERN } = require('../utils/constant.utils');
const { getSignedURLForVideoKycUpload, getDownloadUrlForVideoKyc } = require('../utils/aws.utils');
const dateFns = require('date-fns')

exports.organization_details = function (req, res, next) {
    const ownerId = req.user.ownerId;
    logger.debug('Enter organization_details - ownerId:' + ownerId);
    organization_details(ownerId, (err, code, organization)=>{
        if(err){
            logger.error('organization_details - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, 
                message: getResponseMsg(code), organization: organization});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit organization_details - ownerId:' + ownerId);
    });
};

exports.org_courses = function (req, res, next) {
    const ownerId = req.user.ownerId;
    logger.debug('Enter org_courses - ownerId:' + ownerId);
    org_courses(ownerId,(err, code, courses)=>{
        if(err){
            logger.error('org_courses - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), courseList: courses});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit org_courses - ownerId:' + ownerId);
    });
};

exports.organization_checkCourseCount = function(req,res, next) {
    const {courseDetails} = req.body;
    masterData_packages((err, code, masterPackages) => {
        if(err){
            logger.error('tutor_checkCourseCount - Error - ' + err);
            return next(err);
        }
        let rescode;
        if(code === ResponseCode.Success){
            var package = masterPackages.filter(function (el) {
                return el.name === 'Basic';
            })[0];
            if(courseDetails.length > package.courseCount){
                rescode = ResponseCode.CourseLimitExceeded;
                res.status(200).json({code : rescode, message: getResponseMsg(rescode)});
            } else {
                rescode = ResponseCode.Success;
                res.status(200).json({code : rescode, message: getResponseMsg(rescode)});
            }
        } else {
            rescode = ResponseCode.MasterPackagesNotFound;
            res.status(422).json({code : rescode, message: getResponseMsg(rescode)});
        }
    })
}

exports.organization_create = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const entityId = req.user.entityId;
    const userId = req.user._id;
    let organization = req.body;
    logger.debug('Enter organization_create - ownerId:' + ownerId);
    masterData_packages((err, code, masterPackages) => {
        if(err){
            logger.error('organization_create - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            var packages = masterPackages.filter(function (el) {
                return el.name === 'Basic';
            });
            if(packages && packages.length > 0)
            {
               organization.package = packages[0]._id; 
            }
            else{
                logger.error('organization_create - Error - Default package not found');
                const code = ResponseCode.DefaultPackageNotFound;
                res.status(422).json({code : code, message: getResponseMsg(code)});
                return;
            }

            if((organization.courseDetails) && organization.courseDetails.length > packages[0].courseCount){
                logger.warn('organization_create - Warn - Course limit exceeded');
                const code = ResponseCode.CourseLimitExceeded;
                res.status(422).json({code : code, message: getResponseMsg(code)});
            }
            
            else if(dateFns.isSameDay(new Date(), dateFns.parseISO(organization.dob))) {
                logger.warn('organization_create - Warn - Date of Incorporation is present date');
                const code = ResponseCode.DateOfIncorporationError;
                res.status(422).json({code : code, message: getResponseMsg(code)});
            }

            else{
                organization_create(ownerId, userId, entityId, organization, (err, code, newOrgData)=>{
                    if(err){
                        logger.error('organization_create - Error - ' + err);
                        return next(err);
                    }
                    if(code === ResponseCode.Success){
                        logger.debug('Exit organization_create - ownerId:' + ownerId);
                        res.status(200).json({code : code, message: getResponseMsg(code), newOrgData: newOrgData});
                    }
                    else{
                         logger.debug('Exit organization_create - ownerId:' + ownerId);
                        res.status(422).json({code : code, message: getResponseMsg(code)});
                    }
                    logger.debug('Exit organization_create - ownerId:' + ownerId);
                });
            }
        } else {
            logger.debug('Exit organization_create - ownerId:' + ownerId);
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
    })
};

exports.organization_onboard = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const entityId = req.user.entityId;
    const userId = req.user._id;
    let organization = req.body;
    logger.debug('Enter organization_onboard - ownerId:' + ownerId);
    masterData_packages((err, code, masterPackages) => {
        if(err){
            logger.error('organization_onboard - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            var packages = masterPackages.filter(function (el) {
                return el.name === 'Basic';
            });
            if(packages && packages.length > 0)
            {
               organization.package = packages[0]._id; 
            }
            else{
                logger.error('organization_onboard - Error - Default package not found');
                const code = ResponseCode.DefaultPackageNotFound;
                res.status(422).json({code : code, message: getResponseMsg(code)});
                return;
            }

            if((organization.courseDetails) && organization.courseDetails.length > packages[0].courseCount){
                logger.warn('organization_onboard - Warn - Course limit exceeded');
                const code = ResponseCode.CourseLimitExceeded;
                res.status(422).json({code : code, message: getResponseMsg(code)});
            }
            
            else if(dateFns.isSameDay(new Date(), dateFns.parseISO(organization.dob))) {
                logger.warn('organization_onboard - Warn - Date of Incorporation is present date');
                const code = ResponseCode.DateOfIncorporationError;
                res.status(422).json({code : code, message: getResponseMsg(code)});
            }

            else{
                organization_onboard(ownerId, userId, entityId, organization, (err, code, accessToken, refreshToken, usr)=>{
                    if(err){
                        logger.error('organization_onboard - Error - ' + err);
                        return next(err);
                    }
                    if(code === ResponseCode.Success){
                        logger.debug('Exit organization_onboard - ownerId:' + ownerId);
                        res.status(200).json({code : code, message: getResponseMsg(code), accessToken: accessToken, refreshToken: refreshToken, owner: usr.owner});
                    }
                    else{
                         logger.debug('Exit organization_onboard - ownerId:' + ownerId);
                        res.status(422).json({code : code, message: getResponseMsg(code)});
                    }
                    logger.debug('Exit organization_onboard - ownerId:' + ownerId);
                });
            }
        } else {
            logger.debug('Exit organization_onboard - ownerId:' + ownerId);
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
    })
};

exports.addTutorsForOrganization = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const userId = req.user._id;
    const tutors = req.body.tutorList;
    logger.debug('Enter addTutorsForOrganization - ownerId:' + ownerId);
    let isValidTutors = true;
    if(tutors != null && tutors.length > 0){
        tutors.forEach(tutor => {  
            if(tutor.mobileNo == null || !PHONE_PATTERN.test(tutor.mobileNo)
            || tutor.emailId == null || !EMAIL_PATTERN.test(tutor.emailId)
            || tutor.tutorName == null || tutor.tutorName.length < 5){
                isValidTutors = false;
            }
        })
    }
    else{
        isValidTutors = false
    }
    if(isValidTutors == false){
        logger.warn('addTutorsForOrganization - ownerId:' + ownerId + ' Invalid tutors input');
        res.status(422).json(
        {code: ResponseCode.InvalidInput, message: getResponseMsg(ResponseCode.InvalidInput)});
        logger.debug('Exit addTutorsForOrganization - ownerId:' + ownerId);
    }
    else{
        addTutorsForOrganization(ownerId, userId, tutors, (err, code,  tutorIds)=>{
            console.log(code)
            if(err){
                logger.error('addTutorsForOrganization - Error - ' + err);
                return next(err);
            }
            if(code === ResponseCode.Success){
                res.status(200).json({code : code, message: getResponseMsg(code), addedTutorList: tutorIds});
            }
            else{
                res.status(422).json({code : code, message: getResponseMsg(code)});
            }
            logger.debug('Exit addTutorsForOrganization - ownerId:' + ownerId);
        });
    }
};

exports.getTutors = function (req, res, next) {
    const ownerId = req.user.ownerId;
    logger.debug('Enter getTutors - ownerId:' + ownerId);
    getTutors(ownerId, (err, code, tutors)=>{
        if(err){
            logger.error('getTutors - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), tutorList: tutors});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit getTutors - ownerId:' + ownerId);
    });
};

exports.removeTutorsForOrganization = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const tutors = req.body.tutorList;
    logger.debug('Enter removeTutorsForOrganization - ownerId:' + ownerId);
    removeTutorsForOrganization(ownerId, tutors, (err, code)=>{
        if(err){
            logger.error('removeTutorsForOrganization - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit removeTutorsForOrganization - ownerId:' + ownerId);
    });
};

exports.org_updateTutor = (req, res, next) => {
    const ownerId = req.user.ownerId;
    const tutor = req.body;
    org_updateTutor(ownerId, tutor, (err, code) => {
        if(err) {
            //logger.error('removeTutorsForOrganization - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
    })
}

exports.org_addStudents = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const userId = req.user._id;
    const students = req.body.studentList;
    logger.debug('Enter org_addStudents - ownerId:' + ownerId);
    let isValidStudents = true;
    if(students != null && students.length > 0){
        students.forEach(student => {  
            if(student.mobileNo == null || !PHONE_PATTERN.test(student.mobileNo)
            || student.studentName == null || student.studentName.length < 5
            || (student.parentMobileNo.length > 0 && !PHONE_PATTERN.test(student.parentMobileNo))){
                isValidStudents = false;
            }
        })
    }
    else{
        isValidStudents = false
    }
    if(isValidStudents == false){
        logger.warn('org_addStudents - ownerId:' + ownerId + ' Invalid students input');
        res.status(422).json(
        {code: ResponseCode.InvalidInput, message: getResponseMsg(ResponseCode.InvalidInput)});
        logger.debug('Exit org_addStudents - ownerId:' + ownerId);
    }
    else{
        org_addStudents(ownerId, userId, students, (err, code, studentIds)=>{
            if(err){
                logger.error('org_addStudents - Error - ' + err);
                return next(err);
            }
            if(code === ResponseCode.Success){
                res.status(200).json({code : code, message: getResponseMsg(code), addedStudentList:studentIds});
            }
            else{
                res.status(422).json({code : code, message: getResponseMsg(code)});
            }
            logger.debug('Exit org_addStudents - ownerId:' + ownerId);
        });
    }
};

exports.org_getStudents = function (req, res, next) {
    const ownerId = req.user.ownerId;
    logger.debug('Enter org_getStudents - ownerId:' + ownerId);
    org_getStudents(ownerId, (err, code, students)=>{
        if(err){
            logger.error('org_getStudents - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), studentList: students});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit org_getStudents - ownerId:' + ownerId);
    });
};

exports.org_getStudentsForClass = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const className= req.query.className
    const boardName = req.query.boardName
    logger.debug('Enter org_getStudentsForClass - ownerId:' + ownerId);
    org_getStudentsForClass(ownerId,boardName,className, (err, code, students)=>{
        if(err){
            logger.error('org_getStudentsForClass - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), studentList: students});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit org_getStudentsForClass - ownerId:' + ownerId);
    });
};


exports.org_removeStudents = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const students = req.body.studentList;
    logger.debug('Enter org_removeStudents - ownerId:' + ownerId);
    org_removeStudents(ownerId, students, (err, code, studentIds)=>{
        if(err){
            logger.error('org_removeStudents - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), removedStudentList: studentIds});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit org_removeStudents - ownerId:' + ownerId);
    });
};


exports.getVideoKycToken = (req, res, next) => {
    const ownerId = req.user.ownerId;
    const entityId = req.user.entityId;
    logger.debug('Enter getVideoKycToken - entityId:' + entityId);
    getVideoKycToken(ownerId, entityId, (err, code, token) => {
        if(err){
            logger.error('getVideoKycToken - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), token:token});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit getVideoKycToken - entityId:' + entityId);
    })
}


exports.generateVideoKycAwsLink = (req, res, next) => {
    const ownerId = req.user.ownerId;
    const entityId = req.user.entityId;
    const fileType = req.body.fileType;
    logger.debug('Enter generateVideoKycAwsLink - entityId:' + entityId);
    getSignedURLForVideoKycUpload(ownerId, fileType, (err, code, signedUrl, newuuid) => {
        if(err){
            logger.error('generateVideoKycAwsLink - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), uploadLink:signedUrl, uuid: newuuid});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit generateVideoKycAwsLink - entityId:' + entityId);
    })
}

exports.updateVideoKycSuccessUuid = (req,res, next) => {
    const ownerId = req.user.ownerId;
    const entityId = req.user.entityId;
    const uuid = req.body.uuid;
    const fileName = req.body.fileName;
    logger.debug('Enter updateVideoKycSuccessUuid - entityId:' + entityId + 'uuid: ' + uuid);
    updateVideoKycSuccessUuid(ownerId, entityId, uuid, fileName, (err, code, videoKycLink) => {
        if(err){
            logger.error('updateVideoKycSuccessUuid - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), videoKycLink:videoKycLink});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit updateVideoKycSuccessUuid - entityId:' + entityId + 'uuid: ' + uuid);
    })
}

exports.getVideoKycDownloadLink = (req, res, next) => {
    const ownerId = req.user.ownerId;
    const uuid = req.query.uuid;
    logger.debug('Enter getVideoKycDownloadLink - ownerId:' + ownerId + 'uuid: ' + uuid);
    getDownloadUrlForVideoKyc(ownerId, uuid, (err, code, signedUrl) => {
        if(err) {
            logger.error('getVideoKycDownloadLink - getDownloadUrlForVideoKyc. AWS utils - Error - ' + err);
            return callback(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), videoKycLink:signedUrl});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit getVideoKycDownloadLink - ownerId:' + ownerId + 'uuid: ' + uuid);
    })
}

exports.redoVideoKyc = (req, res, next) => {
    const ownerId = req.user.ownerId;
    const entityId = req.user.entityId;
    logger.debug('Enter redoVideoKyc - ownerId:' + ownerId + 'entityId: ' + entityId);
    redoVideoKyc(ownerId, entityId, (err, code) => {
        if(err) {
            logger.error('redoVideoKyc - Error - ' + err);
            return callback(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit redoVideoKyc - ownerId:' + ownerId + 'entityId: ' + entityId);
    })
}

exports.fetchKycDetails = (req, res, next) => {
    // const ownerId = req.user.ownerId;
    // const entityId = req.user.entityId;
    const organizationId = req.query.organizationId;
    // logger.debug('Enter fetchKycDetails - ownerId:' + ownerId + 'entityId: ' + entityId);
    fetchKycDetails(organizationId, (err, code, kycDetails, videoKycDetails) => {
        if(err) {
            logger.error('fetchKycDetails - Error - ' + err);
            return callback(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), kycDetails:kycDetails, videoKycDetails:videoKycDetails});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        // logger.debug('Exit fetchKycDetails - ownerId:' + ownerId + 'entityId: ' + entityId);
    })
}

exports.student_update = function (req, res, next) {
    const ownerId= req.user.ownerId
    const userId = req.user.userId
    const student = req.body.student;
    logger.debug('Enter student_update - ownerId'+ownerId+' mobileNo:' + student.mobileNo);
    student_update(ownerId, userId, student, (err, code)=>{

        if(err){
            logger.error('student_update - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit student_update - ownerId'+ownerId+' mobileNo:' + student.mobileNo);
    });
};
