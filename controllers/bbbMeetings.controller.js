const logger = require("../utils/logger.utils");
const sha1 = require("sha1");
const { getBBBResponse } = require('../utils/bbbresponseparser.utils');
const { ResponseCode, getResponseMsg } = require("../utils/response.utils");
const { bbbMeetings_getPostMeetingEventsInfo, bbbMeetings_getPostIndividualMeetingEventsInfo, bbbMeetings_feedback } = require("../dbc/bbbMeetings.dbc");

//CREATE A MEETING
exports.bbbMeetings_createMeeting = async (req, res) => {
  const params = req.body;
  const meetingID = params.meetingID;
  const meetingName = params.meetingName;
  const scheduleID = params.scheduleID;
  const callbackURL = `https://edumatica.tk/feedback/${meetingID}/${scheduleID}/${meetingName.split('+').join('%20')}`

  logger.debug(
    "Enter bbbMeetings_createMeeting - meetingID:" +
      meetingID +
      ", meetingName:" +
      meetingName
  );

  const API_STRING = `createallowStartStopRecording=true&attendeePW=${process.env.BBB_ATTENDE_KEY}&autoStartRecording=false&meetingID=${meetingID}&moderatorPW=${process.env.BBB_MODERATOR_KEY}&name=${meetingName}&record=true&logo=${process.env.BBB_LOGO_URL}&meta_endCallbackUrl=${callbackURL}&meta_scheduleID=${scheduleID}&logoutURL=${callbackURL}&moderatorOnlyMessage=You+are+a+Moderator`;
  const API_STRING_WTH_SECRET = `${API_STRING}${process.env.BBB_CONFIG_SECRET}`;
  const SHA1_STRING = sha1(API_STRING_WTH_SECRET);
  const CREATE_MEETING_URL = `${process.env.BBB_URI_ENDPOINT}create?allowStartStopRecording=true&attendeePW=${process.env.BBB_ATTENDE_KEY}&autoStartRecording=false&meetingID=${meetingID}&moderatorPW=${process.env.BBB_MODERATOR_KEY}&name=${meetingName}&record=true&logo=${process.env.BBB_LOGO_URL}&meta_endCallbackUrl=${callbackURL}&meta_scheduleID=${scheduleID}&logoutURL=${callbackURL}&moderatorOnlyMessage=You+are+a+Moderator&checksum=${SHA1_STRING}`;

  getBBBResponse(CREATE_MEETING_URL).then(result => {
    try {
      res.status(201).json({
        code: ResponseCode.Success,
        message: getResponseMsg(ResponseCode.Success),
        data: result,
      });
    } catch (err) {
      logger.error("bbbMeetings_createMeeting - Error - " + err);
  
      res.status(422).json({
        code: ResponseCode.MeetingDataCorrupted,
        message: getResponseMsg(ResponseCode.MeetingDataCorrupted),
        error: err,
      });
    }
  })
  
  logger.debug(
    "Exit bbbMeetings_createMeeting - meetingID:" +
      meetingID +
      ", meetingName:" +
      meetingName
  );
};

//JOIN A MEETING
exports.bbbMeetings_joinMeeting = async (req, res) => {
  const params = req.body;
  const meetingID = params.meetingID;
  const joineeName = params.joineeName;
  const joineerole = params.joineerole;
  const userID = params.userID;
  const BBB_CSS = "%3Aroot%7B--loader-bg%3A%23212121%3B%7D.overlay--1aTlbi%7Bbackground-color%3A%23212121!important%3B%7Dbody%7Bbackground-color%3A%23212121!important%3B%7D"

  logger.debug(
    "Enter bbbMeetings_joinMeeting - meetingID:" +
      meetingID +
      ", joineeName:" +
      joineeName + ", joineerole:" + joineerole + ", joineerole:" + joineerole + ", userID:" + userID
  );

  try {
    if (joineerole === "Moderator") {
      const API_STRING = `joinfullName=${joineeName}&meetingID=${meetingID}&password=${process.env.BBB_MODERATOR_KEY}&redirect=true&userID=${userID}&userdata-bbb_display_branding_area=true&userdata-bbb_custom_style=${BBB_CSS}`;
      const API_STRING_WTH_SECRET = `${API_STRING}${process.env.BBB_CONFIG_SECRET}`;
      const SHA1_STRING = sha1(API_STRING_WTH_SECRET);
      const JOIN_MEETING_URL = `${process.env.BBB_URI_ENDPOINT}join?fullName=${joineeName}&meetingID=${meetingID}&password=${process.env.BBB_MODERATOR_KEY}&redirect=true&userID=${userID}&userdata-bbb_display_branding_area=true&userdata-bbb_custom_style=${BBB_CSS}&checksum=${SHA1_STRING}`;
      
      res.status(200).json({
        code: ResponseCode.Success,
        message: getResponseMsg(ResponseCode.Success),
        data: JOIN_MEETING_URL,
      });
    } else {
      const API_STRING = `joinfullName=${joineeName}&meetingID=${meetingID}&password=${process.env.BBB_ATTENDE_KEY}&redirect=true&userID=${userID}&userdata-bbb_display_branding_area=true&userdata-bbb_custom_style=${BBB_CSS}`;
      const API_STRING_WTH_SECRET = `${API_STRING}${process.env.BBB_CONFIG_SECRET}`;
      const SHA1_STRING = sha1(API_STRING_WTH_SECRET);
      const JOIN_MEETING_URL = `${process.env.BBB_URI_ENDPOINT}join?fullName=${joineeName}&meetingID=${meetingID}&password=${process.env.BBB_ATTENDE_KEY}&redirect=true&userID=${userID}&userdata-bbb_display_branding_area=true&userdata-bbb_custom_style=${BBB_CSS}&checksum=${SHA1_STRING}`;
      
      res.status(200).json({
        code: ResponseCode.Success,
        message: getResponseMsg(ResponseCode.Success),
        data: JOIN_MEETING_URL,
      });
    }
  } catch (err) {
    logger.error('bbbMeetings_joinMeeting - Error - ' + err);
        
    res.status(422).json({
      code: ResponseCode.MeetingDataCorrupted,
      message: getResponseMsg(ResponseCode.MeetingDataCorrupted),
      error: err,
    });
  }

  logger.debug(
    "Exit bbbMeetings_joinMeeting - meetingID:" +
      meetingID +
      ", joineeName:" +
      joineeName
  );
};

//IS MEETING RUNNING
exports.bbbMeetings_isMeetingRunning = async (req, res) => {
  const params = req.body;
  const meetingID = params.meetingID;

  logger.debug(
    "Enter bbbMeetings_isMeetingRunning - meetingID:" +
      meetingID
  );

  const API_STRING = `isMeetingRunningmeetingID=${meetingID}`;
  const API_STRING_WTH_SECRET = `${API_STRING}${process.env.BBB_CONFIG_SECRET}`;
  const SHA1_STRING = sha1(API_STRING_WTH_SECRET);
  const IS_MEETING_RUNNING = `${process.env.BBB_URI_ENDPOINT}isMeetingRunning?meetingID=${meetingID}&checksum=${SHA1_STRING}`;
  
  getBBBResponse(IS_MEETING_RUNNING).then(result => {
    try {
      res.status(200).json({
        code: ResponseCode.Success,
        message: getResponseMsg(ResponseCode.Success),
        data: result,
      });
    } catch (err) {
      logger.error('bbbMeetings_isMeetingRunning - Error - ' + err);
          
      res.status(422).json({
        code: ResponseCode.MeetingDataCorrupted,
        message: getResponseMsg(ResponseCode.MeetingDataCorrupted),
        error: err,
      });
    }
  }).catch(err => {
    // console.log(err)
  })
  
  logger.debug(
    "Exit bbbMeetings_isMeetingRunning - meetingID:" +
      meetingID
  );
};

//GET MEETING INFO BY ID
exports.bbbMeetings_getMeetingInfo = async (req, res) => {
  const params = req.body;
  const meetingID = params.meetingID;

  logger.debug(
    "Enter bbbMeetings_getMeetingInfo - meetingID:" +
      meetingID
  );

  const API_STRING = `getMeetingInfomeetingID=${meetingID}&password=${process.env.BBB_MODERATOR_KEY}`;
  const API_STRING_WTH_SECRET = `${API_STRING}${process.env.BBB_CONFIG_SECRET}`;
  const SHA1_STRING = sha1(API_STRING_WTH_SECRET);
  const GET_MEETING_INFO = `${process.env.BBB_URI_ENDPOINT}getMeetingInfo?meetingID=${meetingID}&password=${process.env.BBB_MODERATOR_KEY}&checksum=${SHA1_STRING}`;

  
    try {
      res.status(200).json({
        code: ResponseCode.Success,
        message: getResponseMsg(ResponseCode.Success),
        data: GET_MEETING_INFO,
      });
    } catch (err) {
      logger.error('bbbMeetings_getMeetingInfo - Error - ' + err);
              
      res.status(422).json({
        code: ResponseCode.MeetingDataCorrupted,
        message: getResponseMsg(ResponseCode.MeetingDataCorrupted),
        error: err,
      });
    }
  
  
  logger.debug(
    "Exit bbbMeetings_getMeetingInfo - meetingID:" +
      meetingID
  );
};

//Get post meeting event details
exports.bbbMeetings_getPostMeetingEventsInfo = (req, res, next) => {
  const bbbMeetingsreq = req.query;
  
  logger.debug('Enter bbbMeetings_getPostMeetingEventsInfo - meetingID:' + bbbMeetingsreq.meetingID);

  bbbMeetings_getPostMeetingEventsInfo(bbbMeetingsreq, (err, code ,meetingData) => {
    if(err){
      logger.error('bbbMeetings_getPostMeetingEventsInfo - Error - ' + err);
      return next(err);
    }
    if(code === ResponseCode.Success || code === ResponseCode.MeetingInfoNotAvailable){
        res.status(200).json({code : code, message: getResponseMsg(code) , data:meetingData});
    }
    else{
        res.status(422).json({code : code, message: getResponseMsg(code)});
    }
    logger.debug('Exit bbbMeetings_getPostMeetingEventsInfo - meetingID:' + bbbMeetingsreq.meetingID);
  })
};

//Get individual post meeting event details by internal Meeting ID
exports.bbbMeetings_getPostIndividualMeetingEventsInfo = (req, res, next) => {
  const internalMeetingID = req.query.internalMeetingID;

  logger.debug('Enter bbbMeetings_getPostIndividualMeetingEventsInfo - internalMeetingID:' + internalMeetingID)
  
  bbbMeetings_getPostIndividualMeetingEventsInfo(internalMeetingID, (err, code, individualMeetingData) => {
    if(err){
      logger.error('bbbMeetings_getPostIndividualMeetingEventsInfo - Error - ' + err);
      return next(err);
    }
    if(code === ResponseCode.Success){
      res.status(200).json({code : code, message: getResponseMsg(code) , data:individualMeetingData});
    } else{
      res.status(422).json({code : code, message: getResponseMsg(code)});
    }
    logger.debug('Exit bbbMeetings_getPostIndividualMeetingEventsInfo - internalMeetingID:' + internalMeetingID);
  })
}

//Feedback Controller
exports.bbbMeetings_feedback = (req, res, next) => {
  const params = req.body;
  const entityId = req.user.entityId

  logger.debug('Enter bbbMeetings_feedback - meetingID/batchId:' + params.batchId + ' entityId:' + entityId)
  bbbMeetings_feedback(entityId,params, (err, code) => {
    if(err){
      logger.error('bbbMeetings_feedback - Error - ' + err);
      return next(err);
    }
    if(code === ResponseCode.Success){
      res.status(200).json({code : code, message: getResponseMsg(code)});
    } else{
      res.status(422).json({code : code, message: getResponseMsg(code)});
    }
    logger.debug('Exit bbbMeetings_feedback - meetingID/batchId:' + params.batchId + ' entityId:' + entityId);
  })
}