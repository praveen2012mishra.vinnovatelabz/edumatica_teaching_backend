const logger = require("../utils/logger.utils");
const {
  bbbWebHook_hookeventlistener_meetingCreated,
  bbbWebHook_hookeventlistener_userEvent,
  bbbWebHook_hookeventlistener_tutorJoinedEvent,
  bbbWebHook_hookeventlistener_eventsToMeeting,
  bbbWebHook_hookeventlistener_endMeetingEvent,
  bbbWebHook_hookeventlistener_endMeetingEventidEntry,
  bbbWebHook_hookeventlistener_recordingEvent,
} = require("../dbc/bbbEvents.dbc");

const { default: axios } = require("axios");
const mongoose = require("mongoose");

const EventLogger = require("../utils/eventLogger.utils");
const eventLogger = new EventLogger();




//Server side event EventStreamer
exports.bbbEvents_eventstreamer = (req, res, next) => {
    req.socket.setTimeout(0);
    req.socket.setNoDelay(true);
    // req.socket.setKeepAlive(true);
    //res.statusCode = 200;
  
    res.writeHeader(200, {
      "Content-Type": "text/event-stream",
      "Cache-Control": "no-cache",
      "Connection": "keep-alive",
      "Access-Control-Allow-Origin": "*",
    });
  
    logger.info("Enter bbbEvents_eventstreamer - Client connected to SSE");
  
    eventLogger.on("sse_msg", function (msg) {
      logger.info('Event Stream: App sent data' + msg);
      res.status(200).write(`data: ${JSON.stringify(msg)}\n\n`);
    });
  };
  

  
  
  //Webhook Listener
  exports.bbbEvents_hookeventlistener = (req, res) => {
    let events = JSON.parse(req.body.event);
  
    events.map((eventdata) => {
      logger.debug(
        "Enter bbbWebHook_hookeventlistener - eventdata:" + eventdata.data
      );
  
      const type = eventdata.data.id;
      const meetingID = eventdata.data.attributes.meeting["external-meeting-id"];
      const internalMeetingID =
        eventdata.data.attributes.meeting["internal-meeting-id"];
  
      //on meeting create
      if (type === "meeting-created") {
        //calling sse
        eventLogger.log(meetingID);
  
        const meetingName = eventdata.data.attributes.meeting.name;
        const createDate = eventdata.data.attributes.meeting["create-date"];
        const createTime = eventdata.data.attributes.meeting["create-time"];
        const scheduleID = eventdata.data.attributes.meeting.metadata.scheduleid
  
        logger.debug("bbbWebHook_hookeventlistener - meeting-created - meetingID:" + meetingID + "meetingName:" + meetingName)
        //console.log("", meetingID, "\n", meetingName, "\n", createDate, "\n", createTime);
  
        bbbWebHook_hookeventlistener_meetingCreated(
          meetingID,
          internalMeetingID,
          scheduleID,
          meetingName,
          createDate,
          createTime
        );
      }
  
      //on user joined or user left
      if (type === "user-joined" || type === "user-left") {
        const userID = eventdata.data.attributes.user["external-user-id"];
        const name = eventdata.data.attributes.user.name;
        const role = eventdata.data.attributes.user.role;
        const eventTriggerTime = eventdata.data.event.ts;
  
        logger.debug("bbbWebHook_hookeventlistener - user-joined/user-left - meetingID:" + meetingID + "userID:" + userID + "name:" + name)
  
        var newEventID = new mongoose.Types.ObjectId();
  
        bbbWebHook_hookeventlistener_userEvent(
          newEventID,
          type,
          userID,
          name,
          role,
          eventTriggerTime
        );
  
        //meeting filter parameter
        const filter = { internalMeetingID: internalMeetingID };
  
        //check if joinee is a MODERATOR, > save details in meeting schema as Tutor
        if (role === "MODERATOR") {
          bbbWebHook_hookeventlistener_tutorJoinedEvent(filter, userID, name);
        }
  
        // saving events to meeting
        bbbWebHook_hookeventlistener_eventsToMeeting(filter, newEventID);
      }


      //on meeting-recording-changed
      if(type === "meeting-recording-changed") {
        const isRecorded = true;
        const recordingURL = `http://muzerobbb.tk/playback/presentation/2.0/playback.html?meetingId=${internalMeetingID}`;
        const filter = { internalMeetingID: internalMeetingID };
        bbbWebHook_hookeventlistener_recordingEvent(filter, isRecorded, recordingURL)
      }

  
      //on meeting end
      if (type === "meeting-ended") {
        logger.debug("bbbWebHook_hookeventlistener - meeting-ended - meetingID:" + meetingID)
  
        var newEventID = new mongoose.Types.ObjectId();
        const eventTriggerTime = eventdata.data.event.ts;
  
        //save event details
        bbbWebHook_hookeventlistener_endMeetingEvent(
          newEventID,
          type,
          eventTriggerTime
        );
  
        const filter = { internalMeetingID: internalMeetingID };
  
        //push event id to events array in meeting model
        bbbWebHook_hookeventlistener_endMeetingEventidEntry(filter, newEventID, internalMeetingID);
  
        //destroy hook after meeting end
        const gethook = async (meetingID) => {
          const response = await axios.post(
            `${process.env.BASE_URL}/bbbhooks/lists`,
            {
              meetingID,
            }
          );
          return response.data;
        };
        gethook(meetingID).then((res) => {
            const js_res = res.data;
            const hookID = js_res.response.hooks.hook.hookID._text;
  
            //delete the hook
            const deletehook = async (hookID) => {
              const response = await axios.post(
                `${process.env.BASE_URL}/bbbhooks/destroy`,
                {
                  hookID,
                }
              );
              return response.data;
            };
            deletehook(hookID)
        });
      }
      logger.debug(
        "Exit bbbWebHook_hookeventlistener - meetingID:" + meetingID
      );
    });
  
    res.status(200).end(); // Responding is important
  };