const logger = require('../utils/logger.utils');
const { tutor_details, tutor_courses, tutor_create, changePassword,
    tutor_addStudents, tutor_getStudents, tutor_updateKYCDocs,
    student_details, student_update, tutor_removeStudentsForTutor,
    parent_details, parent_update, parentDetailsForTutor, getVideoKycToken, 
    updateVideoKycSuccessUuid, redoVideoKyc, tutor_getStudentsbyParent, fetchKycDetails,
    tutor_getStudents_id, tutor_onboard, student_onboard, parent_onboard, getProfilePercentage
} = require('../dbc/tutor.dbc');
const { PHONE_PATTERN } = require('../utils/constant.utils');
const { getUploadUrlForKYCDoc, getDownloadUrlForKYCDoc, getSignedURLForVideoKycUpload, getDownloadUrlForVideoKyc, deleteObject } = require('../utils/aws.utils');
const { masterData_packages } = require('../dbc/masterData.dbc');
const { ResponseCode, getResponseMsg } = require('../utils/response.utils');
const { organization_create } = require('../dbc/organization.dbc');

exports.tutor_details = function (req, res, next) {
    const entityId = req.user.entityId;
    logger.debug('Enter tutor_details - entityId:' + entityId);
    tutor_details(entityId, (err, code, tutor)=>{
        if(err){
            logger.error('tutor_details - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), tutor: tutor});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit tutor_details - entityId:' + entityId);
    });
};

exports.tutor_courses = function (req, res, next) {
    const ownerId = req.user.ownerId;
    logger.debug('Enter tutor_courses - ownerId:' + ownerId);
    tutor_courses(ownerId, (err, code, courses)=>{
        if(err){
            logger.error('tutor_courses - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), courseList: courses});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit tutor_courses - ownerId:' + ownerId);
    });
};

exports.tutor_checkCourseCount = function(req,res, next) {
    const {courseDetails} = req.body;
    masterData_packages((err, code, masterPackages) => {
        if(err){
            logger.error('tutor_checkCourseCount - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            var package = masterPackages.filter(function (el) {
                return el.name === 'Basic';
            })[0];
            let code;
            if(courseDetails.length > package.courseCount){
                code = ResponseCode.CourseLimitExceeded;
                res.status(200).json({code : code, message: getResponseMsg(code)});
            } else {
                code = ResponseCode.Success;
                res.status(200).json({code : code, message: getResponseMsg(code)});
            }
        }
    })
}

exports.tutor_create = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const entityId = req.user.entityId;
    const userId = req.user._id;
    let tutor = req.body;
    logger.debug('Enter tutor_create - ownerId:' + ownerId);
    masterData_packages((err, code, masterPackages) => {
        if(err){
            logger.error('tutor_create - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            var package = masterPackages.filter(function (el) {
                return el.name === 'Basic';
            })[0];
            tutor.package = package._id;

            if((tutor.courseDetails) && tutor.courseDetails.length > package.courseCount){
                const code = ResponseCode.CourseLimitExceeded;
                res.status(422).json({code : code, message: getResponseMsg(code)});
            }
            else{
                tutor_create(ownerId, userId, entityId, tutor, (err, code)=>{
                    if(err){
                        logger.error('tutor_create - Error - ' + err);
                        return next(err);
                    }
                    if(code === ResponseCode.Success){
                        res.status(200).json({code : code, message: getResponseMsg(code)});
                    }
                    else{
                        res.status(422).json({code : code, message: getResponseMsg(code)});
                    }
                    logger.debug('Exit tutor_create - ownerId:' + ownerId);
                });
            }
        }
    })
};

exports.tutor_onboard = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const entityId = req.user.entityId;
    const userId = req.user._id;
    let tutor = req.body;
    logger.debug('Enter tutor_onboard - ownerId:' + ownerId);
    masterData_packages((err, code, masterPackages) => {
        if(err){
            logger.error('tutor_onboard - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            var package = masterPackages.filter(function (el) {
                return el.name === 'Basic';
            })[0];
            tutor.package = package._id;

            if((tutor.courseDetails) && tutor.courseDetails.length > package.courseCount){
                const code = ResponseCode.CourseLimitExceeded;
                res.status(422).json({code : code, message: getResponseMsg(code)});
            }
            else{
                tutor_onboard(ownerId, userId, entityId, tutor, (err, code, accessToken, refreshToken, usr)=>{
                    if(err){
                        logger.error('tutor_onboard - Error - ' + err);
                        return next(err);
                    }
                    if(code === ResponseCode.Success){
                        res.status(200).json({code : code, message: getResponseMsg(code), accessToken: accessToken, refreshToken: refreshToken, owner: usr.owner});
                    }
                    else{
                        res.status(422).json({code : code, message: getResponseMsg(code)});
                    }
                    logger.debug('Exit tutor_onboard - ownerId:' + ownerId);
                });
            }
        }
    })
};

exports.tutor_addStudents = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const userId = req.user._id;
    const students = req.body.studentList;
    logger.debug('Enter tutor_addStudents - ownerId:' + ownerId + students);
    let isValidStudents = true;
    if(students != null && students.length > 0){
        students.forEach(student => { 
            let count = 0; 
            if(student.mobileNo == null || !PHONE_PATTERN.test(student.mobileNo)
            || student.studentName == null || student.studentName.length < 5
            || (student.parentMobileNo.length > 0 && !PHONE_PATTERN.test(student.parentMobileNo))){
                isValidStudents = false;
            }
            students.map(each => {
                if(student.emailId === each.emailId) count++
            })
            if(count>1) isValidStudents = false
        })
    }
    else{
        isValidStudents = false
    }
    if(isValidStudents == false){
        logger.warn('tutor_addStudents - ownerId:' + ownerId + ' Invalid students input');
        res.status(422).json(
        {code: ResponseCode.InvalidInput, message: getResponseMsg(ResponseCode.InvalidInput)});
        logger.debug('Exit tutor_addStudents - ownerId:' + ownerId);
    }
    else{
        tutor_addStudents(ownerId, userId, students, (err, code)=>{
            if(err){
                logger.error('tutor_addStudents - Error - ' + err);
                return next(err);
            }
            if(code === ResponseCode.Success){
                res.status(200).json({code : code, message: getResponseMsg(code)});
            }
            else{
                res.status(422).json({code : code, message: getResponseMsg(code)});
            }
            logger.debug('Exit tutor_addStudents - ownerId:' + ownerId);
        });
    }
};

exports.tutor_getStudents = function (req, res, next) {
    const ownerId = req.user.ownerId;
    logger.debug('Enter tutor_getStudents - ownerId:' + ownerId);
    tutor_getStudents(ownerId, (err, code, students)=>{
        if(err){
            logger.error('tutor_getStudents - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), studentList: students});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit tutor_getStudents - ownerId:' + ownerId);
    });
};

exports.tutor_getStudents_id = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const tutorid = req.query.tutorid;
    logger.debug('Enter tutor_getStudents - ownerId:' + ownerId);
    tutor_getStudents_id(ownerId, tutorid, (err, code, students)=>{
        if(err){
            logger.error('tutor_getStudents - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), studentList: students});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit tutor_getStudents - ownerId:' + ownerId);
    });
};

exports.tutor_getStudentsbyParent = function (req, res, next) {
    const entityId = req.user.entityId;
    logger.debug('Enter tutor_getStudentsbyParent - entityId:' + entityId);
    tutor_getStudentsbyParent(entityId, (err, code, students)=>{
        if(err){
            logger.error('tutor_getStudentsbyParent - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), studentList: students});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit tutor_getStudentsbyParent - entityId:' + entityId);
    });
};

exports.tutor_removeStudentsForTutor = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const students = req.body.studentList;
    logger.debug('Enter tutor_removeStudentForTutor - ownerId:' + ownerId);
    tutor_removeStudentsForTutor(ownerId, students, (err, code, removedStudentList)=>{
        if(err){
            logger.error('tutor_removeStudentForTutor - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), removedStudentList});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit tutor_removeStudentForTutor - ownerId:' + ownerId);
    });
};

exports.tutor_updateKYCDocs = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const kycDocs = req.body;
    logger.debug('Enter tutor_updateKYCDocs - ownerId:' + ownerId);
    if(kycDocs == null){ 
        logger.warn('tutor_updateKYCDocs - ownerId:' + ownerId + ' Invalid kyc input');
        res.status(422).json({ code: ResponseCode.InvalidInput,
        message: getResponseMsg(ResponseCode.InvalidInput)});
        logger.debug('Exit tutor_updateKYCDocs - ownerId:' + ownerId);
    }
    else{
        tutor_updateKYCDocs(ownerId, kycDocs, (err, code)=>{
            if(err){
                logger.error('tutor_updateKYCDocs - Error - ' + err);
                return next(err);
            }
            if(code === ResponseCode.Success){
                res.status(200).json({code : code, message: getResponseMsg(code)});
            }
            else{
                res.status(422).json({code : code, message: getResponseMsg(code)});
            }
            logger.debug('Exit tutor_updateKYCDocs - ownerId:' + ownerId);
        });
    }
};

exports.tutor_getUploadUrlForKYCDoc = (req,res,next)=>{
    const ownerId = req.user.ownerId;
    const { contentType } = req.query;
    logger.debug('Enter tutor_getUploadUrlForKYCDoc - ownerId:' + ownerId);
    getUploadUrlForKYCDoc(ownerId, contentType, (err, code, url, uuid)=>{
        if(err){
            logger.error('tutor_getUploadUrlForKYCDoc - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), url: url, uuid: uuid});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit tutor_getUploadUrlForKYCDoc - ownerId:' + ownerId);
    });
}

exports.tutor_deleteUploadKYCDoc = (req,res,next)=>{
    const ownerId = req.user.ownerId;
    const { uuid } = req.query;
    const fileName = ownerId + "/KYCdocs/" + uuid;

    logger.debug('Enter tutor_deleteUploadKYCDoc - ownerId:' + ownerId);
    
    deleteObject(fileName, (err, code)=>{
        if(err){
            logger.error('tutor_deleteUploadKYCDoc - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit tutor_deleteUploadKYCDoc - ownerId:' + ownerId);
    });
}

exports.tutor_getDownloadUrlForKYCDoc = (req,res,next)=>{
    const ownerId = req.user.ownerId;
    const uuid = req.query.uuid;
    logger.debug('Enter tutor_getDownloadUrlForKYCDoc - ownerId:' + ownerId);
    getDownloadUrlForKYCDoc(ownerId, uuid, (err, code, url)=>{
        if(err){
            logger.error('tutor_getDownloadUrlForKYCDoc - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), url: url});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit tutor_getDownloadUrlForKYCDoc - ownerId:' + ownerId);
    });
}

const deleteFolderRecursive = (path) => {
    const fs = require('fs');
    if( fs.existsSync(path) ) {
        fs.readdirSync(path).forEach(function(file) {
          var curPath = path + "/" + file;
            if(fs.lstatSync(curPath).isDirectory()) { // recurse
                deleteFolderRecursive(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
      }
};

const checkXml = async (xmlString, mobile, shareCode, numOfIteration) => {

    /********** Verifying XML signature ****************** */

    const select = require('xml-crypto').xpath
	  , jsdom = require('jsdom'), dom = new jsdom.JSDOM()
      , parser = new dom.window.DOMParser()
	  , SignedXml = require('xml-crypto').SignedXml
	  , FileKeyInfo = require('xml-crypto').FileKeyInfo  
	  , path = require('path')
      , crypto = require('crypto');

	const xml = xmlString;
	const doc = parser.parseFromString(xml, 'text/xml');    

	const signature = select(doc, "//*[local-name(.)='Signature' and namespace-uri(.)='http://www.w3.org/2000/09/xmldsig#']")[0]
	const sig = new SignedXml()
	sig.keyInfoProvider = new FileKeyInfo(path.join(__dirname, '..', 'key', 'uidai_auth_sign_prod_2023.cer'))
	sig.loadSignature(signature)
	const res = sig.checkSignature(xml)
	if (!res) console.log(sig.validationErrors)

    /********** Mobile Hash Check ****************** */

    const inputMobileHash = doc.getElementsByTagName('Poi')[0].getAttribute('m');
    
    const hash = crypto.createHash('sha256');
    hash.update(mobile+shareCode);
    let mobileHash = hash.digest('hex');
    let i=1;
    while(i < numOfIteration) {
        const hash = crypto.createHash('sha256');
        hash.update(mobileHash);
        mobileHash = hash.digest('hex')
        i+=1;
    };

    return (res && (mobileHash === inputMobileHash));
}

const saveUser = (ownerId, user, userType, kycStatus, res, next, userId = '', entityId = '') => {
    user.aadhaarKycStatus = kycStatus;

    if(userType === 'ROLE_TUTOR'){
        tutor_create(ownerId, userId, entityId, user, (err, code) => {
            if(err){
                logger.error('tutor_create - Error - ' + err);
                return next(err);
            }
            if(code === ResponseCode.Success){
                res.status(200).json({code : code, message: kycStatus});
            }
            else{
                res.status(422).json({code : code, message: kycStatus});
            }
            logger.debug('Exit tutor_create - ownerId:' + ownerId);
        })
    } else if (userType ==='ROLE_ORGANIZATION') {
        organization_create(ownerId, user, (err, code) => {
            if(err){
                logger.error('organization_create - Error - ' + err);
                return next(err);
            }
            if(code === ResponseCode.Success){
                logger.debug('Exit organization_create - ownerId:' + ownerId);
                res.status(200).json({code : code, message: kycStatus});
            }
            else{
                logger.debug('Exit organization_create - ownerId:' + ownerId);
                res.status(422).json({code : code, message: kycStatus});
            }
            logger.debug('Exit organization_create - ownerId:' + ownerId);
        })
    }
}

exports.uploadZip = (req, res, next) => {
    const ownerId = req.user.ownerId;
    const unzipper = require('unzipper');
    const path = require('path');
    const {mobile, shareCode, lastDigit} = req.body;
    const userType = req.user.roles[0].name;
    const user = req.body;
    const hash = require('crypto').createHash('sha256').update(user.aadhaarKycShareCode);
    user.aadhaarKycShareCode = hash.digest('hex');
    user.aadhaarMobile = mobile;
    if(userType === 'ROLE_TUTOR') {
        var entityId = req.user.entityId;
        var userId = req.user._id;
    }

    (async () => {
        logger.debug('Enter upload_zip - ownerId:' + ownerId);
        const verified = 'Verified';
        const rejected = 'Rejected';
        try {
          const directory = await unzipper.Open.file(req.file.path);
          const extracted = await directory.files[0].buffer(shareCode);
          
          if(await checkXml(extracted.toString(), mobile, shareCode, lastDigit)) {            
            deleteFolderRecursive(path.dirname(req.file.path));

            saveUser(ownerId, user, userType, verified, res, next, userId, entityId);
          } else {
            deleteFolderRecursive(path.dirname(req.file.path));

            saveUser(ownerId, user, userType, rejected, res, next, userId, entityId);
          }
        } catch(e) {
          logger.error('upload_zip - Error - ' + e);
          if(req.file.path) deleteFolderRecursive(path.dirname(req.file.path));
          saveUser(ownerId, user, userType, rejected, res, next, userId, entityId);
        }
        logger.debug('Exit upload_zip - ownerId:' + ownerId);
    })();
}

exports.student_details = function (req, res, next) {
    const entityId = req.user.entityId;
    logger.debug('Enter student_details - ownerId:' + entityId);
    student_details(entityId, (err, code, student)=>{
        if(err){
            logger.error('student_details - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), student: student});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit student_details - entityId:' + entityId);
    });
};

exports.student_update = function (req, res, next) {
  const ownerId= req.user.ownerId
    const userId = req.user.userId
    const student = req.body.student;
    logger.debug('Enter student_update - ownerId'+ownerId+' mobileNo:' + student.mobileNo);
    student_update(ownerId, userId, student, (err, code)=>{

        if(err){
            logger.error('student_update - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit student_update - ownerId'+ownerId+' mobileNo:' + student.mobileNo);
    });
};

exports.student_onboard = function (req, res, next) {
    const ownerId= req.user.ownerId
      const userId = req.user.userId
      const student = req.body.student;
      logger.debug('Enter student_onboard - ownerId'+ownerId+' mobileNo:' + student.mobileNo);
      student_onboard(ownerId, userId, student, (err, code)=>{
  
          if(err){
              logger.error('student_onboard - Error - ' + err);
              return next(err);
          }
          if(code === ResponseCode.Success){
              res.status(200).json({code : code, message: getResponseMsg(code)});
          }
          else{
              res.status(422).json({code : code, message: getResponseMsg(code)});
          }
          logger.debug('Exit student_onboard - ownerId'+ownerId+' mobileNo:' + student.mobileNo);
      });
  };

exports.parent_details_for_tutor = (req,res,next) => {
    const ownerId = req.user.ownerId
    const mobileNo = req.query.mobileNo
    logger.debug('Enter parent_details_for_tutor ownerId:'+ownerId+ ' mobileNo:'+mobileNo)
    parentDetailsForTutor(ownerId,mobileNo , (err,code,parent)=>{
        if(err){
            logger.error('parent_details_for_tutor - Error - ' + err);
            next(err)
        }
        else{
            if(code === ResponseCode.Success){
                res.status(200).json({code : code, message: getResponseMsg(code), parent: parent});
            }
            else{
                res.status(422).json({code : code, message: getResponseMsg(code)});
            }
        }
        logger.debug('Exit parent_details_for_tutor - ownerId:' + ownerId+' mobileNo:'+mobileNo);
    })
}
exports.parent_details = function (req, res, next) {
    const entityId = req.user.entityId;
    logger.debug('Enter parent_details - entityId:' + entityId);
    parent_details(entityId, (err, code, parent)=>{
        if(err){
            logger.error('parent_details - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), parent: parent});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit parent_details - entityId:' + entityId);
    });
};

exports.parent_update = function (req, res, next) {
    const entityId = req.user.entityId;
    const parent = req.body;
    logger.debug('Enter parent_update - entityId:' + entityId);
    parent_update(entityId, parent, (err, code)=>{
        if(err){
            logger.error('parent_update - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit parent_update - entityId:' + entityId);
    });
};

exports.parent_onboard = function (req, res, next) {
    const entityId = req.user.entityId;
    const parent = req.body;
    logger.debug('Enter parent_onboard - entityId:' + entityId);
    parent_onboard(entityId, parent, (err, code)=>{
        if(err){
            logger.error('parent_onboard - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit parent_onboard - entityId:' + entityId);
    });
};

exports.changePassword = function (req, res, next) {
    const userId = req.user._id;
    console.log(userId)
    const currPassword = req.body.currPassword;
    const newPassword = req.body.newPassword;
      logger.debug('Enter changePassword - entityId:' + userId);
    changePassword(userId, currPassword, newPassword, (err, code)=>{

        if(err){
            logger.error('changePassword - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit changePassword - userId:' + userId);
    });
};

exports.getVideoKycToken = (req, res, next) => {
    const ownerId = req.user.ownerId;
    const entityId = req.user.entityId;
    logger.debug('Enter getVideoKycToken - entityId:' + entityId);
    getVideoKycToken(ownerId, entityId, (err, code, token) => {
        if(err){
            logger.error('getVideoKycToken - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), token:token});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit getVideoKycToken - entityId:' + entityId);
    })
}


exports.generateVideoKycAwsLink = (req, res, next) => {
    const ownerId = req.user.ownerId;
    const entityId = req.user.entityId;
    const fileType = req.body.fileType;
    logger.debug('Enter generateVideoKycAwsLink - entityId:' + entityId);
    getSignedURLForVideoKycUpload(ownerId, fileType, (err, code, signedUrl, newuuid) => {
        if(err){
            logger.error('generateVideoKycAwsLink - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), uploadLink:signedUrl, uuid: newuuid});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit generateVideoKycAwsLink - entityId:' + entityId);
    })
}

exports.updateVideoKycSuccessUuid = (req,res, next) => {
    const ownerId = req.user.ownerId;
    const entityId = req.user.entityId;
    const uuid = req.body.uuid;
    const fileName = req.body.fileName;
    logger.debug('Enter updateVideoKycSuccessUuid - entityId:' + entityId + 'uuid: ' + uuid);
    updateVideoKycSuccessUuid(ownerId, entityId, uuid, fileName, (err, code, videoKycLink) => {
        if(err){
            logger.error('updateVideoKycSuccessUuid - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), videoKycLink:videoKycLink});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit updateVideoKycSuccessUuid - entityId:' + entityId + 'uuid: ' + uuid);
    })
}

exports.getVideoKycDownloadLink = (req, res, next) => {
    const ownerId = req.user.ownerId;
    const uuid = req.query.uuid;
    logger.debug('Enter getVideoKycDownloadLink - ownerId:' + ownerId + 'uuid: ' + uuid);
    getDownloadUrlForVideoKyc(ownerId, uuid, (err, code, signedUrl) => {
        if(err) {
            logger.error('getVideoKycDownloadLink - getDownloadUrlForVideoKyc. AWS utils - Error - ' + err);
            return callback(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), videoKycLink:signedUrl});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit getVideoKycDownloadLink - ownerId:' + ownerId + 'uuid: ' + uuid);
    })
}

exports.redoVideoKyc = (req, res, next) => {
    const ownerId = req.user.ownerId;
    const entityId = req.user.entityId;
    logger.debug('Enter redoVideoKyc - ownerId:' + ownerId + 'entityId: ' + entityId);
    redoVideoKyc(ownerId, entityId, (err, code) => {
        if(err) {
            logger.error('redoVideoKyc - Error - ' + err);
            return callback(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit redoVideoKyc - ownerId:' + ownerId + 'entityId: ' + entityId);
    })
}

exports.fetchKycDetails = (req, res, next) => {
    // const ownerId = req.user.ownerId;
    // const entityId = req.user.entityId;
    const tutorId = req.query.tutorId;
    // logger.debug('Enter fetchKycDetails - ownerId:' + ownerId + 'entityId: ' + entityId);
    fetchKycDetails(tutorId, (err, code, kycDetails, videoKycDetails) => {
        if(err) {
            logger.error('fetchKycDetails - Error - ' + err);
            return callback(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), kycDetails:kycDetails, videoKycDetails:videoKycDetails});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        // logger.debug('Exit fetchKycDetails - ownerId:' + ownerId + 'entityId: ' + entityId);
    })
}

exports.getProfilePercentage = (req, res, next) => {
    const entityId = req.user.entityId
    const role = req.user.role.name

    logger.debug('Enter fetchKycDetails - entityId: ' + entityId);
    getProfilePercentage(entityId, role, (err, code, percentageCompleted, points, totalPoints, missingDatas) => {
        if(err) {
            logger.error('getProfilePercentage - Error - ' + err);
            return callback(err);
        }
        if(code === ResponseCode.Success){
            logger.debug('Exit getProfilePercentage - entityId: ' + entityId);
            res.status(200).json({code : code, message: getResponseMsg(code), percentageCompleted, points, totalPoints, missingDatas});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
    })
}