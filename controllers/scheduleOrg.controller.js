const logger = require('../utils/logger.utils');
const { saveSchedulesInBatch, deleteSchedule, schedule_update
} = require('../dbc/scheduleOrg.dbc');
const { ResponseCode, getResponseMsg } = require('../utils/response.utils');

function timeToDecimal(t) {
    var arr = t.split(':');
    var dec = parseInt((arr[1]/6)*10, 10);

    return parseFloat(parseInt(arr[0], 10) + '.' + (dec<10?'0':'') + dec);
}

exports.schedule_create = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const userId = req.user._id;
    const batch = req.body;
    logger.debug('Enter schedule_create - ownerId:' + ownerId + ', batchfriendlyname:' + batch.batchfriendlyname);
    let scheduleOverlaps = false;
    let schedulesArray = batch.schedules;
    let msg = '';
    for (var i = 0; i < schedulesArray.length; i++){
        let a = timeToDecimal(schedulesArray[i].fromhour);
        let b = timeToDecimal(schedulesArray[i].tohour);
        if(Number.isNaN(a) || Number.isNaN(b) || a < 0 || a>24 || b <0 || b>24 || a >= b){
            scheduleOverlaps = true;
            msg = 'Invalid Schedule- Dayname:' + schedulesArray[i].dayname 
            + ' fromhour:' + schedulesArray[i].fromhour 
            + ' tohour:' + schedulesArray[i].tohour;
            break;
        }
    }
    if(schedulesArray.length > 1)
    {        
        for (var i = 0; i < schedulesArray.length - 1; i++){
            let a = timeToDecimal(schedulesArray[i].fromhour);
            let b = timeToDecimal(schedulesArray[i].tohour);
            for (var j = i + 1; j < schedulesArray.length; j++){
                let x = timeToDecimal(schedulesArray[j].fromhour);
                let y = timeToDecimal(schedulesArray[j].tohour);
                if (schedulesArray[i].dayname == schedulesArray[j].dayname) {
                    if (!((a  < x && b <= x) || (y <= a && y < b))){
                            scheduleOverlaps = true;
                            msg = 'Invalid Schedule- Dayname:' + schedulesArray[i].dayname + ' fromhour:' + schedulesArray[i].fromhour 
                            + ' tohour:' + schedulesArray[i].tohour 
                            + ' overlaps with Dayname:' + schedulesArray[j].dayname + ' fromhour:' + schedulesArray[j].fromhour 
                            + ' tohour:' + schedulesArray[j].tohour;
                            break;
                    }
                }
            }
        }
    }
    if(scheduleOverlaps == true){
        logger.warn('schedule_create - ownerId:' + ownerId
                                    + ' ' + msg);
        res.status(422).json({code: ResponseCode.SchedulesOverlap, message: msg});
    }
    else{
        saveSchedulesInBatch(ownerId, userId, batch, (err, code, msg)=>{
            if(err){
                logger.error('schedule_create - Error - ' + err);
                return next(err);
            }
            if(code === ResponseCode.Success){
                res.status(200).json({code : code, message: getResponseMsg(code)});
            }
            else if(code === ResponseCode.SchedulesOverlap){
                logger.warn('schedule_create - ownerId:' + ownerId
                                            + ' ' + msg);
                res.status(422).json({code: ResponseCode.SchedulesOverlap, message: msg});
            }
            else{
                res.status(422).json({code : code, message: getResponseMsg(code)});
            }
        });
    }
    logger.debug('Exit schedule_create - ownerId:' + ownerId + ', batchfriendlyname:' + batch.batchfriendlyname);
};

exports.schedule_delete = function (req, res, next) {
    const ownerId = req.user.ownerId;
    const tutorId = req.body.tutorId;    
    const dayname = req.body.dayname;
    const fromhour = req.body.fromhour;
    logger.debug('Enter schedule_delete - ownerId:' + ownerId
    + ', dayname:' + dayname + ' , fromhour:' + fromhour+ ', tutorId:'+tutorId);
    deleteSchedule(ownerId, tutorId, dayname, fromhour, (err, code)=>{
        if(err){
            logger.error('schedule_delete - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit schedule_delete - ownerId:' + ownerId
        + ', dayname:' + dayname + ' , fromhour:' + fromhour);
    });
};

exports.schedule_update = function(req, res, next) {
    const ownerId = req.user.ownerId;
    const userId = req.user._id;
    const scheduleReq = req.body;

    logger.debug('Enter schedule_update - ownerId:' + ownerId);
    schedule_update(ownerId, userId, scheduleReq, (err, code)=>{
        if(err){
            logger.error('schedule_update - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit schedule_update - ownerId:' + ownerId);
    });
}
