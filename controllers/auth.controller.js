const logger = require('../utils/logger.utils');
const { register, setPassword, checkOwnerId, verifyEmail } = require('../dbc/auth.dbc');
const { refreshToken } = require('../utils/passport.utils')
const { ResponseCode, getResponseMsg } = require('../utils/response.utils');
require("es6-promise").polyfill()
require("isomorphic-fetch")

exports.register = function (req, res, next) {
    const orgCode = req.body.orgCode;
    const mobileNo = req.body.mobileNo;
    const otp = req.body.otp;
    const password = req.body.password;
    const userType = req.body.userType;
    logger.debug('Enter register - mobileNo:' + mobileNo + ', userType:' + userType);
    register(orgCode, mobileNo, otp, password, userType, (err, code, returnUserType)=>{
        if(err){
            logger.error('register - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), returnUserType: returnUserType});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit register - mobileNo:' + mobileNo + ', userType:' + userType);    
    });
};

exports.checkOrgCode = function (req, res, next) {
    const orgCode = req.body.orgCode;
    logger.debug('Enter checkOrgCode - orgCode:' + orgCode);
    checkOwnerId(orgCode, (err, code)=>{
        if(err){
            logger.error('checkOrgCode - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        } else if(code === ResponseCode.CodeAlreadyExists) {
            res.status(200).json({code : code, message: getResponseMsg(code)});
        } else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit checkOrgCode - orgCode:' + orgCode);  
    });
};

exports.setPassword = function (req, res, next) {
    const mobileNo = req.body.mobileNo;
    const otp = req.body.otp;
    const password = req.body.password;
    logger.debug('Enter setPassword - mobileNo:' + mobileNo);
    setPassword(undefined,mobileNo, otp, password, undefined, (err, code)=>{
        if(err){
            logger.error('setPassword - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit setPassword - mobileNo:' + mobileNo);
    });
};

exports.verifyCaptcha = async function (req, res, next) {    
    const captchaValue = req.body.captchaValue;
    logger.debug('Enter generateOTP - captchaValue:' + captchaValue);
    const RECAPTCHA_SERVER_KEY = process.env.RECAPTCHA_SERVER_KEY;
    // Validate Human
    const isHuman = await fetch(`https://www.google.com/recaptcha/api/siteverify`, {
    method: "post",
    headers: {
        Accept: "application/json",
        "Content-Type": "application/x-www-form-urlencoded; charset=utf-8"
    },
    body: `secret=${RECAPTCHA_SERVER_KEY}&response=${captchaValue}`
    })
    .then(res => res.json())
    .then(json => json.success)
    .catch(err => {
        logger.error('Exit generateOTP - Error - ' + err);
        return next(err);
    })
    
    logger.debug(isHuman)
    if (captchaValue === null || !isHuman) {
        res.status(422).json({code : ResponseCode.InvalidCaptcha, message: getResponseMsg(157)});
    }
    else {
        res.status(200).json({code : ResponseCode.Success, 
            message: getResponseMsg(ResponseCode.Success)});
    }
};

exports.refreshtoken = async function (req, res, next) {    
    try {
        const { token } = req.body;
        if(!token) {
            return res.status(401).json({type: 'JwtError', message: 'Unauthorized'});
        }

        refreshToken(token, (freshToken) => {
            if(!freshToken) {
                return res.status(401).json({type: 'JwtError', message: 'Unauthorized'});
            }
            return res.status(200).json({
                accessToken: freshToken
            });
        })
        
    } catch (error) {
        next(error)
    }
};

exports.verifyEmail = (req, res, next) => {
    const role = req.body.role;
    const entityId = req.body.entityId;
    const verificationtoken = req.body.verificationtoken;
    logger.debug('Enter verifyEmail - entityId:' + entityId);
    verifyEmail(role, entityId, verificationtoken, (err, code) => {
        if(err){
            logger.error('Exit verifyEmail - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit verifyEmail - entityId:' + entityId);
    })
}