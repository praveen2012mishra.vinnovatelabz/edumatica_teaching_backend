const logger = require('../utils/logger.utils');
const { ResponseCode, getResponseMsg } = require('../utils/response.utils');
const { uploadAssignmentFile, getDownloadUrlForAssignmentDoc, getUploadUrlForAssignmentDoc } = require('../utils/aws.utils');
const { zipFiles } = require('../utils/archiver.utils')
const { deleteFolderRecursive } = require('../utils/multer.uploadzip')
const { 
    createAssignment, fetchAssignment, fetchStudentAssignment, fetchAssignedAssignment,
    submitAssignmentDoc, createAssignmentDraft, evaluateAssignmentDoc
} = require('../dbc/assignments.dbc');
const { convertImagesToPdf } = require('../utils/pdfkit.utils')

exports.zipFilesAndUpload = (req, res, next) => {
    const ownerId = req.user.ownerId;
    const path = require('path');

    const filePaths = [];
    const fileNames = []
    req.files.forEach(cur => {
        filePaths.push(cur.path)
        fileNames.push(cur.originalname)
    })

    zipFiles(filePaths, fileNames, (zipPath) => {
      uploadAssignmentFile(ownerId, 'zippedDocuments.zip', 'AssignmentTaskDocs', 'application/zip', zipPath, (err, code, uuid, contentType, fileName) => {
            deleteFolderRecursive(path.dirname(filePaths[0]))
            if(err) {
                logger.error('uploadFile - Error - ' + err);
                return next(err);
            }
            if(code === ResponseCode.Success) {
              res.status(200).json({code : code, message: getResponseMsg(code), uuid: uuid, fileType: contentType, fileName: fileName});

            } else {
                res.status(422).json({code : code, message: getResponseMsg(code)});
            }
            logger.debug('Exit uploadFile - ownerId:' + ownerId);
        })
    })
}

exports.createAssignment = (req, res, next) => {
    const ownerId = req.user.ownerId;
    const assignment = req.body;

    logger.debug('Enter createAssignment - ownerId:' + ownerId);
    createAssignment(ownerId, assignment, (err, code)=>{
        if(err){
            logger.error('createAssignment - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        } else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit createAssignment - ownerId:' + ownerId);  
    });
}

exports.createAssignmentDraft = (req, res, next) => {
    const ownerId = req.user.ownerId;
    const assignment = req.body;

    logger.debug('Enter createAssignmentDraft - ownerId:' + ownerId);
    createAssignmentDraft(ownerId, assignment, (err, code)=>{
        if(err){
            logger.error('createAssignmentDraft - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        } else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit createAssignmentDraft - ownerId:' + ownerId);  
    });
}

exports.fetchAssignment = (req, res, next) => {
    const ownerId = req.user.ownerId;
    const query = req.query;

    logger.debug('Enter fetchAssignment - ownerId:' + ownerId);
    fetchAssignment(ownerId, query, (err, code, assignments)=>{
        if(err){
            logger.error('fetchAssignment - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), assignments: assignments});
        } else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit fetchAssignment - ownerId:' + ownerId);  
    });
}

exports.fetchAssignedAssignment = (req, res, next) => {
    const ownerId = req.user.ownerId;
    const query = req.query;

    logger.debug('Enter fetchAssignedAssignment - ownerId:' + ownerId);
    fetchAssignedAssignment(ownerId, query, (err, code, assignments)=>{
        if(err){
            logger.error('fetchAssignedAssignment - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), assignments: assignments});
        } else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit fetchAssignedAssignment - ownerId:' + ownerId);  
    });
}

exports.fetchStudentAssignment = (req, res, next) => {
    const ownerId = req.user.ownerId;
    const query = req.query;

    logger.debug('Enter fetchStudentAssignment - ownerId:' + ownerId);
    fetchStudentAssignment(ownerId, query, (err, code, assignments)=>{
        if(err){
            logger.error('fetchStudentAssignment - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), assignments: assignments});
        } else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit fetchStudentAssignment - ownerId:' + ownerId);  
    });
}

exports.getDownloadUrlForAssignmentDoc = (req, res, next)=>{
    const ownerId = req.user.ownerId;
    const uuid = req.query.uuid;
    const { fileName, docType } = req.query;
    const userFileName = fileName 
    logger.debug('Enter getDownloadUrlForAssignmentDoc - ownerId:' + ownerId);
    getDownloadUrlForAssignmentDoc(ownerId, docType, uuid, userFileName, (err, code, url)=>{
        if(err){
            logger.error('getDownloadUrlForAssignmentDoc - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code), url: url});
        }
        else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit getDownloadUrlForAssignmentDoc - ownerId:' + ownerId);
    });
}

exports.getUploadUrlForAssignmentDoc = (req,res,next)=>{
    const ownerId = req.user.ownerId;
    const { contentType, fileName, docType } = req.query;
    logger.debug('Enter getUploadUrlForAssignmentDoc - ownerId:' + ownerId);
    const userFileName = fileName
    logger.debug(userFileName)
    getUploadUrlForAssignmentDoc(ownerId, userFileName, docType, contentType, (err, code, url, uuid) => {
        if(err) {
            logger.error('getUploadUrlForAssignmentDoc - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success) {
            res.status(200).json({code : code, message: getResponseMsg(code), url: url, uuid: uuid});
        }
        else {
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit getUploadUrlForAssignmentDoc - ownerId:' + ownerId);
    });
}

exports.submitAssignmentDoc = (req, res, next) => {
    const ownerId = req.user.ownerId;
    const submitData = req.body;

    logger.debug('Enter submitAssignmentDoc - ownerId:' + ownerId);
    submitAssignmentDoc(ownerId, submitData, (err, code)=>{
        if(err){
            logger.error('submitAssignmentDoc - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        } else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit submitAssignmentDoc - ownerId:' + ownerId);  
    });
}

exports.evaluateAssignmentDoc = (req, res, next) => {
    const ownerId = req.user.ownerId;
    const evalData = req.body;

    logger.debug('Enter evaluateAssignmentDoc - ownerId:' + ownerId);
    evaluateAssignmentDoc(ownerId, evalData, (err, code)=>{
        if(err){
            logger.error('evaluateAssignmentDoc - Error - ' + err);
            return next(err);
        }
        if(code === ResponseCode.Success){
            res.status(200).json({code : code, message: getResponseMsg(code)});
        } else{
            res.status(422).json({code : code, message: getResponseMsg(code)});
        }
        logger.debug('Exit evaluateAssignmentDoc - ownerId:' + ownerId);  
    });
}

exports.topdf = (req, res, next) => {
    const ownerId = req.user.ownerId;
    const path = require('path');
    const allPaths = req.files.map(cur => cur.path)
    const doctype = req.headers['doctype']

    convertImagesToPdf(allPaths, (pdfPath) => {
        uploadAssignmentFile(ownerId, 'images.pdf', doctype, 'application/pdf', pdfPath, (err, code, uuid) => {
            deleteFolderRecursive(path.dirname(allPaths[0]))
            if(err) {
                logger.error('uploadFile - Error - ' + err);

                return next(err);
            }
            if(code === ResponseCode.Success) {
                res.status(200).json({code : code, message: getResponseMsg(code), uuid: uuid});
            }
            else {
                res.status(422).json({code : code, message: getResponseMsg(code)});
            }
            logger.debug('Exit uploadFile - ownerId:' + ownerId);
        })
    })    
}
