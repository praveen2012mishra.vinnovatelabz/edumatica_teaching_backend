// Include the Sendinblue library
var SibApiV3Sdk = require("sib-api-v3-sdk");
const { notifications_auditEmail } = require("../dbc/notificationEmail.dbc");
const { notificationHtmlTemplate } = require("../dbc/notificationTemplate.dbc");
const Email = require("./email.utils");
const logger = require("./logger.utils");
const { ResponseCode } = require("./response.utils");
SibApiV3Sdk.ApiClient.instance.authentications["api-key"].apiKey =
  process.env.SENDINBLUE_API_KEY;

class SendInBlue extends Email {
  sendMail(mail, args, callback) {
    notificationHtmlTemplate(args, (subject, msg) => {
      if (
        process.env.GLOBALNOTIFICATIONSWITCH === "true" &&
        process.env.SENDEMAIL === "true"
      ) {
        console.log(mail);
        new SibApiV3Sdk.TransactionalEmailsApi()
          .sendTransacEmail({
            subject: subject,
            sender: { email: process.env.EMAILSENDERID, name: process.env.EMAILSENDERNAME },
            replyTo: { email: process.env.EMAILSENDERID, name: process.env.EMAILSENDERNAME },
            to: mail,
            htmlContent: msg,
            // to: [{ name: mail.name, email: mail.email }], // this template to be followed
            // "templateId":1,
            // 'htmlContent' : '<html><body><h1>This is a transactional email {{params.bodyMessage}}</h1></body></html>',
            // 'params' : {'bodyMessage':'Made just for you!'}
          })
          .then(
            function (data) {
              logger.info("SendInBlue Email sent" + data);
              notifications_auditEmail(mail, "success", msg, data);
              callback(null, ResponseCode.Success, data);
            },
            function (error) {
              logger.error("SendInBlue Email not sent" + error);
              notifications_auditEmail(mail, "failure", msg, data);
              callback(error);
            }
          );
      }
    });
  }
  // sendTemplateMail() {
  //     new SibApiV3Sdk.TransactionalEmailsApi().sendTransacEmail({

  //         // "sender":{ "email":"avigyan.bhakta@mu-zero.io", "name":"Avigyan"},
  //         "subject":"Welcome to Edumatica",
  //         "templateId":1,
  //         // "params":{
  //         //    "greeting":"This is the default greeting",
  //         //    "headline":"This is the default headline"
  //         // },
  //         "messageVersions":[
  //           //Definition for Message Version 1
  //           {
  //               "to":[
  //                  {
  //                     "email":"avigyanbhaktacontai@gmail.com",
  //                     "name":"Avigyan Bhakta"
  //                  },
  //                  {
  //                     "email":"avigyan.bhakta2017@vitstudent.ac.in",
  //                     "name":"Avigyan Bhakta"
  //                  }
  //               ],
  //             //   "params":{
  //             //      "greeting":"Hello again!",
  //             //      "headline":"Take advantage of our summer deals, taylored just for you"
  //             //   },
  //               "subject":"Welcome to Edumatica"
  //            },

  //            //Definition for Message Version 2
  //         //    {
  //         //        "to":[
  //         //           {
  //         //              "email":"marie@example.com",
  //         //              "name":"Marie Delvaux"
  //         //           }
  //         //        ],
  //         //        "params":{
  //         //           "greeting":"Hello Marie, we have prepared some exclusive summer deals for you.",
  //         //           "headline":"Some bathing suits you might like"
  //         //        },
  //         //        "subject":"Marie, new bathing suits are here."
  //         //     }
  //        ]

  //    }).then(function(data) {
  //      console.log(data);
  //    }, function(error) {
  //      console.error(error);
  //    });
  // }
}

module.exports = SendInBlue;
