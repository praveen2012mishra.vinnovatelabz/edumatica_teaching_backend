const Push = require("./pushNotification.utils");
var fcm = require("fcm-notification");
const {
  notifications_auditPush,
  notification_saveToUserPullDb,
} = require("../dbc/notificationPush.dbc");
const { ResponseCode } = require("./response.utils");
const { notificationPushTemplate } = require("../dbc/notificationTemplate.dbc");
var FCM = new fcm(
  "./utils/firebase_key/mu-zero-edumatica-firebase-adminsdk-4vgy4-2c6fd2915b.json"
);

class Fcm extends Push {
  sendPush(userIds, deviceToken, title, msg, data, callback) {
    console.log(process.env.GLOBALNOTIFICATIONSWITCH, process.env.SENDPUSH);
    if (
      process.env.GLOBALNOTIFICATIONSWITCH === "true" &&
      process.env.SENDPUSH === "true"
    ) {
      
      var Tokens = deviceToken;
      if (data) {
        var message = {
          data: data,
          notification: {
            title: title,
            body: msg,
            // specific to android
            // icon: 'only link',
            // color: '#f45342'
          },
        };
      }
      if (!data) {
        var message = {
          notification: {
            title: title,
            body: msg,
            // specific to android
            // icon: 'only link',
            // color: '#f45342'
          },
        };
      }
      FCM.sendToMultipleToken(message, Tokens, function (err, response) {
        if (err) {
          // console.log(err)
          notifications_auditPush(Tokens, "failure", msg, response);
          callback(err);
          return;
        }
        // console.log(response)
        notifications_auditPush(Tokens, "success", msg, response);
        notification_saveToUserPullDb(userIds, title, msg, data);
        callback(null, ResponseCode.Success, response);
      });
    }
  }

  sendTemplatePush(userIds, deviceToken, args, callback) {
    if (
      process.env.GLOBALNOTIFICATIONSWITCH === "true" &&
      process.env.SENDPUSH === "true"
    ) {
      notificationPushTemplate(args, (msg) => {
        var Tokens = deviceToken;
        if (args.data) {
          var message = {
            data: args.data,
            notification: {
              title: "Edumatica",
              body: msg,
              // specific to android
              // icon: 'only link',
              // color: '#f45342'
            },
          };
        }
        if (!args.data) {
          var message = {
            notification: {
              title: "Edumatica",
              body: msg,
              // specific to android
              // icon: 'only link',
              // color: '#f45342'
            },
          };
        }
        FCM.sendToMultipleToken(message, Tokens, function (err, response) {
          if (err) {
            notifications_auditPush(Tokens, "failure", msg, response);
            callback(err);
            return;
          }
          notifications_auditPush(Tokens, "success", msg, response);
          notification_saveToUserPullDb(userIds, "Edumatica", msg, data);
          callback(null, ResponseCode.Success, response);
        });
      });
    }
  }
}

module.exports = Fcm;
