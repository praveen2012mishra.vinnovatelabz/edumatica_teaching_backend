const ResponseCode = {
    Success: 0,
    InvalidInput: 1,
    IncorrectUserOrPassword : 101,
    MaxLoginAttemptCountExceeded: 102,
    StudentNotFound: 103,
    StudentPresentExistingBatch: 127,
    RegistrationRestriction: 128,
    EmailDuplicate: 130,
    TutorAlreadyRegistered: 104,
    RoleNotFound: 105,
    UserNotExist: 106,
    UserRegistered: 129,
    ParentNotFound: 107,
    StudentAlreadyRegistered: 108,
    ParentAlreadyRegistered: 109,
    OrganizationAlreadyRegistered: 110,

    BatchAlreadyExists: 111,
    BatchNotFound: 112,
    ChapterAlreadyExists: 113,
    ChapterNotFound: 114,
    ContentAlreadyExists: 115,
    ContentNotFound: 116,
    ContentTypeNotAllowed: 117,
    ContentSizeNotAllowed: 118,
    ChapterAlreadyPublished: 119,

    
    SchedulesOverlap: 121,
    ScheduleNotFound: 122,
    SessionNotStarted: 123,
    SessionIncorrectDay: 124,   
    

    TutorNotFound: 131,
    ContentLimitExceeded: 132,
    CourseLimitExceeded: 133,
    StudentLimitExceeded: 134,
    TutorStudentAlreadyExist: 135,
    BatchLimitExceeded: 136,
    ScheduleLimitExceeded: 137,
    TutorLimitExceeded: 138,
    DateOfIncorporationError: 231,
    TutorAsStudentError: 232,

    SolutionsNotAccesible:139,
    AttemptedAssessmentNotSubmitted :140,
    AssessmentAlreadyExists: 141,
    AssessmentNotFound: 142,
    QuestionNotFound: 143,
    TopicAlreadyExists : 144,
    TopicNotFound : 145,
    SectionNotFound:146,
    AttemptedAssessmentAlreadyExists:147,
    AttemptedAssessmentSaveFailed:148,
    AttemptedAssessmentNotFound:149,
    AttemptedAssessmentAlreadySubmitted:150,

    MaxOtpSentCountExceeded: 151,
    OtpNotVerified: 152,
    IncorrectOtp: 153,
    MaxOtpAttemptCountExceeded: 154,
    OtpNotGenerated: 155,
    SmsSendingFailed: 156,
    InvalidCaptcha: 157,
    CodeAlreadyExists: 158,
    VideoKycAttemptsExceeded: 159,

    OrganizationNotFound: 161,
    OrganizationTutorAlreadyExist: 162,
    OrganizationStudentAlreadyExist: 163,

    MasterPackagesNotFound: 171,
    DefaultPackageNotFound: 172,
    MasterCitiesNotFound: 173,


    SamePasswordAlreadyExist: 174,

    MissingHeadersInFile:176,
    DuplicateExcelFileName:177,

    AdminNotFound: 181,

    AbilitiesNotFound :191,
    AbilityNotCreated:192,

    PermissionNotGranted: 211,

    MeetingInfoNotAvailable: 322,
    MeetingDataCorrupted: 323,

    AssignmentAlreadyExist:350,
    AssignmentAlreadyAssigned: 351,
    AssignmentNotFound:352,

    NotificationTemplateNotAvailable: 606,

    EmptyCourseBundle: 611,
    CourseBundleAlreadyExists: 612,
    PurchasedCoursePackageAlreadyExists: 613,
    UnprocessablePayOrder: 614,
    InvalidReferralCode: 615,
    PurchasedCoursePackageAlreadyExists: 616,
    ReferralAlreadyUsed: 617,
    ReferralTimeExceeded: 618,
    PaymentAlreadyCompleted: 619,

    BatchNotSelected: 711,
    S3Error: 712,
    StudentNotSelected: 713,
    IncorrectPassword: 701,

    BackOfficeAdminAlreadyExists: 801,
    RoleAlreadyExists: 802,
    PermissionNotFound: 803,
    PermissionAlreadyExists: 804,
    OtpTimeExpired: 805,
}

const getResponseMsg  = (code) => {
    switch (code) {
        case ResponseCode.Success:
            return 'Success';
        case ResponseCode.InvalidInput:
            return 'Invalid Input';
        case ResponseCode.IncorrectUserOrPassword:
            return 'Incorrect user or password';
        case ResponseCode.SamePasswordAlreadyExist:
            return 'New password cannot be same as the old password';
        case ResponseCode.MaxLoginAttemptCountExceeded:
            return 'Max login attempt count exceeded';
        case ResponseCode.StudentNotFound:
            return 'Student Not Found';   
        case ResponseCode.StudentPresentExistingBatch:
            return 'Student(s) present in existing batch of same course';
        case ResponseCode.TutorAlreadyRegistered:
            return 'Tutor already registered';
        case ResponseCode.OrganizationAlreadyRegistered:
            return 'Institute already registered';
        case ResponseCode.RoleNotFound:
            return 'Role Not Found';
        case ResponseCode.UserNotExist:
            return 'User doesn\'t exist';
        case ResponseCode.ParentNotFound:
            return 'Parent Not Found';
        case ResponseCode.StudentAlreadyRegistered:
            return 'Student already registered';
        case ResponseCode.ParentAlreadyRegistered:
            return 'Parent already registered';
        case ResponseCode.RegistrationRestriction:
            return 'Registration Restriction';
        case ResponseCode.UserRegistered:
            return 'Mobile Number Already Registered';
        case ResponseCode.EmailDuplicate:
            return 'Found email repeat. Please check'


        case ResponseCode.BatchAlreadyExists:
            return 'Batch already exists. Please change the Batch Name and retry.';
        case ResponseCode.BatchNotFound:
            return 'Batch Not found';
        case ResponseCode.ChapterAlreadyExists:
            return 'Chapter already exists';
        case ResponseCode.ChapterNotFound:
                return 'Chapter Not found';
        case ResponseCode.ContentAlreadyExists:
            return 'Content already exists';
        case ResponseCode.ContentNotFound:
            return 'Content Not found';            
        case ResponseCode.ChapterAlreadyPublished:
            return 'Chapter Already Published';


        case ResponseCode.SchedulesOverlap:
            return 'Input Schedule(s) Overlap with existing schedules'; 
        case ResponseCode.ScheduleNotFound:
            return 'Schedule not found';
        case ResponseCode.SessionNotStarted:
            return 'Session not started';
        case ResponseCode.SessionIncorrectDay:
            return 'Session Incorrect day of Week';
        case ResponseCode.ContentTypeNotAllowed:
            return 'Content Type not allowed';
        case ResponseCode.ContentSizeNotAllowed:
            return 'Content size not allowed';
        
        case ResponseCode.TutorNotFound:
            return 'Tutor not found';
        case ResponseCode.ContentLimitExceeded:
            return 'Content limit exceeded';
        case ResponseCode.CourseLimitExceeded:
            return 'Course limit exceeded';
        case ResponseCode.StudentLimitExceeded:
            return 'Student limit exceeded';
        case ResponseCode.TutorStudentAlreadyExist:
            return 'Tutor Student Already Exist';
        case ResponseCode.BatchLimitExceeded:
            return 'Batch limit exceeded';
        case ResponseCode.ScheduleLimitExceeded:
            return 'Schedule limit exceeded'; 
        case ResponseCode.TutorLimitExceeded:
            return 'Tutor limit exceeded';    
        case ResponseCode.DateOfIncorporationError:
            return 'Date of Incorporation cannot be present date';      
         
        case ResponseCode.OrganizationNotFound:
            return 'Organization not found';            
        case ResponseCode.OrganizationTutorAlreadyExist:
            return 'Organization Tutor Already Exist';            
        case ResponseCode.OrganizationStudentAlreadyExist:
            return 'Organization Student Already Exist';
        
        case ResponseCode.AdminNotFound:
            return 'Admin Not Found';
            
        case ResponseCode.MasterPackagesNotFound:
            return 'Master Packages Not Found';
        case ResponseCode.DefaultPackageNotFound:
            return 'Default Package No tFound';
        
                case ResponseCode.SolutionsNotAccesible : 
            return 'Solutions not accessible'
        case ResponseCode.AttemptedAssessmentNotSubmitted : 
            return 'Attempted Assessment not submitted'
        case ResponseCode.MasterCitiesNotFound:
            return 'Master Cities Not Found';

        case ResponseCode.AssessmentAlreadyExists:
                return 'Assessment already exists';
        case ResponseCode.AssessmentNotFound:
            return 'Assessment not found';
        case ResponseCode.QuestionNotFound:
            return 'Question not found';
        case ResponseCode.TopicAlreadyExists:
            return 'Topic already exists'
        case ResponseCode.TopicNotFound :
            return 'Topic(s) not Found'
        case ResponseCode.SectionNotFound:
            return 'Section Not found'
        case ResponseCode.AttemptedAssessmentAlreadyExists:
            return 'Attempted Assessment already started'
        case ResponseCode.AttemptedAssessmentSaveFailed : 
            return 'Attempted Assessment Save failed'
        case ResponseCode.AttemptedAssessmentAlreadySubmitted  :
            return 'Attempted Assessment Already submit'
        case ResponseCode.AttemptedAssessmentNotFound : 
            return 'Attempted Assessment not Found'
            
        case ResponseCode.MaxOtpSentCountExceeded:
            return 'Max Otp sent count exceeded';
        case ResponseCode.OtpNotVerified:
            return 'Otp not verified';
        case ResponseCode.IncorrectOtp:
            return 'Incorrect Otp entered';
        case ResponseCode.MaxOtpAttemptCountExceeded:
            return 'Max Otp attempt count exceeded';
        case ResponseCode.OtpNotGenerated:
            return 'Otp Not generted'; 
        case ResponseCode.SmsSendingFailed:
            return 'Sms sending failed'; 
        case ResponseCode.InvalidCaptcha:
            return 'Invalid Captcha';            
        case ResponseCode.CodeAlreadyExists:
            return 'This Code is Registered with Edumatica';  
        case ResponseCode.VideoKycAttemptsExceeded:
            return 'Max Video KYC attempts exceeded'

        case ResponseCode.MissingHeadersInFile:
            return 'Excel file should contain at least following headers: classname, subjectname, chaptername, option1, option2,option3, option4, questionDescription, answer, answerDescription'

        case ResponseCode.AbilitiesNotFound:
            return 'Searched Abilities not found in DB';
        case ResponseCode.AbilityNotCreated:
            return 'Ability not created' ;
        case ResponseCode.PermissionNotGranted:
            return 'Access Permission not granted for this route'
        case ResponseCode.DuplicateExcelFileName:
            return 'Duplicate Excel File. Please Check for filename'
        case ResponseCode.MeetingDataCorrupted:
            return 'Error in fetching Meeting Data through api'
        case ResponseCode.TutorAsStudentError:
            return 'Error Tutor cannot be a Student'
        case ResponseCode.NotificationTemplateNotAvailable:
            return 'Error Notification Template Not Available'
        case ResponseCode.EmptyCourseBundle:
            return 'Error No Course Bundle Available, please create one'
        case ResponseCode.CourseBundleAlreadyExists:
            return 'Course Bundle already exists with the same batch'
        case ResponseCode.PurchasedCoursePackageAlreadyExists:
            return 'Puchased Course Package already exists for student'
        case ResponseCode.PurchasedCoursePackageAlreadyExists:
            return 'Puchased Course Package does not exists for student'
        case ResponseCode.PaymentAlreadyCompleted:
            return 'Payment already completed'
        case ResponseCode.UnprocessablePayOrder:
            return 'Generated PayOrder can not be processed'
        case ResponseCode.InvalidReferralCode:
            return 'Applied Referral Code is invalid'
        case ResponseCode.ReferralTimeExceeded:
            return 'Referral Code must be used within 5 days of user registration'
        case ResponseCode.ReferralAlreadyUsed:
            return 'Referral system can only be used once per account'
        case ResponseCode.AssessmentAlreadyExists:
            return 'Assignment already exists'
        case ResponseCode.AssignmentAlreadyAssigned:
            return 'Assignment already assigned to students'
        case ResponseCode.AssignmentNotFound:
            return 'Assignment not found'

        case ResponseCode.BatchNotSelected:
            return 'No Batches were Selected'
        case ResponseCode.S3Error:
            return 'Upload Failed. Please Check your Internet'
        case ResponseCode.StudentNotSelected:
            return 'No Students were Selected'
        case ResponseCode.IncorrectPassword:
            return 'Incorrect password'

        case ResponseCode.BackOfficeAdminAlreadyExists:
            return 'Admin already exists'
        case ResponseCode.RoleAlreadyExists:
            return 'Role already exists'
        case ResponseCode.PermissionNotFound:
            return 'Permission not found'
        case ResponseCode.PermissionAlreadyExists:
            return 'Permission already exists'
        case ResponseCode.OtpTimeExpired:
            return 'Time limit for otp verification exceeded'
    }
}

module.exports = { ResponseCode, getResponseMsg }