const Fcm = require('../utils/fcm.utils')
 
class PushNotificationFactory {
    create(type) {
        switch (type) {
            case 'Fcm':
                return new Fcm();
            default:
                return new Fcm();
        }
    }
}

module.exports = new PushNotificationFactory();