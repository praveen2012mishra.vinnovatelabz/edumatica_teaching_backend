const passport    = require('passport');
const passportJWT = require("passport-jwt");
const jwt      = require('jsonwebtoken');
const ExtractJWT = passportJWT.ExtractJwt;
const LocalStrategy = require('passport-local').Strategy;
const JWTStrategy   = passportJWT.Strategy;
const {authenticate} = require('../dbc/adminAuth.dbc');
const logger = require('./logger.utils');
const { ResponseCode } = require('../utils/response.utils');
const { getResponseMsg } = require('./response.utils');

const AUTH_EXPIRES_IN = process.env.JWTKEY_EXPIRY || '24h';
const REFRESH_EXPIRES_IN = process.env.JWT_REFRESH_KEY_EXPIRY || '45d'

passport.use("strategyA1", new LocalStrategy({
        usernameField:'userName',
        passwordField:'password'
    },    
    function (userName, password, callback) {  
        if(userName)
        {   
            return authenticate(userName, password, (err, code, user)=>{
                if(err){
                    logger.error('LocalStrategy - authenticate - Error - ' + err);
                    return callback(err);
                }
                return callback(null, code, user);           
            })
        }
        else{
            return callback(null, ResponseCode.InvalidInput);
        }
    }
));

passport.use("strategyA2", new JWTStrategy({
        jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
        secretOrKey   : process.env.JWTKEY || 'your_jwt_secret'
    },
    function (jwtPayload, cb) {
        return  cb(null, jwtPayload);
    }
));

exports.adminAuthenticate = function (req, res, next) {
    passport.authenticate("strategyA1", {session: false}, (err, code, user) => {
        const userName = req.body.userName;
        
        if (err) {
            logger.error('authenticate - userName:' + userName + ' Error - ' + err);
            return res.status(400).json({code : code, message: getResponseMsg(code)});
        }
        if (!user) {
            logger.warn('authenticate - userName:' + userName+ ', Login failed');
            return res.status(400).json({code : code, message: getResponseMsg(code)});
        }
        req.login(user, {session: false}, (err) => {
            if (err) {
                logger.error('authenticate - req.login - Error - ' + err);
                res.send(err);
            }
            
            const jwtUser = {
                _id: user._id,
                entityId: user.entityId,
                adminRole: user.adminRole.name,
                permissions: user.adminRole.permissions.map(cur => cur.name),
                username: user.username,
                password: user.password,
                status: user.status,
                __v: user.__v,
                failedAttemptCount: user.failedAttemptCount,
                failedOn: user.failedOn
            }
            const token = jwt.sign(jwtUser, process.env.JWTKEY || 'your_jwt_secret', { expiresIn: AUTH_EXPIRES_IN });
            const refresh = jwt.sign(jwtUser, process.env.JWT_REFRESH_KEY || 'your_refresh_secret', { expiresIn: REFRESH_EXPIRES_IN })
            logger.info('authenticate - userName:' + userName + ', Logged In Successfully');
            return res.status(200).json({
                username: user.mobile, tokenType:'Bearer',
                accessToken: token, refreshToken:refresh
            });

        });
    })(req, res);
};

exports.verifyAdmin = function (req, res, next) {
    passport.authenticate("strategyA2", {session: false}, (err, user) => {
        if(user && user.adminRole) {
            req.user = user
            next();
        } else {
            return res.status(401).json({type: 'JwtError', message: 'Unauthorized'});
        }
    })(req, res);
};