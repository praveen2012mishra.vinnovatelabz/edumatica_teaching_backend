const cron = require('node-cron');
const { paymentCron_calculateStudentPayableCron, paymentCron_calculateStudentPaymentAcc, paymentCron_calculateRetainershipFeeCron, paymentCron_batchEndCheckerCron} = require('../dbc/payments.dbc');
const logger = require('./logger.utils');
const { ResponseCode } = require('./response.utils');
// var task = cron.schedule('* * * * * *', () => {
//     console.log('Printing this line every second in the terminal');
// });
// var task = cron.schedule(' * * * * *', () => {
//   console.log('Printing this line every minute in the terminal');
// });
// var task = cron.schedule('1 03 * * *', () => {
//   console.log('Printing this line every day at 03:01 AM in the terminal');
// });
// task.start();

exports.paymentCron_calculateStudentPaymentAcc = () => {
    var task = cron.schedule(process.env.CALCULATE_STUDENTPAYMENT_CRON, () => {
        logger.debug('Enter paymentCron_calculateStudentPaymentAcc - Time:' + new Date());
        paymentCron_calculateStudentPaymentAcc()
      });
      task.start();
}

exports.paymentCron_calculateStudentPayableCron = () => {
    var task = cron.schedule(process.env.CALCULATE_STUDENTPAYABLE_CRON, () => {
        logger.debug('Enter paymentCron_calculateStudentPayableCron - Time:' + new Date());
        paymentCron_calculateStudentPayableCron()
    });
    task.start();
}

exports.paymentCron_calculateRetainershipFeeCron = () => {
    var task = cron.schedule(process.env.CALCULATE_RETAINERSHIPFEE_CRON, () => {
        logger.debug('Enter paymentCron_calculateRetainershipFeeCron - Time:' + new Date());
        paymentCron_calculateRetainershipFeeCron()
    });
    task.start();
}

exports.paymentCron_batchEndCheckerCron = () => {
    var task = cron.schedule(process.env.BATCH_END_DATE_CHECKER_CRON, () => {
        logger.debug('Enter paymentCron_batchEndCheckerCron - Time:' + new Date());
        paymentCron_batchEndCheckerCron()
    });
    task.start();
}
