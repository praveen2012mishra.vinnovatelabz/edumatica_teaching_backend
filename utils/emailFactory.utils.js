const SendInBlue = require('../utils/sendInBlue.utils')
 
class EmailFactory {
    create(type) {
        switch (type) {
            case 'SendInBlue':
                return new SendInBlue();
            default:
                return new SendInBlue();
        }
    }
}

module.exports = new EmailFactory();