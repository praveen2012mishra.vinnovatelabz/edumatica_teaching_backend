const AES = require('crypto-js/aes');
const Utf8 = require ('crypto-js/enc-utf8');

const encryptWithAES = (text) => {
  return AES.encrypt(text, process.env.REFERRAL_PASSPHRASE).toString();
};

const decryptWithAES = (ciphertext) => {
  const bytes = AES.decrypt(ciphertext, process.env.REFERRAL_PASSPHRASE);
  const originalText = bytes.toString(Utf8);
  return originalText;
};

module.exports = {encryptWithAES, decryptWithAES}