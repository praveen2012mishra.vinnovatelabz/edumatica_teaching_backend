const PDFDocument = require('pdfkit');
const pdf = require('pdfjs')
const fs = require('fs');
var path = require('path');

exports.convertImagesToPdf = (imagePaths, callback) => {
    // Create a document
    var pdf = new PDFDocument({
        autoFirstPage: false
    });
    const pdfPath = path.join('uploads', 'output.pdf')
    pdf.pipe(fs.createWriteStream(pdfPath));

    imagePaths.forEach((path) => {
        var img = pdf.openImage(path);
        pdf.addPage({size: [img.width, img.height]});
        pdf.image(img, 0, 0);
    })

    pdf.end();

    pdf.on('end', () => {
        // var file = fs.readFileSync('out.pdf');
        // res.setHeader('content-type', 'application/pdf');
        // const file = `out.pdf`;
        // res.download(file); // Set disposition and send it.
        callback(pdfPath)
    })
}

exports.convertToPdf = async (paths, callback) => {
    const doc = new pdf.Document({})
    const pdfPath = path.join('uploads', 'output.pdf')
    doc.pipe(fs.createWriteStream(pdfPath))
      
    paths.forEach((upPath) => {
        const thistype = upPath.substr(upPath.length - 3);
        if(thistype === 'pdf'){
            const src = fs.readFileSync(upPath)
            const ext = new pdf.ExternalDocument(src)
            doc.addPagesOf(ext)
        } else {
            const src = fs.readFileSync(upPath)
            const img = new pdf.Image(src)
            doc.image(img)
        }
    })
      
    await doc.end().then(
        callback(pdfPath)
    )
}