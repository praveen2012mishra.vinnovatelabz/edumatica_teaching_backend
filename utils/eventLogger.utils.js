const EventEmitter = require("events");

class EventLogger extends EventEmitter {
  log(message) {
    console.log(message);

    // Raising the event
    this.emit("sse_msg", {
      title: "SSE Event",
      meetingID: message,
      timestamp: new Date(),
    });
  }
}

module.exports = EventLogger;
