const Pinpoint = require('./pinpoint.utils');
const TextLocal = require("./textLocal.utils");
 
class SmsFactory {
    create(type) {
        switch (type) {
            case 'TextLocal':
                return new TextLocal();
            case 'Pinpoint':
                return new Pinpoint();
            default:
                return new Pinpoint();
        }
    }
}

module.exports = new SmsFactory();