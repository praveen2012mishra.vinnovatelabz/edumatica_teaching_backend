var AWS = require('aws-sdk');
const logger = require('../utils/logger.utils');
const Sms = require('./sms.utils');
const { ResponseCode } = require('./response.utils');

class Pinpoint extends Sms {    
    sendSms (mobileNo, args, callback) {
        notificationMsgTemplate(args, (msg) => {
            AWS.config.update({accessKeyId: process.env.PINPOINTACCESSID || 'PINPOINTACCESSID',
                     secretAccessKey: process.env.PINPOINTACCESSKEY || 'PINPOINTACCESSKEY',
                     signatureVersion:'v4',
                     region: process.env.PINPOINTREGION || 'PINPOINTREGION'});
        var pinpoint = new AWS.Pinpoint();
        // Specify the parameters to pass to the API.
        mobileNo = '+91'+ mobileNo
        var params = {
            ApplicationId: process.env.PINPOINTAPPID || "PINPOINTAPPID",
            MessageRequest: {
            Addresses: {
                [mobileNo]: {
                ChannelType: 'SMS'
                }
            },
            MessageConfiguration: {
                SMSMessage: {
                Body: msg,
                MessageType: "TRANSACTIONAL",
                SenderId: process.env.SMSSENDERID || "MySenderID",
                }
            }
            }
        };
        
        pinpoint.sendMessages(params, function(err, data) {
            if(err) {
                const error = {status:"Internal Error"}
                notification_message(args.type, mobileNo, msg, error)
                logger.error('sendTransactionSms - pinpoint.sendMessages - Error - ' + err);
            } else {
                notification_message(args.type, mobileNo, msg, data)
                logger.info("Message sent! " 
                + data['MessageResponse']['Result'][mobileNo]['StatusMessage']);
                callback(null, ResponseCode.Success, data);
            }
        });
        })
    }
}

module.exports = Pinpoint;
