const log4js = require("log4js");

log4js.configure({
  appenders: { service: { type: "file", filename: "service.log" } },
  categories: { default: { appenders: ["service"], level: "debug" } }
});
const logger = log4js.getLogger("service");

module.exports = logger;