const { Ability } = require('@casl/ability');

exports.StudentAbility = new Ability(
[
    {
        action:'read',
        subject:'AbilityStore'
    },
    {
        action : 'manage' , 
        subject : 'Admin' ,
        inverted : true,
    },
    {
        action : 'read',
        subject : 'Assessment' ,
        reason : 'Only read access allowed'
    },
    {
        action: 'read' ,
        subject : 'AssessmentQuestion',
        reason: 'Only read access allowed'
    },
    {
        action: ['read','update'],
        subject : 'AttemptedAssessment'
    },
    {
        action: 'read',
        subject: 'Batch'
    },
    {
        action : 'read',
        subject: 'BatchChapter'
    },
    {
        action : 'read',
        subject: 'Chapter'
    },
    {
        action:'read',
        subject : 'Content'
    },
    {
        action:'read',
        subject:'MasterChapter'
    },
    {
        action:'read',
        subject:'MasterCity'
    },
    {
        action:'read',
        subject:'MasterPackage'
    },
    {
        action:'manage',
        subject:'MasterQuestion',
        inverted:true
    },
    {
        action:'read',
        subject:'MasterSchool'
    },
    {
        action: 'manage',
        subject: 'Organization',
        inverted:true
    },
    {
        action:'manage',
        subject:'Parent'
    },
    {
        action:'read',
        subject:'QuizDetail'
    },
    {
        action:'manage',
        subject:'Role',
        inverted:true
    },
    {
        action:'read',
        subject:'Schedule',
    },
    {
        action:'read',
        subject:'Section',
    },
    {
        action:'read',
        subject:'Session',
    },
    {
        action:'manage',
        subject:'Sms',
        inverted:true
    },
    {
        action:'read',
        subject:'Student'
    },
    {
        action:'read',
        subject:'StudentAttendance'
    },
    {
        action:'manage',
        subject:'StudentContent'
    },
    {
        action:'read',
        subject:'Tutor'
    },
    {
        action:'manage',
        subject:'User'
    }

])


exports.TutorAbility =new Ability([
    {
        action:'manage',
        subject:'AbilityStore'
    },
    {
        action : 'manage' , 
        subject : 'Admin' ,
        inverted : true
    },
    {
        action : 'manage',
        subject : 'Assessment' 
    },
    {
        action : 'manage' ,
        subject : 'AssessmentQuestion'
    },
    {
        action: ['create' , 'read'],
        subject : 'AttemptedAssessment'
    },
    {
        action: 'manage',
        subject: 'Batch'
    },
    {
        action : 'manage',
        subject: 'BatchChapter'
    },
    {
        action : 'manage',
        subject: 'Chapter'
    },
    {
        action:'manage',
        subject : 'Content'
    },
    {
        action:'manage',
        subject:'MasterChapter'
    },
    {
        action:'manage',
        subject:'MasterCity'
    },
    {
        action:'manage',
        subject:'MasterPackage'
    },
    {
        action:'manage',
        subject:'MasterQuestion'
    },
    {
        action:['create','read','update'],
        subject : 'MasterSchool'
    },
    {
        action: 'manage',
        subject: 'Organization',
        inverted:true
    },
    {
        action:['create','read','update'],
        subject:'Parent'
    },
    {
        action:'manage',
        subject:'QuizDetail'
    },
    {
        action:'manage',
        subject:'Role',
        inverted:true
    },
    {
        action:'manage',
        subject:'Schedule',
    },
    {
        action:'manage',
        subject:'Section',
    },
    {
        action:'manage',
        subject:'Session',
    },
    {
        action:'manage',
        subject:'Sms',
    },
    {
        action:'manage',
        subject:'Student'
    },
    {
        action:'read',
        subject:'StudentAttendance'
    },
    {
        action:['create','read'],
        subject:'StudentContent'
    },
    {
        action:'manage',
        subject:'Tutor'
    },
    {
        action:'manage',
        subject:'User'
    }
]) 

exports.OrgAbility = new Ability([
    {
        action:'manage',
        subject:'AbilityStore'
    },
    {
        action : 'manage' , 
        subject : 'Admin' ,
        inverted : true
    },
    {
        action : 'manage',
        subject : 'Assessment' 
    },
    {
        action : 'manage',
        subject : 'AssessmentQuestion'
    },
    {
        action : ['create','read','update'],
        subject : 'AttemptedAssessment',
        reason : 'Delete operation not allowed'
    },
    {
        action: ['create' , 'read'],
        subject : 'AttemptedAssessment'
    },
    {
        action: 'manage',
        subject: 'Batch'
    },
    {
        action : 'manage',
        subject: 'BatchChapter'
    },
    {
        action : 'manage',
        subject: 'Chapter'
    },
    {
        action:'manage',
        subject : 'Content'
    },
    {
        action:'manage',
        subject:'MasterChapter'
    },
    {
        action:'manage',
        subject:'MasterCity'
    },
    {
        action:'manage',
        subject:'MasterPackage'
    },
    {
        action:'read',
        subject:'MasterQuestion'
    },
    {
        action:'manage',
        subject:'MasterSchool'
    },
    {
        action:'manage',
        subject:'Organization'
    },
    {
        action:'manage',
        subject:'Parent'
    },
    {
        action:'manage',
        subject:'QuizDetail'
    },
    {
        action:'manage',
        subject:'Role',
        inverted:true
    },
    {
        action:'manage',
        subject:'Schedule',
    },
    {
        action:'manage',
        subject:'Section',
    },
    {
        action:'manage',
        subject:'Session',
    },
    {
        action:'manage',
        subject:'Sms',
    },
    {
        action:'manage',
        subject:'Student'
    },
    {
        action:'read',
        subject:'StudentAttendance'
    },
    {
        action:['create','read'],
        subject:'StudentContent'
    },
    {
        action:'manage',
        subject:'Tutor'
    },
    {
        action:'manage',
        subject:'User'
    }
])

exports.AdminAbility = new Ability([
    {
        action : 'manage',
        subject : 'all'
    },
])

