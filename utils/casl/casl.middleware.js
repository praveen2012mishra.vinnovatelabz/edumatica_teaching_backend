const {routesDesc} =require("./casl.routesDesc") 
const {TutorAbility , StudentAbility ,OrgAbility ,AdminAbility} = require("./casl.abilities")
const {getResponseMsg,ResponseCode} = require('../response.utils')
const logger = require("../logger.utils")
const casl =  (req,res,next) =>{
    const roles = req.user.roles
    const reqMethod = req.method.toLowerCase()
    
    const reqUrl = (req.baseUrl+req.path).split('/').filter(el=>el.length!==0).map(el=>el.toLowerCase())
    const reqString = [reqMethod,...reqUrl].join("_")
    logger.debug(reqString)
    const reqPermissions = routesDesc[reqString]
    logger.debug(reqPermissions)
    logger.debug(roles)
    const abilities = roles.map((el)=>{
        switch(el.name) {
            case 'ROLE_TUTOR' :
                return TutorAbility ;
            case 'ROLE_ORGANIZATION_TUTOR':
                return TutorAbility;
            case 'ROLE_STUDENT' : 
                return StudentAbility ; 
            case 'ROLE_ORGANIZATION':
                return OrgAbility;
            case 'ROLE_ADMIN':
                return AdminAbility
            case 'ROLE_PARENT' : 
                return StudentAbility ; 
        }
    })

    
    const permissionCheck = abilities.map((ability)=>{
        return reqPermissions.map((permission)=>{
            return ability.can(permission[0],permission[1])
        })
    }).map((permissionDataForRole)=>{
        logger.debug(permissionDataForRole)
        return permissionDataForRole.every(el=>el)
    }).some(el=>el)

    if(permissionCheck){
        logger.debug("nect")
        next()
    }
    else{
        res.status(401).json({code:ResponseCode.PermissionNotGranted,message:getResponseMsg(ResponseCode.PermissionNotGranted)})
    }
}

module.exports ={casl}