const { sendSms, getBalance, getTemplates } = require("textlocal-complete");
const { notification_message } = require("../dbc/notificationMessage.dbc");
const { notificationMsgTemplate } = require("../dbc/notificationTemplate.dbc");
const logger = require("../utils/logger.utils");
const { ResponseCode } = require("./response.utils");
const Sms = require("./sms.utils");

class TextLocal extends Sms {
  sendSms(mobileNo, args, callback) {
    if (
      process.env.GLOBALNOTIFICATIONSWITCH === "true" &&
      process.env.SENDSMS === "true"
    ) {
      console.log(process.env.GLOBALNOTIFICATIONSWITCH,process.env.SENDSMS)
      notificationMsgTemplate(args, (msg) => {
        sendSms(
          process.env.TEXT_LOCAL_API,
          mobileNo,
          process.env.SMSSENDERID,
          msg
        )
          .then((res_data) => {
            //console.log(res_data.data)
            notification_message(args.type, mobileNo, msg, res_data.data);
            callback(null, ResponseCode.Success, res_data.data);
            logger.debug(
              "Notification: " +
                msg +
                " request to mobile number " +
                mobileNo +
                ". Response: " +
                JSON.stringify(res_data.data)
            );
          })
          .catch((err) => {
            callback(err);
            const error = { status: "Internal Error" };
            notification_message(args.type, mobileNo, msg, error);
            logger.error(
              "Internal Error in sending Notification to user mobile number. Error: " +
                err
            );
          });
      });
    }
  }
}

module.exports = TextLocal;
