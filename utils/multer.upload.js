const multer = require("multer");

const path = require("path");
var storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, path.normalize(__dirname+'/../uploadData'));
     },
    filename: function (req, file, cb) {
        cb(null , file.originalname);
    }
});

var upload = multer({ storage: storage ,onFileUploadStart: function (file) {
    console.log(file.originalname + ' is starting ...')
  }});

module.exports = {upload};