
const Eureka = require('eureka-js-client').Eureka;
var eurekaClient

exports.startEurekaClient = (HOST, PORT, appName, eurekaHost, eurekaPort,callback)=> {
    eurekaClient = new Eureka({
        // application instance information
        instance: {
        app: appName,
        hostName: HOST,
        ipAddr: HOST,
		statusPageUrl:'http://__HOST__:8082/info',
        vipAddress: appName,
        port: {
            $: PORT,
            '@enabled':'true',
        },
		// For Windows
        dataCenterInfo: {
            '@class':'com.netflix.appinfo.InstanceInfo$DefaultDataCenterInfo',
            name:'MyOwn',
        },
		// For Amazon EC2
		//dataCenterInfo: {
        //    '@class':'com.netflix.appinfo.AmazonInfo',
        //    name:'Amazon',
        //},
        registerWithEureka: true,
        fetchRegistry: true,
        },
        eureka: {
        // eureka server host / port
        useLocalMetadata: true,
        host: eurekaHost,
        port: eurekaPort,
        servicePath:'/eureka/apps/',
        },
    });
        
    eurekaClient.logger.level('debug');
    eurekaClient.start(error => {
        console.log(error || 'NodeJS Eureka Started!');    
        const instanceInfo = eurekaClient.getInstancesByAppId(appName);
        console.log(instanceInfo);   
    });
}

exports.getInstancesByAppId = (appName, callback)=> {
    const instanceInfo = eurekaClient.getInstancesByAppId(appName);
    return callback(instanceInfo[0].ipAddr + ':'+ instanceInfo[0].port.$);
  }