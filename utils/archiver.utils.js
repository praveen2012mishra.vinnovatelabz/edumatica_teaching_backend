// require modules
const fs = require('fs');
const archiver = require('archiver');

exports.zipFiles = (files, fileNames, callback) => {

    const zipPath = 'uploads/zippedDocuments.zip'
    var output = fs.createWriteStream(zipPath);
    var archive = archiver('zip');

    output.on('close', function() {
    console.log(archive.pointer() + ' total bytes');
    console.log('archiver has been finalized and the output file descriptor has closed.');
    callback(zipPath)
    });

    archive.on('error', function(err) {
    throw err;
    });

    archive.pipe(output);

    files.forEach((file, i) => {
        archive.append(fs.createReadStream(file), { name: fileNames[i] });
    })

    archive.finalize();
}