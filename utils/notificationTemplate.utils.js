const Template = {
    otpRegisterUser: 'otp_registerUser',
    otpResetPass: 'otp_resetPass',
    assessmentAssigned: 'assessment_assigned',
    assignmentAssigned: 'assignment_assigned',
    paymentSuccessful: 'payment_successful',
    adminResetPassword: 'admin_resetPassword'
}

module.exports = { Template }