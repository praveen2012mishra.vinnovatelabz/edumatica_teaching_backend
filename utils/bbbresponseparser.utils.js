const { default: axios } = require("axios");
const { xml2js } = require("xml-js");
const logger = require('./logger.utils')

exports.getBBBResponse = async (URI) => {
  const response = await axios.get(URI);
  console.log(URI, response.data)
  logger.debug("ENTER BBBResponseparser Response:" + JSON.stringify(response.data))
  return xml2js(response.data, { compact: true });
};
