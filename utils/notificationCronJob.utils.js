const cron = require('node-cron');
// var task = cron.schedule('* * * * * *', () => {
//     console.log('Printing this line every second in the terminal');
// });
// var task = cron.schedule(' * * * * *', () => {
//   console.log('Printing this line every minute in the terminal');
// });
// var task = cron.schedule('1 03 * * *', () => {
//   console.log('Printing this line every day at 03:01 AM in the terminal');
// });
// task.start();

exports.notificationCron = () => {
    var task = cron.schedule(process.env.NOTIFICATION_CRONJOB_RUNTIME, () => {
        logger.debug('Enter notificationCron - Time:' + new Date());
        notificationCron_deleteOldPullNotifications()
      });
      task.start();
}