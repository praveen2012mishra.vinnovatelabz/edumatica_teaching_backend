const passport    = require('passport');
const passportJWT = require("passport-jwt");
const jwt      = require('jsonwebtoken');
const ExtractJWT = passportJWT.ExtractJwt;
const LocalStrategy = require('passport-local').Strategy;
const JWTStrategy   = passportJWT.Strategy;
const {authenticate} = require('../dbc/auth.dbc');
const logger = require('./logger.utils');
const { ResponseCode } = require('../utils/response.utils');
const { getResponseMsg } = require('./response.utils');

const AUTH_EXPIRES_IN = process.env.JWTKEY_EXPIRY || '24h';
const REFRESH_EXPIRES_IN = process.env.JWT_REFRESH_KEY_EXPIRY || '45d'

exports.authenticate = function (req, res, next) {
    passport.authenticate("strategyU1", {session: false}, (err, code, user) => {
        const userName = req.body.userName;
        
        if (err) {
            logger.error('authenticate - userName:' + userName + ' Error - ' + err);
            return res.status(400).json({code : code, message: getResponseMsg(code)});
        }
        if (!user) {
            logger.warn('authenticate - userName:' + userName+ ', Login failed');
            return res.status(400).json({code : code, message: getResponseMsg(code)});
        }
        req.login(user, {session: false}, (err) => {
            if (err) {
                logger.error('authenticate - req.login - Error - ' + err);
                res.send(err);
            }
            
            const jwtUser = {
                _id: user._id,
                password: user.password,
                status: user.status,
                owner: user.owner,
                __v: user.__v,
                failedAttemptCount: user.failedAttemptCount,
                failedOn: user.failedOn
            }
            const token = jwt.sign(jwtUser, process.env.JWTKEY || 'your_jwt_secret', { expiresIn: AUTH_EXPIRES_IN });
            const refresh = jwt.sign(jwtUser, process.env.JWT_REFRESH_KEY || 'your_refresh_secret', { expiresIn: REFRESH_EXPIRES_IN })
            logger.info('authenticate - userName:' + userName + ', Logged In Successfully');
            return res.status(200).json({
                username: user.mobile, owner: user.owner, tokenType:'Bearer',
                accessToken: token, refreshToken:refresh
            });

        });
    })(req, res);
};

exports.updateJWTOnboarding = (user) => {
    const jwtUser = {
        _id: user._id,
        password: user.password,
        status: user.status,
        owner: user.owner,
        __v: user.__v,
        failedAttemptCount: user.failedAttemptCount,
        failedOn: user.failedOn
    }
    const token = jwt.sign(jwtUser, process.env.JWTKEY || 'your_jwt_secret', { expiresIn: AUTH_EXPIRES_IN });
    const refresh = jwt.sign(jwtUser, process.env.JWT_REFRESH_KEY || 'your_refresh_secret', { expiresIn: REFRESH_EXPIRES_IN })
    
    return {
        accessToken: token,
        refreshToken: refresh
    }
}

exports.verifyUser = function (req, res, next) {
    passport.authenticate("strategyU2", {session: false}, (err, user, info) => {
        if(user) {
            const currentUser = String(req.headers['currentuser'])
            const [currentOwner, currentRole] = currentUser.split('|||');
            const currentOwnerObj = user.owner.find(elem => elem._id === currentOwner)
            const currentRoleObj = currentOwnerObj.roles.find(elem => elem._id === currentRole)
            const roles = currentOwnerObj.roles.map(elem => elem.role)
            const tokenUser = {
                _id: user._id,
                password: user.password,
                status: user.status,
                __v: user.__v,
                iat: user.iat,
                exp: user.exp,
                ownerId: currentOwnerObj.ownerId,
                entityId: currentRoleObj.entityId,
                roles: roles,
                role: currentRoleObj.role
            }
            req.user = tokenUser;
            next();
        } else if(!user && info.name =='JsonWebTokenError') {
            return res.status(401).json({type: 'JwtError', message: 'Unauthorized'});
        } else {
            return res.status(401).json({type: 'JwtError', message: info.name});
        }
    })(req, res);
};

exports.refreshToken  = async (token, callback)=> {    
    const tokenResp = await new Promise(resolve => {
        jwt.verify(token, process.env.JWT_REFRESH_KEY || 'your_refresh_secret', (err, data) => {
            if(err) {
                resolve(false);
            } else {
                resolve(data);
            }
        })
    });
    if(!tokenResp) {
        callback(false)
    }

    const jwtUser = {
        _id: tokenResp._id,
        password: tokenResp.password,
        status: tokenResp.status,
        owner: tokenResp.owner,
        __v: tokenResp.__v,
        failedAttemptCount: tokenResp.failedAttemptCount,
        failedOn: tokenResp.failedOn
    }
    const freshToken = jwt.sign(jwtUser, process.env.JWTKEY || 'your_jwt_secret', { expiresIn: AUTH_EXPIRES_IN });

    callback(freshToken)
}

passport.use("strategyU1", new LocalStrategy({
        usernameField:'userName',
        passwordField:'password'
    },    
    function (userName, password, callback) {          
        if(userName)
        {   
            return authenticate(userName, password, (err, code, user)=>{
                if(err){
                    logger.error('LocalStrategy - authenticate - Error - ' + err);
                    return callback(err);
                }
                return callback(null, code, user);           
            })
        }
        else{
            return callback(null, ResponseCode.InvalidInput);
        }
    }
));

passport.use("strategyU2", new JWTStrategy({
        jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
        secretOrKey   : process.env.JWTKEY || 'your_jwt_secret'
    },
    function (jwtPayload, cb) {
        return  cb(null, jwtPayload);
    }
));