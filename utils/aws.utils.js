const logger = require('../utils/logger.utils');
const uuid = require('uuid');
const axios = require('axios')
const AWS = require('aws-sdk');
AWS.config.update({accessKeyId: process.env.S3ACCESSID || 'S3ACCESSID',
                     secretAccessKey: process.env.S3ACCESSKEY || 'S3ACCESSKEY',
                     signatureVersion:'v4',
                     region: process.env.S3REGION || 'ap-south-1'});
const s3 = new AWS.S3();

const myBucket = process.env.S3BUCKETNAME;
const signedUrlExpireSeconds = 60 * 5; // 5 mins
const appName = 'S3ADAPTER-SERVICE';
const { ResponseCode } = require('./response.utils');

const path = require('path')
const fs= require('fs')


exports.uploadFile = async function uploadFile(file) {
    return new Promise(async function(resolve, reject) {
        logger.debug(file)
    const params = {
    Bucket: myBucket, // pass your bucket name
    Key: 'masterQuestionImages' + '/' + file,
    Body: fileSystem.createReadStream(file)
    };
    s3.upload(params, function(s3Err, data) {
    if (s3Err){
    reject(s3Err);
    }
    console.log(`File uploaded successfully at ${data.Location}`);
    resolve({fileName:path.basename(file).toLowerCase(),location:data.Location});
    });
    });
}

exports.getSignedURLForUpload  = (ownerId, content,contentType, callback)=> {
    let folder = content.boardname + '-' 
                    + content.classname + '-' 
                    + content.subjectname + '-'
                    + content.chaptername;
    let newuuid =  uuid.v4();
    let fileName = ownerId + '/' + folder + '/' + newuuid;
    
    // const signedUrl = s3.createPresignedPost({
    //     Bucket: myBucket,
    //     Fields: {
	// 		key: fileName,
	// 	},
    //     Expires: signedUrlExpireSeconds,
    //     Conditions:[['content-length-range', 1, 1000000]]
    // });
    logger.debug(contentType)
    const signedUrl = s3.getSignedUrl('putObject',{
        Bucket: myBucket,
        Key: fileName,
        ContentType:contentType,
        Expires: signedUrlExpireSeconds
    });
    logger.info('getSignedURLForUpload - ownerId:' + ownerId
                + ', boardname:' + content.boardname + ', classname:' + content.classname
                + ', subjectname:' + content.subjectname + ', chaptername:' + content.chaptername
                + ', uuid:' + newuuid + ', url:' + signedUrl
                + ', Signed URL retrieved successfully');   
    callback(null, ResponseCode.Success, signedUrl, newuuid);
}


exports.getSignedURLForDownload  = (ownerId, content, callback)=> {
    var folder = content.boardname + '-' 
                    + content.classname + '-' 
                    + content.subjectname + '-'
                    + content.chaptername;
    var fileName = ownerId + '/' + folder + '/' + content.uuid;
    const signedUrl = s3.getSignedUrl('getObject', {
        Bucket: myBucket,
        Key: fileName,
        Expires: signedUrlExpireSeconds
    });
    logger.info('getSignedURLForDownload - ownerId:' + ownerId
                + ', boardname:' + content.boardname + ', classname:' + content.classname
                + ', subjectname:' + content.subjectname + ', chaptername:' + content.chaptername
                + ', uuid:' + content.uuid + ', url:' + signedUrl
                + ', Signed URL retrieved successfully');
    callback(null, ResponseCode.Success, signedUrl);
}

exports.getSignedURLsForDownload  = (ownerId,chapter, contentArr)=> {
    
        const toReturn =  contentArr.map((content)=>{
        var folder = chapter.boardname + '-' 
                    + chapter.classname + '-' 
                    + chapter.subjectname + '-'
                    + chapter.chaptername;
        var fileName = ownerId + '/' + folder + '/' + content.uuid;
        logger.debug(s3.getSignedUrl('getObject', {
            Bucket: myBucket,
            Key: fileName,
            Expires: signedUrlExpireSeconds
        }) )
        return {
        ...content,
        signedUrl: s3.getSignedUrl('getObject', {
        Bucket: myBucket,
        Key: fileName,
        Expires: signedUrlExpireSeconds
    }) }

    
    })

    logger.info('getSignedURLsForDownload - ownerId:' + ownerId
                + ', boardname:' + chapter.boardname + ', classname:' + chapter.classname
                + ', subjectname:' + chapter.subjectname + ', chaptername:' + chapter.chaptername
                + ', contentUUIDs:' + contentArr.map(el=>el.uuid).join(',') + ', urls:' + contentArr.map(el=>el.signedUrl).join(',')
                + ', Signed URLs retrieved successfully');
    return toReturn;

    
}

exports.sizeOf  = (key, callback)=> {
    s3.headObject({ Key: key, Bucket: myBucket }, function (err, data) {
        if (err) {
            return next(err);
        }
        logger.info('sizeOf - key:' + key
                    + ', Object retrieved successfully');
        callback(null, ResponseCode.Success, data.ContentLength);
    })
}

exports.deleteObject  = (key, callback)=> {
    var params = {
        Bucket: myBucket,
        Key: key
    };
    s3.deleteObject(params, function (err, data) {
        if (err) {
            return next(err);
        }
        logger.info('deleteObject - key:' + key
                    + ', Object deleted successfully');
        callback(null, ResponseCode.Success);
    })
}

exports.getUploadUrlForKYCDoc  = (ownerId, contentType, callback)=> {
    let newuuid =  uuid.v4();
    let folderName = ownerId + "/KYCdocs/" + newuuid;
    const signedUrl = s3.getSignedUrl('putObject', {
        Bucket: myBucket,
        ContentType: contentType,
        Key: folderName,
        Expires: signedUrlExpireSeconds
    });
    logger.info('getUploadUrlForKYCDoc - ownerId:' + ownerId
                + ', uuid:' + newuuid + ', url:' + signedUrl
                + ', Signed URL retrieved successfully');
    callback(null, ResponseCode.Success, signedUrl, newuuid);
}

exports.getDownloadUrlForKYCDoc  = (ownerId, uuid, callback)=> {
    var fileName = ownerId + "/KYCdocs/" + uuid;
    const signedUrl = s3.getSignedUrl('getObject', {
        Bucket: myBucket,
        Key: fileName,
        Expires: signedUrlExpireSeconds
    });
    logger.info('getDownloadUrlForKYCDoc - ownerId:' + ownerId
                + ', uuid:' + uuid + ', url:' + signedUrl
                + ', Signed URL retrieved successfully');
    callback(null, ResponseCode.Success, signedUrl);
}

exports.getSignedURLForVideoKycUpload  = (ownerId, contentType, callback)=> {
    let newuuid =  uuid.v4();
    let folderName = ownerId + "/VideoKYCdocs/" + newuuid;
    const signedUrl = s3.getSignedUrl('putObject', {
        Bucket: myBucket,
        ContentType: contentType,
        Key: folderName,
        Expires: signedUrlExpireSeconds
    });
    logger.info('getSignedURLForVideoKycUpload - ownerId:' + ownerId
                + ', uuid:' + newuuid + ', url:' + signedUrl
                + ', Signed URL retrieved successfully');
    callback(null, ResponseCode.Success, signedUrl, newuuid);
}

exports.getDownloadUrlForVideoKyc  = (ownerId, uuid, callback)=> {
    let folderName = ownerId + "/VideoKYCdocs/" + uuid;
    const signedUrl = s3.getSignedUrl('getObject', {
        Bucket: myBucket,
        Key: folderName,
        Expires: signedUrlExpireSeconds
    });
    logger.info('getDownloadUrlForVideoKyc - ownerId:' + ownerId
                + ', uuid:' + uuid + ', url:' + signedUrl
                + ', Signed URL retrieved successfully');
    callback(null, ResponseCode.Success, signedUrl);
}

exports.getSignedURLForAnnouncementFileUpload  = (ownerId, contentType, callback)=> {
    let newuuid =  uuid.v4();
    let folderName = ownerId + "/AnnouncementFile/" + newuuid;
    const signedUrl = s3.getSignedUrl('putObject', {
        Bucket: myBucket,
        ContentType: contentType,
        Key: folderName,
        Expires: signedUrlExpireSeconds
    });
    logger.info('getSignedURLForVideoKycUpload - ownerId:' + ownerId
                + ', uuid:' + newuuid + ', url:' + signedUrl
                + ', Signed URL retrieved successfully');
    callback(null, ResponseCode.Success, signedUrl, newuuid);
}

exports.getDownloadUrlForAnnouncementFileDownload  = (ownerId, uuid, callback)=> {
    let folderName = ownerId + "/AnnouncementFile/" + uuid;
    const signedUrl = s3.getSignedUrl('getObject', {
        Bucket: myBucket,
        Key: folderName,
        Expires: signedUrlExpireSeconds
    });
    logger.info('getDownloadUrlForVideoKyc - ownerId:' + ownerId
                + ', uuid:' + uuid + ', url:' + signedUrl
                + ', Signed URL retrieved successfully');
    callback(null, ResponseCode.Success, signedUrl);
}

exports.getUploadUrlForAssignmentDoc  = (ownerId, userFileName, docType, fileType, callback)=> {

    let newuuid =  uuid.v4();
    let folder = `${docType}/${newuuid}`;
    let fileName = `${ownerId}/${folder}/${userFileName}`
    const signedUrl = s3.getSignedUrl('putObject', {
        Bucket: myBucket,
        Key: fileName,
      Expires: signedUrlExpireSeconds,
        ContentType: fileType,

    });
    logger.info('getUploadUrlForAssignmentDoc - ownerId:' + ownerId
                + ', uuid:' + newuuid + ', url:' + signedUrl
                + ', Signed URL retrieved successfully');
    callback(null, ResponseCode.Success, signedUrl, newuuid);
}

exports.getDownloadUrlForAssignmentDoc  = (ownerId, docType, uuid, userFileName, callback)=> {
    var fileName = `${ownerId}/${docType}/${uuid}/${userFileName}`;
    const signedUrl = s3.getSignedUrl('getObject', {
        Bucket: myBucket,
        Key: fileName,
        Expires: signedUrlExpireSeconds
    });
    logger.info('getDownloadUrlForAssignmentDoc - ownerId:' + ownerId
                + ', uuid:' + uuid + ', url:' + signedUrl
                + ', Signed URL retrieved successfully');
    callback(null, ResponseCode.Success, signedUrl);
}

exports.uploadAssignmentFile = (ownerId, userFileName, docType, contentType, filePath, callback) => {
    // Read content from the file
    //const fileContent = fs.readFileSync(filePath);
    const fileContent = fs.createReadStream(filePath)
    // let encodedFileContent = Buffer.from(fileContent).toString('base64')

//   let decodedFileContent = Buffer.from(fileContent, 'base64')
    let newuuid =  uuid.v4();
    let folder = `${docType}/${newuuid}`;
    let fileName = `${ownerId}/${folder}/${userFileName}`

    // Setting up S3 upload parameters
    const params = {
        Bucket: myBucket,
        Key: fileName, // File name you want to save as in S3
        Body: fileContent,
        ContentType: contentType,
        // ContentEncoding: "base64"
    };

    // Uploading files to the bucket
    s3.upload(params, function(err) {
        if (err) {
            callback(null, ResponseCode.S3Error)
        } else {
            callback(null, ResponseCode.Success, newuuid, contentType, userFileName)
        }
    });
  
};

exports.getUploadUrlForSyllabus  = (ownerId, batch, contentType, callback)=> {
    let newuuid =  uuid.v4();
    let folder = `syllabus/${newuuid}`;
    let fileName = `${ownerId}/${batch}/${folder}`
    const signedUrl = s3.getSignedUrl('putObject', {
        Bucket: myBucket,
        ContentType: contentType,
        Key: fileName,
        Expires: signedUrlExpireSeconds
    });
    logger.info('getUploadUrlForSyllabus - ownerId:' + ownerId
                + ', uuid:' + newuuid + ', url:' + signedUrl
                + ', Signed URL retrieved successfully');
    callback(null, ResponseCode.Success, signedUrl, newuuid);
}

exports.getDownloadUrlForSyllabus  = (ownerId, batch, uuid, callback)=> {
    var fileName = `${ownerId}/${batch}/syllabus/${uuid}`;
    const signedUrl = s3.getSignedUrl('getObject', {
        Bucket: myBucket,
        Key: fileName,
        Expires: signedUrlExpireSeconds
    });
    logger.info('getDownloadUrlForSyllabus - ownerId:' + ownerId
                + ', uuid:' + uuid + ', url:' + signedUrl
                + ', Signed URL retrieved successfully');
    callback(null, ResponseCode.Success, signedUrl);
}


