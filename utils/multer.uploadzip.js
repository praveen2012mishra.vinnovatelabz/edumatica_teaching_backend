const multer = require("multer");
const path = require('path');

const storage = multer.diskStorage({
  destination: './uploads',
  filename: function(req, file, cb){
    cb(null,file.fieldname + '-' + Date.now() + path.extname(file.originalname));
  }
});

const upload = multer({ storage: storage });

const deleteFolderRecursive = (path) => {
  const fs = require('fs');
  if( fs.existsSync(path) ) {
      fs.readdirSync(path).forEach(function(file) {
        var curPath = path + "/" + file;
          if(fs.lstatSync(curPath).isDirectory()) { // recurse
              deleteFolderRecursive(curPath);
          } else { // delete file
              fs.unlinkSync(curPath);
          }
      });
    }
};

module.exports = {upload, deleteFolderRecursive};