const datefns = require("date-fns")

const checkAnswer = (correctAnswer,yourAnswer,type ,percentageError, negativeMarking,marks) =>{
    if(type==="numeric"){
        if(yourAnswer[0]===undefined){
            return {
                result:"NA",
                marks:0
            }
        }
        else if(Number(yourAnswer[0])>Number(correctAnswer[0])*(1-(percentageError/100)) && Number(yourAnswer[0])<Number(correctAnswer[0])*(1+(percentageError/100))){
            return {
                result:"Correct",
                marks:marks
            }
        }
        else{
            return {
                result:"Incorrect",
                marks:0-negativeMarking
            }
        }
    }
    else if(type==="single"){
        if(yourAnswer[0]===undefined){
            return {
                result:"NA",
                marks:0
            }
        }
        else if(yourAnswer[0]===correctAnswer[0]){
            return {
                result:"Correct",
                marks:marks
            }
        }
        else{
            return {
                result:"Incorrect",
                marks:0-negativeMarking
            }
        }
    }
    else{
        if(yourAnswer[0]===undefined){
            return {
                result:"NA",
                marks:0
            }
        }
        else{
            const yourNumberArr = yourAnswer.map((el)=>{
                switch(el) {
                    case "A" : 
                        return 1
                    case "B" : 
                        return 4
                    case "C" : 
                        return 9
                    case "D" : 
                        return 16
                    default :
                        return 0
                }
            })
            var yourSum = 0
            yourNumberArr.forEach((num)=>{
                yourSum = yourSum+num
            })
            const correctNumberArr = correctAnswer.map((el)=>{
                switch(el) {
                    case "A" : 
                        return 1
                    case "B" : 
                        return 4
                    case "C" : 
                        return 9
                    case "D" : 
                        return 16
                    default :
                        return 0
                }
            })
            var correctSum = 0
            correctNumberArr.forEach((num)=>{
                correctSum = correctSum+num
            })

            if(correctSum===yourSum){
                return {
                    result:"Correct",
                    marks:marks
                }
            }
            else{
                return {
                    result:"Incorrect",
                    marks:0-negativeMarking
                }
            }
        }
        
    }
    
} 

const renewalCalculator = (previousRenewalDate, plan, endDate) => {
    if (plan.paymentcycle === "LUMPSUM") {
        var isFullfilled = true;
        var monthsLeft = 0;
        var renewalDate = new Date()
        var newgrace = null
        return {
            isFullfilled:isFullfilled,
            renewalDate:renewalDate,
            newgrace:newgrace
        }
    }
    if (plan.paymentcycle === "HALFYEARLY") {
        var monthsLeft = datefns.differenceInCalendarMonths(endDate,new Date()) - 6;
        if(monthsLeft<=0) {
            var isFullfilled = true;
            var renewalDate = new Date()
            var newgrace = null
            return {
                isFullfilled:isFullfilled,
                renewalDate:renewalDate,
                newgrace:newgrace
            }
        } else{
            var isFullfilled = false;
            var newgrace = plan.graceperiod;
            var renewalDate = datefns.addMonths(previousRenewalDate, 6)
            return {
                isFullfilled:isFullfilled,
                renewalDate:renewalDate,
                newgrace:newgrace
            }
        }
    }
    if (plan.paymentcycle === "QUARTERLY") {
        var monthsLeft = datefns.differenceInCalendarMonths(endDate,new Date()) - 3;
        if(monthsLeft<=0) {
            var isFullfilled = true;
            var renewalDate = new Date()
            var newgrace = null
            return {
                isFullfilled:isFullfilled,
                renewalDate:renewalDate,
                newgrace:newgrace
            }
        } else{
            var isFullfilled = false;
            var newgrace = plan.graceperiod;
            var renewalDate = datefns.addMonths(previousRenewalDate, 3)
            return {
                isFullfilled:isFullfilled,
                renewalDate:renewalDate,
                newgrace:newgrace
            }
        }
    }
    if (plan.paymentcycle === "MONTHLY") {
        var monthsLeft = datefns.differenceInCalendarMonths(endDate,new Date()) - 1;
        if(monthsLeft<=0) {
            var isFullfilled = true;
            var renewalDate = new Date()
            var newgrace = null
            return {
                isFullfilled:isFullfilled,
                renewalDate:renewalDate,
                newgrace:newgrace
            }
        } else{
            var isFullfilled = false;
            var newgrace = plan.graceperiod;
            var renewalDate = datefns.addDays(previousRenewalDate, 30)
            console.log(renewalDate)
            return {
                isFullfilled:isFullfilled,
                renewalDate:renewalDate,
                newgrace:newgrace
            }
        }
    }
}

module.exports = {checkAnswer, renewalCalculator}